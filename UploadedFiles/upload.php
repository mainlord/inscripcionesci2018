<?php
    /* error_reporting(E_ALL);
    ini_set('display_errors','On'); */

    global $RutaUpload, $maxFileSize, $tamIMG, $tamTHU, $idpeople, $clavecontrol;

    /* en caso de disponer de una imagen de carnet.*/
    if (isset($_FILES["imagen"])) {
        if ((($_FILES["imagen"]["type"] == "image/gif") 
            || ($_FILES["imagen"]["type"] == "image/jpeg") 
            || ($_FILES["imagen"]["type"] == "image/png") 
            || ($_FILES["imagen"]["type"] == "image/pjpeg")) 
            && ($_FILES["imagen"]["size"] < ($maxFileSize * 1024))) {

            if ($_FILES["imagen"]["error"] > 0) {

                // echo "Return Code: " . $_FILES["imagen"]["error"] . "<br />";

            } else {
                /*
                echo "Upload: " . $_FILES["imagen"]["name"] . "<br />";
                echo "Type: " . $_FILES["imagen"]["type"] . "<br />";
                echo "Size: " . ($_FILES["imagen"]["size"] / 1024) . " Kb<br />";
                echo "Temp file: " . $_FILES["imagen"]["tmp_name"] . "<br />";
                */  
                $extension = explode(".", $_FILES["imagen"]["name"]);
                $extension=$extension[(count($extension)-1)];                

                // le ponemos un hash sencillico
                $hash = date("YmdHis");

                $nombre = $hash."_".$file_idins."_".$file_numImg.".".$extension;
                $nombreORI = $RutaUpload."ORI/".$nombre;
                $nombreIMG = $RutaUpload."IMG/".$nombre;
                $nombreTHU = $RutaUpload."THU/".$nombre;
                $nombreIMGdef = $RutaImagenes."IMG/".$nombre;
                $nombreTHUdef = $RutaImagenes."THU/".$nombre;

                // en caso que tengamos un segundo tipo de foto aqui tendria que comprovarse si hay que borrar las del tipo 1 o del tipo 2...
                if (file_exists($val_Foto)) {
                    // borramos el fichero original
                    unlink($val_Foto);
                }

                if (file_exists($val_thumbnail)) {
                    // borramos el fichero original
                    unlink($val_thumbnail);
                }


                move_uploaded_file( $_FILES["imagen"]["tmp_name"], $nombreORI);
                // echo "Stored in: " . $nombreORI;

                    require_once 'modIMGs.php';

                    /* procesamos la imagen grande */
                    $image = new ModifiedImage($nombreORI);
                    $image->resizeToWidth($tamIMG);
                    $image->save($nombreIMG);
                    unset($image);  

                    /* procesamos la imagen pequeña */
                    $image = new ModifiedImage($nombreORI);
                    $image->resizeToWidth($tamTHU);
                    $image->save($nombreTHU);
                    unset($image);

                    unlink($nombreORI);

                update_ImatgeInscripcio($file_idins, $nombreIMG, $nombreTHU, $idpeople, $clavecontrol );
            }
        } else {
            // echo "Invalid file.";
            $nombreORI = "";
            $nombreIMG = "";
            $nombreTHU = "";
            $nombreIMGdef = "";
            $nombreTHUdef = "";
        }
    }
    /* en caso de disponer de una copia del pasaporte */
    if (isset($_FILES["imagen_pasaporte"])) {

        //echo "<pre>: imagen_pasaporte " . print_r($_FILES["imagen_pasaporte"],true) . "</pre>";

        if ((($_FILES["imagen_pasaporte"]["type"] == "image/gif") 
            || ($_FILES["imagen_pasaporte"]["type"] == "application/pdf") 
            || ($_FILES["imagen_pasaporte"]["type"] == "image/jpeg") 
            || ($_FILES["imagen_pasaporte"]["type"] == "image/png") 
            || ($_FILES["imagen_pasaporte"]["type"] == "image/pjpeg")) 
            && ($_FILES["imagen_pasaporte"]["size"] < ($maxFileSize * 1024))) {

            if ($_FILES["imagen_pasaporte"]["error"] > 0) {

                // echo "Return Code: " . $_FILES["imagen"]["error"] . "<br />";

            } else {
                /*
                echo "Upload: " . $_FILES["imagen"]["name"] . "<br />";
                echo "Type: " . $_FILES["imagen"]["type"] . "<br />";
                echo "Size: " . ($_FILES["imagen"]["size"] / 1024) . " Kb<br />";
                echo "Temp file: " . $_FILES["imagen"]["tmp_name"] . "<br />";
                */  
                $extension = explode(".", $_FILES["imagen_pasaporte"]["name"]);
                $extension=$extension[(count($extension)-1)];

                $hash = date("YmdHis");

                $nombre = $hash."_".$file_idins_pas."_".$file_numImg_pas.".".$extension;
                $nombreORI = $RutaUpload."ORI/".$nombre;
                $nombreIMG = $RutaUpload."IMG/".$nombre;
                $nombreTHU = $RutaUpload."THU/".$nombre;
                $nombreIMGdef = $RutaImagenes."IMG/".$nombre;
                $nombreTHUdef = $RutaImagenes."THU/".$nombre;
                //$nombreORI = $RutaUpload."ORI/".$file_idins_pas."_".$file_numImg_pas.".".$extension;
                //$nombreIMG = $RutaUpload."IMG/".$file_idins_pas."_".$file_numImg_pas.".".$extension;
                //$nombreTHU = $RutaUpload."THU/".$file_idins_pas."_".$file_numImg_pas.".".$extension;
                //$nombreIMGdef = $RutaImagenes."IMG/".$file_idins_pas."_".$file_numImg_pas.".".$extension;
                //$nombreTHUdef = $RutaImagenes."THU/".$file_idins_pas."_".$file_numImg_pas.".".$extension;

                if (file_exists($nombreORI)) {
                    /* borramos el fichero original*/
                    // echo $nombreORI . " already exists. ";
                    //unlink($nombreORI);
                }

                if (file_exists($nombreIMG)) {
                    /* borramos el fichero Imagen*/
                    // echo $nombreIMG . " already exists. ";
                    unlink($nombreIMG);
                }    

                if (file_exists($nombreTHU)) {
                    /* borramos el fichero Thumbnail*/
                    // echo $nombreTHU . " already exists. ";
                    unlink($nombreTHU);
                } 

                move_uploaded_file( $_FILES["imagen_pasaporte"]["tmp_name"], $nombreORI);
                // echo "Stored in: " . $nombreORI;

                if (($_FILES["imagen_pasaporte"]["type"] == "application/pdf")) {
                    copy($nombreORI, $nombreIMG);
                    $nombreTHU = $RutaUpload."THU/nofoto35.png";

                    unlink($nombreORI);                    

                } else {
                    require_once 'modIMGs.php';

                    /* procesamos la imagen grande */
                    $image = new ModifiedImage($nombreORI);
                    $image->resizeToWidth($tamIMG_pas);
                    $image->save($nombreIMG);
                    unset($image);  

                    /* procesamos la imagen pequeña */
                    $image = new ModifiedImage($nombreORI);
                    $image->resizeToWidth($tamTHU_pas);
                    $image->save($nombreTHU);
                    unset($image);
                }

                    //unlink($nombreORI);

                update_ImatgeInscripcio_pas($file_idins_pas, $nombreIMG, $nombreTHU );
            }
        } else {
            // echo "Invalid file.";
            $nombreORI = "";
            $nombreIMG = "";
            $nombreTHU = "";
            $nombreIMGdef = "";
            $nombreTHUdef = "";
        }
    }
?>