<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['imglib_source_image_required'] = 'Debes especificar una imagen de origene en las preferencias.';
$lang['imglib_gd_required'] = 'La libreria GD image es necesaria para esta funcionalidad..';
$lang['imglib_gd_required_for_props'] = 'Tu servidor debe sorportar la libreria GD image para determinar las propiedades de la imagen.';
$lang['imglib_unsupported_imagecreate'] = 'Tu servidor no soporta la funcion GD requerida para procesar este tipo de imagen.';
$lang['imglib_gif_not_supported'] = 'Las imagenes tipo GIF usualmente no son soportadas debido a restricciones de licencia. Probablemente debas usar imagenes JPG o PNG en su lugar.';
$lang['imglib_jpg_not_supported'] = 'Las imagenes tipo JPG no estan soportadas.';
$lang['imglib_png_not_supported'] = 'Las imagenes tipo PNG no estan soportadas.';
$lang['imglib_jpg_or_png_required'] = 'El protocolo de cambio de tamaño de la imagen especificado en las preferencias solo funciona con imagenes de tipo JPEG o PNG.';
$lang['imglib_copy_error'] = 'Ha ocurrido un error mientras intentaba reemplazar el fichero. Por favor asegurese que el fichero y el directorio son modificables.';
$lang['imglib_rotate_unsupported'] = 'Rotar una imagen parece no estar soportado por tu servidor.';
$lang['imglib_libpath_invalid'] = 'La ruta de la libreria de imagen no es correcta. Por favor, establezca la ruta correcta en las preferencias de imagen.';
$lang['imglib_image_process_failed'] = 'Proceso fallido. Por favor verifique que el servidor soporta el protocolo escogido y que la ruta de la libreria de imagen es correcta.';
$lang['imglib_rotation_angle_required'] = 'Se requiere un angulo, para rotar la imagen.';
$lang['imglib_invalid_path'] = 'La ruta de la imagen no es correcta.';
$lang['imglib_invalid_image'] = 'La imagen proporcionada no es valida.';
$lang['imglib_copy_failed'] = 'La copia de la imagen ha fallado.';
$lang['imglib_missing_font'] = 'No es posible encontrar la fuente.';
$lang['imglib_save_failed'] = 'No es posible guardar la imagen. Por favor asegurate que la imagen y el fichero son modificables.';
