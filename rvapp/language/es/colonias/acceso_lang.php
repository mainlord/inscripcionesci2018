<?php
	$lang["TitolWeb"] = "Sistema de inscripción a Colonias";
	$lang["Titol"]      = "Acceso a la ficha de inscripción.";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localizador";
	$lang["centro"] = "Seleccione Centro";
	$lang["programa"] = "Seleccione Programa";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ej.: 00000000A";
	$lang["BtnSubmit"] = "Acceder al sistema";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificativos.<br />Por favor inténtelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
	$lang["MensajeFinalizado"] = "Su reserva ha finalizado.<br/><br/> Gracias por su atención y <br/>Muchas gracias por confiar en Rosa dels Vents.";
	$lang["MensajeParametrosInsuficientes"] = "No ha sido posible acceder a la aplicación.<br/><br/>Por favor, vuelva a acceder a su reserva.";
	$lang["MensajeNoExiste"] = "Atención, la ficha a la que intenta acceder ha dejado de existir.<br/><br/>Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	$lang["MensajeYaFinalizado"] = "Atención, la ficha a la que intenta acceder ya ha sido finalizada con anterioridad.<br/><br/>Por favor, si necesita hacer alguna modificación llame al Servicio de Atención de Rosa dels Vents.";
	$lang["atencion_no_reserva"] = "Información Importante:<br>Está a punto de entrar en la Ficha de Inscripción de Colonias de Rosa dels Vents.";
/*
	$lang["atencion_no_reserva2"] = "<i><u>Esta página no és un formulario de reserva</i></u> ni da derecho a ella sin su correspondiente pago. És un formulario de inscripción para ampliar la información de los participantes y ofrecerle un mejor servicio de la reserva que ya ha efectuado.<br><br>Si quiere modificar cualquier aspecto de la reserva como el número de participantes, cambiar las fechas, el centro... Debe ponerse en contacto con el departamento comercial de Rosa dels Vents (93 409 20 71 o <a href='http://www.rosadelsvents.es/es/'>www.rosadelsvents.es</a>). Tenga a mano el localizador de la reserva y su DNI.<br><br><center>¡Gracias por confiar en Rosa dels Vents!<br>Haga click en el botón para inscribir a los participantes de la reserva que acaba de efectuar</center>";
*/
	$lang["atencion_no_reserva2"] = "<i><u>Esta página no es un formulario de reserva</u></i> ni da derecho a ella sin su correspondiente pago. Es un formulario de inscripción para ampliar la información de los participantes y ofrecerle un mejor servicio de la reserva que ya ha efectuado.<br/><br/>
										Antes de empezar la inscripción es recomendable tener a disposición <i><u>los datos sanitarios, la foto, la fotocopia del carnet de vacunaciones y la fotocopia del DNI (en caso de que lo tenga) del participante</u></i>.<br/><br/>
										Si quiere modificar cualquier aspecto de la reserva como el número de participantes, cambiar las fechas, el centro... Debe ponerse en contacto con el departamento comercial de Rosa dels Vents (93 409 20 71 o <a href='http://www.rosadelsvents.es/es/'>www.rosadelsvents.es</a>). Tenga a mano el localizador de la reserva y su DNI.<br/><br/>
										<center>¡Gracias por confiar en Rosa dels Vents!<br/>
										Haga click en el botón para inscribir a los participantes de la reserva que acaba de efectuar</center>";
	$lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder al test de nivel.<br/>El período para realizar el test de nivel es desde el día de la<br/>reserva hasta 15 días antes del inicio del curso.<br/>Los participantes que no hayan podido hacer el test de nivel on-line<br/>lo harán en la escuela el primer día.";
	$lang["titulo_info"] = "Importante.";
	$lang["finaliza_correctamente"] = "Su Inscripción ha finalizado.";
	$lang["finaliza_despedida"] = "Gracias por su atención, y Muchas gracias por confiar en Rosa dels Vents.";
	$lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
	$lang["MensajeDe1En1"] = "Atención: Ya tiene una <u>Ficha de inscripción</u> o <u>Prueba de nivel</u> abierta en este navegador. <br/><br/>Por favor, finalicela antes de continuar con las siguientes.<br/><br/>Si la ha cerrado y continua saliendo este mensaje, por favor, cierre este navegador y vuelva a entrar.<br/><br/>Disculpe las molestias.";
	$lang["Aciertos"] = "<br/><br/>¡Ha acertado <b>{respuestas}/30 respuestas!</b>";
	$lang["AciertosNSNC"] = "<br/><br/>He decidido pertenecer al <b>nivel Básico</b>";
	
	$lang["MensajeNoExisteTaf"] = "Atención, la información a la que intenta acceder ha dejado de existir.<br/><br/>Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	$lang["MensajeYaFinalizadoTaf"] = "Atención, la información a la que intenta acceder ya ha sido finalizada con anterioridad.<br/><br/>Por favor, si necesita hacer alguna modificación llame al Servicio de Atención de Rosa dels Vents.";
	
?>