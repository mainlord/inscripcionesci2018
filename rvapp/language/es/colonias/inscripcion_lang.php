<?php
	$lang["AclaracionFoto"] ="* La foto no será visible hasta que haya Guardado el los datos.";
	$lang["texto_caja_email"]= "Introduzca aquí su email si quiere recibir un correo electrónico con la confirmación de la inscripción.";
	$lang["confirmacionBorrado"] = "¿Esta seguro que quiere eliminar esta ficha?";
	$lang["AlertaValidacio"]="Todos los campos son obligatorios";
	$lang["AlertaFinNivell"]="Aun hay fichas en las que no ha realizado la Prueba de Nivel de Ingles.";
	$lang["AlertaFinalitzacio"] ="¿Esta seguro que quiere finalizar la inscripción de su reserva? ";
	
	// formateig de dates.
	$lang["enero"] = "Enero"; 
	$lang["febrero"] = "Febrero";
	$lang["marzo"] = "Marzo";
	$lang["abril"] = "Abril";
	$lang["mayo"] = "Mayo";
	$lang["junio"] = "Junio";
	$lang["julio"] = "Julio";
	$lang["agosto"] = "Agosto";
	$lang["septiembre"] = "Septiembre";
	$lang["octubre"] = " Octubre";
	$lang["noviembre"] = " Noviembre";
	$lang["diciembre"] = " Diciembre";
	
	$lang["a"] = "a";
	$lang["lunes"] = "Lunes";
	$lang["martes"] = "Martes";
	$lang["miercoles"] = "Miércoles";
	$lang["jueves"] = "Jueves";
	$lang["viernes"] = "Viernes";
	$lang["sabado"] = "Sábado";
	$lang["domingo"] = "Domingo";
	$lang["de"] = "de";
	$lang["del"] = "del";
	
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	
	$lang["Enviado"] = "Enviado";
	$lang["Pendiente"] = "Pendiente de enviar";
	$lang["Estatus"] = "Estado";
	$lang["EnquestaPendent"] = "Pendiente de la Encuesta";
	
	// resta de la plana.
	$lang["TitolWeb"] = "Sistema de inscripción a Colonias";
	$lang["Titol"] = "Hoja de inscripción";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localizador";
	$lang["Dni"] = "DNI";
	$lang["centro"] = "Centro";
	$lang["programa"] = "Programa";
	
	$lang["True"] = "Sí";
	$lang["False"] = "No";
	$lang["val_True"] = 1;
	$lang["val_False"] = 0;
	
	$lang["H_Resumen"] = "Resumen de la Inscripción";
	
	$lang["H_SeccioDPersonals"] = "Datos personales del participante:";
	$lang["NomComplet"] = "Nombre completo del participante NIÑO / NIÑA";
	$lang["Chico_chica"] = "Sexo del participante";
	$lang["chico"] = "Chico";
	$lang["chica"] = "Chica";
	$lang["Telefon"] = "Teléfono del responsable";
	$lang["Adreça"] = "Dirección";
	$lang["Poblacio"] = "Población";
	$lang["CP"] = "CP";
	$lang["DataNaixement"] = "Fecha de nacimiento (DD/MM/AAAA)";
	$lang["AltresTelefons"] = "Otros teléfonos en caso de Urgencia";
	$lang["Foto"] = "Foto del Niño/Niña";
	
	$lang["H_InformacioSanitaria"] = "Información Sanitaria";
	$lang["MalaltSovint"] = "¿Se pone enfermo a menudo?";
	$lang["MalaltSovintDetall"] = "Enfermedades frecuentes";
	$lang["PrenMedicament"] = "¿Toma algún medicamento?";
	$lang["PrenMedicamentDetall"] = "¿Cual?";
	$lang["PrenMedicamentAdministracio"] = "Administración";
	$lang["SotaTractament"] = "¿Está bajo algún tratamiento específico?";
	$lang["SotaTractamentDetall"] = "¿Cual?";
	$lang["Regim"] = "¿Hace alguna dieta especial o régimen?";
	$lang["RegimDetall"] = "¿Cual?";
	$lang["DesordreAlimentari"] = "¿Sufre algún tipo de transtorno de orden alimentario?";
	$lang["DesordreAlimentariDetall"] = "¿Cual?";
	$lang["Operat"] = "¿Ha sido intervenido quirúrgicamente?";
	$lang["OperatDetall"] = "Especificar";
	/*
	$lang["Alergic"] = "¿Es alérgico?";
	$lang["AlergicDetall"] = "Especificar";
	*/
	$lang["Alergic"] = "Alérgias:";
	$lang["Alergic_celiac"] = "¿Celiaquia?";
	$lang["Alergic_lactosa"] = "¿Lactosa?";
	$lang["Alergic_ou"] = "¿Al huevo?";
	$lang["Alergic_altres"] = "¿Otros?";
	$lang["Alergic_Detall"] = "Especificar (Animales, insectos...):";
	
	$lang["H_InteresGeneral"] = "Datos de interés general";
	/* $lang["AltresMateixTipus"] = "¿Has participado en otras estadas de este tipo?"; */
	$lang["AltresMateixTipus"] = "¿Es la primera vez que participas en colonias de verano?";
	$lang["Nedar"] = "¿Sabes Nadar?";
	$lang["PorAigua"] = "¿Tienes miedo al agua?";
	$lang["Bicicleta"] = "¿Sabes ir en bicicleta?";
	$lang["AnglesForaEscola"] = "¿Estudias inglés fuera de la escuela?";
	$lang["AnglesForaEscolaDetall"] = "¿Qué curso?";
	$lang["Vertigen"] = "¿Tienes Vértigo?";
	$lang["DificultatSports"] = "¿Tienes alguna dificultad para practicar algún deporte?";
	$lang["DificultatSports_Quins"] = "¿Cual?";
	
	
	$lang["H_LPD"] = "Protección de datos";
	$lang["LPD"] = "Le informamos que todos los datos de carácter personal a los que se refiere este formulario, incluida la información sanitaria, serán tratadas con el objetivo de facilitar la gestión de las actividades de educación en el tiempo libre en las cuales participan menores de 18 años.<br /><br />El interesado/da, o su representante legal, autorizan expresamente a Colònies RV SA, al tratamiento de los datos con esta finalidad.<br /><br />Del mismo modo, le informamos de la posibilidad de ejercer, mediante los términos establecidos en la Ley Orgánica de Protección de Datos de Carácter Personal (LOPD), los derechos de acceso, rectificación, cancelación y oposición, dirigiéndose a Colònies RV SA; C/ Diputación, núm. 238, entresuelo 3ª, Barcelona, donde se le facilitaran los impresos oficiales oportunos.";
	
	
	$lang["H_AutoritzacioPersonal"] = "Autorización (para menores de 18 años)";
	$lang["Autoritzacio_senyor"] = "Señor/a";
	$lang["Autoritzacio_dni"] = ", con DNI";
	$lang["Autoritzacio_nom_fill"] = "autoriza a su hijo/a";
	$lang["Autoritzacio_del"] = "a asistir a los campamentos del";
	$lang["Autoritzacio_al"] = "al";
	$lang["Autoritzacio_casa_colonies"] = "a la casa de colonias";
	$lang["Autoritzacio_poblacio_casa_colonies"] = "de";
	$lang["Autoritzacio_compromis"] = "Hago extensiva esta autorización a las decisiones medico quirúrgicas que fueran necesarias adoptar en caso de extrema urgencia, bajo la pertinente acción facultativa. Así mismo autorizo, que en caso de enfermedad o indisposición, mi hijo/a sea trasladado de visita al centro de salud más cercano.<br/><br/>Durante toda la estancia nuestros monitores hacen fotografías y/o vídeos de los niños y niñas mientras realizan actividades con la finalidad de ponerlas en el <strong>Punt de Trobada</strong> que hay para cada programa. Así podéis ir siguiendo todas las actividades que van realizando vuestros hijos diariamente.<br/><br/>Todo el material audiovisual resultante se elimina de nuestros servidores y no se utiliza para ninguna otra finalidad distinta de la descrita anteriormente.";
	$lang["Autoritzacio_compromisC"] = "Hago extensiva esta autorización a las decisiones medico quirúrgicas que fueran necesarias adoptar en caso de extrema urgencia, bajo la pertinente acción facultativa.<br/><br/>Durante toda la estancia nuestros monitores hacen fotografías y/o vídeos de los niños y niñas mientras realizan actividades con la finalidad de ponerlas en el <strong>Punt de Trobada</strong> que hay para cada programa. Así podéis ir siguiendo todas las actividades que van realizando vuestros hijos diariamente.<br/><br/>Todo el material audiovisual resultante se elimina de nuestros servidores y no se utiliza para ninguna otra finalidad distinta de la descrita anteriormente.";
	$lang["Autoritzacio_compromis_6"] = "Hago extensiva esta autorización a las decisiones medico quirúrgicas que fueran necesarias adoptar en caso de extrema urgencia, bajo la pertinente acción facultativa. Así mismo autorizo, que en caso de enfermedad o indisposición, mi hijo/a sea trasladado de visita al centro de salud más cercano.";
	$lang["Autoritzacio_acceptacio"] = "Marca la casilla conforme aceptas los términos explicados en este documento.";
	$lang["Autoritzacio_poblacio"] = " Acepto en (población)";
	
	$lang["H_Acceptacio_Normativa"] = " Aceptación de la Normativa de Funcionamiento";
	$lang["H_Acceptacio_NormativaC"] = " Aceptación de la Normativa";
	$lang["Normativa_autoritzacio"] = "Marca la casilla conforme aceptas los terminos explicados en la Normativa.";
	$lang["Normativa_acceptacio"] = "Acepto las condiciones detalladas en la Normativa de funcionamento.";
	$lang["Normativa_acceptacioC"] = "Acepto las condiciones detalladas en la Normativa.";
	$lang["Normativa_llegir"] = "Si desea leer las condiciones haga click";
	$lang["Normativa_texte_enllac"] = "aquí";$lang["Normativa_enllac_pdf"] = "./pdf/normativa_es.pdf";
	
	$lang["H_Botonera"] = "Formalización de la inscripción";
	$lang["Botonera"] = "Una vez introducidos los datos pulse el botón Grabar para continuar con el proceso de inscripción.";
	$lang["SyC"] = "Grabar los datos del participante";
	$lang["NEW"] = "Introducir un nuevo Participante sin Guardar";
	$lang["UPD"] = "Actualizar el Participante actual";
	$lang["END"] = "Poner fin a la Inscripción";
	
	// Variables introducidas por Juanma
	$lang["titulo_hipica"] = "Encuesta H&iacute;pica";
	$lang["cabecera_hipica"] = "Datos de inter&eacute;s para la h&iacute;pica:";
	$lang["q_1_hipica"] = "&iquest;Has montado a caballo antes?";
	$lang["hipica1_radio1"] = "No.";
	$lang["hipica1_radio2"] = "S&iacute;, una experiencia positiva.";
	$lang["hipica1_radio3"] = "S&iacute;, una experiencia negativa.";
	$lang["comentario_hipica"] = "Si ha seleccionado la primera opci&oacute;n, no hace falta que responda a las siguientes preguntas.";
	$lang["q_2_hipica"] = "&iquest;Cuantas veces has montado, aproximadamente?";
	$lang["hipica2_radio1"] = "Entre 1 y 5 veces.";
	$lang["hipica2_radio2"] = "Entre 5 y 10 veces.";
	$lang["hipica2_radio3"] = "M&aacutes de 10 veces.";
	$lang["q_3_hipica"] = "&iquest;Has trotado alguna vez?";
	$lang["hipica3_radio1"] = "No, nunca.";
	$lang["hipica3_radio2"] = "He trotado, pero no s&eacute; cómo hacerlo.";
	$lang["hipica3_radio3"] = "He trotado, y s&eacute; como hacer el trote a la inglesa.";
	$lang["q_4_hipica"] = "&iquest;Has galopado alguna vez?";
	$lang["hipica4_radio1"] = "No, nunca.";
	$lang["hipica4_radio2"] = "He galopado, pero no s&eacute; cómo hacerlo.";
	$lang["hipica4_radio3"] = "He galopado, y s&eacute; como hacer el movimiento correctamente.";
	$lang["comentario_observaciones_hipica"] = "Si cres que hay alg&uacute;n dato m&aacute;s (clases recibidas, acceso a caballos propios o de amigos,etc...), hazlo constar en el apartado de observaciones.";
	$lang["observaciones_hipica"] = "Observacions: especificad en este apartado si sois repetidores de programa de equitación.";
	
	$lang["titulo_grupo7"] = "Autorizaci&oacute;n para paintball, motos y quad.";
	$lang["autorizacion_grupo7"] = "Autorizo a mi hijo/a a practicar las actividades de paintball, motos y quad durante su estada en las colonias de Rosa dels Vents.";
	$lang["exp_grupo7"] = "Marca la casilla conforme autorizas a tu hijo/a.";
	// Fin Juanma
	
	$lang["EnlaceBorrar"] = "X";
	
	$lang["ObsGeneral"] = "Otros aspectos a tener en cuenta (insomnio, sonambulismo...):";
	
	$lang["H_Mallorca"] = "DNI obligatorio";
	$lang["exp_mallorca"] = "Para este programa es requisito indispensable poner el DNI del participante. Este DNI no puede encontrarse caducado bajo ningún concepto.";
	
	$lang["H_futbol"] = "Campus de Futbol";
	$lang["exp_futbol"] = "Escoje el equipamiento que quieres vestir en el curso de futbol";
	$lang["Portero"] = "Necesitaré equipamiento de Portero";
	$lang["Jugador"] = "Necesitaré equipamiento de Jugador de campo";
	
	$lang["H_Informacio"] = "Como cumplimentar este formulario ";
	$lang["Informacio_Presentacion"] = "Este texto está destinado a ayudarloa completar los datos de este formulario. ";
	$lang["Informacio_Basica"] = "Lo primero a tener en cuenta es que debe rellenar un formulario por cada uno de los participantes de la reserva. ".
							 "Para introducir la ficha será necesario llenar Todos los campos que encontrara en este formulario.<br/><br/>".
							 "Los únicos campos que podrá dejar vacios son los campos de observación general.<br/><br/>".
							 "Una vez introducidos todos los datos, deberá pulsar el botón <b>".$lang["SyC"]."</b>.<br/><br/>".
							 "Cuando la información de su primera ficha haya sido guardada aparecerá, en la parte superior del formulario, un cuadro informativo con el listado de los participantes introducidos. Si necesita introducir más fichas podrá continuar gracias a que una vez guardado el formulario quedara listo para la introducción de la siguiente ficha justo debajo de la caja de <b>".$lang["H_Resumen"]."</b>.<br/>";
	$lang["Informacio_Update"] = "En caso que quisiera modificar la información introducida en alguna de las fichas, podrá hacerlo clicando sobre la foto o el nombre del participante que encontrará sobre la caja de <b>".$lang["H_Resumen"]."</b><br/>";
	$lang["Informacio_EncuestasPendientes"] = "Como puede comprobar en la caja de <b>".$lang["H_Resumen"]."</b> tiene fichas pendientes de finalizar la <b>Prueba de Nivel de Ingles </b>. Para acceder a la prueba solo debe hacer click sobre el texto <b>".$lang["EnquestaPendent"]."</b><br />";
	$lang["Informacio_BtnFinalizar"] = "Si ha acabado de introducir las fichas de los participantes y no tiene ninguna alerta pendiente ya puede apretar el botón <b>".$lang["END"]."</b> para finalizar el proceso de inscripción. <br/>";
	
	$lang["H_ErrorFechas"] = "Error de Datos";
	$lang["MsgErrorFechas"] = "No ha sido posible actualizar los datos. Por favor, vuelva a realizar las modificaciones";
	
	$lang["texto_adjuntar_1"] = "Para adjuntar un fichero arrastralo aquí, o ";
	$lang["texto_adjuntar_2"] = "Escoje una aquí";
	$lang["MensajeNoExiste"] = "Atención, la Ficha de inscripción a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	
	$lang["finalizada_correctamente"] = "Su Ficha de Inscripci&oacute;n ha sido finalizada correctamente.";
	$lang["obligatorio"] = "Dato obligatorio";
	$lang["campos_obligatorios_pendientes"] = "Quedan datos obligatorios pendientes de introducir";

	$lang["FotoDniDelantera"] 	= "Foto DNI delantera";
	$lang["FotoDniTrasera"] 	= "Foto DNI trasera";
	$lang["FotosCartillaVacunas"] = "Cartilla de vacunaciones";
	$lang["no_cvac"] 			= "Todavía no ha adjuntado ninguna foto del carnet de vacunaciones.";
	$lang["OtrasConsideraciones"] = "Otras consideraciones";

	// a partir de aqui son los textos de la pagina de dnis para vuelos
	$lang["Titol_vuelo"] = "Datos de los participantes";
	$lang["Participant_N"] = "Participante nº ";
	$lang["Nom"] = "Nombre";
	$lang["Cognom1"] = "Primer apellido";
	$lang["Cognom2"] = "Segundo apellido";
	$lang["ValidarInformacio"] = "Validar la información de los participantes";
	$lang["Validado"] = "Ya ha introducido toda la información necesaria. Ahora puede enviar el formulario, Gracias!";
	$lang["taf_finalizada_correctamente"] = "La informaci&oacute;n ha sido registrada correctamente.";

	// covid 19
	$lang["covid19_titol_declaracio"] 				= "Declaració responsable en relació a la situació de pandèmia generada per la Covid-19";
	$lang["covid19_texte_declaracio_introduccio1"]	= "Los nuevos protocolos de la Direcció General de Joventut de la Generalitat incluyen una declaración responsable que han de firmar todos los padres/madres y/o tutores de niños/as inscritos en estancias de campamentos, acampadas, “casals”, campus deportivos...<br/><br/>En esta declaración firman que son conocedores del contexto de pandemia actual y que han	estado informados y declaran cúal es el estado de salud de su hijo/a antes de iniciar el campamento.";
	$lang["covid19_texte_declaracio_subtitol"] 		= "Declaro sota la meva responsabilitat:";
	$lang["covid19_texte_declaracio_li1_1"] 		= "Que sóc coneixedor/a del context de pandèmia actual provocada per la Covid-19 i que accepto les circumstàncies i riscos que aquesta situació pot comportar durant el	desenvolupament de l’activitat d’educació en el lleure en la que en/la ";
	$lang["covid19_texte_declaracio_li1_2"] 		= "participa. Així mateix, entenc que l'equip de dirigents i l'entitat organitzadora de l'activitat no són responsables de les contingències que puguin ocasionar-se en relació a la pandèmia durant l'activitat.";
	$lang["covid19_texte_declaracio_li2"]   		= "Que he sigut informat/da i estic d’acord amb les mesures de prevenció general i amb les actuacions necessàries que poden haver-se de dur a terme si apareix un cas d’un menor d’edat amb simptomatologia compatible amb la Covid-19 durant el desenvolupament de l’activitat.";
	$lang["covid19_texte_declaracio_li3_1"] 		= "Que m’encarrego d’aportar les quantitat necessària de mascaretes per	en/la ";
	$lang["covid19_texte_declaracio_li3_2"] 		= "pels dies que duri l’activitat, en cas que així ho requereixi l’equip de dirigents.";
	$lang["covid19_texte_declaracio_li4_1"] 		= "Que informaré a l’entitat organitzadora de qualsevol variació de l’estat de salut d’en/la";
	$lang["covid19_texte_declaracio_li4_2"] 		= "compatible amb la simptomatologia Covid-19 mentre duri l’activitat, així com de l’aparició de qualsevol cas de Covid-19 en el seu entorn familiar.";
	$lang["covid19_texte_declaracio_introduccio2_1"]= "Finalment, que amb caràcter previ a la realització de l’activitat, en/la";
	$lang["covid19_texte_declaracio_introduccio2_2"]= "compleix els requisits de salut següents:";
	$lang["covid19_texte_declaracio_li5"]   		= "Presenta absència de malaltia i simptomatologia compatible amb la Covid-19 (febre, tos, dificultat respiratòria, malestar, diarrea...) o amb qualsevol altre quadre infecciós.";
	$lang["covid19_texte_declaracio_li6"]   		= "No ha conviscut o no ha tingut contacte estret amb una persona positiva de Covid-19 confirmada o amb una persona que ha tingut simptomatologia compatible en els 14 dies anteriors a la realització de l’activitat.";
	$lang["covid19_texte_declaracio_opcional_1"]	= "Només marcar en cas de menor amb patologia crònica complexa considerada de risc per la Covid-19:";
	$lang["covid19_texte_declaracio_opcional_2"]	= "Que els serveis mèdics han valorat positivament i de manera individual la idoneïtat	de la seva participació en l’activitat.";
	$lang["covid19_texte_declaracio_accepto_1"] 	= "Accepta en/na ";
	$lang["covid19_texte_declaracio_accepto_2"] 	= " amb DNI ";
	$lang["covid19_texte_declaracio_accepto_3"] 	= " com a pare/mare o tutor/tutora a data ";
	$lang["covid19_texte_declaracio_accepto_4"] 	= " a ";
	$lang["covid19_texte_info_adicional"] 			= "En caso de que lo crea conveniente, cuéntenos cuál es el estado emocional de su hijo/a, en el sentido de cómo ha vivido estos meses de confinamiento, si han sufrido alguna pérdida en el entorno familiar y/o entorno de proximidad, si ha sido un periodo de estrés elevado… Cualquier información que considere relevante para poder hacer un mejor acompañamiento durante las colonias.";

?>	