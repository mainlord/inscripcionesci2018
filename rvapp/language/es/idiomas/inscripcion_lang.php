<?php
	$lang["titulo_pagina"]          = "Acceso a la ficha de inscripción.";
    
// formateig de dates.
	$lang["enero"] = "Gener"; 
	$lang["febrero"] = "Febrer";
	$lang["marzo"] = "Març";
	$lang["abril"] = "Abril";
	$lang["mayo"] = "Maig";
	$lang["junio"] = "Juny";
	$lang["julio"] = "Juliol";
	$lang["agosto"] = "Agost";
	$lang["septiembre"] = "Setembre";
	$lang["octubre"] = "Octubre";
	$lang["noviembre"] = "Novembre";
	$lang["diciembre"] = "Decembre";

	$lang["a"] = "a";
	$lang["lunes"] = "Dilluns";
	$lang["martes"] = "Dimarts";
	$lang["miercoles"] = "Dimecres";
	$lang["jueves"] = "Dijous";
	$lang["viernes"] = "Divendres";
	$lang["sabado"] = "Dissabte";
	$lang["domingo"] = "Diumenge";
	$lang["de"] = "de";
	$lang["del"] = "del";

	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellà";

	$lang["Enviado"] = "Enviat";
	$lang["Pendiente"] = "Pendent";

// resta de la plana.
	$lang["TitolWeb"] = "Sistema de inscripción a Colonias";
	$lang["Titol"] = "Ficha de inscripción";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localizador";
	$lang["Dni"] = "DNI";
	$lang["centro"] = "Centro";
	$lang["programa"] = "Programa";

	$lang["True"] = "Sí";
	$lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;

	$lang["H_Resumen"] = "Resum de la Inscripció";

	$lang["H_SeccioDPersonals"] = "Dades personals del participant:";
	$lang["NomComplet"] = "Nom i cognoms";
	$lang["Telefon"] = "Telèfon";
	$lang["Adreça"] = "Adreça";
	$lang["Poblacio"] = "Població";
	$lang["CP"] = "CP";
	$lang["DataNaixement"] = "Data de naixement";
	$lang["AltresTelefons"] = "Altres telèfons en cas d'urgencia";
	$lang["Foto"] = "Foto";
	$lang["Pasaporte"] = "Nº pasaporte";

	$lang["H_InformacioSanitaria"] = "Informació Sanitària";
	$lang["MalaltSovint"] = "Es posa malalt sovint?";
	$lang["MalaltSovintDetall"] = "Malalties més freqüents";
	$lang["PrenMedicament"] = "Pren algun medicament?";
	$lang["PrenMedicamentDetall"] = "Quin?";
	$lang["PrenMedicamentAdministracio"] = "Administració";
	$lang["Regim"] = "Està sota algun règim?";
	$lang["RegimDetall"] = "Quin?";
	$lang["Operat"] = "Ha sigut intervingut quirúrgicament?";
	$lang["OperatDetall"] = "Especificar";
	$lang["Alergic"] = "És al·lèrgic?";
	$lang["AlergicDetall"] = "Especificar";

	$lang["H_InteresGeneral"] = "Dades d'interès general";
	$lang["AltresMateixTipus"] = "Has participat en altres estades d'aquest tipus?";
	$lang["Nedar"] = "Saps Nedar?";
	$lang["PorAigua"] = "Tens por a l'aigua?";
	$lang["Bicicleta"] = "Saps anar en bicicleta?";
	$lang["AnglesForaEscola"] = "Estudies anglès fora de l'escola?";
	$lang["AnglesForaEscolaDetall"] = "Quin curs?";

	$lang["H_LPD"] = "Proteccio de dades";
	$lang["LPD"] = "Texte de proteccio de dades";

	$lang["H_AutoritzacioPersonal"] = "Autorització (per a menors de 18 anys)";
	$lang["Autoritzacio_senyor"] = "Senyor/a";
	$lang["Autoritzacio_dni"] = ", amb DNI";
	$lang["Autoritzacio_nom_fill"] = "autoritza al seu fill/a";
	$lang["Autoritzacio_del"] = "a assistir als campaments del";
	$lang["Autoritzacio_al"] = "al";
	$lang["Autoritzacio_casa_colonies"] = "a la casa de colònies";
	$lang["Autoritzacio_poblacio_casa_colonies"] = "de";
	$lang["Autoritzacio_compromis"] = "Faig extensiva aquesta autorització a les decisions medico-quirúrgiques que fossin necessàries adoptar en cas d'extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu fill/a sigui traslladat de visiata al centre de salut més proper.";
	$lang["Autoritzacio_acceptacio"] = "Marca la casella conforme acceptes els termes explicats en aquest document.";
	$lang["Autoritzacio_poblacio"] = "Accepto en (població)";

	$lang["H_Acceptacio_Normativa"] = "Aceptación de la Normativa";
	$lang["Normativa_autoritzacio"] = "Marque la casilla conforme acepta los términos y condiciones generales de la reserva.";
	$lang["Normativa_acceptacio"] = "Acepto los términos y condiciones generales de la reserva.";
	$lang["Normativa_llegir"] = "Si desea leer las condiciones clicke";
	$lang["Normativa_texte_enllac"] = "CONDICIONES";

	$lang["SyC"] = "Introducir ficha para otro participante";
	$lang["NEW"] = "Introduir un nou Participant desde 0";
	$lang["UPD"] = "Actualitzar el Participant actual";
	$lang["END"] = "Poner fin a la Inscripción";

/*Variables introducidas por Juanma*/
	$lang["titulo_hipica"] = "- Encuesta H&iacute;pica";
	$lang["cabecera_hipica"] = "Datos de inter&eacute;s para la h&iacute;pica:";
	$lang["q_1_hipica"] = "&iquest;Has montado a caballo antes?";
	$lang["hipica1_radio1"] = "-No.";
	$lang["hipica1_radio2"] = "-S&iacute;, una experiencia positiva.";
	$lang["hipica1_radio3"] = "-S&iacute;, una experiencia negativa.";
	$lang["comentario_hipica"] = "Si ha seleccionado la primera opci&oacute;n, no hace falta que responda a las siguientes preguntas.";
	$lang["q_2_hipica"] = "&iquest;Cuantas veces has montado, aproximadamente?";
	$lang["hipica2_radio1"] = "-Entre 1 y 5 veces.";
	$lang["hipica2_radio2"] = "-Entre 5 y 10 veces.";
	$lang["hipica2_radio3"] = "-M&aacutes de 10 veces.";
	$lang["q_3_hipica"] = "&iquest;Has trotado alguna vez?";
	$lang["hipica3_radio1"] = "-No, nunca.";
	$lang["hipica3_radio2"] = "-He trotado, pero no s&eacute; como hacerlo.";
	$lang["hipica3_radio3"] = "-He trotado, y s&eacute; como hacer el trote a la inglesa.";
	$lang["q_4_hipica"] = "&iquest;Has galopado alguna vez?";
	$lang["hipica4_radio1"] = "-No, nunca.";
	$lang["hipica4_radio2"] = "-He galopado, pero no s&eacute; como hacerlo.";
	$lang["hipica4_radio3"] = "-He galopado, y s&eacute; como hacer el movimiento correctamente.";
	$lang["comentario_observaciones_hipica"] = "Si cres que hay alg&uacute;n dato m&aacute;s (clases recibidas, acceso a caballos propios o de amigos,etc...), hazlo constar en el apartado de observaciones.";
	$lang["observaciones_hipica"] = "Observaciones:";

	$lang["titulo_grupo7"] = "7 .- Autorizaci&oacute;n para paintball y quad.";
	$lang["autorizacion_grupo7"] = "Autorizo a mi hijo/a a practicar las actividades de paintball y quad durante su estada en las colonias de Rosa dels Vents.";
	$lang["exp_grupo7"] = "Marca la casesilla conforme autorizas a tu hijo/a.";

	$lang["titulo_datos_curso"] = "DATOS DEL CURSO CONTRATADO";
	$lang["NomPrograma"] = "Nombre del programa";
	$lang["Fechas"] = "Fechas";
	$lang["Amigos_Familiares"] = "¿Tiene amigos o familiares en el mismo programa?";
	$lang["Nombre_amigo"] = "Nombre y apellidos del amig@ o familiar";
	$lang["Ciudad_salida"] = "Ciudad de Salida";
	$lang["titulo_datos_participante"] = "DATOS DEL PARTICIPANTE";
// 	$lang["importante_pasaporte"] = "IMPORTANTE: La copia del pasaporte es muy importante para formalizar los vuelos.  Nos la puede enviar en formato JPG, PNG y PDF<br/><br/>Si en este momento no dispone de la copia, puede enviarla antes de 15 días a: 'info@rosadelsventsidiomas.es'. ";
	$lang["importante_pasaporte"] = "IMPORTANTE: La copia del pasaporte es muy importante para formalizar los vuelos.  Nos la puede enviar en formato JPG, PNG y PDF<br/><br/>Si en este momento no dispone de la copia del pasaporte, <strong>puede enviarla como máximo hasta el 31 de Mayo</strong>, por email ​a: <a href='mailto:info@rosadelsventsidiomas.es' target='_blank'>info@rosadelsventsidiomas.es</a>.";
	$lang["importante_nombres"] = "Rogamos compruebe que el nombre, apellidos y fecha de nacimiento del participante coinciden con los datos del pasaporte. En caso contrario, enviar un email con los datos correctos a: <a href=\"info@rosadelsventsidiomas.es\">info@rosadelsventsidiomas.es</a>";
	$lang["Nom_Participante"] = "Nombre";
	$lang["Cognom_Participante"] = "Apellidos";
	$lang["chico"] = "Chico";
	$lang["chica"] = "Chica";
	$lang["Direccion_Participante"] = "Dirección";
	$lang["Direccion_Padres"] = "Dirección";
	$lang["Codigo_postal"] = "Código Postal";
	$lang["CP_Padres"] = "Código Postal";
	$lang["Poblacion"] = "Población";
	$lang["Poblacion_Padres"] = "Población";
	$lang["Telefono"] = "Teléfono";
	$lang["Movil"] = "Móvil de contacto Padre";
	$lang["Movil2"] = "Móvil de contacto Madre";
	$lang["Fecha_nacimiento"] = "Fecha de nacimiento";
	$lang["Edad_Participante"] = "Edad";
	$lang["copia_pasaporte"] = "Copia Escaneada del Pasaporte";
	$lang["Nacionalidad"] = "Nacionalidad";
	$lang["Email_Participantes"] = "E-mail estudiante";
	$lang["Email_Padres"] = "E-mail de contacto";
	$lang["Movil_Participante"] = "Móvil estudiante";
	$lang["Movil_Padres"] = "Móvil de contacto";
	$lang["Nombre_Padres"] = "Nombre del padre, madre o tutor";
	$lang["DNI_Padres"] = "DNI";
	$lang["Algun_Curso"] = "¿Es la primera vez que realiza un curso de idiomas en el extranjero?"; /* Has realizado algún curso de idiomas en el extrangero?"; */
	$lang["Algun_Curso_Detalle"] = "¿A dónde fue?";
	$lang["Algun_Curso_Detalle2"] = "¿Con qué organización?";
	$lang["Alguna_Enfermedad"] = "¿Padece algún tipo de enfermedad?";
	$lang["Alguna_Enfermedad_Detalle"] = "Especificar (TDH, TDHA, asma, diabetes, lesiones, ...)";
	$lang["Alguna_alergia"] = "¿Padece algún tipo de alergia (alimentaria, animales, medicamentos)?";
	$lang["Alguna_alergia_Detalle"] = "Especificar (frutos secos, marisco, huevos, perros, gatos, hámsters, ...)";
	$lang["Dieta_Especial"] = "¿Necesita alguna dieta especial?";
	$lang["Dieta_Especial_Detalle"] = "Especificar (sin gluten, sin lactosa, vegetariana, vegana, ...)";
	$lang["Algun_Medicamento"] = "¿Necesita medicación durante la estancia?";
	$lang["Algun_Medicamento_Detalle"] = "Especificar";
	$lang["titulo_datos_academicos"] = "DATOS ACADÉMICOS";
	$lang["titulo_datos_contacto"] = "DATOS DE CONTACTO";
	$lang["Colegio"] = "Escuela o Instituto al que asiste su hij@";
	$lang["Direccion_Colegio"] = "Dirección";
	$lang["CP_Colegio"] = "Código Postal";
	$lang["Poblacion_Colegio"] = "Población";
	$lang["Nombre_Profesor"] = "Nombre del profesor de idiomas";
	$lang["Curso"] = "Curso escolar";
	$lang["Nivel_Idioma"] = "Nivel del idioma";
	$lang["Bajo"] = "Bajo";
	$lang["Medio"] = "Medio";
	$lang["Alto"] = "Alto";
	$lang["Bilingue"] = "Bilingüe";
	$lang["Colegio_miniestancia"] = "¿La escuela organiza estancias en el extranjero?";

	$lang["titulo_autorizacion_audiovisual"] = "AUTORIZACIÓN AUDIOVISUAL";
	$lang["autorizacion_audiovisual_padres"] = "Durante las estancias en el extranjero, nuestros monitores hacen fotografías y/o vídeos de los estudiantes a lo largo del curso, con la finalidad de ponerlas  en el blog que hay para cada programa. Así podéis ir siguiendo  todas las actividades que van realizando vuestros hijos diariamente.";
	$lang["Nombre_autorizacion_audiovisual_padres"] = "Nombre padre, madre o tutor";
	$lang["Dni_autorizacion_audiovisual_padres"] = "DNI";
	$lang["Autoritzacio_acceptacio_audiovisual"] = "AUTORIZO la utilización de cualquier material fotográfico y/o audiovisual en que aparezca el participante para las promociones comerciales y de marketing de la compañía (página web, blogs y/o redes sociales).";

	$lang["titulo_autorizacion_sin_monitor"] = "AUTORIZACIÓN DE SALIDA SIN MONITOR";
	$lang["para_15_17"] = "Sólo para estudiantes de 15 a 17 años. Ver normas sobre salidas sin monitor en el documento información práctica.";
/*	$lang["autorizacion_sin_monitor_padres"] = "Los estudiantes que participan en nuestros cursos y que tengan 13 años o más, podrán salir sin monitor en su tiempo libre y antes o después de cenar. El horario de regreso como máximo: las 21H00 para estancias en Residencia, las 21H30 para estancias en Familia y las 23H00 para los cursos de Malta. <u>Excepción:</u> Los estudiantes que participen en los programas St Martins Ampleforth, Tipperary y Trowbridge <u>NO PODRÁN SALIR DEL ALOJAMIENTO SOLOS</u>. Ver normas específicas en el documento de <a target=\"_blank\" href=\"https://www.cursosidiomas.com/pdf/info-practica-es.pdf\">Información Práctica</a>.</br></br>Autorizo a mi hijo/a a salir de su alojamiento durante los tiempos libres estipulados. (En caso de no estar de acuerdo rogamos nos los comuniquen por escrito.)";*/
	$lang["autorizacion_sin_monitor_padres"] = "<u>Para estancias en Residencia:</u> Todos los jóvenes a partir de 13 años están autorizados a salir solos de la Residencia sin los monitores durante los tiempos libres y antes o después de cenar. La hora máxima de regreso a la Residencia son las  21H00. Los jóvenes que participen en el curso Ampleforth College no podrán salir de la residencia, ya que no es imposible acceder a pie hasta el centro urbano.<br/><br/><u>Para estancias en Familia:</u> Siempre y cuando la familia de acogida lo permita, los jóvenes mayores de 13 años tienen permiso para salir solos sin monitor durante el tiempo libre y antes o después de cenar. La hora máxima de regreso a la familia es las 21H00.<br/>Los jóvenes pueden salir por la zona en donde se encuentra la familia, en ningún caso la familia está obligada ​de llevar al estudiante al centro para encontrarse con otros estudiantes.<br/><u>Los estudiantes podrán salir solo si la familia se lo permite, en algunos casos las familias no dejan que los estudiantes salgan solos.</u><br/>Excepcionalmente en  Malta la hora de regreso a la familia es a las 22H00.<br/>Los jóvenes que participen en el curso Tipperary no podrán salir solos después de cenar ya que las familias no lo permiten. <br/><br/>​Entiendo y acepto la normativa de salidas sin monitor. En caso de no estar de acuerdo rogamos nos lo comuniquen por escrito enviando un email a <a href=\"mailto:info@rosadelsventsidiomas.es\" target=\"_blank\">info@rosadelsventsidiomas.es</a>. ";
	$lang["Nombre_autorizacion_padre"] = "Nombre padre, madre o tutor";
	$lang["DNI_autorizacion_padres"] = "DNI";
	$lang["Autoritzacio_acceptacio_monitor"] = "Marque la casilla conforme autoriza a su hijo/a a las salidas sin monitor.";
	$lang["titulo_autorizacion_medica"] = "AUTORIZACIÓN MÉDICA";
	$lang["autorizacion_medica_padres"] = "Autorizo a mi hijo/a a asistir al curso de inglés en el extranjero en el que ha sido inscrito. Hago extensiva esta autorización a las decisiones médico-quirúrgicas que fuesen necesarias adoptar en caso de extrema urgencia, bajo la pertinente acción facultativa. Asimismo autorizo, que en caso de enfermedad o indisposición, mi hijo/a sea trasladado al centro de salud más próximo.";
	$lang["Nombre_autorizacion_padre_medica"] = "Nombre padre, madre o tutor";
	$lang["DNI_autorizacion_padres_medica"] = "DNI";
	$lang["Autoritzacio_acceptacio_medica"] = "Marque la casilla conforme da la autorización.";
	$lang["titulo_observaciones"] = "OBSERVACIONES";
	$lang["titulo_como_conocido"] = "¿CÓMO NOS HA CONOCIDO?";
	$lang["conocido_radio"] = "Radio";
	$lang["conocido_TV"] = "TV";
	$lang["conocido_amigos"] = "Recomendación amigos o familiares";
	$lang["conocido_internet"] = "Internet";
	$lang["conocido_Colegio"] = "Colegio";
	$lang["conocido_Prensa"] = "Prensa";
	$lang["conocido_clientes"] = "Somos clientes";
	$lang["Autobus"] = "Autobús";
	$lang["Otros"] = "Otros";
	$lang["Metro"] = "Metro";
	$lang["condiciones_generales"] = "ACEPTACIÓN DE LAS CONDICIONES GENERALES DE RESERVA";
	$lang["proteccion_datos"] = "Todos los datos de carácter personal a los que se refiere este formulario, serán tratados exclusivamente con finalidades comerciales y operativas de la organización de acuerdo con la legislación vigente en materia de protección de datos de carácter personal. Asimismo, le informamos de la posibilidad de ejercer los derechos de acceso, rectificación, cancelación y oposición, dirigiéndose a nuestras oficinas.";
	$lang["titulo_ficha"] = "Ficha de inscripción";
	$lang["Amigos_Juntos"] = "¿Desea alojarse en la misma habitación o familia que sus amigos o familiares?";

	$lang["texto_caja_email"]= "Introduzca aquí su email si quiere recibir un correo electrónico con la confirmación de la inscripción.";
	$lang["EnlaceBorrar"] = "X";
	$lang["confirmacionBorrado"] = "¿Está seguro que quiere eliminar esta ficha?";
	$lang["Foto"] = "Foto";
	$lang["pdf_condiciones"] = "esp condiciones generales 2012.pdf";
	$lang["Botonera"] = "Finalizar la Ficha de Inscripción.";
	$lang["no_tenemos_pasaporte"] = "*Necesita una copia del pasaporte para terminar la inscripción.";
	$lang["observaciones_desc"] = "Por favor, indíquenos algún otro detalle que considere importante.";

	$lang["no_hay_programa"] = "El campo nombre del programa de datos del curso está vacío.";
	$lang["no_hay_fechas"] = "El campo fechas de datos del curso está vacío.";
	$lang["no_hay_nombre_participante"] = "El campo nombre de datos del participante está vacío.";
	$lang["no_hay_apellidos_participante"] = "El campo apellidos de datos del participante está vacío.";
	$lang["no_hay_direccion_participante"] = "El campo dirección de datos del participante está vacío.";
	$lang["no_hay_codigo_postal"] = "El campo código postal de datos del participante está vacío.";
	$lang["no_hay_poblacion"] = "El campo población de datos del participante está vacío.";
	$lang["no_hay_telefono"] = "El campo teléfono de datos del participante está vacío.";
	$lang["no_hay_movil"] = "El campo móvil de datos del participante está vacío.";
	$lang["no_hay_fecha_nacimiento"] = "El campo fecha de nacimiento de datos del participante está vacío.";
	$lang["no_hay_pasaporte"] = "El campo pasaporte de datos del participante está vacío.";
	$lang["no_hay_nacionalidad"] = "El campo nacionalidad de datos del participante está vacío.";
	$lang["no_email_participantes"] = "El campo E-mail de datos del participante está vacío.";
	$lang["no_email_padres"] = "El campo E-mail de datos de contacto está vacío.";
	$lang["no_movil_padres"] = "El campo móvil de los padres de datos de contacto está vacío.";
	$lang["no_movil_participante"] = "El campo móvil del estudiante de datos del participante está vacío.";
	$lang["no_hay_chico_chica"] = "El campo Chico / Chica no ha sido marcado.";

	$lang["no_nombre_padres"] = "El campo nombre de los padres de datos del participante está vacío.";
	$lang["no_dni_padres"] = "El campo dni de los padres de datos del participante está vacío.";

	$lang["no_nombre_autorizacion_audiovisual_padres"] = "El campo nombre de los padres de la autorización audiovisual del participante está vacío.";
	$lang["no_dni_autorizacion_audiovisual_padres"] = "El campo dni de los padres de la autorización audiovisual del participante está vacío.";
	$lang["no_autoritzacio_acceptacio_audiovisual"] = "La casilla de la autorización audiovisual no se ha marcado.";

	$lang["no_colegio"] = "El campo colegio de datos académicos está vacío.";
	$lang["no_direccion_colegio"] = "El campo dirección de datos académicos está vacío.";
	$lang["no_cp_colegio"] = "El campo código postal de datos académicos está vacío.";
	$lang["no_poblacion_colegio"] = "El campo población de datos académicos está vacío.";
	$lang["no_nombre_profesor"] = "El campo nombre del profesor de datos académicos está vacío.";
	$lang["no_curso"] = "El campo curso de datos académicos está vacío.";
	$lang["no_nombre_autorizacion_padre_medica"] = "El campo nombre del padre, madre o tutor de la autorización médica está vacío.";
	$lang["no_dni_autorizacion_padres_medica"] = "El campo dni de la autorización médica está vacío.";
	$lang["no_nombre_autorizacion_padre"] = "El campo nombre del padre, madre o tutor de la autorización de salida sín monitor está vacío.";
	$lang["no_dni_autorizacion_padres"] = "El campo dni de la autorización de salida sín monitor está vacío.";
	$lang["no_formato_fecha_nacimiento"] = "El campo fecha de nacimiento de datos del participante no tiene el fomato correcto o no es una fecha válida.";
	$lang["no_formato_data_in"] = "El campo fechas de datos del curso no tiene el fomato correcto o no es una fecha válida.";
	$lang["no_autoritzacio_acceptacio_medica"] = "La casilla de la autorización médica no se ha marcado.";
	$lang["no_normativa_acceptacio"] = "La casilla de la autorización de las condiciones generales de la reserva no se ha marcado.";
	$lang["no_autoritzacio_acceptacio_monitor"] = "La casilla de la autorización de las salidas sin monitor no se ha marcado.";

	$lang["no_algun_curso"] = "No has detallado dónde o con qué organización has realizado un curso de idiomas en el extranjero.";
	$lang["no_alguna_enfermedad"] = "No has detallado que tipo de enfermedad tienes.";
	$lang["no_alguna_alergia"] = "No has detallado que tipo de alergia tienes.";
	$lang["no_amics_familiars"] = "No has detallado el nombre de tu amigo o familiar.";
	$lang["no_dieta_especial"] = "No has detallado cual es la dieta especial que debes seguir.";
	$lang["no_algun_medicamento_detalle"] = "No has detallado que medicación necesitas durante la estancia.";
	$lang["no_conocido"] = "No has detallado por que otro medio nos has conocido.";
/*Fin Juanma*/

	$lang["H_ErrorFechas"] = "Error de Datos";
	$lang["MsgErrorFechas"] = "No ha sido posible actualizar los datos. Por favor, vuelva a realizar las modificaciones";

// 	$lang["terms_and_conditions_titulo"] = getCustomerStrings("terms_and_conditions");
// 	$lang["terms_and_conditions_texto"] = getHotelStrings($hostel, "terms_and_conditions");

	$lang["texto_adjuntar_1"] = "Para adjuntar un fichero arrastralo aquí, o ";
	$lang["texto_adjuntar_2"] = "escoje uno aquí";
	$lang["MensajeNoExiste"] = "Atención, la Ficha de inscripción a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	
	$lang["finalizada_correctamente"] = "Su Ficha de Inscripci&oacute;n ha sido finalizada correctamente.";
	$lang["obligatorio"] = "Dato obligatorio";
	$lang["campos_obligatorios_pendientes"] = "Quedan datos obligatorios pendientes de introducir";
	$lang["OtrasConsideraciones"] = "Otras consideraciones";

?>