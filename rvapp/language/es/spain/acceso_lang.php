<?php
	$lang["Titol"]          = "Sistema de inscripción de Blah Blah";
	$lang["TitolWeb"] 				= "Sistema de inscripción de Blah Blah";
	$lang["atencion_no_reserva"]    = "	<center>
											<h1>Informacion importante:</h1>
										</center> 
										<h2>Está a punto de entrar en la Ficha de inscripción de Blah Blah Student Travel Camps.</h2>
										En esta ficha de Inscripción podrá ampliar la información sobre su hij@ y así podremos ofrecerle un mejor servicio. Por favor no olvide detallar los datos sanitarios, alergias, etc.<br/>
										<br/>
										Una vez completada la ficha de inscripción, si  desea ampliar algún dato: nueva enfermedad, uso de medicamentos, cualquier otra información que usted considere importante, por favor envíenos un email a <a href='mailto:info@blahblah.es'>info@blahblah.es</a>, indicando<br/>
										<ul>
											<li>el localizador de la reserva</li>
											<li>el nombre del niñ@ o joven</li>
											<li>fechas de entrada y salida del Campamento</li>
											<li>la información adicional que desee incluir en la Ficha de Inscripción</li>
										</ul>
										Esta ficha de inscripción no da derecho alguno a plaza si no se han realizado todos los pagos correspondientes.<br/>
										<br/>";
	$lang["atencion_no_reserva2"]   = "";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localizador";
	$lang["centro"] = "Centro";
	$lang["programa"] ="Programa";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ej: 00000000A";
	$lang["BtnSubmit"] = "Acceder al sistema";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificacivos.<br />Por favor intentelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
	$lang["titulo_acceso"] = "Acceso a la inscripción";
	$lang["Texto_aplazado1"] = "El formulario de inscripción a los cursos de idiomas estará disponible a partir de la fecha: ";
	$lang["Texto_aplazado2"] = ", por favor vuelva pasada esta fecha para completar el proceso de inscripción de sus hijos/as";
	$lang["finaliza_correctamente"] = "Su inscripcion ha finalizado.";
	$lang["finaliza_despedida"] = "Gracias por su atención, y Muchas gracias por confiar en Rosa dels Vents.";
	$lang["titulo_info"] = "Importante.";
	$lang["test_nom"] = "Nombre y apellidos del participante";
	$lang["finaliza_test_correctamente"] = "Ha finalizado la prueba.";
	$lang["Titol_test"] = "Acceso a la prueba de nivel de inglés.";
	$lang["MensajeFinalizado"] = "<div style=\"font-size:175%;\">Su reserva ha finalizado.<br/><br/>Gracias por su atención y muchas gracias por confiar en Blah Blah.</div>";
	$lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
	$lang["MensajeNoExiste"]  = "<h2 style='text-align:center'>Atención,</h2> La ficha a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	$lang["MensajeNoExisteP"] = "<h2 style='text-align:center'>Atención,</h2> La Prueba de Nivel a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso.";
	$lang["MensajeYaFinalizado"]  = "<h2 style='text-align:center'>Atención,</h2> La Ficha de inscripción a la que intenta acceder ya ha sido finalizada con anterioridad. Por favor, si necesita hacer alguna modificación llame al Servicio de Comercial de Blah Blah (tel. 914 681 383).<br/><br/><br/>Muchas Gracias!";
	$lang["MensajeYaFinalizadoP"] = "<h2 style='text-align:center'>Atención,</h2> La prueba de Nivel de Inglés a la que intenta acceder, ya ha sido finalizada<br/><br/>Si necesita realizar alguna modificación o completar la información sobre el nivel de Ingles de su hij@ por favor envíenos un email a <a href='mailto:info@blahblah.es'>info@blahblah.es</a>, indicando:<br/><ul><li>el localizador de la reserva</li><li>el nombre del niñ@ o joven</li><li>fechas de entrada y salida del Campamento</li><li>la información adicional que desee incluir  en la Prueba del nivel de Inglés</li></ul><br/><br/><br/>Muchas gracias!";
	$lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder al test de nivel.<br/>El período para realizar el test de nivel es desde el día de la<br/>reserva hasta 15 días antes del inicio del curso.<br/>Los participantes que no hayan podido hacer el test de nivel on-line<br/>lo harán en la escuela el primer día.";
	$lang["MensajeParametrosInsuficientes"]  = "<h2 style='text-align:center'>Atención,</h2> No ha sido posible acceder a la aplicación. Por favor, vuelva a acceder a su reserva.";
	$lang["MensajeParametrosInsuficientesP"] = "<h2 style='text-align:center'>Atención,</h2> No ha sido posible acceder a la aplicación. Por favor, vuelva a acceder a su reserva.";
		
	$lang["MensajeDe1En1"] = "Atención: Ya tiene una <u>Ficha de inscripción</u> o <u>Prueba de nivel</u> abierta en este navegador. <br/><br/>Por favor, finalicela antes de continuar con las siguientes.<br/><br/>Si la ha cerrado y continua saliendo este mensaje, por favor, cierre este navegador y vuelva a entrar.<br/><br/>Disculpe las molestias.";
?>