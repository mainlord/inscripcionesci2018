<?php
    // $lang[""] ="";

    $lang["TitolWeb"] = "Encuesta de validación de nivel de Inglés";
    $lang["Titol"] = "Encuesta de validación<br/>de nivel de Inglés";
    $lang["NReserva"] = "Número Reserva";
    $lang["Localitzador"] = "Localizador";
    $lang["Dni"] = "DNI";
    $lang["Participant"] = "Participante";
    
    $lang["H_Titulo"] = "Level Test";
    $lang["H3_Enunciado"] = "Select the correct answer. Only ONE answer is correct";
    
    $lang["boton"] = "Enviar la Encuesta";
    $lang["AlertaEnviament"] = "Esta seguro que quiere enviar la encuesta? una vez enviada no podrá volver a entrar a la encuesta.";
    $lang["AlertaFaltanCampos"] = "Para poder continuar has de responder todas las preguntas";
    
    $lang["True"] = "Sí";
    $lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;
    
    $lang["Idioma"] = "Idioma";
    $lang["Idioma1"] = "Catalán";
    $lang["Idioma2"] = "Castellano";
    
    $lang["H_Info"] = "Información adicional";
    $lang["Info_Descripcion"] = "A continuación podréis responder a las cuestiones de la siguiente prueba de nivel. El motivo del cuestionario es separar  a los participantes en tres niveles de inglés: Básico, Intermedio y Alto. <br /><br /><b style='color:firebrick;'>ATENCI&Oacute;N: El test no se puede repetir, por favor, meditad bien las respuestas.</b><br /><br />En el caso de que no desee contestar las preguntas y quedar inscrito en el nivel básico clicar aquí: ";
    $lang["NSNC"] = "Deseo pertenecer al nivel Básico";
    $lang["MensajeNoExiste"] = "Atención, la Prueba de nivel a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";

    $lang["Aciertos"] = "<br/><br/>¡Ha acertado <b>{respuestas}/30 respuestas!</b>";
    $lang["AciertosNSNC"] = "<br/><br/>He decidido pertenecer al <b>nivel Básico</b>";
    $lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
    $lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder al test de nivel.<br/>El período para realizar el test de nivel es desde el día de la<br/>reserva hasta 15 días antes del inicio del curso.<br/>Los participantes que no hayan podido hacer el test de nivel on-line<br/>lo harán en la escuela el primer día.";
    $lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificativos.<br />Por favor inténtelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
    $lang["AlreadyDone"] = "Prueba de nivel realizada con anterioridad.";

    $lang["Splitter"]   ="|@|";
    $lang["npreguntas"] = 30;
    $lang["Textaxo"] =  "Choose the correct sentence".$lang["Splitter"].
                        "There am a pencil on the desk".$lang["Splitter"].
                        "There is a pencil on the desk".$lang["Splitter"].
                        "There be a pencil on the desk".$lang["Splitter"].
                        "There are a pencil on the desk".$lang["Splitter"].

                        "Choose the correct sentence".$lang["Splitter"].
                        "He do like oranges".$lang["Splitter"].
                        "He like oranges".$lang["Splitter"].
                        "He likes oranges".$lang["Splitter"].
                        "He does like oranges".$lang["Splitter"].

                        "Choose the correct sentence".$lang["Splitter"].
                        "There isn't no Coca Cola in the fridge".$lang["Splitter"].
                        "There is any Coca Cola in the fridge".$lang["Splitter"].
                        "There isn't any Coca Cola in the fridge".$lang["Splitter"].
                        "There aren't Coca Cola in the fridge".$lang["Splitter"].

                        "Do you like going shopping?".$lang["Splitter"].
                        "Yes, I do ".$lang["Splitter"].
                        "Yes, I am".$lang["Splitter"].
                        "Yes, I like".$lang["Splitter"].
                        "Yes, I go".$lang["Splitter"].

                        "Choose the correct sentence".$lang["Splitter"].
                        "Always he wakes up at 9am".$lang["Splitter"].
                        "He wakes up always at 9am".$lang["Splitter"].
                        "He always wakes up at 9am".$lang["Splitter"].
                        "He wakes up at 9am always".$lang["Splitter"].

                        "My birthday is....".$lang["Splitter"].
                        "in December".$lang["Splitter"].
                        "at December".$lang["Splitter"].
                        "on December".$lang["Splitter"].
                        "by December".$lang["Splitter"].

                        "I'm going to call my Mum I need to talk to....".$lang["Splitter"].
                        "it".$lang["Splitter"].
                        "him".$lang["Splitter"].
                        "her".$lang["Splitter"].
                        "you".$lang["Splitter"].

                        "Are these.... bags? You had them yesterday".$lang["Splitter"].
                        "your".$lang["Splitter"].
                        "yours".$lang["Splitter"].
                        "you're".$lang["Splitter"].
                        "its".$lang["Splitter"].

                        "This is Elena. She.... with my brother in Valencia".$lang["Splitter"].
                        "stay ".$lang["Splitter"].
                        "is staying ".$lang["Splitter"].
                        "stays ".$lang["Splitter"].
                        "staying ".$lang["Splitter"].

                        "There.... only three things I had to remember yesterday".$lang["Splitter"].
                        "is".$lang["Splitter"].
                        "are".$lang["Splitter"].
                        "was".$lang["Splitter"].
                        "were".$lang["Splitter"].

                        "We.... about your party. We had problems with our phone and email.".$lang["Splitter"].
                        "didn't know ".$lang["Splitter"].
                        "didn't knew ".$lang["Splitter"].
                        "haven’t known ".$lang["Splitter"].
                        "knew not ".$lang["Splitter"].

                        "Did you.... anywhere interesting last weekend? ".$lang["Splitter"].
                        "go".$lang["Splitter"].
                        "going".$lang["Splitter"].
                        "was".$lang["Splitter"].
                        "went".$lang["Splitter"].

                        "Where.... on holiday this year? To Menorca; we’ve got a house there. ".$lang["Splitter"].
                        "are you going ".$lang["Splitter"].
                        "do you go".$lang["Splitter"].
                        "does you go ".$lang["Splitter"].
                        "you going ".$lang["Splitter"].

                        "I've lost my keys. I can't find them.... ".$lang["Splitter"].
                        "anywhere".$lang["Splitter"].
                        "nowhere".$lang["Splitter"].
                        "nothing".$lang["Splitter"].
                        "somewhere".$lang["Splitter"].

                        "I haven't had lunch there.... a year. ".$lang["Splitter"].
                        "for".$lang["Splitter"].
                        "since".$lang["Splitter"].
                        "during".$lang["Splitter"].
                        "at".$lang["Splitter"].

                        "The kitchen can't be dirty he.... ".$lang["Splitter"].
                        "is just clean it.".$lang["Splitter"].
                        "have just cleaned it. ".$lang["Splitter"].
                        "just clean it. ".$lang["Splitter"].
                        "has just cleaned it. ".$lang["Splitter"].

                        "Simon.... in Madrid since 1982. ".$lang["Splitter"].
                        "lives ".$lang["Splitter"].
                        "is living ".$lang["Splitter"].
                        "have lived".$lang["Splitter"].
                        "has lived ".$lang["Splitter"].

                        "If I won the lottery, I.... a Ferrari. ".$lang["Splitter"].
                        "would buy".$lang["Splitter"].
                        "have bought ".$lang["Splitter"].
                        "will buy ".$lang["Splitter"].
                        "would have bought. ".$lang["Splitter"].

                        "Have you done your homework? Yes, I've.... done it.".$lang["Splitter"].
                        "still".$lang["Splitter"].
                        "already".$lang["Splitter"].
                        "yet".$lang["Splitter"].
                        "now".$lang["Splitter"].

                        ".... have you been studying English? ".$lang["Splitter"].
                        "How long ".$lang["Splitter"].
                        "What time".$lang["Splitter"].
                        "How far ".$lang["Splitter"].
                        "When".$lang["Splitter"].

                        "At my school we.... wear a uniform".$lang["Splitter"].
                        "have to".$lang["Splitter"].
                        "must to".$lang["Splitter"].
                        "would".$lang["Splitter"].
                        "might".$lang["Splitter"].

                        "This is the girl.... I saw yesterday. ".$lang["Splitter"].
                        "which".$lang["Splitter"].
                        "where".$lang["Splitter"].
                        "what".$lang["Splitter"].
                        "who".$lang["Splitter"].

                        "I.... working at night nowadays.".$lang["Splitter"].
                        "used to".$lang["Splitter"].
                        "used ".$lang["Splitter"].
                        "am used to".$lang["Splitter"].
                        "would	".$lang["Splitter"].

                        "She.... go to the dentist's yesterday.".$lang["Splitter"].
                        "must".$lang["Splitter"].
                        "had to ".$lang["Splitter"].
                        "ought to".$lang["Splitter"].
                        "should have ".$lang["Splitter"].

                        "I wouldn't mind.... tonight. ".$lang["Splitter"].
                        "to go out ".$lang["Splitter"].
                        "go out ".$lang["Splitter"].
                        "going out ".$lang["Splitter"].
                        "to going out ".$lang["Splitter"].

                        "Don't forget.... those letters. ".$lang["Splitter"].
                        "to post".$lang["Splitter"].
                        "posting".$lang["Splitter"].
                        "to posting".$lang["Splitter"].
                        "post".$lang["Splitter"].

                        "Where are my glasses? I had them a minute ago so I.... left them at school.".$lang["Splitter"].
                        "can't".$lang["Splitter"].
                        "must have".$lang["Splitter"].
                        "have to".$lang["Splitter"].
                        "can't have".$lang["Splitter"].

                        "You look tired. You.... go to bed.".$lang["Splitter"].
                        "need".$lang["Splitter"].
                        "have".$lang["Splitter"].
                        "should have ".$lang["Splitter"].
                        "ought to ".$lang["Splitter"].

                        "That was a great match. I'll never forget.... Rivaldo score that goal. ".$lang["Splitter"].
                        "see".$lang["Splitter"].
                        "to see".$lang["Splitter"].
                        "seeing".$lang["Splitter"].
                        "to seeing ".$lang["Splitter"].

                        "If I.... you, I'd take the risk. ".$lang["Splitter"].
                        "am".$lang["Splitter"].
                        "have been".$lang["Splitter"].
                        "were".$lang["Splitter"].
                        "would be ";
?>