<?php
	$lang["TitolWeb"] = "Acceso a la ficha de inscripción.";
	$lang["Titol"] = "Acceso a la ficha de inscripción.";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Referencia";
	$lang["centro"] = "Centro";
	$lang["programa"] ="Programa";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ej: 00000000A";
	$lang["BtnSubmit"] = "Acceder al sistema";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificacivos.<br />Por favor intentelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
	$lang["titulo_acceso"] = "Acceso a la inscripción";
	$lang["Texto_aplazado1"] = "El formulario de inscripción a Blah Blah estará disponible a partir de la fecha: ";
	$lang["Texto_aplazado2"] = ", por favor vuelva pasada esta fecha para completar el proceso de inscripción de sus hijos/as";
	$lang["finaliza_correctamente"] = "Ha finalizado la inscripción.";
	$lang["finaliza_despedida"] = "Muchas gracias por su atención.";
	//$lang["atencion_no_reserva"] = "<center><h1>Información Importate:<br><br>Está a punto de entrar en la página de inscripción de Blah Blah.<br><br></h1><h3></center><i><u>Esta página no és un formulario de reserva</i></u> ni da derecho a ella sin su correspondiente pago. És un formulario de inscripción para ampliar la información de los participantes y ofrecerle un mejor servicio de la reserva que ya ha efectuado.<br><br>Si quiere modificar cualquier aspecto de la reserva como el número de participantes, cambiar las fechas, el centro... Debe ponerse en contacto con el departamento comercial de Blah Blah (91 468 1383 o <a href='mailto:info@blahblah.es'>info@blahblah.es</a>). Tenga a mano el localizador de la reserva y su DNI.<br><br><center>¡Gracias por confiar en Blah Blah!</center><br><br><center>Haga click en el botón para inscribir a los participantes de la reserva que acaba de efectuar<input type='submit' class='input' value='Continuar'></center></h3>";
	$lang["atencion_no_reserva"] = "
			<center>
				<h1>Información Importante:</h1>
				<h2>
					Está a punto de entrar en la Ficha de Inscripción<br/>
					de Blah Blah Student Travel Camps.
				</h2>
			</center> 
			En esta Ficha de Inscripción podrá ampliar la información sobre su hij@ y así podremos ofrecerle un mejor servicio.<br/>
			<br/>
			Por favor no olvide detallar los datos sanitarios, alergias, etc. <br/>
			<br/>
			Una vez completada la Ficha de Inscripción, si desea ampliar algún dato: nueva enfermedad, uso de medicamentos, cualquier otra información que usted considere importante, por favor envíenos un email a  <a href='mailto:info@blahblah.es'>info@blahblah.es</a>, indicando:<br/>
			<ul>
				<li>Número de la reserva</li>
				<li>Nombre del participante</li>
				<li>Fechas de entrada y salida</li>
				<li>Información adicional que desee incluir en la Ficha de Inscripción</li>
			</ul>
			Esta Ficha de Inscripción no da derecho alguno a plaza si no se han realizado todos los pagos correspondientes.<br/>
		<br/>";
	$lang["titulo_info"] = "Importante.";
	$lang["test_nom"] = "Nombre y apellidos del participante";
	$lang["finaliza_test_correctamente"] = "Ha finalizado la prueba.";
	$lang["Titol_test"] = "Acceso a la prueba de nivel de inglés.";
	$lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
	$lang["MensajeNoExiste"] = "Atención, la Ficha de inscripción a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	// $lang["MensajeYaFinalizado"] = "Atención, la Ficha de inscripción a la que intenta acceder ya ha sido finalizada con anterioridad. Por favor, si necesita hacer alguna modificación llame al Servicio Comercial de Blah Blah.";
	$lang["MensajeYaFinalizado"] = "La Ficha de Inscripción a la que intenta acceder ya ha sido finalizada anteriormente.<br/><br/>Si necesita hacer alguna modificación, le agradeceremos nos envié un email al Servicio de Atención al cliente de Blah Blah Student Travel Camps a: <a href=\"email:info@blahblah.es\">info@blahblah.es</a>";
	$lang["MensajeNoExisteP"] = "Atención, la Prueba de nivel a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	// $lang["MensajeYaFinalizadoP"] = "Atención, la Prueba de nivel a la que intenta acceder ya ha sido finalizada con anterioridad. Por favor, si necesita hacer alguna modificación llame al Servicio Comercial de Blah Blah.";
	$lang["MensajeYaFinalizadoP"] = "La Prueba de inglés a la que intenta acceder ya ha sido finalizada anteriormente.<br/><br/>Si necesita hacer alguna modificación, le agradeceremos nos envié un email al Servicio de Atención al cliente de Blah Blah Student Travel Camps a: <a href=\"email:info@blahblah.es\">info@blahblah.es</a>";
	$lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder ni a la Ficha de Inscripción ni al Test de Nivel.<br/><br/>El período para realizar estos tramites finaliza 30 días después de formalizar la reserva.<br/><br/>Los participantes que no hayan podido hacer el test de nivel on-line lo harán en la escuela el primer día.";
	$lang["atencion_no_reserva2"]   = "";
	$lang["MensajeFinalizado"] = "<div style=\"font-size:175%;\">Su reserva ha finalizado.<br/><br/>Gracias por su atención y muchas gracias por confiar en Blah Blah.</div>";
	$lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder ni a la Ficha de Inscripción ni al Test de Nivel.<br/><br/>El período para realizar estos tramites finaliza 30 días después de formalizar la reserva.<br/><br/>Los participantes que no hayan podido hacer el test de nivel on-line lo harán en la escuela el primer día.";
	$lang["MensajeParametrosInsuficientes"]  = "<h2 style='text-align:center'>Atención,</h2> No ha sido posible acceder a la aplicación. Por favor, vuelva a acceder a su reserva.";
	$lang["MensajeParametrosInsuficientesP"] = "<h2 style='text-align:center'>Atención,</h2> No ha sido posible acceder a la aplicación. Por favor, vuelva a acceder a su reserva.";
	$lang["MensajeDe1En1"] = "Atención: Ya tiene una <u>Ficha de inscripción</u> o <u>Prueba de nivel</u> abierta en este navegador. <br/><br/>Por favor, finalicela antes de continuar con las siguientes.<br/><br/>Si la ha cerrado y continua saliendo este mensaje, por favor, cierre este navegador y vuelva a entrar.<br/><br/>Disculpe las molestias.";
?>