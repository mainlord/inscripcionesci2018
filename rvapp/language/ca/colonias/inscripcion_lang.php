<?php

	$lang["AclaracionFoto"] ="* La foto no serà visible fins que no hagi Salvat les dades.";
	$lang["texto_caja_email"]= "Posi aquí el seu email si desitja rebre un correu electrònic amb la confirmació de la inscripció.";
	$lang["confirmacionBorrado"] = "Esta segur que vol esborrar aquesta fitxa?";
	$lang["AlertaValidacio"]="Tots els camps son obligatoris";
	$lang["AlertaFinNivell"]="Encara te fitxes de participants que no han realitzat la Prova de Nivell d'Angles.";
	$lang["AlertaFinalitzacio"] ="Esta segur que vol finalitzar la inscripció de la seva reserva?";

	// formateig de dates.
	$lang["enero"] = "Gener"; 
	$lang["febrero"] = "Febrer";
	$lang["marzo"] = "Març";
	$lang["abril"] = "Abril";
	$lang["mayo"] = "Maig";
	$lang["junio"] = "Juny";
	$lang["julio"] = "Juliol";
	$lang["agosto"] = "Agost";
	$lang["septiembre"] = "Setembre";
	$lang["octubre"] = "Octubre";
	$lang["noviembre"] = "Novembre";
	$lang["diciembre"] = "Desembre";
		
	$lang["a"] = "a";
	$lang["lunes"] = "Dilluns";
	$lang["martes"] = "Dimarts";
	$lang["miercoles"] = "Dimecres";
	$lang["jueves"] = "Dijous";
	$lang["viernes"] = "Divendres";
	$lang["sabado"] = "Dissabte";
	$lang["domingo"] = "Diumenge";
	$lang["de"] = "de";
	$lang["del"] = "del";
		
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";

	$lang["Enviado"] = "Enviat";
	$lang["Pendiente"] = "Pendent d'enviar";
	$lang["Estatus"] = "Estat";
	$lang["EnquestaPendent"] = "Pendent de la enquesta";
		
		// resta de la plana.
	$lang["TitolWeb"] = "Sistema d’inscripció a Colònies";
	$lang["Titol"] = "Full d'inscripció";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localitzador";
	$lang["Dni"] = "DNI";
	$lang["centro"] = "Centre";
	$lang["programa"] = "Programa";

	$lang["True"] = "Sí";
	$lang["False"] = "No";
	$lang["val_True"] = 1;
	$lang["val_False"] = 0;
		
	$lang["H_Resumen"] = "Resum de la Inscripció";

	$lang["H_SeccioDPersonals"] = "Dades personals del participant:";
	$lang["NomComplet"] = "Nom complert del participant NEN / NENA";
	$lang["Telefon"] = "Telèfon del responsable";
	$lang["Chico_chica"] = "Sexe del participant";
	$lang["chico"] = "Nen";
	$lang["chica"] = "Nena";
	$lang["Adreça"] = "Adreça";
	$lang["Poblacio"] = "Població";
	$lang["CP"] = "CP";
	$lang["DataNaixement"] = "Data de naixement (DD/MM/AAAA)";
	$lang["AltresTelefons"] = "Altres telèfons en cas d’urgència";
	$lang["Foto"] = "Foto del Nen/Nena";
		
	$lang["H_InformacioSanitaria"] = "Informació Sanitària";
	$lang["MalaltSovint"] = "Es posa malalt sovint?";
	$lang["MalaltSovintDetall"] = "Malalties més freqüents";
	$lang["PrenMedicament"] = "Pren algun medicament?";
	$lang["PrenMedicamentDetall"] = "Quin?";
	$lang["PrenMedicamentAdministracio"] = "Administració";
	$lang["SotaTractament"] = "Està sota algun tratament específic?";
	$lang["SotaTractamentDetall"] = "Quin?";
	$lang["Regim"] = "Està sota algun règim?";
	$lang["RegimDetall"] = "Quin?";
	$lang["DesordreAlimentari"] = "Pateix algun tipus de trastorn d'ordre alimentari?";
	$lang["DesordreAlimentariDetall"] = "Quin?";
	$lang["Operat"] = "Ha sigut intervingut quirúrgicament?";
	$lang["OperatDetall"] = "Especificar";
	/*
	$lang["Alergic"] = "És al•lèrgic?";
	$lang["AlergicDetall"] = "Especificar";
	*/
	$lang["Alergic"] = "Al•lèrgies:";
	$lang["Alergic_celiac"] = "Celiaquia?";
	$lang["Alergic_lactosa"] = "Lactosa?";
	$lang["Alergic_ou"] = "A l'ou?";
	$lang["Alergic_altres"] = "Altres?";
	$lang["Alergic_Detall"] = "Especificar (Animals, insectes...):";

	$lang["H_InteresGeneral"] = "Dades d'interès general";

	/* $lang["AltresMateixTipus"] = "Has participat en altres estades d'aquest tipus?"; */
	$lang["AltresMateixTipus"] = "És la primera vegada que participes en colònies d'estiu?";
	$lang["Nedar"] = "Saps Nedar?";
	$lang["PorAigua"] = "Tens por a l'aigua?";
	$lang["Bicicleta"] = "Saps anar en bicicleta?";
	$lang["AnglesForaEscola"] = "Estudies anglès fora de l'escola?";
	$lang["AnglesForaEscolaDetall"] = "Quin curs?";
	$lang["Vertigen"] = "Tens Vertigen?";
	$lang["DificultatSports"] = "Tens alguna dificultat per practicar algun esport?";
	$lang["DificultatSports_Quins"] = "Quin?";
		
		
	$lang["H_LPD"] = "Protecció de dades";
	$lang["LPD"] = "L'informem que totes les dades de caràcter personal a les que es refereix aquest formulari, fins i tot la informació sanitària, seran tractades amb l'objectiu de facilitar la gestió de les activitats d’educació en el lleure en les quals participen menors de 18 anys.<br /><br />L'interessat/da, o el seu representant legal, autoritzen expressament a Colònies RV SA, al tractament de les dades amb aquesta finalitat.<br /><br />Tanmateix, l'informem de la possibilitat d'exercir, en termes establerts en la Llei Orgànica de Protecció de Dades de Caràcter Personal (LOPD), els drets d'accés, rectificació, cancel•lació i oposició, adreçant-se a Colònies RV SA; C/ Diputació, núm. 238, entresol 3ª, Barcelona, on  se li facilitaran els impresos oficials oportuns.";

	$lang["H_AutoritzacioPersonal"] = "Autorització (per a menors de 18 anys)";
	$lang["Autoritzacio_senyor"] = "Senyor/a";
	$lang["Autoritzacio_dni"] = ", amb DNI";
	$lang["Autoritzacio_nom_fill"] = "autoritza al seu fill/a";
	$lang["Autoritzacio_del"] = "a assistir als campaments del";
	$lang["Autoritzacio_al"] = "al";
	$lang["Autoritzacio_casa_colonies"] = "a la casa de colònies";
	$lang["Autoritzacio_poblacio_casa_colonies"] = "de";
	$lang["Autoritzacio_compromis"] = "Faig extensiva aquesta autorització a les decisions medicoquirúrgiques  que fossin necessàries adoptar en cas d'extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu fill/a sigui traslladat de visita al centre de salut més proper.<br/><br/>Durant tota l'estada els nostres monitors fan fotografies i / o vídeos dels nens i nenes mentre realitzen les colònies amb la finalitat de posar-les en el <strong>Punt de Trobada</strong> que hi ha per a cada programa. Així podeu anar seguint totes les activitats que realitzen els vostres fills diàriament.<br/><br/>Tot el material audiovisual resultant s'elimina dels nostres servidors i no s'empra per a cap altre finalitat diferent de la descrita anteriorment.";
	$lang["Autoritzacio_compromis_6"] = "Faig extensiva aquesta autorització a les decisions medicoquirúrgiques  que fossin necessàries adoptar en cas d'extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu fill/a sigui traslladat de visita al centre de salut més proper.";
	$lang["Autoritzacio_acceptacio"] = "Marca la casella conforme acceptes els termes explicats en aquest document.";
	$lang["Autoritzacio_poblacio"] = "Accepto en (població)";

	$lang["H_Acceptacio_Normativa"] = "Acceptació de la Normativa de Funcionament";
	$lang["Normativa_autoritzacio"] = "Marca la casella conforme acceptes els termes explicats en la Normativa.";
	$lang["Normativa_acceptacio"] = "He llegit i Accepto les condicions detallades a la Normativa de funcionament.";
	$lang["Normativa_llegir"] = "Llegir les";
	$lang["Normativa_texte_enllac"] = "condicions";
	$lang["Normativa_enllac_pdf"] = "./pdf/normativa_ca.pdf";
		
	$lang["H_Botonera"] = "Formalització de la inscripció";
	$lang["Botonera"] = "Un cop introduïdes les dades premi el botó adient per continuar amb el procés d’inscripció.";
	$lang["SyC"] = "Salvar les dades del participant";
	$lang["NEW"] = "Introduir un nou Participant sense Salvar";
	$lang["UPD"] = "Actualitzar el Participant actual";
	$lang["END"] = "Posar fi a la Inscripció";

	/*variables introducidas por Juanma*/
	$lang["titulo_hipica"] = "Enquesta H&iacute;pica";
	$lang["cabecera_hipica"] = "Dades d&#39inter&egrave;s per l&#39h&iacute;pica:";
	$lang["q_1_hipica"] = "Has muntat a cavall anteriorment?";
	$lang["hipica1_radio1"] = "No.";
	$lang["hipica1_radio2"] = "Si, una experi&egrave;cia positiva.";
	$lang["hipica1_radio3"] = "Si, una experi&egrave;cia negativa.";
	$lang["comentario_hipica"] = "En cas d&#39haver marcat la primera opci&oacute;, no cal que responguis les seg&uuml;ents q&uuml;estions.";
	$lang["q_2_hipica"] = "Quantes vegades has muntat, aproximadament?";
	$lang["hipica2_radio1"] = "Entre 1 i 5 vegades.";
	$lang["hipica2_radio2"] = "Entre 5 i 10 vegades.";
	$lang["hipica2_radio3"] = "M&eacute;s de 10 vegades.";
	$lang["q_3_hipica"] = "Has trotat alguna vegada?";
	$lang["hipica3_radio1"] = "No, mai.";
	$lang["hipica3_radio2"] = "He trotat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica3_radio3"] = "He trotat, i s&eacute; fer el trot aixecat o angl&egrave;s";
	$lang["q_4_hipica"] = "Has galopat alguna vegada?";
	$lang["hipica4_radio1"] = "No, mai.";
	$lang["hipica4_radio2"] = "He galopat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica4_radio3"] = "He galopat, i s&eacute; fer el moviment correcte.";
	$lang["comentario_observaciones_hipica"] = "Si creus que hi ha alguna dada m&eacute;s (classes rebudes, acc&eacute;s a cavalls propis o d&#39amics,etc...), fes-ho constar en l&#39apartat d&#39observacions.)";
	$lang["observaciones_hipica"] = "Observacions: especifiqueu en aquest apartat si sou repetidors de programa d'equitació.";

	$lang["titulo_grupo7"] = "Autoritzaci&oacute; per paintball, motos i quad.";
	$lang["autorizacion_grupo7"] = "Autoritzo al meu fill/a a practicar les activitats de paintball, motos i quad durant la seva estada a les col&ograve;nies de Rosa dels Vents.";
	$lang["exp_grupo7"] = "Marca la casella conforme autoritzes al teu fill/a.";
	/*Fin variables Juanma*/	


	$lang["EnlaceBorrar"] = "X";

	$lang["ObsGeneral"] = "Altres aspectes a tenir en compte (insomni, sonambulisme...):";

	$lang["H_Mallorca"] = "DNI obligatori";
	$lang["exp_mallorca"] = "Per a aquest programa es requisit indispensable posar el DNI del participant. Aquest DNI no pot estar caducat.";

	$lang["H_futbol"] = "Campus de Futbol";
	$lang["exp_futbol"] = "Escull l'equipament que vols lluir al curs de futbol";
	$lang["Portero"] = "Necessitaré equipament de Porter";
	$lang["Jugador"] = "Necessitaré equipament de Jugador de camp";

	$lang["H_Informacio"] = "Com complimentar aquest formulari";
	$lang["Informacio_Presentacion"] = "Aquest petit text esta destinat a ajudar-lo a complimentar les dades d'aquest formulari.";
	$lang["Informacio_Basica"] = "El primer a tenir en compte es que s'ha d'omplir un formulari per cada un dels participants de la reserva.".
							"Per a introduir aquestes dades serà necessari omplir tots els camps que trobarà en el formulari que te davant seu.<br/><br/>".
							"Els únics camps que podrà deixar buits, son els camps d'Observacions generals.<br/><br/>".
							"Un cop introduïdes les dades al formulari haurà de prémer el boto <b>".$lang["SyC"]."</b>.<br/><br/>".
							"Un cop Salvada la informació de la seva primera fitxa de participant, apareixerà un quadre en la part superior de la plana amb el llistat dels participants introduïts. Si ha de introduir mes fitxes podrà continuar introduint les dades en el formulari que haurà quedat sota la caixa de <b>".$lang["H_Resumen"]."</b>.<br/>";
	$lang["Informacio_Update"] = "En cas que volgués modificar la informació introduïda, podrà fer-ho clickant sobre la foto o el nom dels participants que trobarà a la caixa de <b>".$lang["H_Resumen"]."</b><br/>";
	$lang["Informacio_EncuestasPendientes"] = "Com pot comprovar a la caixa de <b>".$lang["H_Resumen"]."</b> te fitxes pendents de finalitzar la <b>Prova de Nivell d'Angles</b>. Per accedir-hi nomes ha de fer click sobre el text <b>".$lang["EnquestaPendent"]."</b><br />";
	$lang["Informacio_BtnFinalizar"] = "Si no ha d'introduir cap fitxa mes i no te cap alerta pendent, ja pot prémer el botó de <b>".$lang["END"]."</b> per finalitzar el procés.<br/>";

	$lang["H_ErrorFechas"] = "Error de Dades";
	$lang["MsgErrorFechas"] = "No ha sigut possible actualitzar les dades. Si us plau, torni a realitzar els canvis.";
		
	$lang["texto_adjuntar_1"] = "Per adjuntar un fitxer arrosegal aquí, o ";
	$lang["texto_adjuntar_2"] = "escull una aquí";
	$lang["MensajeNoExiste"] = "Atenció, la Fitxa d'inscripció a la que intenta accedir ha deixat d'existir. Torni a accedir a la seva reserva per continuar amb el proces d'inscripció.";	
	$lang["finalizada_correctamente"] = "La Fitxa d'Inscripci&oacute; ha sigut finalitzada correctament.";
	$lang["obligatorio"] = "Dada obligatoria";
	$lang["campos_obligatorios_pendientes"] = "Queden dades obligatòries pendents d'introduir";
	
	$lang["FotoDniDelantera"] 	= "Foto DNI devant";
	$lang["FotoDniTrasera"] 	= "Foto DNI darrera";
	$lang["FotosCartillaVacunas"] = "Llibre de vacunacions";
	$lang["no_cvac"] 			= "Encara no ha adjuntat cap imatge del llibre de vacunacions.";
	$lang["OtrasConsideraciones"] = "Altres consideracions";

	// a partir de aqui son los textos de la pagina de dnis para vuelos
	$lang["Titol_vuelo"] = "Dades dels participants";
	$lang["Participant_N"] = "Participant nº ";
	$lang["Nom"] = "Nom";
	$lang["Cognom1"] = "Primer cognom";
	$lang["Cognom2"] = "Segón cognom";
	$lang["ValidarInformacio"] = "Validar la informació dels participants";
	$lang["Validado"] = "Ja has introduit tota la informació necessaria. Ara ja pots enviar el formulari, Gracies!";
	$lang["taf_finalizada_correctamente"] = "L'informaci&oacute; ha sigut enregistrada correctament.";

	$lang["covid19_titol_declaracio"] 				= "Declaració responsable en relació a la situació de pandèmia generada per la Covid-19";
	$lang["covid19_texte_declaracio_introduccio1"]	= "Els nous protocols de la Direcció General de Joventut de la Generalitat inclouen una declaració responsable que han de signar tots els pares/mares i /o tutors de nens/es inscrits en estades de colònies, acampades, casals, campus esportius...<br/><br/>En aquesta declaració signen que són coneixedors del context de pandèmia actual i de què han estat informats i declaren quin és l’estat de salut del seu fill/a abans d’iniciar les colònies.";
	$lang["covid19_texte_declaracio_subtitol"] 		= "Declaro sota la meva responsabilitat:";
	$lang["covid19_texte_declaracio_li1_1"] 		= "Que sóc coneixedor/a del context de pandèmia actual provocada per la Covid-19 i que accepto les circumstàncies i riscos que aquesta situació pot comportar durant el	desenvolupament de l’activitat d’educació en el lleure en la que en/la ";
	$lang["covid19_texte_declaracio_li1_2"] 		= "participa. Així mateix, entenc que l'equip de dirigents i l'entitat organitzadora de l'activitat no són responsables de les contingències que puguin ocasionar-se en relació a la pandèmia durant l'activitat.";
	$lang["covid19_texte_declaracio_li2"]   		= "Que he sigut informat/da i estic d’acord amb les mesures de prevenció general i amb les actuacions necessàries que poden haver-se de dur a terme si apareix un cas d’un menor d’edat amb simptomatologia compatible amb la Covid-19 durant el desenvolupament de l’activitat.";
	$lang["covid19_texte_declaracio_li3_1"] 		= "Que m’encarrego d’aportar les quantitat necessària de mascaretes per	en/la ";
	$lang["covid19_texte_declaracio_li3_2"] 		= "pels dies que duri l’activitat, en cas que així ho requereixi l’equip de dirigents.";
	$lang["covid19_texte_declaracio_li4_1"] 		= "Que informaré a l’entitat organitzadora de qualsevol variació de l’estat de salut d’en/la";
	$lang["covid19_texte_declaracio_li4_2"] 		= "compatible amb la simptomatologia Covid-19 mentre duri l’activitat, així com de l’aparició de qualsevol cas de Covid-19 en el seu entorn familiar.";
	$lang["covid19_texte_declaracio_introduccio2_1"]= "Finalment, que amb caràcter previ a la realització de l’activitat, en/la";
	$lang["covid19_texte_declaracio_introduccio2_2"]= "compleix els requisits de salut següents:";
	$lang["covid19_texte_declaracio_li5"]   		= "Presenta absència de malaltia i simptomatologia compatible amb la Covid-19 (febre, tos, dificultat respiratòria, malestar, diarrea...) o amb qualsevol altre quadre infecciós.";
	$lang["covid19_texte_declaracio_li6"]   		= "No ha conviscut o no ha tingut contacte estret amb una persona positiva de Covid-19 confirmada o amb una persona que ha tingut simptomatologia compatible en els 14 dies anteriors a la realització de l’activitat.";
	$lang["covid19_texte_declaracio_opcional_1"]	= "Només marcar en cas de menor amb patologia crònica complexa considerada de risc per la Covid-19:";
	$lang["covid19_texte_declaracio_opcional_2"]	= "Que els serveis mèdics han valorat positivament i de manera individual la idoneïtat	de la seva participació en l’activitat.";
	$lang["covid19_texte_declaracio_accepto_1"] 	= "Accepta en/na ";
	$lang["covid19_texte_declaracio_accepto_2"] 	= " amb DNI ";
	$lang["covid19_texte_declaracio_accepto_3"] 	= " com a pare/mare o tutor/tutora a data ";
	$lang["covid19_texte_declaracio_accepto_4"] 	= " a ";
	$lang["covid19_texte_info_adicional"] 			= "En caso de que lo crea conveniente, cuéntenos cuál es el estado emocional de su hijo/a, en el sentido de cómo ha vivido estos meses de confinamiento, si han sufrido alguna pérdida en el entorno familiar y/o entorno de proximidad, si ha sido un periodo de estrés elevado… Cualquier información que considere relevante para poder hacer un mejor acompañamiento durante las colonias.";

?>	