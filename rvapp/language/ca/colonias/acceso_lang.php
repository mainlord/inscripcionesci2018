<?php
	$lang["TitolWeb"] = "Sistema d’inscripció a Colònies";
	$lang["Titol"]      = "Sistema d’inscripció a Colònies";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localitzador";
	$lang["centro"] = "Seleccione Centro";
	$lang["programa"] = "Seleccione Programa";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ej.: 00000000A";
	$lang["BtnSubmit"] = "Accedir al sistemaa";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["NoFound"] = "No s'ha trobat cap reserva amb aquestes dades identificatives.<br />Si us plau intenti-ho de nou en 24 hores<br />(Es possible que encara no hagi sigut processada pel sistema).";
	$lang["MensajeFinalizado"] = "La reserva ha finalitzat.<br/><br/>Gràcies per la seva atenció i <br/>Moltes gràcies per confiar en Rosa dels Vents.";
	$lang["finaliza_correctamente"] = "La seva inscripció ha sigut finalitzada amb exit.";
	$lang["finaliza_despedida"] = "Gracies per la seva atenció i confiar en Rosa dels Vents.";
		
	$lang["MensajeParametrosInsuficientes"] = "No ha estat possible accedir a la aplicació.<br/><br/>Si us plau, torni a posar les seves dades en el formulari d’accés.";
	$lang["MensajeNoExiste"] = "Atenció, la fitxa a la que intenta accedir ha deixat d'existir.<br/><br/>Si us plau, torni a posar les seves dades en el formulari d’accés.";
	$lang["MensajeYaFinalizado"] = "Atenció, la fitxa a la que intenta accedir ja ha sigut Finalitzada amb anterioritat.<br/><br/>Si us plau, si necesita fer alguna modificació truqui al Servei d'Atenció de Rosa dels Vents.";
	$lang["atencion_no_reserva"] = "Informació Important:<br>Està a punt d'entrar a la pàgina d'inscripció de colònies de Rosa dels Vents.";
	/*
	$lang["atencion_no_reserva2"] = "<i><u>Aquesta pàgina no és un formulari de reserva</i></u> ni dóna dret a ella sense el corresponent pagament. És un formulari d'inscripció per ampliar la informació dels participants i oferir-li un millor servei sobre la reserva que ja ha efectuat.<br><br>
										Si vol modificar qualsevol aspecte de la reserva com el nombre de participants, canviar les dates, centre... S'ha de posar en contacte amb el departament comercial de Rosa dels Vents (93 409 20 71 o <a href='http://www.rosadelsvents.es'>www.rosadelsvents.es</a>). Tingui a mà el localitzador de la reserva i el seu DNI.<br><br>
										<center>Gràcies per confiar en Rosa dels Vents!<br/>
										Premi el botó per inscriure els participants de la reserva que acaba d'efectuar</center>";
	*/
	$lang["atencion_no_reserva2"] = "<i><u>Aquesta pàgina no és un formulari de reserva</u></i> ni dóna dret a ella sense el corresponent pagament. És un formulari d&#39;inscripció per ampliar la informació dels participants i oferir-li un millor servei sobre la reserva que ja ha efectuat.<br/><br/>
										Abans de començar la inscripció és recomanable tenir a disposició <i><u>les dades sanitàries, la foto, la fotocòpia del carnet de vacunacions i la fotocòpia del DNI (en cas de que en tingui) del participant</u></i>.<br/><br/>
										Si vol modificar qualsevol aspecte de la reserva com el nombre de participants, canviar les dates, centre... S&#39;ha de posar en contacte amb el departament comercial de Rosa dels Vents (93 409 20 71 o <a href='http://www.rosadelsvents.es/es/'>www.rosadelsvents.es</a>). Tingui a mà el localitzador de la reserva i el seu DNI.<br/><br/>
										<center>Gràcies per confiar en Rosa dels Vents!<br/>
										Premi el botó per inscriure els participants de la reserva que acaba d&#39;efectuar</center>";
	$lang["titulo_info"] = "Important.";
	$lang["MensajeFinalizadoTest"] = "La prova de nivell ha sigut completada correctament.";
	$lang["MensajeTestOutDated"] = "Atenció, ja no és possible accedir al test de nivell.<br/>El període per realitzar el test de nivell és des del dia de la<br/>reserva fins a 15 dies abans de l'inici del curs.<br/>Els participants que no hagin pogut fer el test de nivell on-line<br/>el faran a l'escola el primer dia.";

	$lang["MensajeDe1En1"] = "Atenció: Ja te una <u>Fitxa d'inscripció</u> o <u>Prova de nivell</u> oberta en aquest navegador.<br/><br/>Si us plau, finalitzi-la abans de continuar amb les següents.<br/><br/>Si la ha tancat i continua veient aquest missatge, si us plau, tanqui aquest navegador i torni a entrar.<br/><br/>Disculpi les molesties.";
	$lang["Aciertos"] = "<br/><br/>Ha encertat <b>{respuestas}/30 respostes!</b>";
	$lang["AciertosNSNC"] = "<br/><br/>He decidit pertanyer al <b>nivell Bàsic</b>";

	$lang["MensajeNoExisteTaf"] = "Atenció, l’informació  a la que intenta accedir ha deixat d'existir.<br/><br/>Si us plau, torni a posar les seves dades en el formulari d’accés.";
	$lang["MensajeYaFinalizadoTaf"] = "Atenció, l’informació a la que intenta accedir ja ha sigut Finalitzada amb anterioritat.<br/><br/>Si us plau, si necesita fer alguna modificació truqui al Servei d'Atenció de Rosa dels Vents.";

?>