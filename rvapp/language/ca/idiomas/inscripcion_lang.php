<?php

	// formateig de dates.
	$lang["enero"] = "Gener"; 
	$lang["febrero"] = "Febrer";
	$lang["marzo"] = "Març";
	$lang["abril"] = "Abril";
	$lang["mayo"] = "Maig";
	$lang["junio"] = "Juny";
	$lang["julio"] = "Juliol";
	$lang["agosto"] = "Agost";
	$lang["septiembre"] = "Setembre";
	$lang["octubre"] = "Octubre";
	$lang["noviembre"] = "Novembre";
	$lang["diciembre"] = "Decembre";

	$lang["a"] = "a";
	$lang["lunes"] = "Dilluns";
	$lang["martes"] = "Dimarts";
	$lang["miercoles"] = "Dimecres";
	$lang["jueves"] = "Dijous";
	$lang["viernes"] = "Divendres";
	$lang["sabado"] = "Dissabte";
	$lang["domingo"] = "Diumenge";
	$lang["de"] = "de";
	$lang["del"] = "del";

	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellà";

	$lang["Enviado"] = "Enviat";
	$lang["Pendiente"] = "Pendent";

	// resta de la plana.
	$lang["TitolWeb"] = "Sistema d'inscripcio a Colonies";
	$lang["Titol"] = "Full d'inscripció";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localitzador";
	$lang["Dni"] = "DNI";
	$lang["centro"] = "Centre";
	$lang["programa"] = "Programa";

	$lang["True"] = "Sí";
	$lang["False"] = "No";
	$lang["val_True"] = 1;
	$lang["val_False"] = 0;

	$lang["H_Resumen"] = "Resum de la Inscripció";

	$lang["H_SeccioDPersonals"] = "Dades personals del participant:";
	$lang["NomComplet"] = "Nom i cognoms";
	$lang["Telefon"] = "Telèfon";
	$lang["Adreça"] = "Adreça";
	$lang["Poblacio"] = "Població";
	$lang["CP"] = "CP";
	$lang["DataNaixement"] = "Data de naixement";
	$lang["AltresTelefons"] = "Altres telèfons en cas d'urgencia";
	$lang["Foto"] = "Foto";
	$lang["Pasaporte"] = "Nº passaport";

	$lang["H_InformacioSanitaria"] = "Informació Sanitària";
	$lang["MalaltSovint"] = "Es posa malalt sovint?";
	$lang["MalaltSovintDetall"] = "Malalties més freqüents";
	$lang["PrenMedicament"] = "Pren algun medicament?";
	$lang["PrenMedicamentDetall"] = "Quin?";
	$lang["PrenMedicamentAdministracio"] = "Administració";
	$lang["Regim"] = "Està sota algun règim?";
	$lang["RegimDetall"] = "Quin?";
	$lang["Operat"] = "Ha sigut intervingut quirúrgicament?";
	$lang["OperatDetall"] = "Especificar";
	$lang["Alergic"] = "És al·lèrgic?";
	$lang["AlergicDetall"] = "Especificar";

	$lang["H_InteresGeneral"] = "Dades d'interès general";
	$lang["AltresMateixTipus"] = "Has participat en altres estades d'aquest tipus?";
	$lang["Nedar"] = "Saps Nedar?";
	$lang["PorAigua"] = "Tens por a l'aigua?";
	$lang["Bicicleta"] = "Saps anar en bicicleta?";
	$lang["AnglesForaEscola"] = "Estudies anglès fora de l'escola?";
	$lang["AnglesForaEscolaDetall"] = "Quin curs?";

	$lang["H_LPD"] = "Proteccio de dades";
	$lang["LPD"] = "L'informem que totes les dades de caràcter personal a les que es refereix aquest formulari, fin i tot la informació sanitària, seran tractades amb l'objectiu de facilitar la gestió de les activitats d'educacio en el lleure en les quals participen menors de 18 anys.<br /><br />L'interessat/da, o el seu representant legal, autoritzen expressament a Colònies RV SA, al tractament de les dades amb aquesta finalitat.<br /><br />Tanmateix, l'informem de la possibilitat d'exercir, en termes establerts en la Llei Orgànica de Protecció de Dades de Caràcter Personal (LOPD), els drets d'accés, rectificació, cancel·lació i oposició, adreçant-se a Colònies RV SA; C/ Diputació, núm. 238, entresol 3ª, Barcelona, on  se li facilitaran els impresos oficials oportuns.";

	$lang["H_AutoritzacioPersonal"] = "Autorització (per a menors de 18 anys)";
	$lang["Autoritzacio_senyor"] = "Senyor/a";
	$lang["Autoritzacio_dni"] = ", amb DNI";
	$lang["Autoritzacio_nom_fill"] = "autoritza al seu fill/a";
	$lang["Autoritzacio_del"] = "a assistir als campaments del";
	$lang["Autoritzacio_al"] = "al";
	$lang["Autoritzacio_casa_colonies"] = "a la casa de colònies";
	$lang["Autoritzacio_poblacio_casa_colonies"] = "de";
	$lang["Autoritzacio_compromis"] = "Faig extensiva aquesta autorització a les decisions medico-quirúrgiques que fossin necessàries adoptar en cas d'extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu fill/a sigui traslladat de visiata al centre de salut més proper.";
	$lang["Autoritzacio_acceptacio"] = "Marqueu la casella conforme accepten els termes explicats en aquest document.";
	$lang["Autoritzacio_poblacio"] = "Accepto en (població)";

	$lang["H_Acceptacio_Normativa"] = "Acceptacio de la Normativa de Funcionament";
	$lang["Normativa_autoritzacio"] = "Marqueu la casella conforme accepten els termes i condicions generals de la reserva.";
	$lang["Normativa_acceptacio"] = "He llegit i accepto els termes i condicions generals de la reserva.";
	$lang["Normativa_llegir"] = "Llegir les";
	$lang["Normativa_texte_enllac"] = "condicions";

	$lang["SyC"] = "Introduir fitxa per un altre participant";
	$lang["NEW"] = "Introduir un nou Participant desde 0";
	$lang["UPD"] = "Actualitzar el Participant actual";
	$lang["END"] = "Posar fi a l'Inscripció";

	/*variables introducidas por Juanma*/
	$lang["titulo_hipica"] = "- Enquesta H&iacute;pica";
	$lang["cabecera_hipica"] = "Dades d&#39inter&egrave;s per l&#39h&iacute;pica:";
	$lang["q_1_hipica"] = "Has muntat a cavall anteriorment?";
	$lang["hipica1_radio1"] = "-No.";
	$lang["hipica1_radio2"] = "-Si, una experi&egrave;cia positiva.";
	$lang["hipica1_radio3"] = "-Si, una experi&egrave;cia negativa.";
	$lang["comentario_hipica"] = "En cas d&#39haver marcat la primera opci&oacute;, no cal que responguis les seg&uuml;ents q&uuml;estions.";
	$lang["q_2_hipica"] = "Quantes vegades has muntat, aproximadament?";
	$lang["hipica2_radio1"] = "-Entre 1 i 5 vegades.";
	$lang["hipica2_radio2"] = "-Entre 5 i 10 vegades.";
	$lang["hipica2_radio3"] = "-M&eacute;s de 10 vegades.";
	$lang["q_3_hipica"] = "Has trotat alguna vegada?";
	$lang["hipica3_radio1"] = "-No, mai.";
	$lang["hipica3_radio2"] = "-He trotat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica3_radio3"] = "-He trotat, i s&eacute; fer el trot aixecat o angl&egrave;s";
	$lang["q_4_hipica"] = "Has galopat alguna vegada?";
	$lang["hipica4_radio1"] = "-No, mai.";
	$lang["hipica4_radio2"] = "-He galopat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica4_radio3"] = "-He galopat, i s&eacute; fer el moviment correcte.";
	$lang["comentario_observaciones_hipica"] = "Si creus que hi ha alguna dada m&eacute;s (classes rebudes, acc&eacute;s a cavalls propis o d&#39amics,etc...), fes-ho constar en l&#39apartat d&#39observacions.)";
	$lang["observaciones_hipica"] = "Observacions:";

	$lang["titulo_grupo7"] = "7 .- Autoritzaci&oacute; per paintball i quad.";
	$lang["autorizacion_grupo7"] = "Autorizo al meu fill/a a practicar les activitats de paintball i quad durant la seva estada a les col&ograve;nies de Rosa dels Vents.";
	$lang["exp_grupo7"] = "Marqui la casella conforme autoritza al teu fill/a.";

	$lang["titulo_datos_curso"] = "DADES DEL CURS CONTRACTAT";
	$lang["NomPrograma"] = "Nom del programa";
	$lang["Fechas"] = "Dates";
	$lang["Amigos_Familiares"] = "Té amics o familiars al mateix programa?";
	$lang["Nombre_amigo"] = "Nom i cognoms de l'amic o familiar";
	$lang["Ciudad_salida"] = "Ciutat de Sortida";
	$lang["titulo_datos_participante"] = "DADES DEL PARTICIPANT";
	// $lang["importante_pasaporte"] = "IMPORTANT: La còpia del passaport és molt important per a la formalització dels vols. Ens la pot enviar en format JPG, PNG y PDF<br/><br/>Si en aquest moment no disposa de la còpia del passaport, pot enviar-la abans de 15 dies a: '<a href:\"mailto://info@rosadelsventsidiomas.es\">info@rosadelsventsidiomas.es</a>'. ";
	$lang["importante_pasaporte"] = "IMPORTANT: La còpia del passaport és molt important per a la formalització dels vols. Ens la pot enviar en format JPG, PNG y PDF<br/><br/>Si en aquest moment no disposa de la còpia del passaport, <strong>pot enviar-la com a màxim fins el 31 de Maig</strong>, a: <a href='mailto:info@rosadelsventsidiomas.es' target='_blank'>info@rosadelsventsidiomas.es</a>. ";

	$lang["importante_nombres"] = "Preguem comprovi que el nom, cognoms i data de naixement del participant coincideixen amb les dades del passaport. En cas contrari, enviar un email amb les dades correctes a: <a href=\"info@rosadelsventsidiomas.es\">info@rosadelsventsidiomas.es</a>";
	$lang["Nom_Participante"] = "Nom de l'estudiant";
	$lang["Cognom_Participante"] = "Cognoms de l'estudiant";
	$lang["chico"] = "Noi";
	$lang["chica"] = "Noia";
	$lang["Direccion_Participante"] = "Adreça";
	$lang["Direccion_Padres"] = "Adreça";
	$lang["Codigo_postal"] = "Codi Postal";
	$lang["CP_Padres"] = "Codi Postal";
	$lang["Poblacion"] = "Població";
	$lang["Poblacion_Padres"] = "Població";
	$lang["Telefono"] = "Telèfon";
	$lang["Movil"] = "Mòbil de contacte Pare";
	$lang["Movil2"] = "Mòbil de contacte Mare";
	$lang["Fecha_nacimiento"] = "Data de Naixement de l'estudiant";
	$lang["Edad_Participante"] = "Edat de l'estudiant";
	$lang["copia_pasaporte"] = "Còpia Escanejada del Passaport";
	$lang["Nacionalidad"] = "Nacionalitat de l'estudiant";
	$lang["Email_Participantes"] = "E-mail de l'estudiant";
	$lang["Email_Padres"] = "E-mail de contacte";
	$lang["Movil_Participante"] = "Mòbil de l'estudiant";
	$lang["Movil_Padres"] = "Mòbil de contacte";

	$lang["Nombre_Padres"] = "Nom del pare, mare o tutor";
	$lang["DNI_Padres"] = "DNI";
	$lang["Algun_Curso"] = "És la primera vegada que realitza un curs d'idiomes a l'estranger?"; /* Has realitzat algun curs d'idiomes a l'estranger?"; */
	$lang["Algun_Curso_Detalle"] = "A on va anar?";
	$lang["Algun_Curso_Detalle2"] = "Amb quina organització?";
	$lang["Alguna_Enfermedad"] = "Té algun tipus de malaltia?";
	$lang["Alguna_Enfermedad_Detalle"] = "Especificar (TDH, TDHA, asma, diabetes, lesions, ...)";
	$lang["Alguna_alergia"] = "Té algun tipus de al·lèrgia (alimentaria, animals, medicaments)?";
	$lang["Alguna_alergia_Detalle"] = "Especificar (fruits secs, marisc, ous, gossos, gats, hàmsters, ...)";
	$lang["Dieta_Especial"] = "Necessita alguna dieta especial?";
	$lang["Dieta_Especial_Detalle"] = "Especificar (sense gluten, sense lactosa, vegetariana, vegana, ...)";
	$lang["Algun_Medicamento"] = "Necessita medicació durant l'estada?";
	$lang["Algun_Medicamento_Detalle"] = "Especificar qualsevol medicament que portin, ja sigui d'ús puntual o tractament";
	$lang["titulo_datos_academicos"] = "DADES ACADÈMIQUES";
	$lang["titulo_datos_contacto_pare"] = "DADES DE CONTACTE PARE / TUTOR";
	$lang["titulo_datos_contacto_mare"] = "DADES DE CONTACTE MARE / TUTORA";
	$lang["titulo_datos_contacto"] = "DADES DE CONTACTE";
	$lang["Colegio"] = "Escola o Institut al que assisteix el seu fill/a";
	$lang["Direccion_Colegio"] = "Adreça";
	$lang["CP_Colegio"] = "Codi Postal";
	$lang["Poblacion_Colegio"] = "Població";
	$lang["Nombre_Profesor"] = "Nom del professor d'idiomes";
	$lang["Curso"] = "Curs escolar";

	$lang["Nivel_Idioma"] = "Nivell de l'idioma";
	$lang["Bajo"] = "Baix";
	$lang["Medio"] = "Mig";
	$lang["Alto"] = "Alt";
	$lang["Bilingue"] = "Bilingüe";
	$lang["Colegio_miniestancia"] = "El col·legi organitza estades a l'estranger?";

	$lang["titulo_autorizacion_audiovisual"] = "AUTORITZACIÓ AUDIOVISUAL";
	$lang["autorizacion_audiovisual_padres"] = "Durant les estades a l'estranger, els nostres monitors fan fotografies i / o vídeos dels estudiants al llarg del curs, amb la finalitat de posar-les en el blogs que hi ha per a cada programa. Així podeu anar seguint totes les activitats que realitzen els vostres fills diàriament.";
	$lang["Nombre_autorizacion_audiovisual_padres"] = "Nom pare, mare o tutor";
	$lang["Dni_autorizacion_audiovisual_padres"] = "DNI";
	$lang["Autoritzacio_acceptacio_audiovisual"] = "AUTORITZO la utilització de qualsevol material fotogràfic i / o audiovisual en què aparegui el participant per a les promocions comercials i de màrqueting de la companyia (pàgina web, blogs i / o xarxes socials).";


	$lang["titulo_autorizacion_sin_monitor"] = "AUTORITZACIÓ DE SORTIDA SENSE MONITOR";
	$lang["para_15_17"] = "Només per a estudiants de 15 a 17 anys. Veure normes sortides sense monitor al document informació pràctica.";
/* 	$lang["autorizacion_sin_monitor_padres"] = "Els estudiants que participen als nostres cursos i que tinguin 13 o més anys, podran sortir sense monitor durant el seu temps lliure o abans o després de sopar. L'horari de retorn serà: les 21H00 per estades en Residència, les 21H30 per estades en Família i les 23H00 per als cursos de Malta. <u>Excepció:</u>  Els estudiants que participen als programes St Martins Ampleforth, Tipperary i Trowbridge <u>NO PODRAN SORTIR DE L'ALLOTJAMENT SOLS</u>. Veure normes específiques en el document <a target=\"_blank\" href=\"https://www.cursosidiomas.com/pdf/info-practica-ca.pdf\">d'Informació Práctica</a></br></br>Autoritzo al meu fill/a a sortir del seu allotjament durant els temps lliures estipulats. (En cas de no estar d'acord preguem ens ho comuniquin per escrit)."; */
	$lang["autorizacion_sin_monitor_padres"] = "<u>Per estades en Residència:</u> Tots el joves a partir de 13 anys estan autoritzats a sortir sols de la residencia sense els monitors durant els temps lliures i abans o després del sopar. L’hora màxima de retorn a la residència són les 21h00. Els joves que participen en el curs Ampleforth College no podran sortir de la residència ja que és impossible accedir a peu fins el centre urbà.<br/><br/><u>Per estades en Família:</u> Sempre i quan la família d'acollida ho permeti, tots els joves majors de 13 anys tenen permís per sortir sols sense monitor durant els temps lliures i abans o desprès de sopar. L’hora màxima de retorn a les famílies és a les 21h00.<br/>Els joves poden sortir per la zona on es troba la família, en cap cas la família està obligada a portar a l’estudiant al centre per trobar-se amb altres estudiants.<br/><u>Els estudiants podran sortir només  si la família ho permet, en alguns casos les famílies no deixen que els  estudiants surtin sols.</u><br/>Excepcionalment a Malta l’hora de retorn a la família és a les 22h00.<br/>Els joves que participen en el curs de Tipperary no podran sortir sols desprès de sopar ja que les famílies no ho permeten.<br/><br/>Entenc i acepto la normativa de sortides sense monitor. En cas de no estar d'acord preguem ens ho comuniquin per escrit, enviant un email a <a href=\"mailto\" target=\"_blank\">info@rosadelsventsidiomas.es</a> ";
	$lang["Nombre_autorizacion_padre"] = "Nom pare, mare o tutor";
	$lang["DNI_autorizacion_padres"] = "DNI";
	$lang["Autoritzacio_acceptacio_monitor"] = "Marqueu la casella conforme autoritza al seu fill/a a les sortides sense monitor.";
	$lang["titulo_autorizacion_medica"] = "AUTORITZACIÓ MÉDICA";
	$lang["autorizacion_medica_padres"] = "Autoritzo al meu fill/a a assistir al curs d’anglès a l’estranger en el que ha estat inscrit. Faig extensiva aquesta autorització a les decisions  medicoquirúrgiques que fossin necessàries adoptar en cas d’extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu  fill/a sigui traslladat al centre de salut més pròxim. ";
	$lang["Nombre_autorizacion_padre_medica"] = "Nom pare, mare o tutor";
	$lang["DNI_autorizacion_padres_medica"] = "DNI";
	$lang["Autoritzacio_acceptacio_medica"] = "Marqueu la casella conforme dóna l'autorització.";
	$lang["titulo_observaciones"] = "OBSERVACIONS";
	$lang["titulo_como_conocido"] = "COM ENS HA CONEGUT?";

	$lang["conocido_radio"] = "Radio";
	$lang["conocido_TV"] = "TV";
	$lang["conocido_amigos"] = "Recomanació amics o familiars";
	$lang["conocido_internet"] = "Internet";
	$lang["conocido_Colegio"] = "Col·legi";
	$lang["conocido_Prensa"] = "Premsa";
	$lang["conocido_clientes"] = "Som clients";
	$lang["Autobus"] = "Autobus";
	$lang["Otros"] = "Altres";
	$lang["Metro"] = "Metro";
	$lang["condiciones_generales"] = "ACCEPTACIÓ DE LES CONDICIONS GENERALS DE LA RESERVA";
	$lang["proteccion_datos"] = "Totes les dades de caràcter personal a les que es refereix aquest formulari, seran tractades exclusivament amb finalitats comercials i operatives de l'organització d’accord amb la legislació vigent en matèria de protecció de dades de caràcter personal. Així mateix, l'informem de la possibilitat d’exercir els drets d’accés, rectificació, cancel·lació i oposició, dirigint-se a les nostres oficines.";
	$lang["titulo_ficha"] = "Ficha d'inscripció";
	$lang["Amigos_Juntos"] = "Dessitja estar allotjat/da a la mateixa habitació o família que els seus amics o familiars?";

	$lang["texto_caja_email"]= "Posi aquí el seu email si desitja rebre un correu electrònic amb la confirmació de la inscripció.";
	$lang["EnlaceBorrar"] = "X";
	$lang["confirmacionBorrado"] = "Està segur que vol esborrar aquesta fitxa?";
	$lang["Foto"] = "Foto";
	$lang["pdf_condiciones"] = "cat condicions generals 2012.pdf";
	$lang["Botonera"] = "Finalitzar la Fitxa d'Inscripció.";
	$lang["no_tenemos_pasaporte"] = "*Necessita una còpia del passaport per acabar la inscripció.";
	$lang["observaciones_desc"] = "Si us plau, indiqueu algun altre detall que consideri important. Ens comprometem a tenir en compte els seus comentaris i preferències, encara que no podem garantir el seu compliment en tots els casos, ja que depenem de les disponibilitats de les residències i de les famílies.";

	$lang["no_hay_programa"] = "El camp nom del programa de dades del curs està buit.";
	$lang["no_hay_fechas"] = "El camp dates de dades del curs està buit.";
	$lang["no_hay_nombre_participante"] = "El camp nom de dades del participant està buit.";
	$lang["no_hay_apellidos_participante"] = "El camp cognoms de dades del participant està buit.";
	$lang["no_hay_direccion_participante"] = "El camp adreça de dades del participant està buit.";
	$lang["no_hay_codigo_postal"] = "El camp codi postal de dades del participant està buit.";
	$lang["no_hay_poblacion"] = "El camp població de dades del participant està buit.";
	$lang["no_hay_telefono"] = "El camp telèfon de dades del participant està buit.";
	$lang["no_hay_movil"] = "El camp mòbil de dades del participant està buit.";
	$lang["no_hay_fecha_nacimiento"] = "El camp data de naixement de dades del participant està buit.";
	$lang["no_hay_pasaporte"] = "El camp pasaport de dades del participant està buit.";
	$lang["no_hay_nacionalidad"] = "El camp nacionalitat de dades del participant està buit.";
	$lang["no_email_participantes"] = "El camp E-mail de dades del participant està buit.";
	$lang["no_email_padres"] = "El camp E-mail de dades de contacte està buit.";
	$lang["no_movil_padres"] = "El camp mòbil dels pares de dades de contacte està buit.";
	$lang["no_movil_participante"] = "El camp mòbil de l\'estudiant de dades del participant està buit.";
	$lang["no_nombre_padres"] = "El camp nom dels pares de dades del participant està buit.";
	$lang["no_hay_chico_chica"] = "El camp Noi / Noia no ha sigut marcat.";

	$lang["no_nombre_autorizacion_audiovisual_padres"] = "El camp nom dels pares de l\'autorizació audiovisual del participant està buit.";
	$lang["no_dni_autorizacion_audiovisual_padres"] = "El camp dni dels pares de l\'autorizació audiovisual del participant està buit.";
	$lang["no_autoritzacio_acceptacio_audiovisual"] = "La casella de l\'autorització de l\'autorizació audiovisual no s\'ha marcat.";

	$lang["no_dni_padres"] = "El camp dni dels pares de dades del participant està buit.";
	$lang["no_colegio"] = "El camp col·legi de dades acadèmiques està buit.";
	$lang["no_direccion_colegio"] = "El camp adreça de dades acadèmiques està buit.";
	$lang["no_cp_colegio"] = "El camp codi postal de dades acadèmiques està buit.";
	$lang["no_poblacion_colegio"] = "El camp població de dades acadèmiques està buit.";
	$lang["no_nombre_profesor"] = "El camp professor de dades acadèmiques està buit.";
	$lang["no_curso"] = "El camp curs de dades acadèmiques està buit.";
	$lang["no_nombre_autorizacion_padre_medica"] = "El camp nom pare, mare o tutor de l\'autorització mèdica està buit.";
	$lang["no_dni_autorizacion_padres_medica"] = "El camp dni de l\'autorització mèdica està buit.";
	$lang["no_nombre_autorizacion_padre"] = "El camp nom pare, mare o tutor de l\'autorització de sortida sense monitor està buit.";
	$lang["no_dni_autorizacion_padres"] = "El camp dni de l\'autorització de sortida sense monitor està buit.";
	$lang["no_formato_fecha_nacimiento"] = "El camp data de naixement de dades del participant no té el fomat correcte o no és una data vàlida.";
	$lang["no_formato_data_in"] = "El camp dates de dades del curs no té el fomat correcte o no és una data vàlida.";
	$lang["no_autoritzacio_acceptacio_medica"] = "La casella de l\'autorització mèdica no s\'ha marcat.";
	$lang["no_normativa_acceptacio"] = "La casella de l\'autorització de les condicions generals de la reserva no s\'ha marcat.";
	$lang["no_autoritzacio_acceptacio_monitor"] = "La casella de l\'autorització de les sortides sense monitor no s\'ha marcat.";

	$lang["no_algun_curso"] = "No has detallat on o amb quina organització has realizat un curs de idiomas a l\'extranger.";
	$lang["no_alguna_enfermedad"] = "No has detallat quin tipus d\'enfermetat tens.";
	$lang["no_alguna_alergia"] = "No has detallat quin tipus d\'alergia tens.";
	$lang["no_amics_familiars"] = "No has detallat el nom del teu amic o familiar.";
	$lang["no_dieta_especial"] = "No has detallat quina es la dieta especial que has de seguir.";
	$lang["no_algun_medicamento_detalle"] = "No has detallat quina medicació necessites durant la teva estancia.";
	$lang["no_conocido"] = "No has detallat per quin altre mitja ens has conegut.";
	/*Fin variables Juanma*/

	$lang["H_ErrorFechas"] = "Error de Dades";
	$lang["MsgErrorFechas"] = "No ha estat possible actualitzar les dades. Si us plau, torni a realitzar els canvis.";

	// $lang["terms_and_conditions_titulo"] = getCustomerStrings("terms_and_conditions");
	// $lang["terms_and_conditions_texto"] = getHotelStrings($hostel, "terms_and_conditions");

	$lang["texto_adjuntar_1"] = "Per a adjuntar un fitxer arrosegal aquí, o ";
	$lang["texto_adjuntar_2"] = "escull uno aquí";
	$lang["MensajeNoExiste"] = "Atenció, la Fitxa d'inscripció a la que intenta accedir ha deixat d'existir. Torni a accedir a la seva reserva per continuar amb el proces d'inscripció.";
	
	$lang["finalizada_correctamente"] = "La Fitxa de Inscripci&oacute; ha sigut finalitzada correctament.";
	$lang["obligatorio"] = "Dada obligatoria";
	$lang["campos_obligatorios_pendientes"] = "Queden dades obligatòries pendents d'introduir";

	$lang["texto_adjunto_ok"] = "Imatge pujada correctament.";
	$lang["texto_adjunto_ko"] = "No ha sigut possible pujar la imatge. Provi a pujar una foto més petita o enviar-la via email a info@rosadelsventsidiomas.es";
	$lang["OtrasConsideraciones"] = "Altres consideracions";
	
?>