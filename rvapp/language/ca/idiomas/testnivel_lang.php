<?php
    // $lang[""] ="";

    $lang["TitolWeb"] = "Test de nivell d'anglès";
    $lang["Titol"] = "Test de nivell d'anglès";
    $lang["NReserva"] = "Número Reserva";
    $lang["Localitzador"] = "Localitzador";
    $lang["Dni"] = "DNI";
    $lang["Participant"] = "Participant";
    
    $lang["H_Titulo"] = "Level Test";
    $lang["H3_Enunciado"] = "Select the correct answer. Only ONE answer is correct";

    $lang["boton"] = "Enviar la prova";
    $lang["AlertaEnviament"] = "Esta segur que vol enviar la prova? un cop enviada no podrà tornar a accedir-hi.";
    $lang["AlertaFaltanCampos"] = "Per poder continuar has de respondre totes les preguntes";

    $lang["True"] = "Sí";
    $lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;
    
    $lang["Idioma"] = "Idioma";
    $lang["Idioma1"] = "Catalán";
    $lang["Idioma2"] = "Castellano";
    
    $lang["H_Info"] = "Informació addicional";
    
    $lang["NSNC"] = "Desitjo pertanyer al nivell Bàsic";

    $lang["AciertosNSNC"] = "<br/><br/>Desitjo pertanyer al <b>nivell Bàsic</b>";
    $lang["MensajeFinalizadoTest"] = "La prova de nivell ha sigut completada correctament.";
    $lang["MensajeTestOutDated"] = "Atenció, ja no és possible accedir ni al Test de Nivell ni a la Fitxa d'Inscripció.<br/><br/>El període per a realitzar aquests tramits finalitza 30 dies despres de formalitzar la reserva.<br/><br/>Els participants que no hagin pogut fer el test de nivell on-line el faran a l'escola el primer dia.";$lang["NoFound"] = "No s'ha trovat cap reserva amb aquestes dades identificatives.<br />Si us plau, torneu a intentar accedir passades 24 hores<br />(es probable que encara no hagi sigut procesada pel nostre sistema).";
    // $lang["AlreadyDone"] = "Prova de nivell realitzada amb anterioritat.";
    $lang["AlreadyDone"] = "La Prova d’anglès a la que intenta accedir ja ha sigut finalitzada amb anterioritat.<br/><br/>Si necessita fer alguna modificació, li agrairem ens envií un email  al Servei d’Atenció al client de Rosa dels Vents a: <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";

    $lang["Splitter"]   ="|@|";
    $lang["npreguntas"] = 50;
    $lang["Info_Descripcion"] = "Aquest test té com a finalitat valorar el nivell de l’estudiant i poder col·locar-lo en el nivell que li correspon junt amb estudiants de nivell similar, pel que es prega facin la prova sense ajuda i sense diccionari.<br/><br/>El test consta de ".$lang["npreguntas"]." preguntes, per poder considerar el test com vàlid totes les preguntes han de tenir resposta.<br/><br/><b style='color:firebrick;'>ATENCIÓ!! Una vegada finalitzat el test no es pot repetir.</b><br/><br/>En el cas de que no vulgueu contestar les preguntes i quedar inscrits en el nivell Bàsic clicar aquí:";
    $lang["Aciertos"] = "<br/><br/>¡Ha encertat <b>{respuestas}/".$lang["npreguntas"]." respostes!</b>";
    $lang["Textaxo"]    =   "What's .... name?".$lang["Splitter"].
                            "you".$lang["Splitter"].
                            "she".$lang["Splitter"].
                            "your".$lang["Splitter"].
                            "yours".$lang["Splitter"].
                            
                            "We're Chinese. We're ... Beijing".$lang["Splitter"].
                            "for".$lang["Splitter"].
                            "from".$lang["Splitter"].
                            "in".$lang["Splitter"].
                            "at".$lang["Splitter"].
                            
                            "Jane's .... nice and polite.".$lang["Splitter"].
                            "a".$lang["Splitter"].
                            "from".$lang["Splitter"].
                            "very".$lang["Splitter"].
                            "at".$lang["Splitter"].
                            
                            ".... a light?".$lang["Splitter"].
                            "Do have you".$lang["Splitter"].
                            "Do you got".$lang["Splitter"].
                            "Have you got".$lang["Splitter"].
                            "Are you have".$lang["Splitter"].
                            
                            "Margaret ..... usually come by bus".$lang["Splitter"].
                            "doesn't".$lang["Splitter"].
                            "isn't".$lang["Splitter"].
                            "don't".$lang["Splitter"].
                            "aren't".$lang["Splitter"].
                            
                            "They ..... at home last night".$lang["Splitter"].
                            "aren't".$lang["Splitter"].
                            "weren't".$lang["Splitter"].
                            "don't".$lang["Splitter"].
                            "didn't".$lang["Splitter"].
                            
                            "What .....you say?".$lang["Splitter"].
                            "are".$lang["Splitter"].
                            "have".$lang["Splitter"].
                            "were".$lang["Splitter"].
                            "did".$lang["Splitter"].
                            
                            "Why ..... crying?".$lang["Splitter"].
                            "are you".$lang["Splitter"].
                            "you are".$lang["Splitter"].
                            "do you".$lang["Splitter"].
                            "you do".$lang["Splitter"].
                            
                            "Where ..... to spend your holidays next summer?".$lang["Splitter"].
                            "you are going".$lang["Splitter"].
                            "are you going".$lang["Splitter"].
                            "you will".$lang["Splitter"].
                            "will you".$lang["Splitter"].
                            
                            "..... never been to the theatre before.".$lang["Splitter"].
                            " I'll".$lang["Splitter"].
                            "I'm".$lang["Splitter"].
                            " I can".$lang["Splitter"].
                            " I've".$lang["Splitter"].
                            
                            "Seiko watches ..... in Japan.".$lang["Splitter"].
                            " are made".$lang["Splitter"].
                            " made".$lang["Splitter"].
                            " make".$lang["Splitter"].
                            " are making".$lang["Splitter"].
                            
                            "Where ..... when you met him?".$lang["Splitter"].
                            "does he live".$lang["Splitter"].
                            "was he live".$lang["Splitter"].
                            "was he living".$lang["Splitter"].
                            "is he living".$lang["Splitter"].
                            
                            "If ..... I'll tell him you called".$lang["Splitter"].
                            "I'll see him".$lang["Splitter"].
                            "I see him".$lang["Splitter"].
                            "I'd see him".$lang["Splitter"].
                            "I saw him".$lang["Splitter"].
                            
                            "What ..... since you arrived.".$lang["Splitter"].
                            "are you doing".$lang["Splitter"].
                            " will you do".$lang["Splitter"].
                            "did you do".$lang["Splitter"].
                            "have you been doing".$lang["Splitter"].
                            
                            "Wine ..... made in Italy for thousands of years.".$lang["Splitter"].
                            " have been".$lang["Splitter"].
                            "is being".$lang["Splitter"].
                            "has been".$lang["Splitter"].
                            "are being".$lang["Splitter"].
                            
                            "My husband ..... live in Spain.".$lang["Splitter"].
                            "use to".$lang["Splitter"].
                            "was use to".$lang["Splitter"].
                            "used to".$lang["Splitter"].
                            "was used to".$lang["Splitter"].
                            
                            "If I ..... I would go out more.".$lang["Splitter"].
                            "wasn't married".$lang["Splitter"].
                            "didn't marry".$lang["Splitter"].
                            "wouldn't marry".$lang["Splitter"].
                            "haven't married".$lang["Splitter"].
                            
                            "I was very ..... in the story.".$lang["Splitter"].
                            "interest".$lang["Splitter"].
                            "interesting".$lang["Splitter"].
                            "interested".$lang["Splitter"].
                            "interests".$lang["Splitter"].
                            
                            "You ..... come if you don't want to.".$lang["Splitter"].
                            "don't need".$lang["Splitter"].
                            "needn't".$lang["Splitter"].
                            "needn't have".$lang["Splitter"].
                            "didn't need".$lang["Splitter"].
                            
                            "I ..... see you tomorrow, I'm not sure".$lang["Splitter"].
                            "maybe".$lang["Splitter"].
                            "will".$lang["Splitter"].
                            "can".$lang["Splitter"].
                            "might".$lang["Splitter"].
                            
                            "..... is bad for you.".$lang["Splitter"].
                            " Smoking".$lang["Splitter"].
                            "The smoking".$lang["Splitter"].
                            "To smoke".$lang["Splitter"].
                            " Smoker".$lang["Splitter"].
                            
                            "I ..... told him if I had known he was your brother.".$lang["Splitter"].
                            " hadn't".$lang["Splitter"].
                            "  wouldn't".$lang["Splitter"].
                            "wouldn't have".$lang["Splitter"].
                            "don't have".$lang["Splitter"].
                            
                            "He ..... living there for three years before they found him.".$lang["Splitter"].
                            " had been".$lang["Splitter"].
                            "has been".$lang["Splitter"].
                            "might be".$lang["Splitter"].
                            "could be".$lang["Splitter"].
                            
                            "I wish you ..... all the time.".$lang["Splitter"].
                            "don't shout".$lang["Splitter"].
                            "won't shout".$lang["Splitter"].
                            "wouldn't shout".$lang["Splitter"].
                            "haven't shout".$lang["Splitter"].
                            
                            "By the time you arrive .....".$lang["Splitter"].
                            "he'll leave".$lang["Splitter"].
                            "he'll have left".$lang["Splitter"].
                            "he leaves".$lang["Splitter"].
                            "he left".$lang["Splitter"].
                            
                            "The house .....built in the 16th century.".$lang["Splitter"].
                            "might have been".$lang["Splitter"].
                            "might be".$lang["Splitter"].
                            "might have be".$lang["Splitter"].
                            "might have".$lang["Splitter"].
                            
                            "Don't forget ..... me a newspaper.".$lang["Splitter"].
                            "buying".$lang["Splitter"].
                            "that you buy".$lang["Splitter"].
                            "to bought".$lang["Splitter"].
                            "to buy".$lang["Splitter"].
                            
                            "Whenever there was a visitor, the dog ..... to the door.".$lang["Splitter"].
                            " will run".$lang["Splitter"].
                            "is running".$lang["Splitter"].
                            "would run".$lang["Splitter"].
                            "was running".$lang["Splitter"].
                            
                            "He is an executive in .....".$lang["Splitter"].
                            " the car industry".$lang["Splitter"].
                            " car industry".$lang["Splitter"].
                            "car industries".$lang["Splitter"].
                            "car industrial".$lang["Splitter"].
                            
                            "Peter sold his car ..... save money.".$lang["Splitter"].
                            "as a result".$lang["Splitter"].
                            "so he".$lang["Splitter"].
                            "in order to".$lang["Splitter"].
                            "because to".$lang["Splitter"].
                            
                            "He advised me ..... the doctor.".$lang["Splitter"].
                            " that I see".$lang["Splitter"].
                            " to see".$lang["Splitter"].
                            "seeing".$lang["Splitter"].
                            "see".$lang["Splitter"].
                            
                            "I ..... travelling by bus.".$lang["Splitter"].
                            " am not used to".$lang["Splitter"].
                            "didn't used to".$lang["Splitter"].
                            "used to".$lang["Splitter"].
                            "do not used to".$lang["Splitter"].
                            
                            "He didn't come last night. I wish that he .".$lang["Splitter"].
                            "had".$lang["Splitter"].
                            "did".$lang["Splitter"].
                            "have".$lang["Splitter"].
                            "has".$lang["Splitter"].
                            
                            "I am going to a wedding. I need to .....".$lang["Splitter"].
                            "be cutting my hair".$lang["Splitter"].
                            "cutting my hair".$lang["Splitter"].
                            "have my hair cut".$lang["Splitter"].
                            "get cut my hair".$lang["Splitter"].
                            
                            "Which would you ..... have, gold or silver?".$lang["Splitter"].
                            "prefer".$lang["Splitter"].
                            "could".$lang["Splitter"].
                            "rather".$lang["Splitter"].
                            "better".$lang["Splitter"].
                            
                            "My sister has been in hospital. I wonder how she .....".$lang["Splitter"].
                            "is getting on".$lang["Splitter"].
                            "gets on".$lang["Splitter"].
                            "has got across".$lang["Splitter"].
                            "is getting away".$lang["Splitter"].
                            
                            "The man said he did not ..... to go by bus.".$lang["Splitter"].
                            " care for".$lang["Splitter"].
                            "bother about".$lang["Splitter"].
                            "mind having".$lang["Splitter"].
                            "much mind".$lang["Splitter"].
                            
                            "Although he confessed to the crime, the judge let the boy .....".$lang["Splitter"].
                            " alone".$lang["Splitter"].
                            "come in".$lang["Splitter"].
                            " off".$lang["Splitter"].
                            "forgive".$lang["Splitter"].
                            
                            "I've never ..... that word before.".$lang["Splitter"].
                            "gave away".$lang["Splitter"].
                            "come across".$lang["Splitter"].
                            "come over".$lang["Splitter"].
                            "come into".$lang["Splitter"].
                            
                            "The student could not answer the question, so he .....".$lang["Splitter"].
                            " gave off".$lang["Splitter"].
                            "gave into".$lang["Splitter"].
                            "gave up".$lang["Splitter"].
                            "gave away".$lang["Splitter"].
                            
                            "There ......... juice for my breakfast.			".$lang["Splitter"].
                            "isn’t some".$lang["Splitter"].
                            "isn’t any".$lang["Splitter"].
                            "any".$lang["Splitter"].
                            "is any".$lang["Splitter"].
                            
                            "Oh! It.......... I’ll take an umbrella with me. 			".$lang["Splitter"].
                            "raining".$lang["Splitter"].
                            "will raining".$lang["Splitter"].
                            "rains".$lang["Splitter"].
                            "’s raining ".$lang["Splitter"].
                            
                            "Did you ……… anywhere interesting last month? ".$lang["Splitter"].
                            "go".$lang["Splitter"].
                            "going".$lang["Splitter"].
                            "was".$lang["Splitter"].
                            "went".$lang["Splitter"].
                            
                            "How long ……… married? ".$lang["Splitter"].
                            "have you been".$lang["Splitter"].
                            "are you".$lang["Splitter"].
                            "have you".$lang["Splitter"].
                            "been".$lang["Splitter"].
                            
                            "Would you like ……… help? ".$lang["Splitter"].
                            "a".$lang["Splitter"].
                            "some".$lang["Splitter"].
                            "me".$lang["Splitter"].
                            "I".$lang["Splitter"].
                            
                            "This is the best coffee I’ve ……… tasted. ".$lang["Splitter"].
                            "never".$lang["Splitter"].
                            "ever".$lang["Splitter"].
                            "already".$lang["Splitter"].
                            "still".$lang["Splitter"].
                            
                            "I’m looking ……… the summer holidays. ".$lang["Splitter"].
                            "before".$lang["Splitter"].
                            "forward".$lang["Splitter"].
                            "for".$lang["Splitter"].
                            "forward to".$lang["Splitter"].
                            
                            "You have a terrible fever! ……… call a doctor? ".$lang["Splitter"].
                            "Shall I".$lang["Splitter"].
                            "Do I".$lang["Splitter"].
                            "Must I".$lang["Splitter"].
                            "Will I".$lang["Splitter"].
                            
                            "Mr Smith wants ……… to his office. ".$lang["Splitter"].
                            "that you come".$lang["Splitter"].
                            "you come to".$lang["Splitter"].
                            "you come".$lang["Splitter"].
                            "you to come".$lang["Splitter"].
                            
                            "These bottles ……… of glass. ".$lang["Splitter"].
                            "are making".$lang["Splitter"].
                            "are make".$lang["Splitter"].
                            "are made".$lang["Splitter"].
                            "made are";
?>