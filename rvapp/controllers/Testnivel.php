<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

    class Testnivel extends CI_Controller {

		protected $devlog = false;
        protected $npreguntas = 30;

		/* propiedades */
			protected $ni = 0;	// id de inscripcion
			protected $tr = 2;	// tipo de reserva  	2 Idiomas 	/ 1 colonias
			protected $tv = 1;	// tipo de venta 		0 Rv 		/ 1 Blahblah
			protected $ip = 0;	// id de people
			protected $ic = 0;	// clave de control para asegurar que el registro que buscamos es el que toca
			protected $param_url = "";	// url encriptada original

			protected $idioma = 'es'; 	// idioma
			protected $forzar = false; 	// usamos este parametro para forzar el acceso en edicion incluso cuando ha sido finalizada.
		/* FIN - propiedades */
		
		public function __construct() {
			parent::__construct();
			
			$this->soporte_xss->inicializar();            

			/* logamos */ if ($this->devlog) echo "<pre>";

			if ($this->uri->segment(5) == "true") {
				$this->forzar = true;
			}
			
			if (($this->uri->segment(3) != "") and ($this->uri->segment(4) != "")) {
				$texto = rawurldecode($this->uri->segment(3));
				$texto = rawurldecode($texto);
				$texto = base64_decode($texto);

				/* logamos */ if ($this->devlog) echo "this->uri->segment(3) ".$texto."<br/>"; // primer parametro: array
				/* logamos */ if ($this->devlog) echo "this->uri->segment(4) ".$this->uri->segment(4)."<br/>"; // segundo parametro: idioma

				$params = $this->soporte_xss->decode($texto, true);
				/* logamos */ if ($this->devlog) echo 'sin decodificar json "'.print_r($params, true).'"\n';
				$params = str_replace("'", '"', $params);

				$params = json_decode($params);

				/* logamos */ if ($this->devlog) echo 'decodificado json "'.print_r($params, true).'"\n';
                
                if ($params=="") {
					/* logamos */ if ($this->devlog) echo __line__.'PROVAMOS decodificado a la antigua\n';
					$texto = rawurldecode($this->uri->segment(3));
					$texto = rawurldecode($texto);
					//$texto = base64_decode($texto);
					
					$params = $this->soporte_xss->oldfashoned_decode($texto, true);
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: sin decodificar json "'.print_r($params, true).'"\n';
					$params = str_replace("'", '"', $params);
	
					$params = json_decode($params);
	
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: decodificado json "'.print_r($params, true).'"\n';
				}
				
				if ($params != "") {
					$this->ni = $params->ni;
					$this->tr = $params->tr;
					$this->tv = $params->tv;
					$this->ip = $params->ip;
					$this->ic = $params->ic;
					$this->param_url = $this->uri->segment(3);
				}

				if ($this->tv == 1) { 		// COMPROBAMOS EL IDIOMA
					$this->idioma = "es"; 	// estamos hablando de Blahblah, solo castellano
				} else { 					// estamos hablando de otros, asi que validamos el parametro idioma
					$this->idioma = $this->soporte_xss->xss_check($this->uri->segment(4)); 
					if ($this->idioma == "es" or $this->idioma == "ca") {
						// todo ok
					} else {
						// aseguramos el idioma por defecto
						$this->idioma = "es";
					}
				} 


				if ($this->tv == 0) {
					if ($this->tr == 2) {
						// idiomas
						$this->load->model('inscripcion_idiomas_model',  "inscripcion_model");
						$this->form_validation->set_error_delimiters('<div class="uk-width-1-1"><div class="uk-alert uk-alert-danger">', '</div></div>');
					} else {
						// colonias
						$this->load->model('inscripcion_colonias_model', "inscripcion_model");
						$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><label>', '</label></div>');
					}
				} else {
					if ($this->tr == 2) {
						// Abroad
						$this->load->model('inscripcion_abroad_model',  "inscripcion_model");
					} else {
						// Spain
						$this->load->model('inscripcion_spain_model', "inscripcion_model");
					}
					$this->form_validation->set_error_delimiters('<div class="uk-width-1-1"><div class="uk-alert uk-alert-danger">', '</div></div>');					
				}
				//die("hasta aqui");
			} else {
                /* PRUEBA DE NIVEL de colonias * /
                        $parametros = array(	"ni"	=>	13177,
                                                "tr"	=>	1,
                                                "tv"	=>	0,
                                                "ip"	=>	79492,
                                                "ic"	=>	59184464);
                        $tparametros = json_encode($parametros);
                        $tparametros = str_replace('"', "'", $tparametros);
                        /* logamos * / if ($this->devlog) echo $tparametros."<br/>";

                        $texto = $this->soporte_xss->encode($tparametros, true);
                        /* logamos * / if ($this->devlog) echo "encoded: ".$texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) echo "decoded: ".$this->soporte_xss->decode($texto, true);
                        /* logamos * / if ($this->devlog) echo "<br/>";

                        $texto = urlencode($texto);
                        $texto = urlencode($texto);
                        /* logamos * / if ($this->devlog) echo $texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) * / echo "<a href='".base_url()."testnivel/ver/".$texto."/es' target='_blank'/>Aqui para abrir PRUEBA DE NIVEL de COLONIAS</a><br/>";
                /* fin de PRUEBA DE NIVEL de colonias */

                /* PRUEBA DE NIVEL de idiomas * /
                        $parametros = array(	"ni"	=>	17046,
                                                "tr"	=>	2,
                                                "tv"	=>	0,
                                                "ip"	=>	96074,
                                                "ic"	=>	27408471);
                        $tparametros = json_encode($parametros);
                        $tparametros = str_replace('"', "'", $tparametros);
                        /* logamos * / if ($this->devlog) echo $tparametros."<br/>";
                        
                        $texto = $this->soporte_xss->encode($tparametros, true);
                        /* logamos * / if ($this->devlog) echo "encoded: ".$texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) echo "decoded: ".$this->soporte_xss->decode($texto, true);
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        
                        $texto = urlencode($texto);
                        $texto = urlencode($texto);
                        /* logamos * / if ($this->devlog) echo $texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) * / echo "<a href='".base_url()."testnivel/ver/".$texto."/es' target='_blank'/>Aqui para abrir PRUEBA DE NIVEL de IDIOMAS</a><br/>";
                /* fin de PRUEBA DE NIVEL de idiomas */

                /* PRUEBA DE NIVEL de spain * /
                        $parametros = array(	"ni"	=>	13179,
                                                "tr"	=>	1,
                                                "tv"	=>	1,
                                                "ip"	=>	79494,
                                                "ic"	=>	42589546);
                        $tparametros = json_encode($parametros);
                        $tparametros = str_replace('"', "'", $tparametros);
                        /* logamos * / if ($this->devlog) echo $tparametros."<br/>";

                        $texto = $this->soporte_xss->encode($tparametros, true);
                        /* logamos * / if ($this->devlog) echo "encoded: ".$texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) echo "decoded: ".$this->soporte_xss->decode($texto, true);
                        /* logamos * / if ($this->devlog) echo "<br/>";

                        $texto = urlencode($texto);
                        $texto = urlencode($texto);
                        /* logamos * / if ($this->devlog) echo $texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) * / echo "<a href='".base_url()."testnivel/ver/".$texto."/es' target='_blank'/>Aqui para abrir PRUEBA DE NIVEL de SPAIN</a><br/>";
                /* fin de PRUEBA DE NIVEL de spain */

                /* PRUEBA DE NIVEL de abroad * /
                        $parametros = array(	"ni"	=>	17048,
                                                "tr"	=>	2,
                                                "tv"	=>	1,
                                                "ip"	=>	79496,
                                                "ic"	=>	83261318);
                        $tparametros = json_encode($parametros);
                        $tparametros = str_replace('"', "'", $tparametros);
                        /* logamos * / if ($this->devlog) echo $tparametros."<br/>";

                        $texto = $this->soporte_xss->encode($tparametros, true);
                        /* logamos * / if ($this->devlog) echo "encoded: ".$texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) echo "decoded: ".$this->soporte_xss->decode($texto, true);
                        /* logamos * / if ($this->devlog) echo "<br/>";

                        $texto = urlencode($texto);
                        $texto = urlencode($texto);
                        /* logamos * / if ($this->devlog) echo $texto;
                        /* logamos * / if ($this->devlog) echo "<br/>";
                        /* logamos * / if ($this->devlog) * / echo "<a href='".base_url()."testnivel/ver/".$texto."/es' target='_blank'/>Aqui para abrir PRUEBA DE NIVEL de ABROAD</a><br/>";
                /* fin de PRUEBA DE NIVEL de abroad */
            }
        }

        /* GENERICOS */
			public function index()	{
				// $this->load->view('welcome_message');

				$this->lang->load('colonias/acceso', $this->idioma);			
				/* logamos */ if ($this->devlog) echo "INDEX:>><br/>";

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view("colonias/mensaje_generico_view", $params);				

            }
            
			public function ruta() {
				if ($this->tv == 0 and $this->tr != 2) return "colonias";
				if ($this->tv == 0 and $this->tr == 2) return "idiomas";
				if ($this->tv == 1 and $this->tr != 2) return "spain";
				if ($this->tv == 1 and $this->tr == 2) return "abroad";
            }
            
            public function ver() {
				/* logamos */ if ($this->devlog) echo "acceso ".$this->idioma;
				
				$ruta = $this->ruta();
				
				$this->lang->load($ruta.'/testnivel', $this->idioma);
                $this->npreguntas = lang("npreguntas");

				if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
					/* logamos */ if ($this->devlog) echo "acceso denegado";

					$params = array("lang"		=> $this->idioma,
									"control"	=> __LINE__." MensajeNoExiste",
									"mensaje"	=> lang("MensajeNoExiste"));
					$this->load->view($ruta."/mensaje_generico_view", $params);
				} else {
					/* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
					
					if ($z_nom == "") {
						/* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." NoFound",
										"mensaje"	=> lang("NoFound"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." AlreadyDone",
                                            "mensaje"	=> lang("AlreadyDone"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                        } else {
                        
                            log_message("error", "vamos a llamar a fecha_checkin");
                            $retorno = $this->inscripcion_model->fecha_checkin($this->ni);
                            log_message("error", "ha retornado (".$retorno.")");
                            if (!$retorno) {
                                /* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
                                $params = array("lang"		=> $this->idioma,
                                                "control"	=> __LINE__." MensajeTestOutDated",
                                                "mensaje"	=> lang("MensajeTestOutDated"));
                                $this->load->view($ruta."/mensaje_generico_view", $params);
                            } else {
                                $z_zurich = $this->inscripcion_model->getIsZurich($this->ni);
                                $params = array("lang"					=> $this->idioma,
                                                "param_url"				=> $this->param_url,
                                                "npreguntas"            => $this->npreguntas,
                                                "z_nom"                 => $z_nom,
                                                "z_zurich"	 			=> $z_zurich);
                                    
                                $this->load->view($ruta."/form_prueba_nivel_view", $params);

                                /* logamos */ if ($this->devlog) echo "acceso garantizado";
                            }
                        }
                    }
				}

				/* logamos */ if ($this->devlog) echo "</pre>";			
            }
            
            public function vp() {
				/* logamos */ if ($this->devlog) echo "acceso ".$this->idioma;
				
				$ruta = $this->ruta();
				
				$this->lang->load($ruta.'/testnivel', $this->idioma);
                $this->npreguntas = lang("npreguntas");

				if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
					/* logamos */ if ($this->devlog) echo "acceso denegado";

					$params = array("lang"		=> $this->idioma,
									"control"	=> __LINE__." MensajeNoExiste",
									"mensaje"	=> lang("MensajeNoExiste"));
					$this->load->view($ruta."/mensaje_generico_view", $params);
				} else {
					/* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
					
					if ($z_nom == "") {
						/* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." NoFound",
										"mensaje"	=> lang("NoFound"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad: Entramos";

                            $datos = $this->inscripcion_model->getProvaNivellJaRealitzada($this->ni);
                            $z_zurich = $this->inscripcion_model->getIsZurich($this->ni);
                            $params = array("lang"					=> $this->idioma,
                                            "param_url"				=> $this->param_url,
                                            "npreguntas"            => $this->npreguntas,
                                            "z_nom"                 => $z_nom,
                                            "z_zurich"	 			=> $z_zurich,
                                            "datos"                 => $datos);
                                
                            $this->load->view($ruta."/form_prueba_nivel_vp_view", $params);

                            /* logamos */ if ($this->devlog) echo "acceso garantizado";
                        
                        } else {
                        
                            /* logamos */ if ($this->devlog) echo "Prueba NO Realizada";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." PruebaNoRealizada",
                                            "mensaje"	=> lang("PruebaNoRealizada"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                            
                        }
                    }
				}

				/* logamos */ if ($this->devlog) echo "</pre>";			
            }

            public function guardar() {
				/* logamos */ if ($this->devlog) echo "guardar ".$this->idioma;
			
				if ($this->tv == 0 and $this->tr != 2) $this->guardar_colonias(); 	// $ruta = "colonias";

				if ($this->tv == 0 and $this->tr == 2) $this->guardar_idiomas(); 	// $ruta = "idiomas";

				if ($this->tv == 1 and $this->tr != 2) $this->guardar_spain(); 		// $ruta = "spain";

				if ($this->tv == 1 and $this->tr == 2) $this->guardar_abroad(); 	// $ruta = "abroad";
			}
        /* FIN - GENERICOS */

        /* ESPECIFICOS COLONIAS */
            public function guardar_colonias() {
                /* logamos */ if ($this->devlog) echo "guardar colonias ".$this->idioma;
                $ruta = $this->ruta();

                $this->lang->load($ruta.'/testnivel', $this->idioma);
                
                if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
                    /* logamos */ if ($this->devlog) echo "acceso denegado";

                    $params = array("lang"		=> $this->idioma,
                                    "control"	=> __LINE__." MensajeNoExiste",
                                    "mensaje"	=> lang("MensajeNoExiste"));
                    $this->load->view($ruta."/mensaje_generico_view", $params);
                } else {
                    /* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
                        
                    if ($z_nom == "") {
                        /* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
                        $params = array("lang"		=> $this->idioma,
                                        "control"	=> __LINE__." NoFound",
                                        "mensaje"	=> lang("NoFound"));
                        $this->load->view($ruta."/mensaje_generico_view", $params);
                    } else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." AlreadyDone",
                                            "mensaje"	=> lang("AlreadyDone"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                        } else {
                            // si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
                            /* logamos */ if ($this->devlog) echo "POST ".print_r($_POST, true)." - ".count($_POST);

                            if (isset($_POST["NSNC"])) {
                                $this->form_validation->set_rules("NSNC", lang("NSNC"), "trim|required");
                            } else {
                                for ($pos=0; $pos < $this->npreguntas; $pos++) { 
                                    $this->form_validation->set_rules("respuesta".$pos, lang("respuesta".$pos), "trim|required");
                                }
                            }
                        
                            if($this->form_validation->run() == FALSE) {
                                // echo validation_errors();
                                $this->ver();
                            } else {
                                /* logamos */ if ($this->devlog) echo "\nGuardando!\n";
                        
                                $this->inscripcion_model->set_provaNivell($this->ni, $this->npreguntas);
                                
                                $rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
                                /* logamos */ if ($this->devlog) echo "\nrsDades: ".print_r($rsDades, true)."\n";
                                
                                $val_numero_reserva = $rsDades[0]->NReserva;
                                $centro = $rsDades[0]->centro;
                                $hostel = $rsDades[0]->hostel;
                                
                                $destino_url = "includes/reservation/sendmail_lan.php?lang=".$this->idioma."&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

                                /* logamos */ if ($this->devlog) echo "\nvamos a lanzar esta url!\n".$destino_url."\n";
                                $this->load->library("enviar_email");
                                $this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
                                /* logamos */ if ($this->devlog) echo "\Enviado!\n";
                                
                                $aciertos = $this->comprovar_aciertos($this->ni);

                                $mensaje = lang("MensajeFinalizadoTest");
                                if ($aciertos == -1) {
                                    // texto automatica
                                    $mensaje .= lang("AciertosNSNC");
                                } else {
                                    // texto general
                                    $mensaje .= lang("Aciertos");
                                    $mensaje = str_replace("{respuestas}", $aciertos, $mensaje);                                    
                                }

                                $params = array("lang"		=> $this->idioma,
                                                "control"	=> __LINE__." MensajeFinalizadoTest",
                                                "mensaje"	=> $mensaje);
                                $this->load->view($ruta."/mensaje_generico_view", $params);						
                            }
                        }
                    }
                    /* logamos */ if ($this->devlog) echo "</pre>";		
                }
            }
        /* FIN - ESPECIFICOS COLONIAS */

        /* ESPECIFICOS IDIOMAS */
            public function guardar_idiomas() {
                /* logamos */ if ($this->devlog) echo "guardar idiomas ".$this->idioma;
                $ruta = $this->ruta();

                $this->lang->load($ruta.'/testnivel', $this->idioma);
                $this->npreguntas = lang("npreguntas");
                
                if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
                    /* logamos */ if ($this->devlog) echo "acceso denegado";

                    $params = array("lang"		=> $this->idioma,
                                    "control"	=> __LINE__." MensajeNoExiste",
                                    "mensaje"	=> lang("MensajeNoExiste"));
                    $this->load->view($ruta."/mensaje_generico_view", $params);
                } else {
                    /* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
                        
                    if ($z_nom == "") {
                        /* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
                        $params = array("lang"		=> $this->idioma,
                                        "control"	=> __LINE__." NoFound",
                                        "mensaje"	=> lang("NoFound"));
                        $this->load->view($ruta."/mensaje_generico_view", $params);
                    } else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." AlreadyDone",
                                            "mensaje"	=> lang("AlreadyDone"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                        } else {
                            // si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
                            /* logamos */ if ($this->devlog) echo "POST ".print_r($_POST, true)." - ".count($_POST);

                            if (isset($_POST["NSNC"])) {
                                $this->form_validation->set_rules("NSNC", lang("NSNC"), "trim|required");
                            } else {
                                for ($pos=0; $pos < $this->npreguntas; $pos++) { 
                                    $this->form_validation->set_rules("respuesta".$pos, lang("respuesta".$pos), "trim|required");
                                }
                            }
                        
                            if($this->form_validation->run() == FALSE) {
                                // echo validation_errors();
                                $this->ver();
                            } else {
                                /* logamos */ if ($this->devlog) echo "\nGuardando!\n";
                        
                                $this->inscripcion_model->set_provaNivell($this->ni, $this->npreguntas);
                                
                                $rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
                                /* logamos */ if ($this->devlog) echo "\nrsDades: ".print_r($rsDades, true)."\n";
                                
                                $val_numero_reserva = $rsDades[0]->NReserva;
                                $centro = $rsDades[0]->centro;
                                $hostel = $rsDades[0]->hostel;
                                
                                $destino_url = "includes/reservation/sendmail_lan.php?lang=".$this->idioma."&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

                                /* logamos */ if ($this->devlog) echo "\nvamos a lanzar esta url!\n".$destino_url."\n";
                                $this->load->library("enviar_email");
                                $this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
                                /* logamos */ if ($this->devlog) echo "\Enviado!\n";
                                
                                $params = array("lang"		=> $this->idioma,
                                                "control"	=> __LINE__." MensajeFinalizadoTest",
                                                "mensaje"	=> lang("MensajeFinalizadoTest"));
                                $this->load->view($ruta."/mensaje_generico_view", $params);						
                            }
                        }
                    }
                    /* logamos */ if ($this->devlog) echo "</pre>";		
                }
            }
        /* FIN - ESPECIFICOS IDIOMAS */

        /* ESPECIFICOS SPAIN */
            public function guardar_spain() {
                /* logamos */ if ($this->devlog) echo "guardar spain ".$this->idioma;
                $ruta = $this->ruta();

                $this->lang->load($ruta.'/testnivel', $this->idioma);
                
                if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
                    /* logamos */ if ($this->devlog) echo "acceso denegado";

                    $params = array("lang"		=> $this->idioma,
                                    "control"	=> __LINE__." MensajeNoExiste",
                                    "mensaje"	=> lang("MensajeNoExiste"));
                    $this->load->view($ruta."/mensaje_generico_view", $params);
                } else {
                    /* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
                        
                    if ($z_nom == "") {
                        /* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
                        $params = array("lang"		=> $this->idioma,
                                        "control"	=> __LINE__." NoFound",
                                        "mensaje"	=> lang("NoFound"));
                        $this->load->view($ruta."/mensaje_generico_view", $params);
                    } else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." AlreadyDone",
                                            "mensaje"	=> lang("AlreadyDone"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                        } else {
                            // si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
                            /* logamos */ if ($this->devlog) echo "POST ".print_r($_POST, true)." - ".count($_POST);

                            if (isset($_POST["NSNC"])) {
                                $this->form_validation->set_rules("NSNC", lang("NSNC"), "trim|required");
                            } else {
                                for ($pos=0; $pos < $this->npreguntas; $pos++) { 
                                    $this->form_validation->set_rules("respuesta".$pos, lang("respuesta".$pos), "trim|required");
                                }
                            }
                        
                            if($this->form_validation->run() == FALSE) {
                                // echo validation_errors();
                                $this->ver();
                            } else {
                                /* logamos */ if ($this->devlog) echo "\nGuardando!\n";
                        
                                $this->inscripcion_model->set_provaNivell($this->ni, $this->npreguntas);

                                $rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
                                /* logamos */ if ($this->devlog) echo "\nrsDades: ".print_r($rsDades, true)."\n";
                                
                                $val_numero_reserva = $rsDades[0]->NReserva;
                                $centro = $rsDades[0]->centro;
                                $hostel = $rsDades[0]->hostel;
                                
                                $destino_url = "includes/reservation/sendmail_lan.php?lang=be&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

                                /* logamos */ if ($this->devlog) echo "\nvamos a lanzar esta url!\n".$destino_url."\n";
                                $this->load->library("enviar_email");
                                $this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
                                /* logamos */ if ($this->devlog) echo "\Enviado!\n";
                                
                                $params = array("lang"		=> $this->idioma,
                                                "control"	=> __LINE__." MensajeFinalizadoTest",
                                                "mensaje"	=> lang("MensajeFinalizadoTest"));
                                $this->load->view($ruta."/mensaje_generico_view", $params);						
                            }
                        }
                    }
                    /* logamos */ if ($this->devlog) echo "</pre>";		
                }
            }
        /* FIN - ESPECIFICOS SPAIN */

        /* ESPECIFICOS ABROAD */
            public function guardar_abroad() {
                /* logamos */ if ($this->devlog) echo "guardar abroad ".$this->idioma;
                $ruta = $this->ruta();

                $this->lang->load($ruta.'/testnivel', $this->idioma);
                
                if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
                    /* logamos */ if ($this->devlog) echo "acceso denegado";

                    $params = array("lang"		=> $this->idioma,
                                    "control"	=> __LINE__." MensajeNoExiste",
                                    "mensaje"	=> lang("MensajeNoExiste"));
                    $this->load->view($ruta."/mensaje_generico_view", $params);
                } else {
                    /* logamos */ if ($this->devlog) echo "\n".$ruta."\n";
                    $z_nom = $this->inscripcion_model->isset_Inscripcio($this->ni, $this->ip, $this->ic /* , $this->forzar*/ );
                        
                    if ($z_nom == "") {
                        /* logamos */ if ($this->devlog) echo "no hay datos - ruta ".$ruta;
                        $params = array("lang"		=> $this->idioma,
                                        "control"	=> __LINE__." NoFound",
                                        "mensaje"	=> lang("NoFound"));
                        $this->load->view($ruta."/mensaje_generico_view", $params);
                    } else {

                        $jaintroduit = $this->inscripcion_model->provaNivellJaRealitzada($this->ni);

                        if ($jaintroduit != 0) {
                            /* logamos */ if ($this->devlog) echo "Realizada con anterioridad";
                            $params = array("lang"		=> $this->idioma,
                                            "control"	=> __LINE__." AlreadyDone",
                                            "mensaje"	=> lang("AlreadyDone"));
                            $this->load->view($ruta."/mensaje_generico_view", $params);
                        } else {
                            // si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
                            /* logamos */ if ($this->devlog) echo "POST ".print_r($_POST, true)." - ".count($_POST);

                            if (isset($_POST["NSNC"])) {
                                $this->form_validation->set_rules("NSNC", lang("NSNC"), "trim|required");
                            } else {
                                for ($pos=0; $pos < $this->npreguntas; $pos++) { 
                                    $this->form_validation->set_rules("respuesta".$pos, lang("respuesta".$pos), "trim|required");
                                }
                            }
                        
                            if($this->form_validation->run() == FALSE) {
                                // echo validation_errors();
                                $this->ver();
                            } else {
                                /* logamos */ if ($this->devlog) echo "\nGuardando!\n";
                        
                                $this->inscripcion_model->set_provaNivell($this->ni, $this->npreguntas);
                                
                                $rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
                                /* logamos */ if ($this->devlog) echo "\nrsDades: ".print_r($rsDades, true)."\n";
                                
                                $val_numero_reserva = $rsDades[0]->NReserva;
                                $centro = $rsDades[0]->centro;
                                $hostel = $rsDades[0]->hostel;
                                
                                $destino_url = "includes/reservation/sendmail_lan.php?lang=be&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

                                /* logamos */ if ($this->devlog) echo "\nvamos a lanzar esta url!\n".$destino_url."\n";
                                $this->load->library("enviar_email");
                                $this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
                                /* logamos */ if ($this->devlog) echo "\Enviado!\n";
                                
                                $params = array("lang"		=> $this->idioma,
                                                "control"	=> __LINE__." MensajeFinalizadoTest",
                                                "mensaje"	=> lang("MensajeFinalizadoTest"));
                                $this->load->view($ruta."/mensaje_generico_view", $params);						
                            }
                        }
                    }
                    /* logamos */ if ($this->devlog) echo "</pre>";		
                }
            }
        /* FIN - ESPECIFICOS ABROAD */
    
        public function comprovar_aciertos($idinscripcio) {
            $datos = $this->inscripcion_model->getProvaNivellJaRealitzada($idinscripcio);
            $aciertos = 0;
            // echo "<pre>datos ".print_r($datos, true)."</pre>";

            if ($datos["NSNC"] == "1") $aciertos--; // ->NSNC            
            else {
                if ($datos['p01'] == "B") $aciertos++;
                if ($datos['p02'] == "C") $aciertos++;
                if ($datos['p03'] == "C") $aciertos++;
                if ($datos['p04'] == "A") $aciertos++;
                if ($datos['p05'] == "C") $aciertos++;
                if ($datos['p06'] == "A") $aciertos++;
                if ($datos['p07'] == "C") $aciertos++;
                if ($datos['p08'] == "A") $aciertos++;
                if ($datos['p09'] == "B") $aciertos++;
                if ($datos['p10'] == "D") $aciertos++;
                if ($datos['p11'] == "A") $aciertos++;
                if ($datos['p12'] == "A") $aciertos++;
                if ($datos['p13'] == "A") $aciertos++;
                if ($datos['p14'] == "A") $aciertos++;
                if ($datos['p15'] == "A") $aciertos++;
                if ($datos['p16'] == "D") $aciertos++;
                if ($datos['p17'] == "D") $aciertos++;
                if ($datos['p18'] == "A") $aciertos++;
                if ($datos['p19'] == "B") $aciertos++;
                if ($datos['p20'] == "A") $aciertos++;
                if ($datos['p21'] == "A") $aciertos++;
                if ($datos['p22'] == "D") $aciertos++;
                if ($datos['p23'] == "C") $aciertos++;
                if ($datos['p24'] == "B") $aciertos++;
                if ($datos['p25'] == "C") $aciertos++;
                if ($datos['p26'] == "A") $aciertos++;
                if ($datos['p27'] == "D") $aciertos++;
                if ($datos['p28'] == "D") $aciertos++;
                if ($datos['p29'] == "C") $aciertos++;
                if ($datos['p30'] == "C") $aciertos++;
            }
            return $aciertos;
        }
    }
?>