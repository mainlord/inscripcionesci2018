<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Doing extends CI_Controller {

		protected $devlog = false;

		/* propiedades */
			protected $ni = 0;	// id de inscripcion
			protected $tr = 1;	// tipo de reserva  	2 Idiomas 	/ 1 colonias
			protected $tv = 0;	// tipo de venta 		0 Rv 		/ 1 Blahblah
			protected $ip = 0;	// id de people
			protected $ic = 0;	// clave de control para asegurar que el registro que buscamos es el que toca
			protected $param_url = "";	// url encriptada original

			protected $idioma = 'es'; // idioma
			protected $forzar = false; 	// usamos este parametro para forzar el acceso en edicion incluso cuando ha sido finalizada.
		/* FIN - propiedades */
		
		public function __construct() {
			parent::__construct();
			
			$this->soporte_xss->inicializar();            

			if ($this->devlog) log_message("error",__line__." - "."<pre>");
			
			if ($this->uri->segment(5) == "true") {
				$this->forzar = true;
			}
			
			if (($this->uri->segment(3) != "") and ($this->uri->segment(4) != "")) {
				$texto = rawurldecode($this->uri->segment(3));
				$texto = rawurldecode($texto);
				$texto = base64_decode($texto);
			
				if ($this->devlog) log_message("error",__line__." - "."this->uri->segment(3) ".$this->uri->segment(3)."\n"); // primer parametro: arra)y
				if ($this->devlog) log_message("error",__line__." - "."this->uri->segment(4) ".$this->uri->segment(4)."\n"); // segundo parametro: tipo image)n
				if ($this->devlog) log_message("error",__line__." - "."texto ".$texto."\n"); // segundo parametro: tipo image)n
				
				$params = $this->soporte_xss->decode($texto, true);
				
				/* logamos */ if ($this->devlog) log_message("error",__line__." - ".'sin decodificar json "'.print_r($params, true).'"\n');
				$params = str_replace("'", '"', $params);

				$params = json_decode($params);
				
				if ($this->devlog) log_message("error",__line__." - ".'"'.print_r($params, true).'"\n');
				
				if ($params=="") {
					/* logamos */ if ($this->devlog) echo __line__.'PROVAMOS decodificado a la antigua\n';
					$texto = rawurldecode($this->uri->segment(3));
					$texto = rawurldecode($texto);
					//$texto = base64_decode($texto);
					
					$params = $this->soporte_xss->oldfashoned_decode($texto, true);
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: sin decodificar json "'.print_r($params, true).'"\n';
					$params = str_replace("'", '"', $params);
	
					$params = json_decode($params);
	
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: decodificado json "'.print_r($params, true).'"\n';
				}

				if ($params != "") {
					$this->ni = $params->ni;
					$this->tr = $params->tr;
					$this->tv = $params->tv;
					$this->ip = $params->ip;
					$this->ic = $params->ic;
					$this->param_url = $this->uri->segment(3);
				}

				if ($this->tv == 0) {
					switch($this->tr) {
						case 1:
						case 6:
							// colonias
							$this->load->model('inscripcion_colonias_model', "inscripcion_model");
						break;

						case 2:
							// idiomas
							$this->load->model('inscripcion_idiomas_model',  "inscripcion_model");
						break;
						
						case 4:
							// Taf
							$this->load->model('inscripcion_taf_model', "inscripcion_model");
						break;
					}
				} else {
					if ($this->tr == 2) {
						// Abroad
						$this->load->model('inscripcion_abroad_model',  "inscripcion_model");
					} else {
						// Spain
						$this->load->model('inscripcion_spain_model', "inscripcion_model");
					}
				}
			} else {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");
				
				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view("abroad/mensaje_generico_view", $params);
			}
        }

		public function index()	{
			// $this->load->view('welcome_message');

			$params = array("lang"		=> $this->idioma,
							"control"	=> __LINE__." MensajeNoExiste",
							"mensaje"	=> lang("MensajeNoExiste"));
			$this->load->view("abroad/mensaje_generico_view", $params);				

        }

		public function ruta() {
			if ($this->tv == 0 and $this->tr != 2) return "colonias";
			if ($this->tv == 0 and $this->tr == 2) return "idiomas";
			if ($this->tv == 1 and $this->tr != 2) return "spain";
			if ($this->tv == 1 and $this->tr == 2) return "abroad";
		}

		public function upload($params, $tipo, $forzar = false) {
			if (($this->tr == 1 && $this->tv == 0) or
			 	($this->tr == 6 && $this->tv == 0)) {
				// para colonias hacemos la subida unitaria forzada.
				$this->upload_manual($params, $tipo, $forzar);
			} else {
				// para los demas tipos de inscripcion hacemos la de uikit.
				$this->upload_uikit($params, $tipo, $forzar);
			}
		}

		public function dniupload($params, $tipo, $forzar = false) {
			$RutaUpload = 'UploadedFiles/';

            if ($this->devlog) log_message("error",__line__." - "."Doing </br>");
			
			$ruta = $this->ruta();

			if ($this->devlog) log_message("error",__line__." - "."ni ".$this->ni."</br>");
			if ($this->devlog) log_message("error",__line__." - "."tr ".$this->tr."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ip ".$this->ip."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ic ".$this->ic."</br>");
			if ($this->devlog) log_message("error",__line__." - "."forzar ".$this->forzar."</br>");
			
			if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			} else {
				if ($this->devlog) log_message("error",__line__." - ".$ruta."</br>");

				if ($this->devlog) log_message("error",__line__." - "."_FILES ".print_r($_FILES, true)."</br>");
				
				if ($this->devlog) log_message("error",__line__." - "."tipo ".$tipo."</br>");
				switch ($tipo) {
					case "DNIA":
						$nom_camp			= "files";
						$file_numImg_pas 	= "3";
						$tipos_auxiliares	= "";
					break;

					case "DNIR":
						$nom_camp			= "files";
						$file_numImg_pas 	= "4";
						$tipos_auxiliares	= "";
					break;

					default:
						log_message("error",__line__." - "."hacking attempt!");				
						die("haking Attempt!!");
					break;
				}

				$this->load->library('upload');				

				$file = $_FILES;

				$config['upload_path']	 		= './'.$RutaUpload.'ORI/';
				$config['allowed_types']	 	= 'gif|jpg|jpeg|png|pdf';
				$config['max_size']				= '5000';
				
				//echo print_r($_FILES);
				$extension 	= explode(".", $_FILES["files"]["name"]);
				if ($this->devlog) log_message("error",__line__." - "."extension exploded ".print_r($extension, true)."</br>");
				if ($this->devlog) log_message("error",__line__." - "."cuenta positions ".print_r((count($extension)-1), true)."</br>");
				if ($this->devlog) log_message("error",__line__." - "."extension a guardar ".print_r($extension[(count($extension)-1)], true)."</br>");
				$extension	= $extension[(count($extension)-1)];
				if ($this->devlog) log_message("error",__line__." - "."extension ".print_r($extension, true)."</br>");

				$hash = date("YmdHis");

				$nombre 		= $hash."_".$this->ni."_".$file_numImg_pas.".".$extension;
				if ($this->devlog) log_message("error",__line__." - "."nombre ".print_r($nombre, true)."</br>");
				
				$nombreORI 		= $RutaUpload."ORI/".$nombre;
				$nombreIMG 		= $RutaUpload."IMG/".$nombre;
				$nombreTHU 		= $RutaUpload."THU/".$nombre;
				
				$nombreIMGdef 	= $RutaUpload."IMG/".$nombre;
				$nombreTHUdef 	= $RutaUpload."THU/".$nombre;

				if ($this->devlog) log_message("error",__line__." - "."nombreORI ".$nombreORI."</br>");
				if ($this->devlog) log_message("error",__line__." - "."nombreIMG ".$nombreIMG."</br>");
				if ($this->devlog) log_message("error",__line__." - "."nombreTHU ".$nombreTHU."</br>");
				
				if ($nombreORI != "" && file_exists($nombreORI)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero original </br>");
					unlink($nombreORI);
				}

				if ($nombreIMG != "" && file_exists($nombreIMG)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Imagen </br>");
					unlink($nombreIMG);
				}    

				if ($nombreTHU != "" && file_exists($nombreTHU)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Thumbnail</br>");
					unlink($nombreTHU);
				} 
				
				$config['file_name']  			= $nombre;

				if ($this->devlog) log_message("error",__line__." - "."config ".print_r($config, true)."</br>");

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('files')) {
					$error = array('error' => $this->upload->display_errors());
					if ($this->devlog) log_message("error",__line__." - "."ERROR </br>".print_r($error,true));
					$this->data['error'] = array('error' => $this->upload->display_errors('<div class="alert alert-danger">', '</div>'));
					echo "Error - ".$this->upload->display_errors()." - "; //error
				} else {
					if ($this->devlog) log_message("error",__line__." - "."OK ");
					
					$imagenes_viejas = $this->inscripcion_model->get_ImatgesViatge($this->ni, $tipo);
					if ($this->devlog) log_message("error",__line__." - "."imagenes_viejas ".print_r($imagenes_viejas, true)."</br>");
					
					if ($imagenes_viejas !== false) {
						// la foto
						$nombre_limpieza = explode("/", $imagenes_viejas->Foto);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
								unlink($RutaUpload."IMG/".$nombre_limpieza);
							}
						}
					}
					
					$procesar = true;
					if (($_FILES["files"]["type"] == "application/pdf")) {
						if ($this->devlog) log_message("error",__line__." - "."pdf");
					
						copy($nombreORI, $nombreIMG);
						if ($this->devlog) log_message("error",__line__." - "."copiado ".$nombreORI.", ".$nombreIMG);
						
						$nombreTHU = $RutaUpload."THU/nofoto35.png";

						if ($nombreORI != "" && file_exists($nombreORI)) {
							unlink($nombreORI);
						}
					} else {
						if ($this->devlog) log_message("error",__line__." - "."Otra ".$tipo." - ".print_r($_FILES, true));
						
						if (($tipo == "DNIA") or ($tipo == "DNIR")) {
							
							if ($this->devlog) log_message("error",__line__." - "."imagen");
							$this->load->library('image_lib'); 
							
							if ($this->devlog) log_message("error",__line__." - "."Creando IMAGEN");
							$configPASS['image_library'] = 'gd2';
							$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$configPASS['new_image'] = $nombreIMG;
							$configPASS['maintain_ratio'] = TRUE;
							$configPASS['overwrite'] = TRUE;
							$configPASS['width'] = 1024;
							// $configPASS['height'] = 768;
							$this->image_lib->initialize($configPASS);
		
							if ( !$this->image_lib->resize()){
								if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								$nombreIMG 		= "error";
								$nombreTHU 		= "error";
								$procesar = false;
							}
						}

						if ($nombreORI != "" && file_exists($nombreORI)) {
							unlink($nombreORI);
						}
					}

					if ($procesar) {
						$nombreIMG 		= base_url($RutaUpload."IMG/").$nombre;
						$this->inscripcion_model->update_ImatgeViatge($this->ni, $nombreIMG, "", $tipo);
					}
			
				}
				echo $nombreIMG;
			}
		}

		public function upload_manual($params, $tipo, $forzar = false) {
			$RutaUpload = 'UploadedFiles/';

            if ($this->devlog) log_message("error",__line__." - "."Doing </br>");
			
			$ruta = $this->ruta();

			if ($this->devlog) log_message("error",__line__." - "."ni ".$this->ni."</br>");
			if ($this->devlog) log_message("error",__line__." - "."tr ".$this->tr."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ip ".$this->ip."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ic ".$this->ic."</br>");
			if ($this->devlog) log_message("error",__line__." - "."forzar ".$this->forzar."</br>");
			
			if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			} else {
				if ($this->devlog) log_message("error",__line__." - ".$ruta."</br>");

				if ($this->devlog) log_message("error",__line__." - "."_FILES ".print_r($_FILES, true)."</br>");
				
				if ($this->devlog) log_message("error",__line__." - "."tipo ".$tipo."</br>");
				switch ($tipo) {
					case "IMG":
						$nom_camp			= "files";
						$file_numImg_pas 	= "1";
						$tipos_auxiliares	= "";
					break;

					case "PASS":
						$nom_camp			= "files";
						$file_numImg_pas 	= "2";
						$tipos_auxiliares	= "pdf";
					break;

					case "DNIA":
						$nom_camp			= "files";
						$file_numImg_pas 	= "3";
						$tipos_auxiliares	= "";
					break;

					case "DNIR":
						$nom_camp			= "files";
						$file_numImg_pas 	= "4";
						$tipos_auxiliares	= "";
					break;

					case "CVAC":
						$nom_camp			= "files";
						$file_numImg_pas 	= "5";
						$tipos_auxiliares	= "";
					break;

					default:
						log_message("error",__line__." - "."hacking attempt!");
				
						die("haking Attempt!!");
					break;
				}

				$this->load->library('upload');				

				$file = $_FILES;

				$config['upload_path']	 		= './'.$RutaUpload.'ORI/';
				$config['allowed_types']	 	= 'gif|jpg|jpeg|png|pdf';
				$config['max_size']				= '5000';
				
				//echo print_r($_FILES);
				$extension 	= explode(".", $_FILES["files"]["name"]);
				if ($this->devlog) log_message("error",__line__." - "."extension exploded ".print_r($extension, true)."</br>");
				if ($this->devlog) log_message("error",__line__." - "."cuenta positions ".print_r((count($extension)-1), true)."</br>");
				if ($this->devlog) log_message("error",__line__." - "."extension a guardar ".print_r($extension[(count($extension)-1)], true)."</br>");
				$extension	= $extension[(count($extension)-1)];
				if ($this->devlog) log_message("error",__line__." - "."extension ".print_r($extension, true)."</br>");

				$hash = date("YmdHis");

				$nombre 		= $hash."_".$this->ni."_".$file_numImg_pas.".".$extension;
				if ($this->devlog) log_message("error",__line__." - "."nombre ".print_r($nombre, true)."</br>");
				
				$nombreORI 		= $RutaUpload."ORI/".$nombre;
				$nombreIMG 		= $RutaUpload."IMG/".$nombre;
				$nombreTHU 		= $RutaUpload."THU/".$nombre;
				
				$nombreIMGdef 	= $RutaUpload."IMG/".$nombre;
				$nombreTHUdef 	= $RutaUpload."THU/".$nombre;

				if ($this->devlog) log_message("error",__line__." - "."nombreORI ".$nombreORI."</br>");
				if ($this->devlog) log_message("error",__line__." - "."nombreIMG ".$nombreIMG."</br>");
				if ($this->devlog) log_message("error",__line__." - "."nombreTHU ".$nombreTHU."</br>");
				
				if ($nombreORI != "" && file_exists($nombreORI)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero original </br>");
					unlink($nombreORI);
				}

				if ($nombreIMG != "" && file_exists($nombreIMG)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Imagen </br>");
					unlink($nombreIMG);
				}    

				if ($nombreTHU != "" && file_exists($nombreTHU)) {
					if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Thumbnail</br>");
					unlink($nombreTHU);
				} 
				
				$config['file_name']  			= $nombre;

				if ($this->devlog) log_message("error",__line__." - "."config ".print_r($config, true)."</br>");

				$this->upload->initialize($config);

				if (!$this->upload->do_upload('files')) {
					$error = array('error' => $this->upload->display_errors());
					if ($this->devlog) log_message("error",__line__." - "."ERROR </br>".print_r($error,true));
					$this->data['error'] = array('error' => $this->upload->display_errors('<div class="alert alert-danger">', '</div>'));
					echo "Error - ".$this->upload->display_errors()." - "; //error
				} else {
					if ($this->devlog) log_message("error",__line__." - "."OK ");
					
					$imagenes_viejas = $this->inscripcion_model->get_ImatgesInscripcio($this->ni, $tipo);
					if ($this->devlog) log_message("error",__line__." - "."imagenes_viejas ".print_r($imagenes_viejas, true)."</br>");
					
					if ($imagenes_viejas !== false) {
						// la foto
						$nombre_limpieza = explode("/", $imagenes_viejas->Foto);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
								unlink($RutaUpload."IMG/".$nombre_limpieza);
							}
						}

						// el thumb
						$nombre_limpieza = explode("/", $imagenes_viejas->Thumb);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."THU/".$nombre_limpieza)) {
								unlink($RutaUpload."THU/".$nombre_limpieza);
							}
						}
					}
					
					$procesar = true;
					if (($_FILES["files"]["type"] == "application/pdf")) {
						if ($this->devlog) log_message("error",__line__." - "."pdf");
					
						copy($nombreORI, $nombreIMG);
						if ($this->devlog) log_message("error",__line__." - "."copiado ".$nombreORI.", ".$nombreIMG);
						
						$nombreTHU = $RutaUpload."THU/nofoto35.png";

						if ($nombreORI != "" && file_exists($nombreORI)) {
							unlink($nombreORI);
						}
					} else {
						if ($this->devlog) log_message("error",__line__." - "."Otra ".$tipo." - ".print_r($_FILES, true));
						
						if (($tipo == "IMG") or ($tipo == "DNIA") or ($tipo == "DNIR")) {
							/*
							if ($this->devlog) log_message("error",__line__." - "."imagen");
							
							copy($nombreORI, $nombreIMG);
							if ($this->devlog) log_message("error",__line__." - "."copiada ".$nombreORI.", ".$nombreIMG);
							*/
							if ($this->devlog) log_message("error",__line__." - "."imagen");
							$this->load->library('image_lib'); 
							
							if ($this->devlog) log_message("error",__line__." - "."Creando IMAGEN");
							////[ THUMB IMAGE ]
							$configPASS['image_library'] = 'gd2';
							$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$configPASS['new_image'] = $nombreIMG;
							$configPASS['maintain_ratio'] = TRUE;
							$configPASS['overwrite'] = TRUE;
							$configPASS['width'] = 1024;
							//$configPASS['height'] = 768;
							$this->image_lib->initialize($configPASS);
		
							if ( !$this->image_lib->resize()){
								if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								$nombreIMG 		= "error";
								$nombreTHU 		= "error";
								$procesar = false;
							}

							if ($tipo == "IMG") {
								if ($this->devlog) log_message("error",__line__." - "."Creando THUMB");
								////[ THUMB IMAGE ]
								$configTHUMB['image_library'] = 'gd2';
								$configTHUMB['source_image'] = $this->upload->upload_path.$this->upload->file_name;
								$configTHUMB['new_image'] = $nombreTHU;
								$configTHUMB['maintain_ratio'] = TRUE;
								$configTHUMB['create_thumb'] = TRUE;
								$configTHUMB['thumb_marker'] = '';
								$configTHUMB['overwrite'] = TRUE;
								$configTHUMB['width'] = 150;
								//$configTHUMB['height'] = 100;
								$this->image_lib->initialize($configTHUMB); 
			
								if ( !$this->image_lib->resize()){
									if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								}
							}
						}

						if ($tipo == "PASS") {
							if ($this->devlog) log_message("error",__line__." - "."pasaporte");
							$this->load->library('image_lib'); 
							
							if ($this->devlog) log_message("error",__line__." - "."Creando PASSAPORTE");
							////[ THUMB IMAGE ]
							$configPASS['image_library'] = 'gd2';
							$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$configPASS['new_image'] = $nombreIMG;
							$configPASS['maintain_ratio'] = TRUE;
							$configPASS['overwrite'] = TRUE;
							$configPASS['width'] = 1024;
							$configPASS['height'] = 768;
							$this->image_lib->initialize($configPASS);
		
							if ( !$this->image_lib->resize()){
								if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
							}

							if ($this->devlog) log_message("error",__line__." - "."Creando THUMB");
							////[ THUMB IMAGE ]
							$configTHUMB['image_library'] = 'gd2';
							$configTHUMB['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$configTHUMB['new_image'] = $nombreTHU;
							$configTHUMB['maintain_ratio'] = TRUE;
							$configTHUMB['create_thumb'] = TRUE;
							$configTHUMB['thumb_marker'] = '';
							$configTHUMB['overwrite'] = TRUE;
							$configTHUMB['width'] = 150;
							$configTHUMB['height'] = 100;
							$this->image_lib->initialize($configTHUMB); 
		
							if ( !$this->image_lib->resize()){
								if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
							}
						}

						if ($tipo == "CVAC") {
							if ($this->devlog) log_message("error",__line__." - "."imagen");
							$this->load->library('image_lib'); 
							
							if ($this->devlog) log_message("error",__line__." - "."Creando IMAGEN");
							////[ THUMB IMAGE ]
							$configPASS['image_library'] = 'gd2';
							$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
							$configPASS['new_image'] = $nombreIMG;
							$configPASS['maintain_ratio'] = TRUE;
							$configPASS['overwrite'] = TRUE;
							$configPASS['width'] = 1024;
							//$configPASS['height'] = 768;
							$this->image_lib->initialize($configPASS);
		
							if ( !$this->image_lib->resize()){
								if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
							}
						}

						if ($nombreORI != "" && file_exists($nombreORI)) {
							unlink($nombreORI);
						}
					}

					if ($procesar) {
						$nombreIMG 		= base_url($RutaUpload."IMG/").$nombre;
						$nombreTHU 		= base_url($RutaUpload."THU/").$nombre;
					
						if ($tipo != "CVAC") {
							$this->inscripcion_model->update_ImatgeInscripcio($this->ni, $nombreIMG, $nombreTHU, $tipo);
						} else {
							$this->inscripcion_model->set_Inscripcions_cvac($this->ni, $nombreIMG);

							$datos_cvac = $this->inscripcion_model->get_Inscripcions_cvac($this->ni);
							
							$entrar = false;
							if (!isset($datos_cvac)) {}
							else if (count($datos_cvac) >= 1) {
								$entrar = true;
								$x=1;
								foreach ($datos_cvac as $cvac) {
									$this->montar_div_cvac($cvac, $x, true, $ruta);
									$x++;
								}
							}

							if (!$entrar) {
								echo "<li><strong>".lang("no_cvac")."</strong></li>";
							}
						}
					}

					/*if (($tipo == "DNIA") or ($tipo == "DNIR") or ($tipo == "CVAC")) {
						$nombreTHU	=$nombreIMG;
					}*/
					
				}
				if ($tipo != "CVAC") {
					echo $nombreIMG;
				}
			}
		}

		function montar_div_cvac($cvac, $x, $permitirModificacion, $ruta) {
			// if ($this->devlog) log_message("error",__line__." - pintamos un DivCVac ".print_r($cvac, true)." - x ".$x." - permitirModificacion ".$permitirModificacion." - ruta ".$ruta."</br>");
			$params = array("cvac"					=> $cvac,
							"x"						=> 	$x,
							"permitirModificacion"	=> $permitirModificacion);
			$this->load->view($ruta."/div_cvac_view", $params);
		}

		
        public function upload_uikit($params, $tipo, $forzar = false) {
			$RutaUpload = 'UploadedFiles/';

            if ($this->devlog) log_message("error",__line__." - "."Doing </br>");
			
			$ruta = $this->ruta();

			if ($this->devlog) log_message("error",__line__." - "."ni ".$this->ni."</br>");
			if ($this->devlog) log_message("error",__line__." - "."tr ".$this->tr."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ip ".$this->ip."</br>");
			if ($this->devlog) log_message("error",__line__." - "."ic ".$this->ic."</br>");
			if ($this->devlog) log_message("error",__line__." - "."forzar ".$this->forzar."</br>");
			
			if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			} else {
				if ($this->devlog) log_message("error",__line__." - ".$ruta."</br>");

				if ($this->devlog) log_message("error",__line__." - "."_FILES ".print_r($_FILES, true)."</br>");
				
				if ($this->devlog) log_message("error",__line__." - "."tipo ".$tipo."</br>");
				switch ($tipo) {
					case "IMG":
						$nom_camp			= "files";
						$file_numImg_pas 	= "1";
						$tipos_auxiliares	= "";
					break;

					case "PASS":
						$nom_camp			= "files";
						$file_numImg_pas 	= "2";
						$tipos_auxiliares	= "pdf";
					break;

					default:
						log_message("error",__line__." - "."hacking attempt!");
				
						die("haking Attempt!!");
					break;
				}

				$this->load->library('upload');				

				$file = $_FILES;
				for ($x = 0; $x < count($_FILES["files"]["name"]); $x++) {

					$config['upload_path']	 		= './'.$RutaUpload.'ORI/';
					$config['allowed_types']	 	= 'gif|jpg|jpeg|png|pdf';
					$config['max_size']				= '2500';
					
					$_FILES["files"]["name"] 		= $file["files"]["name"][$x];
					$_FILES["files"]["type"] 		= $file["files"]["type"][$x];
					$_FILES["files"]["tmp_name"]	= $file["files"]["tmp_name"][$x];
					$_FILES["files"]["error"] 		= $file["files"]["error"][$x];
					$_FILES["files"]["size"] 		= $file["files"]["size"][$x];
				
					$extension 	= explode(".", $_FILES["files"]["name"]);
					if ($this->devlog) log_message("error",__line__." - "."extension exploded ".print_r($extension, true)."</br>");
					if ($this->devlog) log_message("error",__line__." - "."cuenta positions ".print_r((count($extension)-1), true)."</br>");
					if ($this->devlog) log_message("error",__line__." - "."extension a guardar ".print_r($extension[(count($extension)-1)], true)."</br>");
					$extension	= $extension[(count($extension)-1)];
					if ($this->devlog) log_message("error",__line__." - "."extension ".print_r($extension, true)."</br>");
	
					$hash = date("YmdHis");
	
					$nombre 		= $hash."_".$this->ni."_".$file_numImg_pas.".".$extension;
					if ($this->devlog) log_message("error",__line__." - "."nombre ".print_r($nombre, true)."</br>");
					
					$nombreORI 		= $RutaUpload."ORI/".$nombre;
					$nombreIMG 		= $RutaUpload."IMG/".$nombre;
					$nombreTHU 		= $RutaUpload."THU/".$nombre;
					
					$nombreIMGdef 	= $RutaUpload."IMG/".$nombre;
					$nombreTHUdef 	= $RutaUpload."THU/".$nombre;
	
					if ($this->devlog) log_message("error",__line__." - "."nombreORI ".$nombreORI."</br>");
					if ($this->devlog) log_message("error",__line__." - "."nombreIMG ".$nombreIMG."</br>");
					if ($this->devlog) log_message("error",__line__." - "."nombreTHU ".$nombreTHU."</br>");
					
					if ($nombreORI != "" && file_exists($nombreORI)) {
						if ($this->devlog) log_message("error",__line__." - "." borramos el fichero original </br>");
						unlink($nombreORI);
					}
	
					if ($nombreIMG != "" && file_exists($nombreIMG)) {
						if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Imagen </br>");
						unlink($nombreIMG);
					}    
	
					if ($nombreTHU != "" && file_exists($nombreTHU)) {
						if ($this->devlog) log_message("error",__line__." - "." borramos el fichero Thumbnail</br>");
						unlink($nombreTHU);
					} 
					
					$config['file_name']  			= $nombre;

					if ($this->devlog) log_message("error",__line__." - "."config ".print_r($config, true)."</br>");

					$this->upload->initialize($config);

					if (!$this->upload->do_upload('files')) {
						$error = array('error' => $this->upload->display_errors());
						if ($this->devlog) log_message("error",__line__." - "."ERROR </br>".print_r($error,true));
						$this->data['error'] = array('error' => $this->upload->display_errors('<div class="alert alert-danger">', '</div>'));
						echo "error - "; //error
					} else {
						if ($this->devlog) log_message("error",__line__." - "."OK ");

						$imagenes_viejas = $this->inscripcion_model->get_ImatgesInscripcio($this->ni, $tipo);
						if ($this->devlog) log_message("error",__line__." - "."imagenes_viejas ".print_r($imagenes_viejas, true)."</br>");
						
						if ($imagenes_viejas !== false) {
							// la foto
							$nombre_limpieza = explode("/", $imagenes_viejas->Foto);
							$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
							if ($nombre_limpieza != "") {
								if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
									unlink($RutaUpload."IMG/".$nombre_limpieza);
								}
							}

							// el thumb
							$nombre_limpieza = explode("/", $imagenes_viejas->Thumb);
							$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
							if ($nombre_limpieza != "") {
								if (file_exists($RutaUpload."THU/".$nombre_limpieza)) {
									unlink($RutaUpload."THU/".$nombre_limpieza);
								}
							}
						}
						
						if (($_FILES["files"]["type"] == "application/pdf")) {
							if ($this->devlog) log_message("error",__line__." - "."pdf");
						
							copy($nombreORI, $nombreIMG);
							if ($this->devlog) log_message("error",__line__." - "."copiado ".$nombreORI.", ".$nombreIMG);
							
							$nombreTHU = $RutaUpload."THU/nofoto35.png";

							if ($nombreORI != "" && file_exists($nombreORI)) {
								unlink($nombreORI);
							}
						} else {
							if ($this->devlog) log_message("error",__line__." - "."Otra ".$tipo." - ".print_r($_FILES, true));
							
							if ($tipo == "IMG") {
								/*
								if ($this->devlog) log_message("error",__line__." - "."imagen");
								
								copy($nombreORI, $nombreIMG);
								if ($this->devlog) log_message("error",__line__." - "."copiada ".$nombreORI.", ".$nombreIMG);
								*/
								if ($this->devlog) log_message("error",__line__." - "."imagen");
								$this->load->library('image_lib'); 
								
								if ($this->devlog) log_message("error",__line__." - "."Creando IMAGEN");
								////[ THUMB IMAGE ]
								$configPASS['image_library'] = 'gd2';
								$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
								$configPASS['new_image'] = $nombreIMG;
								$configPASS['maintain_ratio'] = TRUE;
								$configPASS['overwrite'] = TRUE;
								$configPASS['width'] = 1024;
								//$configPASS['height'] = 768;
								$this->image_lib->initialize($configPASS);
			
								if ( !$this->image_lib->resize()){
									if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								}

								if ($this->devlog) log_message("error",__line__." - "."Creando THUMB");
								////[ THUMB IMAGE ]
								$configTHUMB['image_library'] = 'gd2';
								$configTHUMB['source_image'] = $this->upload->upload_path.$this->upload->file_name;
								$configTHUMB['new_image'] = $nombreTHU;
								$configTHUMB['maintain_ratio'] = TRUE;
								$configTHUMB['create_thumb'] = TRUE;
								$configTHUMB['thumb_marker'] = '';
								$configTHUMB['overwrite'] = TRUE;
								$configTHUMB['width'] = 150;
								//$configTHUMB['height'] = 100;
								$this->image_lib->initialize($configTHUMB); 
			
								if ( !$this->image_lib->resize()){
									if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								}
							}

							if ($tipo == "PASS") {
								if ($this->devlog) log_message("error",__line__." - "."pasaporte");
								$this->load->library('image_lib'); 
								
								if ($this->devlog) log_message("error",__line__." - "."Creando PASSAPORTE");
								////[ THUMB IMAGE ]
								$configPASS['image_library'] = 'gd2';
								$configPASS['source_image'] = $this->upload->upload_path.$this->upload->file_name;
								$configPASS['new_image'] = $nombreIMG;
								$configPASS['maintain_ratio'] = TRUE;
								$configPASS['overwrite'] = TRUE;
								$configPASS['width'] = 1024;
								$configPASS['height'] = 768;
								$this->image_lib->initialize($configPASS);
			
								if ( !$this->image_lib->resize()){
									if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								}

								if ($this->devlog) log_message("error",__line__." - "."Creando THUMB");
								////[ THUMB IMAGE ]
								$configTHUMB['image_library'] = 'gd2';
								$configTHUMB['source_image'] = $this->upload->upload_path.$this->upload->file_name;
								$configTHUMB['new_image'] = $nombreTHU;
								$configTHUMB['maintain_ratio'] = TRUE;
								$configTHUMB['create_thumb'] = TRUE;
								$configTHUMB['thumb_marker'] = '';
								$configTHUMB['overwrite'] = TRUE;
								$configTHUMB['width'] = 150;
								$configTHUMB['height'] = 100;
								$this->image_lib->initialize($configTHUMB); 
			
								if ( !$this->image_lib->resize()){
									if ($this->devlog) log_message("error",__line__." - "."Error Creando Thumb ".$this->image_lib->display_errors('', ''));
								}
							}
							if ($nombreORI != "" && file_exists($nombreORI)) {
								unlink($nombreORI);
							}
						}

						$nombreIMG 		= base_url($RutaUpload."IMG/").$nombre;
						$nombreTHU 		= base_url($RutaUpload."THU/").$nombre;

						$this->inscripcion_model->update_ImatgeInscripcio($this->ni, $nombreIMG, $nombreTHU, $tipo);
					}
				}
				echo $nombreTHU;
			}
		}

		public function dnidelete($params, $test) {
			$RutaUpload = 'UploadedFiles/';

			$ruta = $this->ruta();
			
			if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0) or (!isset($_POST["param1"])) or (!isset($_POST["param2"]))) {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			} else {
				
				if ($this->devlog) log_message("error",__line__." - ".$ruta."</br>");

				$selector=$this->soporte_xss->xss_check($_POST["param1"]);
				$tipo=$this->soporte_xss->xss_check($_POST["param2"]);
				
				if ($this->devlog) log_message("error",__line__." - "."tipo ".$tipo."</br>");
				if (!is_numeric($tipo) or (is_numeric($tipo) and ($tipo > 5 or $tipo < 1))) {
					log_message("error",__line__." - "."hacking attempt!");
			
					die("haking Attempt!!");
				}

				if ($this->devlog) log_message("error",__line__." - ". "Tipo ".$tipo."</br>");
				
				$url_borrado1 = "";
				$url_borrado2 = "";
				switch ($tipo) {
					case 3:
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->dni_delantero;
						$textTipo ="DNIA";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nodnia150.gif";
					break;

					case 4:
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->dni_trasero;
						$textTipo ="DNIR";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nodnir150.gif";
					break;
				}
				
				if (count($datos) > 0) {
					if ($url_borrado1 != "") {
						$nombre_limpieza = explode("/", $url_borrado1);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($this->devlog) log_message("error",__line__." - ". "nombre_limpieza ".print_r($nombre_limpieza,true)."</br>");
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
								unlink($RutaUpload."IMG/".$nombre_limpieza);
							}
						}
					}
				}

				$this->inscripcion_model->update_ImatgeViatge($this->ni, "", "", $textTipo);
				echo $nombreRetorno;
			}			
		}

		public function delete($params, $test) {
			$RutaUpload = 'UploadedFiles/';

			$ruta = $this->ruta();
			
			if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0) or (!isset($_POST["param1"])) or (!isset($_POST["param2"]))) {
				if ($this->devlog) log_message("error",__line__." - "."acceso denegado");

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			} else {
				
				if ($this->devlog) log_message("error",__line__." - ".$ruta."</br>");

				$selector=$this->soporte_xss->xss_check($_POST["param1"]);
				$tipo=$this->soporte_xss->xss_check($_POST["param2"]);
				
				if ($this->devlog) log_message("error",__line__." - "."tipo ".$tipo."</br>");
				if (!is_numeric($tipo) or (is_numeric($tipo) and ($tipo > 5 or $tipo < 1))) {
					log_message("error",__line__." - "."hacking attempt!");
			
					die("haking Attempt!!");
				}

				if ($this->devlog) log_message("error",__line__." - ". "Tipo ".$tipo."</br>");
				
				$url_borrado1 = "";
				$url_borrado2 = "";
				switch ($tipo) {
					case 1:
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->foto;
						$url_borrado2 = $datos[0]->thumbnail;
						$textTipo ="IMG";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nofoto150.png";
					break;
					
					case 2: // esto ya lo arreglaremos cuando lo hagamos en idiomas
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->foto;
						$url_borrado2 = $datos[0]->thumbnail;
						$textTipo ="PASS";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nofoto150.png";
					break;

					case 3:
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->dni_delantero;
						$textTipo ="DNIA";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nodnia150.gif";
					break;

					case 4:
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->dni_trasero;
						$textTipo ="DNIR";
						$nombreRetorno = base_url()."UploadedFiles/IMG/nodnir150.gif";
					break;

					case 5:					
						$datos = $this->inscripcion_model->get_Inscripcions_cvac($this->ni, $selector);
						if ($this->devlog) log_message("error",__line__." - ". "datos ".count($datos)."</br>");
						if ($this->devlog) log_message("error",__line__." - ". "datos ".print_r($datos,true)."</br>");
						$url_borrado1 = $datos[0]->url;
						$textTipo ="CVAC";
					break;
				}
				
				if (count($datos) > 0) {
					if ($url_borrado1 != "") {
						$nombre_limpieza = explode("/", $url_borrado1);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($this->devlog) log_message("error",__line__." - ". "nombre_limpieza ".print_r($nombre_limpieza,true)."</br>");
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
								unlink($RutaUpload."IMG/".$nombre_limpieza);
							}
						}
					}

					if ($url_borrado2 != "") {
						$nombre_limpieza = explode("/", $url_borrado2);
						$nombre_limpieza = $nombre_limpieza[count($nombre_limpieza) - 1];
						if ($nombre_limpieza != "") {
							if (file_exists($RutaUpload."IMG/".$nombre_limpieza)) {
								unlink($RutaUpload."IMG/".$nombre_limpieza);
							}
						}
					}
				}

				if ($tipo != 5) {
					$this->inscripcion_model->update_ImatgeInscripcio($this->ni, "", "", $textTipo);
					echo $nombreRetorno;
				} else {
					$this->inscripcion_model->delete_Inscripcions_cvac($this->ni, $selector);

					$datos_cvac = $this->inscripcion_model->get_Inscripcions_cvac($this->ni);
					
					$entrar = false;
					if (!isset($datos_cvac)) {}
					else if (count($datos_cvac) >= 1) {
						$entrar = true;
						$x=1;
						foreach ($datos_cvac as $cvac) {
							$this->montar_div_cvac($cvac, $x, true, $ruta);
							$x++;
						}
					}

					if (!$entrar) {
						$this->lang->load($ruta.'/inscripcion', $this->idioma);
						echo "<li><strong>".lang("no_cvac")."</strong></li>";
					}
				}
			}			
		}
    }
?>