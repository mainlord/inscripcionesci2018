<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Doencode extends CI_Controller {

		protected $devlog = false;
		protected $screenlog = true;
		
		public function __construct() {
			parent::__construct();
			
			$this->soporte_xss->inicializar();            
			if ($this->devlog) log_message("error",__line__." - "."<pre>");

			$this->load->model('doencode_model');

			// $params = $this->soporte_xss->decode($texto, true);
        }

		public function index()	{
			die("hacking Attempt");
        }

		public function nextColonias($limit = 5) {

			$this->logar("<pre>Vamos a iniciar la encriptacion de la tabla inscripciones de Colonias.</br>", "");
			
			$cuenta = $this->doencode_model->getCuentaColonias();

			$this->logar("vamos a procesar ".$limit."/".$cuenta." registros</br>", __line__);

			$datos = $this->doencode_model->getNextColonias($limit);

			$this->logar("inscripciones a procesar ".count($datos)."</br>", __line__);

			foreach ($datos as $dato) {
				$this->logar("inscripcion ".print_r($dato,true)."</br>", __line__);

				$actualizar = array(	"dni"							=> $this->soporte_xss->encode($dato->dni),
										"nom_complet"					=> $this->soporte_xss->encode($dato->nom_complet),
										"telefon"						=> $this->soporte_xss->encode($dato->telefon),
										"adressa"						=> $this->soporte_xss->encode($dato->adressa),
										"poblacio"						=> $this->soporte_xss->encode($dato->poblacio),
										"cp"							=> $this->soporte_xss->encode($dato->cp),
										"altres_telefons"				=> $this->soporte_xss->encode($dato->altres_telefons),
										"foto"							=> $this->soporte_xss->encode($dato->foto),
										"thumbnail"						=> $this->soporte_xss->encode($dato->thumbnail),
										"malalt_detall"					=> $this->soporte_xss->encode($dato->malalt_detall),
										"medicaments_detall"			=> $this->soporte_xss->encode($dato->medicaments_detall),
										"medicaments_administracio"		=> $this->soporte_xss->encode($dato->medicaments_administracio),
										"regim_detall"					=> $this->soporte_xss->encode($dato->regim_detall),
										"desordre_alimentari_detall"	=> $this->soporte_xss->encode($dato->desordre_alimentari_detall),
										"operat_detall"					=> $this->soporte_xss->encode($dato->operat_detall),
										"alergic_detall"				=> $this->soporte_xss->encode($dato->alergic_detall),
										"angles_fora_detall"			=> $this->soporte_xss->encode($dato->angles_fora_detall),
										"autoritzacio_nom"				=> $this->soporte_xss->encode($dato->autoritzacio_nom),
										"autoritzacio_dni"				=> $this->soporte_xss->encode($dato->autoritzacio_dni),
										"autoritzacio_poblacio"			=> $this->soporte_xss->encode($dato->autoritzacio_poblacio),
										"observaciones_hipica"			=> $this->soporte_xss->encode($dato->observaciones_hipica),
										"mallorca_dni"					=> $this->soporte_xss->encode($dato->mallorca_dni),
										"observaciones_generales"		=> $this->soporte_xss->encode($dato->observaciones_generales),
										"email"							=> $this->soporte_xss->encode($dato->email),
										"sotatractamentdetall"			=> $this->soporte_xss->encode($dato->sotatractamentdetall),
										"dificultatesports_quins"		=> $this->soporte_xss->encode($dato->dificultatesports_quins),
										"observaciones"					=> $this->soporte_xss->encode($dato->observaciones),
										"otros"							=> $this->soporte_xss->encode($dato->otros),
										"dni_delantero"					=> $this->soporte_xss->encode($dato->dni_delantero),
										"dni_trasero"					=> $this->soporte_xss->encode($dato->dni_trasero)
				);
				$this->logar("actualizar ".print_r($actualizar,true)."</br>", __line__);

				$this->logar("Actualizamos...</br>", __line__);
				$this->doencode_model->setEncColonias(  $dato->idinscripciones, $dato->idpeople, $dato->control, $actualizar );

				$this->logar("recuperamos...</br>", __line__);
				$actualizado = $this->doencode_model->getEncColonias(  $dato->idinscripciones, $dato->idpeople, $dato->control);
				$desactualizar = array(	"dni"							=> $this->soporte_xss->decode($actualizado["dni"]),
										"nom_complet"					=> $this->soporte_xss->decode($actualizado["nom_complet"]),
										"telefon"						=> $this->soporte_xss->decode($actualizado["telefon"]),
										"adressa"						=> $this->soporte_xss->decode($actualizado["adressa"]),
										"poblacio"						=> $this->soporte_xss->decode($actualizado["poblacio"]),
										"cp"							=> $this->soporte_xss->decode($actualizado["cp"]),
										"altres_telefons"				=> $this->soporte_xss->decode($actualizado["altres_telefons"]),
										"foto"							=> $this->soporte_xss->decode($actualizado["foto"]),
										"thumbnail"						=> $this->soporte_xss->decode($actualizado["thumbnail"]),
										"malalt_detall"					=> $this->soporte_xss->decode($actualizado["malalt_detall"]),
										"medicaments_detall"			=> $this->soporte_xss->decode($actualizado["medicaments_detall"]),
										"medicaments_administracio"		=> $this->soporte_xss->decode($actualizado["medicaments_administracio"]),
										"regim_detall"					=> $this->soporte_xss->decode($actualizado["regim_detall"]),
										"desordre_alimentari_detall"	=> $this->soporte_xss->decode($actualizado["desordre_alimentari_detall"]),
										"operat_detall"					=> $this->soporte_xss->decode($actualizado["operat_detall"]),
										"alergic_detall"				=> $this->soporte_xss->decode($actualizado["alergic_detall"]),
										"angles_fora_detall"			=> $this->soporte_xss->decode($actualizado["angles_fora_detall"]),
										"autoritzacio_nom"				=> $this->soporte_xss->decode($actualizado["autoritzacio_nom"]),
										"autoritzacio_dni"				=> $this->soporte_xss->decode($actualizado["autoritzacio_dni"]),
										"autoritzacio_poblacio"			=> $this->soporte_xss->decode($actualizado["autoritzacio_poblacio"]),
										"observaciones_hipica"			=> $this->soporte_xss->decode($actualizado["observaciones_hipica"]),
										"mallorca_dni"					=> $this->soporte_xss->decode($actualizado["mallorca_dni"]),
										"observaciones_generales"		=> $this->soporte_xss->decode($actualizado["observaciones_generales"]),
										"email"							=> $this->soporte_xss->decode($actualizado["email"]),
										"sotatractamentdetall"			=> $this->soporte_xss->decode($actualizado["sotatractamentdetall"]),
										"dificultatesports_quins"		=> $this->soporte_xss->decode($actualizado["dificultatesports_quins"]),
										"observaciones"					=> $this->soporte_xss->decode($actualizado["observaciones"]),
										"otros"							=> $this->soporte_xss->decode($actualizado["otros"]),
										"dni_delantero"					=> $this->soporte_xss->decode($actualizado["dni_delantero"]),
										"dni_trasero"					=> $this->soporte_xss->decode($actualizado["dni_trasero"])
				);
				$this->logar("desactualizar ".print_r($desactualizar,true)."</br>", __line__);

			}

		}

		public function nextIdiomas($limit = 5) {

			$this->logar("<pre>Vamos a iniciar la encriptacion de la tabla inscripciones de Idiomas</br>", "");
			
			$cuenta = $this->doencode_model->getCuentaIdiomas();

			$this->logar("vamos a procesar ".$limit."/".$cuenta." registros</br>", __line__);

			$datos = $this->doencode_model->getNextIdiomas($limit);

			$this->logar("inscripciones a procesar ".count($datos)."</br>", __line__);

			foreach ($datos as $dato) {
				$this->logar("inscripcion ".print_r($dato,true)."</br>", __line__);

				$actualizar = array(	"dni"									=> $this->soporte_xss->encode($dato->dni),
										"nombre_amigo"							=> $this->soporte_xss->encode($dato->nombre_amigo),
										"nombre_participante"					=> $this->soporte_xss->encode($dato->nombre_participante),
										"apellidos_participante"				=> $this->soporte_xss->encode($dato->apellidos_participante),
										"direccion_participante"				=> $this->soporte_xss->encode($dato->direccion_participante),
										"codigo_postal"							=> $this->soporte_xss->encode($dato->codigo_postal),
										"poblacion"								=> $this->soporte_xss->encode($dato->poblacion),
										"telefono"								=> $this->soporte_xss->encode($dato->telefono),
										"movil"									=> $this->soporte_xss->encode($dato->movil),
										"edad_participante"						=> $this->soporte_xss->encode($dato->edad_participante),
										"nacionalidad"							=> $this->soporte_xss->encode($dato->nacionalidad),
										"movil_participantes"					=> $this->soporte_xss->encode($dato->movil_participantes),
										"email_participantes"					=> $this->soporte_xss->encode($dato->email_participantes),
										"movil_padres"							=> $this->soporte_xss->encode($dato->movil_padres),
										"nombre_padres"							=> $this->soporte_xss->encode($dato->nombre_padres),
										"email_padres"							=> $this->soporte_xss->encode($dato->email_padres),
										"dni_padres"							=> $this->soporte_xss->encode($dato->dni_padres),
										"algun_curso_detalle"					=> $this->soporte_xss->encode($dato->algun_curso_detalle),
										"algun_curso_detalle2"					=> $this->soporte_xss->encode($dato->algun_curso_detalle2),
										"alguna_enfermedad_detalle"				=> $this->soporte_xss->encode($dato->alguna_enfermedad_detalle),
										"dieta_especial_detalle"				=> $this->soporte_xss->encode($dato->dieta_especial_detalle),
										"algun_medicamento_detalle"				=> $this->soporte_xss->encode($dato->algun_medicamento_detalle),
										"alguna_alergia_detalle"				=> $this->soporte_xss->encode($dato->alguna_alergia_detalle),
										"colegio"								=> $this->soporte_xss->encode($dato->colegio),
										"direccion_colegio"						=> $this->soporte_xss->encode($dato->direccion_colegio),
										"cp_colegio"							=> $this->soporte_xss->encode($dato->cp_colegio),
										"poblacion_colegio"						=> $this->soporte_xss->encode($dato->poblacion_colegio),
										"nombre_profesor"						=> $this->soporte_xss->encode($dato->nombre_profesor),
										"curso"									=> $this->soporte_xss->encode($dato->curso),
										"nombre_autorizacion_padre"				=> $this->soporte_xss->encode($dato->nombre_autorizacion_padre),
										"dni_autorizacion_padres"				=> $this->soporte_xss->encode($dato->dni_autorizacion_padres),
										"nombre_autorizacion_padre_medica"		=> $this->soporte_xss->encode($dato->nombre_autorizacion_padre_medica),
										"dni_autorizacion_padres_medica"		=> $this->soporte_xss->encode($dato->dni_autorizacion_padres_medica),
										"observaciones"							=> $this->soporte_xss->encode($dato->observaciones),
										"otros"									=> $this->soporte_xss->encode($dato->otros),
										"foto"									=> $this->soporte_xss->encode($dato->foto),
										"mini_foto"								=> $this->soporte_xss->encode($dato->mini_foto),
										"copia_pasaporte"						=> $this->soporte_xss->encode($dato->copia_pasaporte),
										"mini_pasaporte"						=> $this->soporte_xss->encode($dato->mini_pasaporte)
				);
				$this->logar("actualizar ".print_r($actualizar,true)."</br>", __line__);

				$this->logar("Actualizamos...</br>", __line__);
				$this->doencode_model->setEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control, $actualizar );

				$this->logar("recuperamos...</br>", __line__);
				$actualizado = $this->doencode_model->getEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control);
				$desactualizar = array(	"dni"									=> $this->soporte_xss->decode($actualizado["dni"]),
										"nombre_amigo"							=> $this->soporte_xss->decode($actualizado["nombre_amigo"]),
										"nombre_participante"					=> $this->soporte_xss->decode($actualizado["nombre_participante"]),
										"apellidos_participante"				=> $this->soporte_xss->decode($actualizado["apellidos_participante"]),
										"direccion_participante"				=> $this->soporte_xss->decode($actualizado["direccion_participante"]),
										"codigo_postal"							=> $this->soporte_xss->decode($actualizado["codigo_postal"]),
										"poblacion"								=> $this->soporte_xss->decode($actualizado["poblacion"]),
										"telefono"								=> $this->soporte_xss->decode($actualizado["telefono"]),
										"movil"									=> $this->soporte_xss->decode($actualizado["movil"]),
										"edad_participante"						=> $this->soporte_xss->decode($actualizado["edad_participante"]),
										"nacionalidad"							=> $this->soporte_xss->decode($actualizado["nacionalidad"]),
										"movil_participantes"					=> $this->soporte_xss->decode($actualizado["movil_participantes"]),
										"email_participantes"					=> $this->soporte_xss->decode($actualizado["email_participantes"]),
										"movil_padres"							=> $this->soporte_xss->decode($actualizado["movil_padres"]),
										"nombre_padres"							=> $this->soporte_xss->decode($actualizado["nombre_padres"]),
										"email_padres"							=> $this->soporte_xss->decode($actualizado["email_padres"]),
										"dni_padres"							=> $this->soporte_xss->decode($actualizado["dni_padres"]),
										"algun_curso_detalle"					=> $this->soporte_xss->decode($actualizado["algun_curso_detalle"]),
										"algun_curso_detalle2"					=> $this->soporte_xss->decode($actualizado["algun_curso_detalle2"]),
										"alguna_enfermedad_detalle"				=> $this->soporte_xss->decode($actualizado["alguna_enfermedad_detalle"]),
										"dieta_especial_detalle"				=> $this->soporte_xss->decode($actualizado["dieta_especial_detalle"]),
										"algun_medicamento_detalle"				=> $this->soporte_xss->decode($actualizado["algun_medicamento_detalle"]),
										"alguna_alergia_detalle"				=> $this->soporte_xss->decode($actualizado["alguna_alergia_detalle"]),
										"colegio"								=> $this->soporte_xss->decode($actualizado["colegio"]),
										"direccion_colegio"						=> $this->soporte_xss->decode($actualizado["direccion_colegio"]),
										"cp_colegio"							=> $this->soporte_xss->decode($actualizado["cp_colegio"]),
										"poblacion_colegio"						=> $this->soporte_xss->decode($actualizado["poblacion_colegio"]),
										"nombre_profesor"						=> $this->soporte_xss->decode($actualizado["nombre_profesor"]),
										"curso"									=> $this->soporte_xss->decode($actualizado["curso"]),
										"nombre_autorizacion_padre"				=> $this->soporte_xss->decode($actualizado["nombre_autorizacion_padre"]),
										"dni_autorizacion_padres"				=> $this->soporte_xss->decode($actualizado["dni_autorizacion_padres"]),
										"nombre_autorizacion_padre_medica"		=> $this->soporte_xss->decode($actualizado["nombre_autorizacion_padre_medica"]),
										"dni_autorizacion_padres_medica"		=> $this->soporte_xss->decode($actualizado["dni_autorizacion_padres_medica"]),
										"observaciones"							=> $this->soporte_xss->decode($actualizado["observaciones"]),
										"otros"									=> $this->soporte_xss->decode($actualizado["otros"]),
										"foto"									=> $this->soporte_xss->decode($actualizado["foto"]),
										"mini_foto"								=> $this->soporte_xss->decode($actualizado["mini_foto"]),
										"copia_pasaporte"						=> $this->soporte_xss->decode($actualizado["copia_pasaporte"]),
										"mini_pasaporte"						=> $this->soporte_xss->decode($actualizado["mini_pasaporte"])
				);
				$this->logar("desactualizar ".print_r($desactualizar,true)."</br>", __line__);
			}
		}

		public function logar($texto, $linea, $forzar = false) {
			if ($this->devlog) log_message("error", $linea." - ".$texto);
			if ($this->screenlog || $forzar) echo $linea." - ".$texto;
		}

		public function test() {
			if (isset($_POST["val"])) {
				$val = $_POST["val"];
				// convertimos arroba en @
				echo "<pre>";
					$val = str_replace("arroba", "@", $val);
					echo "Valor = ".$val."<br/>";
					$val = $this->soporte_xss->encode($val);
					echo "resultado = ".$val."<br/>";
					echo "resultado = ".$this->soporte_xss->decode($val)."<br/>";
				echo "</pre>";
			}
			?>
				<form action="" method="post">
					<textarea name="val" id="val" cols="30" rows="10"></textarea>
					<input type="submit" value="enviar">
				</form>
			<?php
		}

		public function test1($id="") {
			if ($id == "") {
				echo "hacking Attempt!";
			} else {
				$datos = $this->doencode_model->getOneIdiomas($id);

				$this->logar("<pre>",0);
				// $this->logar("inscripcion ".print_r($datos,true)."<br/>", __line__);
				foreach ($datos as $dato) {
					// $this->logar("inscripcion ".print_r($dato,true)."</br>", __line__);

					$this->logar("inscripcion ".print_r($dato->otras_consideraciones,true)."</br>", __line__);
	
					$actualizar = array("otras_consideraciones"	=> $this->soporte_xss->encode($dato->otras_consideraciones));

					$this->logar("actualizar ".print_r($actualizar,true)."</br>", __line__);

					// $this->logar("Actualizamos...</br>", __line__);
					$this->doencode_model->setEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control, $actualizar );

					$actualizado = $this->doencode_model->getEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control);
					
					$desactualizar = $this->soporte_xss->decode($actualizar["otras_consideraciones"]);

					$this->logar("comprobando: ".$desactualizar."</br>", __line__);
				}
			
				$this->logar("</pre>",0);

			}
		}

		public function testEdad($next="") {
			if ($next == "") {
				echo "hacking Attempt!";
			} else {
				$datos = $this->doencode_model->getNextIdiomasAge($next);

				$this->logar("<pre>Comenzamos a codificar la edad de las siguientes $next inscripciones",0);

				// $this->logar("inscripcion ".print_r($datos,true)."<br/>", __line__);
				foreach ($datos as $dato) {
					// $this->logar("inscripcion ".print_r($dato,true)."</br>", __line__);

					$this->logar("inscripcion ".print_r($dato->numero_reserva_localizador,true)." - ".print_r($dato->id_inscripcion,true)." - ".print_r($dato->edad_participante,true)."</br>", __line__);
	
					$actualizar = array("edad_participante"	=> $this->soporte_xss->encode($dato->edad_participante));

					$this->logar("actualizar ".print_r($actualizar,true)."</br>", __line__);

					// $this->logar("Actualizamos...</br>", __line__);
					$this->doencode_model->setEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control, $actualizar );

					$actualizado = $this->doencode_model->getEncIdiomas(  $dato->id_inscripcion, $dato->idpeople, $dato->control);
					
					$desactualizar = $this->soporte_xss->decode($actualizado["edad_participante"]);

					$this->logar("comprobando: ".$desactualizar."</br>", __line__);
				}
			
				$this->logar("</pre>",0);

			}
		}
	}
?>