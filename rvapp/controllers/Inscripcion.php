<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Inscripcion extends CI_Controller {

		protected $devlog = false;

		/* propiedades */
			protected $ni = 0;	// id de inscripcion
			protected $tr = 2;	// tipo de reserva  	2 Idiomas 	/ 1 colonias 	/ 4 Taf Hotelera
			protected $tv = 1;	// tipo de venta 		0 Rv 		/ 1 Blahblah
			protected $ip = 0;	// id de people
			protected $ic = 0;	// clave de control para asegurar que el registro que buscamos es el que toca
			protected $param_url = "";	// url encriptada original

			protected $idioma = 'es'; 	// idioma test
			protected $forzar = false; 	// usamos este parametro para forzar el acceso en edicion incluso cuando ha sido finalizada.
		/* FIN - propiedades */
		
		public function __construct() {
			parent::__construct();
			 
			$this->soporte_xss->inicializar();    
			$this->soporte_xss->sanitizar();
        

			/* logamos */ if ($this->devlog) echo __line__."<pre>";
			
			if ($this->uri->segment(5) == "true") {
				$this->forzar = true;
			}
			
			if (($this->uri->segment(3) != "") and ($this->uri->segment(4) != "")) {
				$texto = rawurldecode($this->uri->segment(3));
				$texto = rawurldecode($texto);
				$texto = base64_decode($texto);

				/* logamos */ if ($this->devlog) echo __line__."this->uri->segment(3) ".$texto."<br/>"; // primer parametro: array
				/* logamos */ if ($this->devlog) echo __line__."this->uri->segment(4) ".$this->uri->segment(4)."<br/>"; // segundo parametro: idioma

				$params = $this->soporte_xss->decode($texto, true);

				/* logamos */ if ($this->devlog) echo __line__.'sin decodificar json "'.print_r($params, true).'"\n';
				$params = str_replace("'", '"', $params);

				$params = json_decode($params);

				/* logamos */ if ($this->devlog) echo __line__.'decodificado json "'.print_r($params, true).'"\n';

				if ($params=="") {
					/* logamos */ if ($this->devlog) echo __line__.'PROVAMOS decodificado a la antigua\n';
					$texto = rawurldecode($this->uri->segment(3));
					$texto = rawurldecode($texto);
					//$texto = base64_decode($texto);
					
					$params = $this->soporte_xss->oldfashoned_decode($texto, true);
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: sin decodificar json "'.print_r($params, true).'"\n';
					$params = str_replace("'", '"', $params);
	
					$params = json_decode($params);
	
					/* logamos */ if ($this->devlog) echo __line__.'OLD FASHONED: decodificado json "'.print_r($params, true).'"\n';
				}
				
				if ($params != "") {
					$this->ni = $params->ni;
					$this->tr = $params->tr;
					$this->tv = $params->tv;
					$this->ip = $params->ip;
					$this->ic = $params->ic;
					$this->param_url = $this->uri->segment(3);
				}

				if ($this->tv == 1) { 		// COMPROBAMOS EL IDIOMA
					$this->idioma = "es"; 	// estamos hablando de Blahblah, solo castellano
				} else { 					// estamos hablando de otros, asi que validamos el parametro idioma
					$this->idioma = $this->soporte_xss->xss_check($this->uri->segment(4)); 
					if ($this->idioma == "es" or $this->idioma == "ca") {
						// todo ok
					} else {
						// aseguramos el idioma por defecto
						$this->idioma = "es";
					}
				} 


				if ($this->tv == 0) {
					switch($this->tr) {
						case 1:
						case 6:
							// colonias
							$this->load->model('inscripcion_colonias_model', "inscripcion_model");
							$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><label>', '</label></div>');
						break;

						case 2:
							// idiomas
							$this->load->model('inscripcion_idiomas_model',  "inscripcion_model");
							$this->form_validation->set_error_delimiters('<div class="uk-width-1-1"><div class="uk-alert uk-alert-danger">', '</div></div>');
						break;
						
						case 4:
							// Taf
							$this->load->model('inscripcion_taf_model', "inscripcion_model");
							$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><label>', '</label></div>');
						break;
					}
				} else {
					if ($this->tr == 2) {
						// Abroad
						$this->load->model('inscripcion_abroad_model',  "inscripcion_model");
					} else {
						// Spain
						$this->load->model('inscripcion_spain_model', "inscripcion_model");
					}
					$this->form_validation->set_error_delimiters('<div class="uk-width-1-1"><div class="uk-alert uk-alert-danger">', '</div></div>');					
				}
				//die("hasta aqui");
			} else {

			}
        }

		/* GENERICOS */
			public function index()	{
				// $this->load->view('welcome_message');
				$ruta = $this->ruta();

				$this->lang->load($ruta.'/acceso', $this->idioma);			
				/* logamos */ if ($this->devlog) echo __line__."INDEX:>><br/>";

				$params = array("lang"		=> $this->idioma,
								"control"	=> __LINE__." MensajeNoExiste",
								"mensaje"	=> lang("MensajeNoExiste"));
				$this->load->view($ruta."/mensaje_generico_view", $params);
			}

			public function get_url($tv, $tr, $centro, $colectivo = 0) {
				switch ($tv) {
					case '0': // Venta Rosa dels vents
						switch ($tr) {
							case '1': // Colonias
							case '6': // Ampas
								if ($colectivo == 0) {
									return "https://www.rosadelsvents.es/inscripcionesCI/pdf/normativa_".strtolower($this->idioma).".pdf";
								} else {
									return "https://www.rosadelsvents.es/inscripcionesCI/pdf/2019_Normativa.pdf";
								}
							break;
							
							case '2': // Idiomas
								return "https://www.rosadelsventsidiomas.es/reservaonline/themes/blue/conditions.php?hostel=".$centro."&lang=".strtolower($this->idioma);
							break;							

							case '4': // taf
								return ""; // no tiene nada
							break;							
						}
					break;

					case '1': // Venta Blah Blah
					break;
				}
				
			}

			public function acceso($parametro = "") {

				/* logamos */ if ($this->devlog) echo __line__."acceso ".$this->idioma;

				$ruta = $this->ruta();

				$this->lang->load($ruta.'/acceso', $this->idioma);
				
				if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
					/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

					$params = array("lang"		=> $this->idioma,
									"control"	=> __LINE__." MensajeNoExiste",
									"mensaje"	=> lang("MensajeNoExiste"));
					$this->load->view($ruta."/mensaje_generico_view", $params);				
				} else {
					/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
					$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, $this->forzar);
					// echo "datos ".print_r($datos,true)." ".count($datos)." ".$this->forzar;
					if ($datos <= 0) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						$control = __LINE__.($datos == 0) ? " MensajeYaFinalizado" : " MensajeNoExiste";
						$mensaje = ($datos == 0) ? lang("MensajeYaFinalizado") : lang("MensajeNoExiste");
						$params = array("lang"		=> $this->idioma,
										"control"	=> $control,
										"mensaje"	=> $mensaje);
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {

						log_message("error", "vamos a llamar a fecha_checkin");
						$retorno = $this->inscripcion_model->fecha_checkin($this->ni, $this->forzar);
						log_message("error", "ha retornado (".$retorno.")");
						if (!$retorno) {
							/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeTestOutDated",
											"mensaje"	=> lang("MensajeTestOutDated"));
							$this->load->view($ruta."/mensaje_generico_view", $params);
						} else {

							$z_zurich = $this->inscripcion_model->getIsZurich($this->ni, $this->forzar);
							$params = array("lang"		=> $this->idioma,
											"z_zurich"	=> $z_zurich,
											"control"	=> "acceso",
											"forzar" 	=> $this->forzar,
											"param_url"	=> $this->param_url);
							$this->load->view($ruta."/info_view", $params);
						}
					}
				}
				/* logamos */ if ($this->devlog) echo __line__."</pre>";
			}

			public function ver() {
				/* logamos */ if ($this->devlog) echo __line__."acceso ".$this->idioma;
				
				$ruta = $this->ruta();
				
				$this->lang->load($ruta.'/acceso', $this->idioma);
				$this->lang->load($ruta.'/inscripcion', $this->idioma);

				if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
					/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

					$params = array("lang"		=> $this->idioma,
									"control"	=> __LINE__." MensajeNoExiste",
									"mensaje"	=> lang("MensajeNoExiste"));
					$this->load->view($ruta."/mensaje_generico_view", $params);
				} else {
					/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
					$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, $this->forzar);
					
					/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

					if ($datos <= 0) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						if ($this->tr == 4) { 	// para el caso de venta taf... (que es la excepcion)
							$control = __LINE__.($datos == 0) ? " MensajeYaFinalizadoTaf" : " MensajeNoExisteTaf";
							$mensaje = ($datos == 0) ? lang("MensajeYaFinalizadoTaf") : lang("MensajeNoExisteTaf");							
						}else{
							$control = __LINE__.($datos == 0) ? " MensajeYaFinalizado" : " MensajeNoExiste";
							$mensaje = ($datos == 0) ? lang("MensajeYaFinalizado") : lang("MensajeNoExiste");	
						}
						$params = array("lang"		=> $this->idioma,
										"control"	=> $control,
										"mensaje"	=> $mensaje);
						$this->load->view($ruta."/mensaje_generico_view", $params);
					
					} else {

						if ($this->tr == 4) { 	// para el caso de venta taf... (que es la excepcion)
							log_message("error", "vamos a llamar a fecha_checkin");
							$retorno = $this->inscripcion_model->get_Inscripcio($this->ni, $this->forzar);
							log_message("error", "ha retornado (".print_r($retorno, true).")");
							if (!(bool)$retorno) {
								/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
								$params = array("lang"		=> $this->idioma,
												"control"	=> __LINE__." MensajeYaFinalizadoTaf",
												"mensaje"	=> lang("MensajeYaFinalizadoTaf"));
								$this->load->view($ruta."/mensaje_generico_view", $params);
							} else {
								/* logamos */ if ($this->devlog) echo __line__."retorno ".print_r($retorno, true);

								$posicion = -1;
								foreach ($retorno as $dato) {
									$posicion++;
									$params = 	array(
													"ni" => $dato->idinscripciones,
													"tr" => $this->tr,
													"tv" => $this->tv,
													"ip" => $dato->idpeople,
													"ic" => $dato->control
												);
									$params = json_encode($params);
									$params = str_replace('"', "'", $params);
									$texto = $this->soporte_xss->encode($params, true);
									$texto = base64_encode($texto);
									$texto = rawurlencode($texto);
									$retorno[$posicion]->params = $texto;
								}
								
								// echo "Linea ".__line__;
								$params = array("lang"					=> $this->idioma,
												"param_url"				=> $this->param_url,
												"datos"					=> $retorno,
												"ruta" 					=> $ruta,
												/* 
												"z_hostel" 				=> $programas[0]->hostel,
												"z_programa" 			=> $programas[0]->programa,
												"z_fdesde_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_entrada, "-", "/"),
												"z_fhasta_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_salida, "-", "/"),
												"z_Anyodesde" 			=> $z_Anyodesde,
												"z_Anyohasta" 			=> $z_Anyohasta,
												"z_centro"	 			=> $programas[0]->Centro,
												*/
												"forzar" 				=> $this->forzar,
												"permitirModificacion"	=> true);
												
								$this->load->view($ruta."/form_vuelo_taf_view", $params);
	
								/* logamos */ if ($this->devlog) echo __line__."acceso garantizado";

							}
						} else { 				// para el resto de tipos de venta
							$datos_cvac = $this->inscripcion_model->get_Inscripcions_cvac($this->ni);

							log_message("error", "vamos a llamar a fecha_checkin");
							$retorno = $this->inscripcion_model->fecha_checkin($this->ni, $this->forzar);
							log_message("error", "ha retornado (".$retorno.")");
							if (!$retorno) {
								/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
								$params = array("lang"		=> $this->idioma,
												"control"	=> __LINE__." MensajeTestOutDated",
												"mensaje"	=> lang("MensajeTestOutDated"));
								$this->load->view($ruta."/mensaje_generico_view", $params);
							} else {
	
								$error_fechas="";
								$programas = $this->inscripcion_model->get_programasxcentroTxt($datos[0]->centro, $datos[0]->idtanda);
	
								/* logamos */ if ($this->devlog) echo __line__."datos[0]->centro ".print_r($datos[0]->centro, true)."<br/>";
								/* logamos */ if ($this->devlog) echo __line__."datos[0]->idtanda ".print_r($datos[0]->idtanda, true)."<br/>";
								/* logamos */ if ($this->devlog) echo __line__."programas[0] ".print_r($programas, true)."<br/>";
								
								$z_quads_js = "";
								if ($programas[0]->quads == 1)      { $z_quads_js = " && validarc('aut_paint_quad') "; } 
							
								$z_dni_nen_js = "";
								if ($programas[0]->dni_nen == 1)    { $z_dni_nen_js = " && validart('mallorca_dni') "; }
								
								$z_hipica_js = ""; 
								if ($programas[0]->hipica == 1)     { $z_hipica_js = "controlActivarhipica();"; }    
								
								$z_futbol_js = ""; 
								// if ($z_futbol == 1)     { $z_futbol_js = " && validarr('futbol_equipacion') "; }    
								if ($programas[0]->futbol == 1)     { $z_futbol_js = " && ($('input[name=futbol_equipacion]:checked').val()=='J' || $('input[name=futbol_equipacion]:checked').val() == 'P')"; }    
	
								$z_fdesde = strtotime($programas[0]->fecha_entrada);
								$z_fhasta = strtotime($programas[0]->fecha_salida);
							
								$z_Anyodesde = date("Y") - $programas[0]->edad_max;
								$z_Anyohasta = date("Y") - $programas[0]->edad_min;
	
								// si el centro es el centro 040028 covid 19 cambiamos el idioma
								if ($programas[0]->hostel == "040028") {
									$this->lang->load($ruta.'/inscripcion040028', $this->idioma);
								}

								// echo "Linea ".__line__;
								$params = array("lang"					=> $this->idioma,
												"param_url"				=> $this->param_url,
												"datos"					=> $datos[0],
												"datos_cvac"			=> $datos_cvac,
												"ruta" 					=> $ruta,
												"z_hostel" 				=> $programas[0]->hostel,
												"z_programa" 			=> $programas[0]->programa,
												"z_fdesde_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_entrada, "-", "/"),
												"z_fhasta_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_salida, "-", "/"),
												"z_Anyodesde" 			=> $z_Anyodesde,
												"z_Anyohasta" 			=> $z_Anyohasta,
												"z_edaddesde" 			=> $programas[0]->edad_min,
												"z_edadhasta" 			=> $programas[0]->edad_max,
												"z_quads" 				=> $programas[0]->quads,
												"z_quads_js"			=> $z_quads_js,
												"z_hipica" 				=> $programas[0]->hipica,
												"z_hipica_js"			=> $z_hipica_js, 
												"z_angles" 				=> $programas[0]->angles,
												"z_futbol" 				=> $programas[0]->futbol,
												"z_futbol_js"			=> $z_futbol_js,										
												"z_dni_nen" 			=> $programas[0]->dni_nen,
												"z_dni_nen_js"			=> $z_dni_nen_js,
												"z_zurich"	 			=> $programas[0]->zurich,
												"z_centro"	 			=> $programas[0]->Centro,
												"colectiu"	 			=> $programas[0]->colectiu,
												"forzar" 				=> $this->forzar,
												"enlaceCondiciones"		=> $this->get_url($this->tv, $this->tr, $datos[0]->centro, $programas[0]->colectiu),
												"errorFechas" 			=> $error_fechas,
												"permitirModificacion"	=> true);
												
								$this->load->view($ruta."/form_inscripcion_view", $params);
	
								/* logamos */ if ($this->devlog) echo __line__."acceso garantizado";
							}	
						}						
					}
				}

				/* logamos */ if ($this->devlog) echo __line__."</pre>";			
			}

			public function vp() {
				/* logamos */ if ($this->devlog) echo __line__."acceso ".$this->idioma;
				
				$ruta = $this->ruta();
				
				$this->lang->load($ruta.'/acceso', $this->idioma);
				$this->lang->load($ruta.'/inscripcion', $this->idioma);

				if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
					/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

					$params = array("lang"		=> $this->idioma,
									"control"	=> __LINE__." MensajeNoExiste",
									"mensaje"	=> lang("MensajeNoExiste"));
					$this->load->view($ruta."/mensaje_generico_view", $params);
				} else {
					/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
					$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
					
					/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

					if (count($datos) == 0) {
						/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExiste",
										"mensaje"	=> lang("MensajeNoExiste"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {

						$datos_cvac = $this->inscripcion_model->get_Inscripcions_cvac($this->ni);

						$error_fechas="";
						$programas = $this->inscripcion_model->get_programasxcentroTxt($datos[0]->centro, $datos[0]->idtanda);
						
						$z_quads_js = "";
						if ($programas[0]->quads == 1)      { $z_quads_js = " && validarc('aut_paint_quad') "; } 
					
						$z_dni_nen_js = "";
						if ($programas[0]->dni_nen == 1)    { $z_dni_nen_js = " && validart('mallorca_dni') "; }
						
						$z_hipica_js = ""; 
						if ($programas[0]->hipica == 1)     { $z_hipica_js = "controlActivarhipica();"; }    
						
						$z_futbol_js = ""; 
						// if ($z_futbol == 1)     { $z_futbol_js = " && validarr('futbol_equipacion') "; }    
						if ($programas[0]->futbol == 1)     { $z_futbol_js = " && ($('input[name=futbol_equipacion]:checked').val()=='J' || $('input[name=futbol_equipacion]:checked').val() == 'P')"; }    

						$z_fdesde = strtotime($programas[0]->fecha_entrada);
						$z_fhasta = strtotime($programas[0]->fecha_salida);
					
						$z_Anyodesde = date("Y") - $programas[0]->edad_max;
						$z_Anyohasta = date("Y") - $programas[0]->edad_min;

						$params = array("lang"					=> $this->idioma,
										"param_url"				=> $this->param_url,
										"datos"					=> $datos[0],
										"datos_cvac"			=> $datos_cvac,
										"ruta" 					=> $ruta,
										"z_programa" 			=> $programas[0]->programa,
										"z_fdesde_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_entrada, "-", "/"),
										"z_fhasta_txt" 			=> $this->soporte_general->cambia_fecha($programas[0]->fecha_salida, "-", "/"),
										"z_Anyodesde" 			=> $z_Anyodesde,
										"z_Anyohasta" 			=> $z_Anyohasta,
										"z_edaddesde" 			=> $programas[0]->edad_min,
										"z_edadhasta" 			=> $programas[0]->edad_max,
										"z_quads" 				=> $programas[0]->quads,
										"z_quads_js"			=> $z_quads_js,
										"z_hipica" 				=> $programas[0]->hipica,
										"z_hipica_js"			=> $z_hipica_js, 
										"z_angles" 				=> $programas[0]->angles,
										"z_futbol" 				=> $programas[0]->futbol,
										"z_futbol_js"			=> $z_futbol_js,										
										"z_dni_nen" 			=> $programas[0]->dni_nen,
										"z_dni_nen_js"			=> $z_dni_nen_js,
										"z_zurich"	 			=> $programas[0]->zurich,
										"z_centro"	 			=> $programas[0]->Centro,
										"forzar" 				=> $this->forzar,
										"enlaceCondiciones"		=> $this->get_url($this->tv, $this->tr, $datos[0]->centro),
										"errorFechas" 			=> $error_fechas,
										"permitirModificacion"	=> false);

										
						$this->load->view($ruta."/form_inscripcion_view", $params);

						/* logamos */ if ($this->devlog) echo __line__."acceso garantizado";
					}
				}

				/* logamos */ if ($this->devlog) echo __line__."</pre>";			
			}

			public function guardar() {
				/* logamos */ if ($this->devlog) echo __line__."guardar ".$this->idioma;
			
				if ($this->tv == 0 and $this->tr != 2) $this->guardar_colonias(); 	// $ruta = "colonias";

				if ($this->tv == 0 and $this->tr == 2) $this->guardar_idiomas(); 	// $ruta = "idiomas";

				if ($this->tv == 1 and $this->tr != 2) $this->guardar_spain(); 		// $ruta = "spain";

				if ($this->tv == 1 and $this->tr == 2) $this->guardar_abroad(); 	// $ruta = "abroad";
			}

			public function ruta() {
				if ($this->tv == 0 and $this->tr != 2) return "colonias";
				if ($this->tv == 0 and $this->tr == 2) return "idiomas";
				if ($this->tv == 1 and $this->tr != 2) return "spain";
				if ($this->tv == 1 and $this->tr == 2) return "abroad";
			}
		/* FIN - GENERICOS */
		
			
		/* ESPECIFICOS ABROAD */
				public function guardar_abroad() {
					// $this->devlog = true;
					// echo "<pre>";
					/* logamos */ if ($this->devlog) echo __line__."guardar abroad ".$this->idioma;
					$ruta = $this->ruta();

					$this->lang->load($ruta.'/inscripcion', $this->idioma);

					if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExiste",
										"mensaje"	=> lang("MensajeNoExiste"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {
						/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						
						/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

						if (count($datos) == 0) {
							/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeNoExiste",
											"mensaje"	=> lang("MensajeNoExiste"));
							$this->load->view($ruta."/mensaje_generico_view", $params);
						} else {

							// si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
							/* logamos */ if ($this->devlog) echo __line__."POST ".print_r($_POST, true)." - ".count($_POST);

							$this->form_validation->set_message("required", lang("obligatorio"));
							$this->form_validation->set_message("check_campo", lang("obligatorio"));
							$this->form_validation->set_rules("nombre_participante", 					lang("agencia_validar_nombre_participante"), 					'trim|required');
							$this->form_validation->set_rules("apellidos_participante", 				lang("agencia_validar_apellidos_participante"),					'trim|required');
							$this->form_validation->set_rules("fecha_nacimiento", 						lang("agencia_validar_fecha_nacimiento"),						'trim|required');
							$this->form_validation->set_rules("chico_chica", 							lang("agencia_validar_chico_chica"), 							'trim|required|integer');
							$this->form_validation->set_rules("edad_participante", 						lang("agencia_validar_edad_participante"), 						'trim|required|integer');
							$this->form_validation->set_rules("nacionalidad", 							lang("agencia_validar_nacionalidad"), 							'trim|required');
							$this->form_validation->set_rules("email_participantes", 					lang("agencia_validar_email_participantes"),					'trim|required');
							$this->form_validation->set_rules("movil_participantes",					lang("agencia_validar_movil_participantes"),					'trim|required');
							$this->form_validation->set_rules("movil",									lang("agencia_validar_movil"),									'trim|required');
							$this->form_validation->set_rules("movil_padres",							lang("agencia_validar_movil_padres"),							'trim|required');
							$this->form_validation->set_rules("algun_curso", 							lang("agencia_validar_algun_curso"), 							'trim|required');
							$this->form_validation->set_rules("algun_curso_detalle", 					lang("agencia_validar_algun_curso_detalle"), 					'trim|callback_check_campo["algun_curso_detalle", "algun_curso", 1]');
							$this->form_validation->set_rules("algun_curso_detalle2", 					lang("agencia_validar_algun_curso_detalle2"), 					'trim|callback_check_campo["algun_curso_detalle2", "algun_curso", 1]');
							$this->form_validation->set_rules("alguna_enfermedad", 						lang("agencia_validar_alguna_enfermedad"),						'trim|required');
							$this->form_validation->set_rules("alguna_enfermedad_detalle", 				lang("agencia_validar_alguna_enfermedad_detalle"),				'trim|callback_check_campo["alguna_enfermedad_detalle", "alguna_enfermedad", 1]');
							$this->form_validation->set_rules("algun_medicamento",						lang("agencia_validar_algun_medicamento"), 						'trim|required');
							$this->form_validation->set_rules("algun_medicamento_detalle",				lang("agencia_validar_algun_medicamento_detalle"), 				'trim|callback_check_campo["algun_medicamento_detalle", "algun_medicamento", 1]');
							$this->form_validation->set_rules("dieta_especial",							lang("agencia_validar_dieta_especial"),							'trim|required');
							$this->form_validation->set_rules("dieta_especial_detalle",					lang("agencia_validar_dieta_especial_detalle"),					'trim|callback_check_campo["dieta_especial_detalle", "dieta_especial", 1]');
							$this->form_validation->set_rules("alguna_alergia",							lang("agencia_validar_alguna_alergia"), 						'trim|required');
							$this->form_validation->set_rules("amics_familiars",						lang("agencia_validar_amics_familiars"), 						'trim|required');
							$this->form_validation->set_rules("amigos_juntos", 							lang("agencia_validar_amigos_juntos"),							'trim|required');
							$this->form_validation->set_rules("nombre_amigo", 							lang("agencia_validar_nombre_amigo"),							'trim|callback_check_campo["nombre_amigo", "amics_familiars", 1]');
							$this->form_validation->set_rules("nombre_padres", 							lang("agencia_validar_nombre_padres"), 							'trim|required');
							$this->form_validation->set_rules("dni_padres",								lang("agencia_validar_dni_padres"), 							'trim|required');
							$this->form_validation->set_rules("email_padres", 							lang("agencia_validar_email_padres"), 							'trim|required');
							$this->form_validation->set_rules("direccion_participante", 				lang("agencia_validar_direccion_participante"),					'trim|required');
							$this->form_validation->set_rules("codigo_postal", 							lang("agencia_validar_codigo_postal"), 							'trim|required');
							$this->form_validation->set_rules("poblacion", 								lang("agencia_validar_poblacion"), 								'trim|required');
							$this->form_validation->set_rules("colegio", 								lang("agencia_validar_colegio"), 								'trim|required');
							$this->form_validation->set_rules("colegio_miniestancia", 					lang("agencia_validar_colegio_miniestancia"),					'trim|required');
							$this->form_validation->set_rules("nombre_profesor", 						lang("agencia_validar_nombre_profesor"),						'trim|required');
							$this->form_validation->set_rules("curso", 									lang("agencia_validar_curso"),	 								'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_padre",				lang("agencia_validar_nombre_autorizacion_padre"), 				'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_padres", 				lang("agencia_validar_dni_autorizacion_padres"), 				'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_monitor", 		lang("agencia_validar_autoritzacio_acceptacio_monitor"), 		'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_padre_medica", 		lang("agencia_validar_nombre_autorizacion_padre_medica"), 		'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_padres_medica", 		lang("agencia_validar_dni_autorizacion_padres_medica"), 		'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_medica", 		lang("agencia_validar_autoritzacio_acceptacio_medica"), 		'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_audiovisual_padres", lang("agencia_validar_nombre_autorizacion_audiovisual_padres"),	'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_audiovisual_padres", 	lang("agencia_validar_dni_autorizacion_audiovisual_padres"), 	'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_audiovisual", 	lang("agencia_validar_autoritzacio_acceptacio_audiovisual"), 	'trim|required');
							$this->form_validation->set_rules("observaciones", 							lang("agencia_validar_observaciones"), 							'trim');
							$this->form_validation->set_rules("conocido_por", 							lang("agencia_validar_conocido_por"), 							'trim|required');
							$this->form_validation->set_rules("otros", 									lang("agencia_validar_otros"), 									'trim|callback_check_campo["otros", "conocido_por", 6]');
							$this->form_validation->set_rules("normativa_acceptacio", 					lang("agencia_validar_normativa_acceptacio"), 					'trim|required');
							$this->form_validation->set_rules("confirmacionxmail", 						lang("agencia_validar_confirmacionxmail"),						'trim|required');
							
							if($this->form_validation->run() == FALSE) {
								$relanzar = false;
								/* echo validation_errors(); */
								$this->ver();
							} else {
								/* logamos */ if ($this->devlog) echo __line__."\nGuardando!\n";

								$this->inscripcion_model->update_Inscripcio($this->ni);
						
								if (isset($_POST["confirmacionxmail"]) and $_POST["confirmacionxmail"] != "") {
									$rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
									/* logamos */ if ($this->devlog) echo __line__."\nrsDades: ".print_r($rsDades, true)."\n";
									
									$val_numero_reserva = $rsDades[0]->NReserva;
									$centro = $rsDades[0]->centro;
									$hostel = $rsDades[0]->hostel;
									
									$destino_url = "includes/reservation/sendmail_ins.php?lang=be&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

									/* logamos */ if ($this->devlog) echo __line__."\nvamos a lanzar esta url!\n".$destino_url."\n";
									$this->load->library("enviar_email");
									$this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
									/* logamos */ if ($this->devlog) echo __line__."\Enviado!\n";
								}
                                
								$params = array("lang"		=> $this->idioma,
												"control"	=> __LINE__." finalizada_correctamente",
												"mensaje"	=> lang("finalizada_correctamente"));
								$this->load->view($ruta."/mensaje_generico_view", $params);						
							}
						}
					}

					/* logamos */ if ($this->devlog) echo __line__."</pre>";		
				}
		/* FIN - ESPECIFICOS ABROAD */

		/* ESPECIFICOS COLONIAS */
				public function guardar_colonias() {
					/* logamos */ if ($this->devlog) echo __line__."guardar colonias ".$this->idioma;
					$ruta = $this->ruta();

					$this->lang->load($ruta.'/inscripcion', $this->idioma);
					
					if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExiste",
										"mensaje"	=> lang("MensajeNoExiste"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {
						/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						
						/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

						if (count($datos) == 0) {
							/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeNoExiste",
											"mensaje"	=> lang("MensajeNoExiste"));
							$this->load->view($ruta."/mensaje_generico_view", $params);
						} else {

							$programas = $this->inscripcion_model->get_programasxcentroTxt($datos[0]->centro, $datos[0]->idtanda);
							
							$z_quads_js = ($programas[0]->quads == 1);
							$z_dni_nen_js = ($programas[0]->dni_nen == 1);
							$z_hipica_js = ($programas[0]->hipica == 1);
							$z_futbol_js = ($programas[0]->futbol == 1);

							// si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
							/* logamos */ if ($this->devlog) echo __line__."POST ".print_r($_POST, true)." - ".count($_POST);

							$cond_hipica = ((isset($_POST["hipica1"])) and ($_POST["hipica1"]==true));
							// $this->form_validation->set_rules("dni", 									lang("agencia_validar_dni"), 									'trim|required');
							$this->form_validation->set_message("required", lang("obligatorio"));
							$this->form_validation->set_message("check_campo", lang("obligatorio"));
							
							$this->form_validation->set_rules("nom_complet", 							lang("agencia_validar_nom_complet"), 							'trim|required');
							$this->form_validation->set_rules("chico_chica", 							lang("agencia_validar_chico_chica"), 							'trim|required|integer');
							$this->form_validation->set_rules("telefon", 								lang("agencia_validar_telefon"), 								'trim|required');
							$this->form_validation->set_rules("adressa", 								lang("agencia_validar_adressa"), 								'trim|required');
							$this->form_validation->set_rules("poblacio", 								lang("agencia_validar_poblacio"), 								'trim|required');
							$this->form_validation->set_rules("cp", 									lang("agencia_validar_cp"), 									'trim|required');
							// $this->form_validation->set_rules("conocido_por", 							lang("agencia_validar_conocido_por"), 							'trim|required');
							// $this->form_validation->set_rules("otros", 									lang("agencia_validar_otros"), 									'trim|callback_check_campo["otros", "conocido_por",  "8"]');
							$this->form_validation->set_rules("data_naixement", 						lang("agencia_validar_data_naixement"), 						'trim|required');
							$this->form_validation->set_rules("altres_telefons", 						lang("agencia_validar_altres_telefons"), 						'trim|required');
							$this->form_validation->set_rules("malalt", 								lang("agencia_validar_malalt"), 								'trim|required');
							$this->form_validation->set_rules("malalt_detall", 							lang("agencia_validar_malalt_detall"), 							'trim|callback_check_campo["malalt_detall", "malalt",1]');
							$this->form_validation->set_rules("medicaments", 							lang("agencia_validar_medicaments"),							'trim|required');
							$this->form_validation->set_rules("medicaments_detall", 					lang("agencia_validar_medicaments_detall"),						'trim|callback_check_campo["medicaments_detall", "medicaments",1]');
							$this->form_validation->set_rules("medicaments_administracio", 				lang("agencia_validar_medicaments_administracio"),				'trim|callback_check_campo["medicaments_administracio", "medicaments",1]');
							$this->form_validation->set_rules("regim", 									lang("agencia_validar_regim"), 									'trim|required');
							$this->form_validation->set_rules("regim_detall", 							lang("agencia_validar_regim_detall"), 							'trim|callback_check_campo["regim_detall", "desordre_alimentari",1]');
							$this->form_validation->set_rules("desordre_alimentari", 					lang("agencia_validar_desordre_alimentari"), 					'trim|required');
							$this->form_validation->set_rules("desordre_alimentari_detall", 			lang("agencia_validar_desordre_alimentari_detall"), 			'trim|callback_check_campo["desordre_alimentari_detall", "desordre_alimentari",1]');
							$this->form_validation->set_rules("operat", 								lang("agencia_validar_operat"), 								'trim|required');
							$this->form_validation->set_rules("operat_detall",				 			lang("agencia_validar_operat_detall"),				 			'trim|callback_check_campo["operat_detall", "operat", 1]');
							$this->form_validation->set_rules("alergic_celiac",							lang("agencia_validar_alergic_celiac"), 						'trim');
							$this->form_validation->set_rules("alergic_lactosa",						lang("agencia_validar_alergic_lactosa"),						'trim');
							$this->form_validation->set_rules("alergic_ou", 							lang("agencia_validar_alergic_ou"), 							'trim');
							$this->form_validation->set_rules("alergic_altres", 						lang("agencia_validar_alergic_altres"), 						'trim');
							$this->form_validation->set_rules("alergic_detall",				 			lang("agencia_validar_alergic_detall"),				 			'trim|callback_check_campo["alergic_detall", "alergic_altres", 1]');
							$this->form_validation->set_rules("estades_abans", 							lang("agencia_validar_estades_abans"), 							'trim|required');
							$this->form_validation->set_rules("nedar", 									lang("agencia_validar_nedar"), 									'trim|required');
							$this->form_validation->set_rules("por_aigua", 								lang("agencia_validar_por_aigua"), 								'trim|required');
							$this->form_validation->set_rules("bicicleta", 								lang("agencia_validar_bicicleta"),	 							'trim|required');
							$this->form_validation->set_rules("observaciones", 							lang("agencia_validar_observaciones"), 							'trim');
							$this->form_validation->set_rules("angles_fora", 							lang("agencia_validar_angles_fora"), 							'trim');
							$this->form_validation->set_rules("angles_fora_detall",						lang("agencia_validar_angles_fora_detall"),			 			'trim|callback_check_campo["alergic_detall", "angles_fora", 1]');
							$this->form_validation->set_rules("autoritzacio_nom", 						lang("agencia_validar_autoritzacio_nom"), 						'trim|required');
							$this->form_validation->set_rules("autoritzacio_dni", 						lang("agencia_validar_autoritzacio_dni"), 						'trim|required');
							$this->form_validation->set_rules("autoritzacio_ok", 						lang("agencia_validar_autoritzacio_ok"), 						'trim|required');
							$this->form_validation->set_rules("normativa_ok", 							lang("agencia_validar_normativa_ok"), 							'trim|required');
							$this->form_validation->set_rules("aut_paint_quad", 						lang("agencia_validar_aut_paint_quad"), 						'trim'.($z_quads_js  ? "|required" : "" ) );
							$this->form_validation->set_rules("hipica1", 								lang("agencia_validar_hipica1"), 								'trim'.($z_hipica_js ? "|required" : "" ) );
							$this->form_validation->set_rules("hipica2", 								lang("agencia_validar_hipica2"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("hipica3",		 						lang("agencia_validar_hipica3"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("hipica4", 								lang("agencia_validar_hipica4"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("observaciones_hipica", 					lang("agencia_validar_observaciones_hipica"), 					'trim');
							$this->form_validation->set_rules("mallorca_dni", 							lang("agencia_validar_mallorca_dni"), 							'trim'.($z_dni_nen_js? "|required" : "" ) );
							$this->form_validation->set_rules("observaciones_generales", 				lang("agencia_validar_observaciones_generales"),				'trim');
							$this->form_validation->set_rules("email", 									lang("agencia_validar_email"),									'trim');
							$this->form_validation->set_rules("sotatractament", 						lang("agencia_validar_sotatractament"),							'trim|required');
							$this->form_validation->set_rules("sotatractamentdetall", 					lang("agencia_validar_sotatractamentdetall"), 					'trim|callback_check_campo["sotatractamentdetall", "sotatractament",1]');
							$this->form_validation->set_rules("vertigen", 								lang("agencia_validar_vertigen"),								'trim|required');
							$this->form_validation->set_rules("dificultatesports", 						lang("agencia_validar_dificultatesports"),						'trim|required');
							$this->form_validation->set_rules("dificultatesports_quins", 				lang("dificultatesports_quins"), 								'trim|callback_check_campo["dificultatesports_quins", "dificultatesports",1]');
							$this->form_validation->set_rules("futbol_equipacion",	 					lang("agencia_validar_futbol_equipacion"), 						'trim'.($z_futbol_js ? "|required" : ""));
							$this->form_validation->set_rules("covid19_ok", 							lang("agencia_validar_covid19_ok"), 							'trim|required');
			
							
							if($this->form_validation->run() == FALSE) {
								$relanzar = false;

								// $_POST["chico_chica"] = ($_POST["chico_chica"] == "1" ? "true" : "false");
								//$_POST["data_naixement"] = $this->soporte_general->cambia_fecha($_POST["data_naixement"], "-", "/");

								// echo validation_errors();
								$this->ver();
							} else {
								$error_fechas = false;
								$test_fecha = $this->inscripcion_model->prepararData(set_value("data_naixement", $_POST["data_naixement"]), "/", "-");
								try {
									$fecha = new DateTime($test_fecha);
								} catch (Exception $e) {
									/* logamos */ if ($this->devlog) echo __line__."\nHa dado error de fechas -- data_naixement: ".$_POST["data_naixement"]."\n";

									$error_fechas = true;
								}
								
								if (!$error_fechas) {
									/* logamos */ if ($this->devlog) echo __line__."\nGuardando!\n";
									// die("post ".print_r($_POST, true));

									$this->inscripcion_model->update_Inscripcio($this->ni);
									
									$rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
									/* logamos */ if ($this->devlog) echo __line__."\nrsDades: ".print_r($rsDades, true)."\n";
									
									$val_numero_reserva = $rsDades[0]->NReserva;
									$centro = $rsDades[0]->centro;
									$hostel = $rsDades[0]->hostel;
									
									$destino_url = "includes/reservation/sendmail_ins.php?lang=".$this->idioma."&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

									/* logamos */ if ($this->devlog) echo __line__."\nvamos a lanzar esta url!\n".$destino_url."\n";
									$this->load->library("enviar_email");
									$this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
									/* logamos */ if ($this->devlog) echo __line__."\Enviado!\n";
									
									$params = array("lang"		=> $this->idioma,
													"control"	=> __LINE__." finalizada_correctamente",
													"mensaje"	=> lang("finalizada_correctamente"));
									$this->load->view($ruta."/mensaje_generico_view", $params);						
								} else {
									$_POST["data_naixement"] = "";
									$this->form_validation->run();
									$relanzar = false;
									$this->ver();
								}
							}
						}
					}

					/* logamos */ if ($this->devlog) echo __line__."</pre>";		
				}

				public function guardar_taf() {
					/* logamos */ if ($this->devlog) log_message("error","> ".__line__." guardar taf ".$this->idioma);
					$ruta = $this->ruta();

					$this->lang->load($ruta.'/inscripcion', $this->idioma);
					
					if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
						/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." acceso denegado");

						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExisteTaf",
										"mensaje"	=> lang("MensajeNoExisteTaf"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {
						/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." POST ".print_r($_POST, true)." - ".count($_POST));

						if (!isset($_POST["pos"])) {

							/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." acceso denegado Few params");
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeNoExisteTaf",
											"mensaje"	=> lang("MensajeNoExisteTaf"));
							$this->load->view($ruta."/mensaje_generico_view", $params);

						} else {
							$locata = "";
							for ($x = 1; $x <= $_POST["pos"]; $x++) {
								if (!isset($_POST["form".$x."idep"]) ||
									!isset($_POST["form".$x."loca"])) {

									/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." acceso denegado Few params detalle");
									$params = array("lang"		=> $this->idioma,
													"control"	=> __LINE__." MensajeNoExisteTaf",
													"mensaje"	=> lang("MensajeNoExisteTaf"));
									$this->load->view($ruta."/mensaje_generico_view", $params);

								} else {	
								
									$idpeople =	$this->soporte_xss->decode($_POST["form".$x."idep"], true);
									$locata =	$this->soporte_xss->decode($_POST["form".$x."loca"], true);
									
									$name =		$_POST["form".$x."name"];
									$sur1 =		$_POST["form".$x."sur1"];
									$sur2 =		$_POST["form".$x."sur2"];
									$born = 	$this->soporte_general->cambia_fecha($_POST["form".$x."born"], "/", "-");

									$datos = array(	"name" 		=> $name, 
													"sur1" 		=> $sur1, 
													"sur2" 		=> $sur2, 
													"fullname"	=> $name." ".$sur1." ".$sur2, 
													"born" 		=> $born);
									/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." vamos a actualizar esto: idpeople ".$idpeople." - ".print_r($datos, true));

									$this->inscripcion_model->set_DatosTaf(  $idpeople, $datos  );
								}
							}
							$this->inscripcion_model->end_DatosTaf( $locata );
						}

						
						/* logamos */ if ($this->devlog) log_message("error", "> ".__line__." Enviado!\n");
						
						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." taf_finalizada_correctamente",
										"mensaje"	=> lang("taf_finalizada_correctamente"));
						$this->load->view($ruta."/mensaje_generico_view", $params);						
					}

					/* logamos */ if ($this->devlog) log_message("error", "> ".__line__."</pre>");		
				}
			/* FIN - ESPECIFICOS COLONIAS */

		/* ESPECIFICOS IDIOMAS */
				public function guardar_idiomas() {
					/* logamos */ if ($this->devlog) echo __line__."guardar idiomas ".$this->idioma;
					$ruta = $this->ruta();

					$this->lang->load($ruta.'/inscripcion', $this->idioma);

					if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExiste",
										"mensaje"	=> lang("MensajeNoExiste"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {
						/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						
						/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

						if (count($datos) == 0) {
							/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeNoExiste",
											"mensaje"	=> lang("MensajeNoExiste"));
							$this->load->view($ruta."/mensaje_generico_view", $params);
						} else {

							// si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
							/* logamos */ if ($this->devlog) echo __line__."POST ".print_r($_POST, true)." - ".count($_POST);
							$this->form_validation->set_message("required", lang("obligatorio"));
							$this->form_validation->set_message("check_campo", lang("obligatorio"));
							
							$this->form_validation->set_rules("nombre_participante", 					lang("agencia_validar_nombre_participante"), 					'trim|required');
							$this->form_validation->set_rules("apellidos_participante", 				lang("agencia_validar_apellidos_participante"),					'trim|required');
							$this->form_validation->set_rules("fecha_nacimiento", 						lang("agencia_validar_fecha_nacimiento"),						'trim|required');
							$this->form_validation->set_rules("chico_chica", 							lang("agencia_validar_chico_chica"), 							'trim|required|integer');
							$this->form_validation->set_rules("edad_participante", 						lang("agencia_validar_edad_participante"), 						'trim|required|integer');
							$this->form_validation->set_rules("nacionalidad", 							lang("agencia_validar_nacionalidad"), 							'trim|required');
							$this->form_validation->set_rules("email_participantes", 					lang("agencia_validar_email_participantes"),					'trim|required');
							$this->form_validation->set_rules("movil_participantes",					lang("agencia_validar_movil_participantes"),					'trim|required');
							$this->form_validation->set_rules("algun_curso", 							lang("agencia_validar_algun_curso"), 							'trim|required');
							$this->form_validation->set_rules("algun_curso_detalle", 					lang("agencia_validar_algun_curso_detalle"), 					'trim|callback_check_campo["algun_curso_detalle", "algun_curso", 0]');
							$this->form_validation->set_rules("algun_curso_detalle2", 					lang("agencia_validar_algun_curso_detalle2"), 					'trim|callback_check_campo["algun_curso_detalle2", "algun_curso", 0]');
							$this->form_validation->set_rules("alguna_enfermedad", 						lang("agencia_validar_alguna_enfermedad"),						'trim|required');
							$this->form_validation->set_rules("alguna_enfermedad_detalle", 				lang("agencia_validar_alguna_enfermedad_detalle"),				'trim|callback_check_campo["alguna_enfermedad_detalle", "alguna_enfermedad", 1]');
							$this->form_validation->set_rules("algun_medicamento",						lang("agencia_validar_algun_medicamento"), 						'trim|required');
							$this->form_validation->set_rules("algun_medicamento_detalle",				lang("agencia_validar_algun_medicamento_detalle"), 				'trim|callback_check_campo["algun_medicamento_detalle", "algun_medicamento", 1]');
							$this->form_validation->set_rules("dieta_especial",							lang("agencia_validar_dieta_especial"),							'trim|required');
							$this->form_validation->set_rules("dieta_especial_detalle",					lang("agencia_validar_dieta_especial_detalle"),					'trim|callback_check_campo["dieta_especial_detalle", "dieta_especial", 1]');
							$this->form_validation->set_rules("alguna_alergia",							lang("agencia_validar_alguna_alergia"), 						'trim|required');
							$this->form_validation->set_rules("amics_familiars",						lang("agencia_validar_amics_familiars"), 						'trim|required');
							$this->form_validation->set_rules("amigos_juntos", 							lang("agencia_validar_amigos_juntos"),							'trim|required');
							$this->form_validation->set_rules("nombre_amigo", 							lang("agencia_validar_nombre_amigo"),							'trim|callback_check_campo["nombre_amigo", "amics_familiars", 1]');
							$this->form_validation->set_rules("nombre_padres", 							lang("agencia_validar_nombre_padres"), 							'trim|required');
							$this->form_validation->set_rules("movil",									lang("agencia_validar_movil"), 									'trim|required');
							$this->form_validation->set_rules("movil_padres",							lang("agencia_validar_movil_padres"), 							'trim|required');
							$this->form_validation->set_rules("dni_padres",								lang("agencia_validar_dni_padres"), 							'trim|required');
							$this->form_validation->set_rules("email_padres", 							lang("agencia_validar_email_padres"), 							'trim|required');
							$this->form_validation->set_rules("direccion_participante", 				lang("agencia_validar_direccion_participante"),					'trim|required');
							$this->form_validation->set_rules("codigo_postal", 							lang("agencia_validar_codigo_postal"), 							'trim|required');
							$this->form_validation->set_rules("poblacion", 								lang("agencia_validar_poblacion"), 								'trim|required');
							$this->form_validation->set_rules("colegio", 								lang("agencia_validar_colegio"), 								'trim|required');
							$this->form_validation->set_rules("colegio_miniestancia", 					lang("agencia_validar_colegio_miniestancia"),					'trim|required');
							$this->form_validation->set_rules("nombre_profesor", 						lang("agencia_validar_nombre_profesor"),						'trim|required');
							$this->form_validation->set_rules("curso", 									lang("agencia_validar_curso"),	 								'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_padre",				lang("agencia_validar_nombre_autorizacion_padre"), 				'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_padres", 				lang("agencia_validar_dni_autorizacion_padres"), 				'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_monitor", 		lang("agencia_validar_autoritzacio_acceptacio_monitor"), 		'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_padre_medica", 		lang("agencia_validar_nombre_autorizacion_padre_medica"), 		'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_padres_medica", 		lang("agencia_validar_dni_autorizacion_padres_medica"), 		'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_medica", 		lang("agencia_validar_autoritzacio_acceptacio_medica"), 		'trim|required');
							$this->form_validation->set_rules("nombre_autorizacion_audiovisual_padres", lang("agencia_validar_nombre_autorizacion_audiovisual_padres"),	'trim|required');
							$this->form_validation->set_rules("dni_autorizacion_audiovisual_padres", 	lang("agencia_validar_dni_autorizacion_audiovisual_padres"), 	'trim|required');
							$this->form_validation->set_rules("autoritzacio_acceptacio_audiovisual", 	lang("agencia_validar_autoritzacio_acceptacio_audiovisual"), 	'trim|required');
							$this->form_validation->set_rules("observaciones", 							lang("agencia_validar_observaciones"), 							'trim');
							$this->form_validation->set_rules("otras_consideraciones", 					lang("agencia_validar_otras_consideraciones"), 					'trim');
							$this->form_validation->set_rules("conocido_por", 							lang("agencia_validar_conocido_por"), 							'trim|required');
							$this->form_validation->set_rules("otros", 									lang("agencia_validar_otros"), 									'trim|callback_check_campo["otros", "conocido_por", 6]');
							$this->form_validation->set_rules("normativa_acceptacio", 					lang("agencia_validar_normativa_acceptacio"), 					'trim|required');
							$this->form_validation->set_rules("confirmacionxmail", 						lang("agencia_validar_confirmacionxmail"),						'trim|required');
							
							if($this->form_validation->run() == FALSE) {
								$relanzar = false;
								/* echo validation_errors(); */
								$this->ver();
							} else {
								/* logamos */ if ($this->devlog) echo __line__."\nGuardando!\n";

								$this->inscripcion_model->update_Inscripcio($this->ni);
						
								if (isset($_POST["confirmacionxmail"]) and $_POST["confirmacionxmail"] != "") {
									$rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
									/* logamos */ if ($this->devlog) echo __line__."\nrsDades: ".print_r($rsDades, true)."\n";
									
									$val_numero_reserva = $rsDades[0]->NReserva;
									$centro = $rsDades[0]->centro;
									$hostel = $rsDades[0]->hostel;
									
									$destino_url = "includes/reservation/sendmail_ins.php?lang=".$this->idioma."&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

									/* logamos */ if ($this->devlog) echo __line__."\nvamos a lanzar esta url!\n".$destino_url."\n";
									$this->load->library("enviar_email");
									$this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
									/* logamos */ if ($this->devlog) echo __line__."\Enviado!\n";
								}
								
								$params = array("lang"		=> $this->idioma,
												"control"	=> __LINE__." finalizada_correctamente",
												"mensaje"	=> lang("finalizada_correctamente"));
								$this->load->view($ruta."/mensaje_generico_view", $params);						
							}
						}
					}

					/* logamos */ if ($this->devlog) echo __line__."</pre>";		
				}
		/* FIN - ESPECIFICOS IDIOMAS */

		/* ESPECIFICOS SPAIN */
				public function guardar_spain() {
					// $this->devlog = true;
					// echo "<pre>";
					/* logamos */ if ($this->devlog) echo __line__."guardar spain ".$this->idioma;
					$ruta = $this->ruta();

					$this->lang->load($ruta.'/inscripcion', $this->idioma);
					
					if (($this->ni == 0) or ($this->tr == 0) or ($this->ip == 0) or ($this->ic == 0)) {
						/* logamos */ if ($this->devlog) echo __line__."acceso denegado";

						$params = array("lang"		=> $this->idioma,
										"control"	=> __LINE__." MensajeNoExiste",
										"mensaje"	=> lang("MensajeNoExiste"));
						$this->load->view($ruta."/mensaje_generico_view", $params);
					} else {
						/* logamos */ if ($this->devlog) echo __line__."\n".$ruta."\n";
						$datos = $this->inscripcion_model->get_Inscripcio($this->ni, $this->ip, $this->ic, true);
						
						/* logamos */ if ($this->devlog) echo __line__."datos ".print_r($datos, true)." - ".count($datos);

						if (count($datos) == 0) {
							/* logamos */ if ($this->devlog) echo __line__."no hay datos - ruta ".$ruta;
							$params = array("lang"		=> $this->idioma,
											"control"	=> __LINE__." MensajeNoExiste",
											"mensaje"	=> lang("MensajeNoExiste"));
							$this->load->view($ruta."/mensaje_generico_view", $params);
						} else {

							$programas = $this->inscripcion_model->get_programasxcentroTxt($datos[0]->centro, $datos[0]->idtanda);
							
							$z_quads_js = ($programas[0]->quads == 1);
							$z_dni_nen_js = ($programas[0]->dni_nen == 1);
							$z_hipica_js = ($programas[0]->hipica == 1);
							$z_futbol_js = ($programas[0]->futbol == 1);

							// si hemos llegado hassta aqui quiere decir que vamos a actualizar una inscripcion que es valida
							/* logamos */ if ($this->devlog) echo __line__."POST ".print_r($_POST, true)." - ".count($_POST);

							$cond_hipica = ((isset($_POST["hipica1"])) and ($_POST["hipica1"]==true));
							// $this->form_validation->set_rules("dni", 									lang("agencia_validar_dni"), 									'trim|required');
							$this->form_validation->set_message("required", lang("obligatorio"));
							$this->form_validation->set_message("check_campo", lang("obligatorio"));
							
							$this->form_validation->set_rules("nom_complet", 							lang("agencia_validar_nom_complet"), 							'trim|required');
							$this->form_validation->set_rules("telefon", 								lang("agencia_validar_telefon"), 								'trim|required');
							$this->form_validation->set_rules("adressa", 								lang("agencia_validar_adressa"), 								'trim|required');
							$this->form_validation->set_rules("poblacio", 								lang("agencia_validar_poblacio"), 								'trim|required');
							$this->form_validation->set_rules("cp", 									lang("agencia_validar_cp"), 									'trim|required');
							$this->form_validation->set_rules("conocido_por", 							lang("agencia_validar_conocido_por"), 							'trim|required');
							$this->form_validation->set_rules("otros", 									lang("agencia_validar_otros"), 									'trim|callback_check_campo["otros", "conocido_por",  "8"]');
							$this->form_validation->set_rules("data_naixement", 						lang("agencia_validar_data_naixement"), 						'trim|required');
							$this->form_validation->set_rules("altres_telefons", 						lang("agencia_validar_altres_telefons"), 						'trim|required');
							$this->form_validation->set_rules("malalt", 								lang("agencia_validar_malalt"), 								'trim|required');
							$this->form_validation->set_rules("malalt_detall", 							lang("agencia_validar_malalt_detall"), 							'trim|callback_check_campo["malalt_detall", "malalt","true"]');
							$this->form_validation->set_rules("medicaments", 							lang("agencia_validar_medicaments"),							'trim|required');
							$this->form_validation->set_rules("medicaments_detall", 					lang("agencia_validar_medicaments_detall"),						'trim|callback_check_campo["medicaments_detall", "medicaments","true"]');
							$this->form_validation->set_rules("medicaments_administracio", 				lang("agencia_validar_medicaments_administracio"),				'trim|callback_check_campo["medicaments_administracio", "medicaments","true"]');
							$this->form_validation->set_rules("regim", 									lang("agencia_validar_regim"), 									'trim|required');
							$this->form_validation->set_rules("regim_detall", 							lang("agencia_validar_regim_detall"), 							'trim|callback_check_campo["regim_detall", "desordre_alimentari","true"]');
							$this->form_validation->set_rules("operat", 								lang("agencia_validar_operat"), 								'trim|required');
							$this->form_validation->set_rules("operat_detall",				 			lang("agencia_validar_operat_detall"),				 			'trim|callback_check_campo["operat_detall", "operat", "true"]');
							$this->form_validation->set_rules("alergic_celiac",							lang("agencia_validar_alergic_celiac"), 						'trim');
							$this->form_validation->set_rules("alergic_lactosa",						lang("agencia_validar_alergic_lactosa"),						'trim');
							$this->form_validation->set_rules("alergic_ou", 							lang("agencia_validar_alergic_ou"), 							'trim');
							$this->form_validation->set_rules("alergic_altres", 						lang("agencia_validar_alergic_altres"), 						'trim');
							$this->form_validation->set_rules("alergic_detall",				 			lang("agencia_validar_alergic_detall"),				 			'trim|callback_check_campo["alergic_detall", "alergic_altres", "true"]');
							$this->form_validation->set_rules("estades_abans", 							lang("agencia_validar_estades_abans"), 							'trim|required');
							$this->form_validation->set_rules("nedar", 									lang("agencia_validar_nedar"), 									'trim|required');
							$this->form_validation->set_rules("por_aigua", 								lang("agencia_validar_por_aigua"), 								'trim|required');
							$this->form_validation->set_rules("bicicleta", 								lang("agencia_validar_bicicleta"),	 							'trim|required');
							$this->form_validation->set_rules("observaciones", 							lang("agencia_validar_observaciones"), 							'trim');
							$this->form_validation->set_rules("angles_fora", 							lang("agencia_validar_angles_fora"), 							'trim');
							$this->form_validation->set_rules("angles_fora_detall",						lang("agencia_validar_angles_fora_detall"),			 			'trim|callback_check_campo["alergic_detall", "angles_fora", "true"]');
							$this->form_validation->set_rules("autoritzacio_nom", 						lang("agencia_validar_autoritzacio_nom"), 						'trim|required');
							$this->form_validation->set_rules("autoritzacio_dni", 						lang("agencia_validar_autoritzacio_dni"), 						'trim|required');
							$this->form_validation->set_rules("autoritzacio_ok", 						lang("agencia_validar_autoritzacio_ok"), 						'trim|required');
							$this->form_validation->set_rules("normativa_ok", 							lang("agencia_validar_normativa_ok"), 							'trim|required');
							$this->form_validation->set_rules("aut_paint_quad", 						lang("agencia_validar_aut_paint_quad"), 						'trim'.($z_quads_js  ? "|required" : "" ) );
							$this->form_validation->set_rules("hipica1", 								lang("agencia_validar_hipica1"), 								'trim'.($z_hipica_js ? "|required" : "" ) );
							$this->form_validation->set_rules("hipica2", 								lang("agencia_validar_hipica2"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("hipica3",		 						lang("agencia_validar_hipica3"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("hipica4", 								lang("agencia_validar_hipica4"), 								'trim'.($z_hipica_js && $cond_hipica ? "|required" : "") );
							$this->form_validation->set_rules("observaciones_hipica", 					lang("agencia_validar_observaciones_hipica"), 					'trim');
							$this->form_validation->set_rules("mallorca_dni", 							lang("agencia_validar_mallorca_dni"), 							'trim'.($z_dni_nen_js? "|required" : "" ) );
							$this->form_validation->set_rules("observaciones_generales", 				lang("agencia_validar_observaciones_generales"),				'trim');
							$this->form_validation->set_rules("email", 									lang("agencia_validar_email"),									'trim');
							$this->form_validation->set_rules("sotatractament", 						lang("agencia_validar_sotatractament"),							'trim|required');
							$this->form_validation->set_rules("sotatractamentdetall", 					lang("agencia_validar_sotatractamentdetall"), 					'trim|callback_check_campo["sotatractamentdetall", "sotatractament","true"]');
							$this->form_validation->set_rules("vertigen", 								lang("agencia_validar_vertigen"),								'trim|required');
							$this->form_validation->set_rules("conocido_por",		 					lang("agencia_validar_conocido_por"), 							'trim|required');
							$this->form_validation->set_rules("otros", 									lang("agencia_validar_otros"), 									'trim|callback_check_campo["otros", "conocido_por","6"]');
							$this->form_validation->set_rules("dificultatesports", 						lang("agencia_validar_dificultatesports"),						'trim|required');
							$this->form_validation->set_rules("dificultatesports_quins", 				lang("dificultatesports_quins"), 								'trim|callback_check_campo["dificultatesports_quins", "dificultatesports","true"]');
							$this->form_validation->set_rules("futbol_equipacion",	 					lang("agencia_validar_futbol_equipacion"), 						'trim'.($z_futbol_js ? "|required" : ""));
			
							
							if($this->form_validation->run() == FALSE) {
								$relanzar = false;
								//$_POST["data_naixement"] = $this->soporte_general->cambia_fecha($_POST["data_naixement"], "-", "/");
								echo validation_errors();
								$this->ver();
							} else {
								/* logamos */ if ($this->devlog) echo __line__."\nGuardando!\n";
						
								$this->inscripcion_model->update_Inscripcio($this->ni);
								
								$rsDades = $this->inscripcion_model->get_DadesInscripcio($this->ni, $this->ip, $this->ic);
                                /* logamos */ if ($this->devlog) echo __line__."\nrsDades: ".print_r($rsDades, true)."\n";
                                
                                $val_numero_reserva = $rsDades[0]->NReserva;
                                $centro = $rsDades[0]->centro;
                                $hostel = $rsDades[0]->hostel;
                                
                                $destino_url = "includes/reservation/sendmail_ins.php?lang=be&hostel=$hostel&idinsc=".$this->ni."&order=$val_numero_reserva&control=".$this->ic."&type=".$this->tr;

                                /* logamos */ if ($this->devlog) echo __line__."\nvamos a lanzar esta url!\n".$destino_url."\n";
                                $this->load->library("enviar_email");
                                $this->enviar_email->enviar_externo($destino_url, $this->tv, $this->tr);  
                                /* logamos */ if ($this->devlog) echo __line__."\Enviado!\n";
								
								$params = array("lang"		=> $this->idioma,
												"control"	=> __LINE__." finalizada_correctamente",
												"mensaje"	=> lang("finalizada_correctamente"));
								$this->load->view($ruta."/mensaje_generico_view", $params);						
							}
						}
					}

					/* logamos */ if ($this->devlog) echo __line__."</pre>";		
				}
		/* FIN - ESPECIFICOS SPAIN */

		function check_campo(/* $Destino, $Origen, $valor */) {

			$args 		= func_get_args();
			log_message("error", "Args ".print_r($args, true));

			$args 		= explode(",",$args[1]);
			log_message("error", "_POST ".print_r($_POST, true));

			$Destino	= str_replace('"', "", trim($args[0]));
			$Origen		= str_replace('"', "", trim($args[1]));
			$valor		= str_replace('"', "", trim($args[2]));
			log_message("error", "D $Destino, O $Origen, V $valor");
			
			/* despues de validar el formulario miramos si el password es correcto. */
			if (isset($_POST[$Origen])) {
				log_message("error", "_POST['".$Origen."'] [".$_POST[$Origen]."] == valor [".$valor."] ==>> ".($_POST[$Origen] == $valor));
				if ($_POST[$Origen] == $valor) {
					log_message("error", "retorno validacion: ".(($_POST[$Destino] != "")?true:false));
					return ($_POST[$Destino] != "")?true:false;
				} else { 
					log_message("error", "retorno default true");
					return true; 
				}
			}
		}

		function montar_div_cvac($cvac, $x, $permitirModificacion, $ruta) {

			$params = array("cvac"					=> $cvac,
							"x"						=> 	$x,
							"permitirModificacion"	=> $permitirModificacion);
			$this->load->view($ruta."/div_cvac_view", $params);
		}

	}

?>