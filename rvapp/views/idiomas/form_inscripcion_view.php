<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. <pre><?php echo print_r($privilegios, true); ?></pre> */
    $CI =& get_instance(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo lang("TitolWeb"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 X-Content-Type-Options=nosniff"/>
        <meta name="google" content="notranslate" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/uikit.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/upload.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/uikit.gradient.min_new.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom_idiomas.css" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />
        <!--<script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>-->
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <!-- script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script-->


        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/uikit.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/components/upload.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <?php
            if ($permitirModificacion) {
                ?><script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script><?php
            }
        ?>
        <style> 
            .btnSubmit {
                padding: 10px;
                background-color: #0069af!important; 
                border: 1px solid #0069af!important; 
                border-radius: 5px!important; 
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px; 
                text-decoration:none; 
                float: right; }
            .uk-width-1-1 {margin-top:10px!important;}
            .uk-width-1-2 {margin-top:10px!important;}
            .formsize {max-width: 60%}
            .fondorojo {
                background-color: firebrick; 
                color:white;
                border-radius: 5px; 
                padding: 5px;
            }
            .box2 {
                border: 2px solid #0069af!important;
                border-radius: 5px!important;                          
                padding: 10px;                
            }
        </style>        
        
    </head>	

    <body onload="javascript:inicializar();">

        <body class="body_css">
            <div class="uk-container uk-container-center uk-main-container">
                <div class="uk-grid uk-grid-preserve">
                    <div class="uk-width-medium-1-2">
                        <img class="logo" src="<?php echo base_url(); ?>images/logo-idiomas-rv-<?php echo strtolower($lang); ?>.png"> <!-- .logo -->                        
                    </div>
                    <div class="uk-width-medium-1-2">
                        <h1 class="uk-article-title">
                            <span><b><?php  echo lang("Titol"); ?></b> <?php /* echo $datos->dni; */ ?></span>
                        </h1>
                    </div>
                </div>
                <br/>
                <br/>
                
                <?php 
                    if ($permitirModificacion) { 
                        ?>
                            <form action="<?php echo base_url(); ?>inscripcion/guardar/<?php echo $param_url; ?>/<?php echo $lang.($forzar==true?"/true":""); ?>" method="post" id="formreservation" name="formreservation" enctype="multipart/form-data" class="uk-form">
                        <?php
                    }
                ?>

                <?php
                    if ($errorFechas) {
                        ?><br /><div id="ErrorFechas">
                                <h2 class="error"><?php echo lang("H_ErrorFechas"); ?></h2>
                                <?php
                                    echo lang("MsgErrorFechas")."<br /><br /><br /><br />";
                                ?>
                        </div><?php
                    }
                ?>
                <br />
                <div id="datos_curso_1">
                    <h2>1 .- <?php echo lang("titulo_datos_curso"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <span><b><?php echo lang("NReserva"); ?> / <?php echo lang("Localitzador"); ?>:</b> <?php echo $datos->numero_reserva_localizador; ?></span>
                            </div>
                            <div class="uk-width-1-2">
                                <span><b><?php echo lang("NomPrograma") ?>:</b> <?php echo $z_programa; ?></span>
                            </div>
                            <div class="uk-width-1-2">
                                <span><b><?php echo lang("Fechas") ?>:</b> <?php echo $z_fdesde_txt; ?> - <?php echo $z_fhasta_txt; ?></span>
                            </div>
                            <div class="uk-width-1-2">
                                <span><b><?php echo lang("Dni") ?>:</b> <?php echo $datos->dni; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <?php 
                    if (validation_errors() != "") {
                        ?>
                            <div class="uk-width-1-1">
                                <div class="uk-alert uk-alert-danger">
                                    <?php echo lang("campos_obligatorios_pendientes"); ?>
                                </div>
                            </div>
                            <br />
                            <br />
                        <?php
                    }
                ?>
                <div id="datos_curso_2">
                    <h2>2 .- <?php echo lang("titulo_datos_participante"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span><b><i><?php echo lang("importante_nombres"); ?></i></b></span>
                                <br/>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nom_Participante"); ?>:</b>            
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_participante", $datos->nombre_participante); }
                                        else { 
                                            ?><input type="text" id="nombre_participante" name="nombre_participante" maxlength="150" class="" value="<?php echo set_value("nombre_participante", $datos->nombre_participante); ?>" /><?php 
                                            echo form_error("nombre_participante");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Cognom_Participante"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("apellidos_participante", $datos->apellidos_participante); }
                                        else { 
                                            ?><input type="text" id="apellidos_participante" name="apellidos_participante" maxlength="150" class="" value="<?php echo set_value("apellidos_participante", $datos->apellidos_participante); ?>" /><?php 
                                            echo form_error("apellidos_participante");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Fecha_nacimiento"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("fecha_nacimiento", $CI->soporte_general->cambia_fecha($datos->fecha_nacimiento, "-", "/")); }
                                        else { 
                                            // echo "<pre>FN:  ".set_value("fecha_nacimiento", $datos->fecha_nacimiento)." - ".$datos->fecha_nacimiento."</pre>";
                                            $fecha_nacimiento = set_value("fecha_nacimiento", $CI->soporte_general->cambia_fecha($datos->fecha_nacimiento, "-", "/"));
                                            /*<input type="hidden" id="fecha_nacimiento" name="fecha_nacimiento" value="<?php echo set_value("fecha_nacimiento", $fecha_nacimiento); ?>"/>*/
                                            ?><br/>
                                                <input type="text" id="fecha_nacimiento" name="fecha_nacimiento" readonly="readonly" maxlength="150" class="formsize" value="<?php echo set_value("fecha_nacimiento", $fecha_nacimiento); ?>" /><?php 
                                                echo form_error("fecha_nacimiento");
                                        } 
                                    ?>                                        
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b>&nbsp;</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("chico_chica", $datos->chico_chica) == 1) { 
                                                echo lang("chico"); 
                                            } else {
                                                echo lang("chica");
                                            }
                                        } else { 
                                                ?><br/><label for="chico_chica"><input type="radio" id="chico_chica" name="chico_chica" <?php if (set_value("chico_chica", $datos->chico_chica) == 1) { echo "checked"; }?> value="1"/><?php echo lang("chico"); ?>
                                                <input type="radio" id="chico_chica" name="chico_chica" <?php if (set_value("chico_chica", $datos->chico_chica) == 0) { echo "checked"; }?> value="0" /><?php echo lang("chica"); ?></label><?php 
                                                echo form_error("chico_chica");
                                        } 
                                    ?>    
                                    
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Edad_Participante"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("edad_participante", $datos->edad_participante); }
                                        else { 
                                                ?><input type="text" id="edad_participante" name="edad_participante" maxlength="2" class="formsize_idiomas_corto" value="<?php echo set_value("edad_participante", $datos->edad_participante); ?>" readonly="readonly"/><?php 
                                                echo form_error("edad_participante");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nacionalidad"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nacionalidad", $datos->nacionalidad); }
                                        else { 
                                                ?><input type="text" id="nacionalidad" name="nacionalidad" maxlength="45" class="formsize_idiomas_4" value="<?php echo set_value("nacionalidad", $datos->nacionalidad); ?>"/><?php 
                                                echo form_error("nacionalidad");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Email_Participantes"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("email_participantes", $datos->email_participantes); }
                                        else { 
                                            ?><input type="text" id="email_participantes" name="email_participantes" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("email_participantes", $datos->email_participantes); ?>" onchange="javascript:update_email(this);"/><?php 
                                            echo form_error("email_participantes");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Movil_Participante"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("movil_participantes", $datos->movil_participantes); }
                                        else { 
                                            ?><input type="text" id="movil_participantes" name="movil_participantes" maxlength="50" class="formsize_idiomas_4" value="<?php echo set_value("movil_participantes", $datos->movil_participantes); ?>"/><?php 
                                            echo form_error("movil_participantes");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <?php 
                                // covid 19
                                // si el centro es el 040028 no mostramos el div de pasaporte...
                                $css_ver_pasaporte = "";
                                if ($z_hostel == "040028") {
                                    $css_ver_pasaporte = " uk-hidden";
                                }
                            ?> 
                            <div class="uk-width-1-1<?php echo $css_ver_pasaporte; ?>">
                                <div class="uk-panel box2">
                                    <div class="uk-grid">
                                        <?php 
                                            $ins_pasaporte = $datos->mini_pasaporte;
                                            if (!$ins_pasaporte) {
                                                $ins_pasaporte =  base_url()."UploadedFiles/IMG/nofoto150.png";
                                            }
                                            if ($permitirModificacion) {
                                                ?>
                                                    <div class="uk-width-1-1">
                                                        <span>
                                                            <b><i><?php echo lang("importante_pasaporte"); ?></i></b>
                                                            <br/>                
                                                            <b><?php echo lang("copia_pasaporte") ?>:</b>
                                                        </span>
                                                    </div>
                                                                
                                                    <div class="uk-width-7-10">
                                                        <style>
                                                            .uk-placeholder {
                                                                margin-bottom: 15px;
                                                                padding: 15px;
                                                                border: 1px dashed #E5E5E5;
                                                                background: #fafafa;
                                                                color: #666;
                                                                overflow: hidden;
                                                            }
                                                            .uk-form-file input[type=file] {
                                                                position: absolute;
                                                                top: 0;
                                                                z-index: 1;
                                                                width: 100%;
                                                                opacity: 0;
                                                                cursor: pointer;
                                                                left: 0;
                                                                font-size: 500px;
                                                            }
                                                            .ok {
                                                                color: darkgreen !important;
                                                            }
                                                            .ko {
                                                                color: firebrick !important;
                                                            }
                                                        </style>

                                                        <div id="upload-drop" class="uk-placeholder uk-text-center uk-margin-top uk-position-relative uk-overflow-hiddden">
                                                            <i class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i> <?php echo lang("texto_adjuntar_1"); ?>
                                                            <a class="uk-form-file"><?php echo lang("texto_adjuntar_2"); ?><input id="upload-select" name="upload-select" type="file"/></a>.
                                                        </div>

                                                        <div id="progressbar" class="uk-progress uk-hidden">
                                                            <div class="uk-progress-bar" style="width: 0%;">...</div>
                                                        </div>
                                                        <div id="resultadothumb" class="uk-hidden">
                                                            <label class="ok uk-hidden"><?php echo lang("texto_adjunto_ok"); ?></label>
                                                            <label class="ko uk-hidden"><?php echo lang("texto_adjunto_ko"); ?></label>
                                                        </div>
                                                    </div>
                                                <?php 
                                            }
                                        ?>
                                        <div class="uk-width-3-10">
                                            <img width="150px" src="<?php echo $ins_pasaporte; ?>" class="input uk-thumbnail uk-width-1-1" name="imagen_pasaporte_th" id="imagen_pasaporte_th" />
                                            <div id="vomito" class="uk-width-1-1 uk-hidden"></div>
                                        </div>
                                    </div><!-- grid -->
                                </div>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Algun_Curso"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("algun_curso", $datos->algun_curso) == "true") { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="algun_curso" name="algun_curso" <?php if (set_value("algun_curso", $datos->algun_curso) == lang("val_True")) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionalesI(this.value, 'algun_curso_detalle'); controlAdicionalesI(this.value, 'algun_curso_detalle2');" /><?php echo lang("True"); ?>
                                            <input type="radio" id="algun_curso" name="algun_curso" <?php if (set_value("algun_curso", $datos->algun_curso) != lang("val_True")) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionalesI(this.value, 'algun_curso_detalle'); controlAdicionalesI(this.value, 'algun_curso_detalle2');" /><?php echo lang("False"); ?><?php 
                                            echo form_error("algun_curso");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Algun_Curso_Detalle"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("algun_curso_detalle", $datos->algun_curso_detalle); }
                                        else { 
                                            ?><input type="text" id="algun_curso_detalle" name="algun_curso_detalle" maxlength="150" class="formsize" value="<?php echo set_value("algun_curso_detalle", $datos->algun_curso_detalle); ?>"/><?php 
                                            echo form_error("algun_curso_detalle");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Algun_Curso_Detalle2"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("algun_curso_detalle2", $datos->algun_curso_detalle2); }
                                        else { 
                                            ?><input type="text" id="algun_curso_detalle2" name="algun_curso_detalle2" maxlength="150" class="formsize" value="<?php echo set_value("algun_curso_detalle2", $datos->algun_curso_detalle2); ?>"/><?php 
                                            echo form_error("algun_curso_detalle2");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <b><?php echo lang("Alguna_Enfermedad"); ?>:</b>
                                <?php 
                                    if (!$permitirModificacion) { 
                                        if (set_value("alguna_enfermedad", $datos->alguna_enfermedad) == 1) { 
                                            echo lang("True");
                                        } else {
                                            echo lang("False");    
                                        }
                                    } else { 
                                            ?><input type="radio" id="alguna_enfermedad" name="alguna_enfermedad" <?php if (set_value("alguna_enfermedad", $datos->alguna_enfermedad) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'alguna_enfermedad_detalle');" /><?php echo lang("True"); ?>
                                            <input type="radio" id="alguna_enfermedad" name="alguna_enfermedad" <?php if (set_value("alguna_enfermedad", $datos->alguna_enfermedad) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'alguna_enfermedad_detalle');" /><?php echo lang("False"); ?><?php 
                                            echo form_error("alguna_enfermedad");
                                    } 
                                ?>
                            </div>
                            <div class="uk-width-1-1">
                                <b>&nbsp;<?php echo lang("Alguna_Enfermedad_Detalle"); ?>:</b>
                                <?php 
                                    if (!$permitirModificacion) { echo set_value("alguna_enfermedad_detalle", $datos->alguna_enfermedad_detalle); }
                                    else { 
                                        ?><input type="text" id="alguna_enfermedad_detalle" name="alguna_enfermedad_detalle" maxlength="150" class="formsize" value="<?php echo set_value("alguna_enfermedad_detalle", $datos->alguna_enfermedad_detalle); ?>"/><?php 
                                        echo form_error("alguna_enfermedad_detalle");
                                    } 
                                ?>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Algun_Medicamento"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("algun_medicamento", $datos->algun_medicamento) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="algun_medicamento" name="algun_medicamento" <?php if (set_value("algun_medicamento", $datos->algun_medicamento) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'algun_medicamento_detalle');" /><?php echo lang("True"); ?>
                                            <input type="radio" id="algun_medicamento" name="algun_medicamento" <?php if (set_value("algun_medicamento", $datos->algun_medicamento) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'algun_medicamento_detalle');" /><?php echo lang("False"); ?><?php 
                                            echo form_error("algun_medicamento");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b>&nbsp;<?php echo lang("Algun_Medicamento_Detalle"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("algun_medicamento_detalle", $datos->algun_medicamento_detalle); }
                                        else { 
                                            ?><input type="text" id="algun_medicamento_detalle" name="algun_medicamento_detalle" maxlength="150" class="formsize" value="<?php echo set_value("algun_medicamento_detalle", $datos->algun_medicamento_detalle); ?>"/><?php 
                                            echo form_error("algun_medicamento_detalle");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Dieta_Especial"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("dieta_especial", $datos->dieta_especial) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="dieta_especial" name="dieta_especial" <?php if (set_value("dieta_especial", $datos->dieta_especial) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'dieta_especial_detalle');" /><?php echo lang("True"); ?>
                                            <input type="radio" id="dieta_especial" name="dieta_especial" <?php if (set_value("dieta_especial", $datos->dieta_especial) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'dieta_especial_detalle');" /><?php echo lang("False"); ?><?php 
                                            echo form_error("dieta_especial");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b>&nbsp;<?php echo lang("Dieta_Especial_Detalle"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("dieta_especial_detalle", $datos->dieta_especial_detalle); }
                                        else { 
                                            ?><input type="text" id="dieta_especial_detalle" name="dieta_especial_detalle" maxlength="150" class="formsize" value="<?php echo set_value("dieta_especial_detalle", $datos->dieta_especial_detalle); ?>"/><?php 
                                            echo form_error("dieta_especial_detalle");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <div class="uk-panel fondorojo">
                                    <div class="uk-grid">
                                        <div class="uk-width-1-1">
                                            <b><?php echo lang("Alguna_alergia"); ?>:</b>
                                            <?php 
                                                if (!$permitirModificacion) { 
                                                    if (set_value("alguna_alergia", $datos->alguna_alergia) == 1) { 
                                                        echo lang("True");
                                                    } else {
                                                        echo lang("False");    
                                                    }
                                                } else { 
                                                    ?><input type="radio" id="alguna_alergia" name="alguna_alergia" <?php if (set_value("alguna_alergia", $datos->alguna_alergia) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'alguna_alergia_detalle');" /><?php echo lang("True"); ?>
                                                    <input type="radio" id="alguna_alergia" name="alguna_alergia" <?php if (set_value("alguna_alergia", $datos->alguna_alergia) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'alguna_alergia_detalle');" /><?php echo lang("False"); ?><?php 
                                                    echo form_error("alguna_alergia");
                                                } 
                                            ?>
                                        </div>
                                        <div class="uk-width-1-1">
                                            <b>&nbsp;<?php echo lang("Alguna_alergia_Detalle"); ?>:</b>
                                            <?php 
                                                if (!$permitirModificacion) { echo set_value("alguna_alergia_detalle", $datos->alguna_alergia_detalle); }
                                                else { 
                                                    ?><input type="text" id="alguna_alergia_detalle" name="alguna_alergia_detalle" maxlength="150" class="formsize" value="<?php echo set_value("alguna_alergia_detalle", $datos->alguna_alergia_detalle); ?>"/><?php 
                                                    echo form_error("alguna_alergia_detalle");
                                                } 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Amigos_Familiares") ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("amics_familiars", $datos->amics_familiars) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="amics_familiars" name="amics_familiars" <?php if (set_value("amics_familiars", $datos->amics_familiars) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'nombre_amigo');" /><?php echo lang("True"); ?>
                                            <input type="radio" id="amics_familiars" name="amics_familiars" <?php if (set_value("amics_familiars", $datos->amics_familiars) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'nombre_amigo');" /><?php echo lang("False"); ?><?php 
                                            echo form_error("amics_familiars");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_amigo") ?>:</b><br/> 
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_amigo", $datos->nombre_amigo); }
                                        else { 
                                            ?><input type="text" id="nombre_amigo" name="nombre_amigo" maxlength="150" class="uk-width-1-2 formsize" value="<?php echo set_value("nombre_amigo", $datos->nombre_amigo); ?>"/><?php 
                                            echo form_error("nombre_amigo");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <div class="uk-panel fondorojo">                                    
                                    &nbsp;<b><?php echo lang("Amigos_Juntos") ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("amigos_juntos", $datos->amigos_juntos) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="amigos_juntos" name="amigos_juntos" <?php if (set_value("amigos_juntos", $datos->amigos_juntos) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>"/><b><?php echo lang("True"); ?></b>
                                            <input type="radio" id="amigos_juntos" name="amigos_juntos" <?php if (set_value("amigos_juntos", $datos->amigos_juntos) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>"/><b><?php echo lang("False"); ?></b><?php 
                                            echo form_error("amigos_juntos");
                                        } 
                                    ?>&nbsp;
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div id="datos_curso_3">
                    <h2>3 .- <?php echo lang("titulo_datos_contacto"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_Padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_padres", $datos->nombre_padres); } 
                                        else { 
                                            ?><input type="text" id="nombre_padres" name="nombre_padres" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("nombre_padres", $datos->nombre_padres); ?>" onchange="javascript:update_name(this);"/><?php 
                                            echo form_error("nombre_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("DNI_Padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("dni_padres", $datos->dni_padres); } 
                                        else { 
                                            ?><input type="text" id="dni_padres" name="dni_padres" maxlength="10" class="formsize_idiomas_4" value="<?php echo set_value("dni_padres", $datos->dni_padres); ?>" onchange="javascript:update_dni(this);"/><?php 
                                            echo form_error("dni_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Email_Padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("email_padres", $datos->email_padres); } 
                                        else { 
                                            ?><input type="text" id="email_padres" name="email_padres" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("email_padres", $datos->email_padres); ?>" onchange="javascript:update_email(this);"/><?php 
                                            echo form_error("email_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Movil"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("movil", $datos->movil); } 
                                        else { 
                                            ?><input type="text" id="movil" name="movil" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("movil", $datos->movil); ?>"/><?php 
                                            echo form_error("movil");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Movil2"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("movil_padres", $datos->movil_padres); } 
                                        else { 
                                            ?><input type="text" id="movil_padres" name="movil_padres" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("movil_padres", $datos->movil_padres); ?>"/><?php 
                                            echo form_error("movil_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Direccion_Participante"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("direccion_participante", $datos->direccion_participante); } 
                                        else { 
                                            ?><input type="text" id="direccion_participante" name="direccion_participante" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("direccion_participante", $datos->direccion_participante); ?>"/><?php 
                                            echo form_error("direccion_participante");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Codigo_postal"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("codigo_postal", $datos->codigo_postal); } 
                                        else { 
                                            ?><input type="text" id="codigo_postal" name="codigo_postal" maxlength="5" class="formsize_idiomas_4" value="<?php echo set_value("codigo_postal", $datos->codigo_postal); ?>"/><?php 
                                            echo form_error("codigo_postal");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Poblacion"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("poblacion", $datos->poblacion); } 
                                        else { 
                                            ?><input type="text" id="poblacion" name="poblacion" maxlength="150" class="formsize_idiomas_4" value="<?php echo set_value("poblacion", $datos->poblacion); ?>"/><?php 
                                            echo form_error("poblacion");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("OtrasConsideraciones"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("otras_consideraciones", $datos->otras_consideraciones); } 
                                        else { 
                                            ?><textarea cols="80" rows="10" id="otras_consideraciones" name="otras_consideraciones" class="formsize_otras_consideraciones"><?php echo set_value("otras_consideraciones", $datos->otras_consideraciones); ?></textarea><?php 
                                            echo form_error("otras_consideraciones");
                                        } 
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div id="datos_curso_4">
                    <h2>4 .- <?php echo lang("titulo_datos_academicos"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Colegio"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("colegio", $datos->colegio); } 
                                        else { 
                                            ?><input type="text" id="colegio" name="colegio" maxlength="150" class="formsize" value="<?php echo set_value("colegio", $datos->colegio); ?>"/><?php 
                                            echo form_error("colegio");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Colegio_miniestancia"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("colegio_miniestancia", $datos->colegio_miniestancia) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="colegio_miniestancia" name="colegio_miniestancia" <?php if (set_value("colegio_miniestancia", $datos->colegio_miniestancia) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" /><?php echo lang("True"); ?>
                                            <input type="radio" id="colegio_miniestancia" name="colegio_miniestancia" <?php if (set_value("colegio_miniestancia", $datos->colegio_miniestancia) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?><?php 
                                            echo form_error("colegio_miniestancia");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_Profesor"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_profesor", $datos->nombre_profesor); } 
                                        else { 
                                            ?><input type="text" id="nombre_profesor" name="nombre_profesor" maxlength="150" class="formsize" value="<?php echo set_value("nombre_profesor", $datos->nombre_profesor); ?>"/><?php 
                                            echo form_error("nombre_profesor");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Curso"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("curso", $datos->curso); } 
                                        else { 
                                            ?><input type="text" id="curso" name="curso" maxlength="45" class="formsize_idiomas_4" value="<?php echo set_value("curso", $datos->curso); ?>"/><?php 
                                            echo form_error("curso");
                                        } 
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div id="datos_curso_5">
                    <h2>5 .- <?php echo lang("titulo_autorizacion_sin_monitor"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("autorizacion_sin_monitor_padres"); ?></b>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_autorizacion_padre"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_autorizacion_padre", $datos->nombre_autorizacion_padre); } 
                                        else { 
                                            ?><input type="text" id="nombre_autorizacion_padre" name="nombre_autorizacion_padre" maxlength="150" class="formsize" value="<?php echo set_value("nombre_autorizacion_padre", $datos->nombre_autorizacion_padre); ?>"/><?php 
                                            echo form_error("nombre_autorizacion_padre");
                                        } 
                                    ?>
                                </span>
                            </div>  
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("DNI_autorizacion_padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("dni_autorizacion_padres", $datos->dni_autorizacion_padres); } 
                                        else { 
                                            ?><input type="text" id="dni_autorizacion_padres" name="dni_autorizacion_padres" maxlength="15" class="formsize_idiomas_4" value="<?php echo set_value("dni_autorizacion_padres", $datos->dni_autorizacion_padres); ?>"/><?php 
                                            echo form_error("dni_autorizacion_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <input type="checkbox" name="autoritzacio_acceptacio_monitor" id="autoritzacio_acceptacio_monitor" <?php if ( ( (count($_POST) == 0)?$datos->autoritzacio_acceptacio_monitor:set_value("autoritzacio_acceptacio_monitor") ) == 1 ) { echo "checked"; } ?> value="1" <?php if (!$permitirModificacion) { echo "disabled"; } ?> /><b><?php echo lang("Autoritzacio_acceptacio_monitor"); ?></b>
                                    <?php echo form_error("autoritzacio_acceptacio_monitor"); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div id="datos_curso_6">
                    <h2>6 .- <?php echo lang("titulo_autorizacion_medica"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("autorizacion_medica_padres"); ?></b>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_autorizacion_padre_medica"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_autorizacion_padre_medica", $datos->nombre_autorizacion_padre_medica); } 
                                        else { 
                                            ?><input type="text" id="nombre_autorizacion_padre_medica" name="nombre_autorizacion_padre_medica" maxlength="150" class="formsize" value="<?php echo set_value("nombre_autorizacion_padre_medica", $datos->nombre_autorizacion_padre_medica); ?>"/><?php 
                                            echo form_error("nombre_autorizacion_padre_medica");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("DNI_autorizacion_padres_medica"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("dni_autorizacion_padres_medica", $datos->dni_autorizacion_padres_medica); } 
                                        else { 
                                            ?><input type="text" id="dni_autorizacion_padres_medica" name="dni_autorizacion_padres_medica" maxlength="15" class="formsize_idiomas_4" value="<?php echo set_value("dni_autorizacion_padres_medica", $datos->dni_autorizacion_padres_medica); ?>"/><?php 
                                            echo form_error("dni_autorizacion_padres_medica");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <?php 
                                        /*
                                            <pre><?php echo "A M: ".set_value("autoritzacio_acceptacio_medica")." - ".$datos->autoritzacio_acceptacio_medica; ?></pre>
                                            <pre><?php echo "POST: ".print_r($_POST, true); ?></pre>
                                        */
                                    ?>
                                    <input type="checkbox" name="autoritzacio_acceptacio_medica" id="autoritzacio_acceptacio_medica" <?php if ( ( (count($_POST) == 0)?$datos->autoritzacio_acceptacio_medica:set_value("autoritzacio_acceptacio_medica") ) == 1 ) { echo "checked"; } ?> value="1" <?php if (!$permitirModificacion) { echo "disabled"; } ?> /><b><?php echo lang("Autoritzacio_acceptacio_medica"); ?></b>
                                    <?php echo form_error("autoritzacio_acceptacio_medica"); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div id="datos_curso_7">
                    <h2>7 .- <?php echo lang("titulo_autorizacion_audiovisual"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("autorizacion_audiovisual_padres"); ?></b>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Nombre_autorizacion_audiovisual_padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("nombre_autorizacion_padre_medica", $datos->nombre_autorizacion_padre_medica); } 
                                        else { 
                                            ?><input type="text" id="nombre_autorizacion_audiovisual_padres" name="nombre_autorizacion_audiovisual_padres" maxlength="150" class="formsize" value="<?php echo set_value("nombre_autorizacion_padre_medica", $datos->nombre_autorizacion_padre_medica); ?>"/><?php 
                                            echo form_error("nombre_autorizacion_audiovisual_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-2">
                                <span>
                                    <b><?php echo lang("Dni_autorizacion_audiovisual_padres"); ?>:</b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("dni_autorizacion_padres_medica", $datos->dni_autorizacion_padres_medica); } 
                                        else { 
                                            ?><input type="text" id="dni_autorizacion_audiovisual_padres" name="dni_autorizacion_audiovisual_padres" maxlength="15" class="formsize_idiomas_4" value="<?php echo set_value("dni_autorizacion_padres_medica", $datos->dni_autorizacion_padres_medica); ?>"/><?php 
                                            echo form_error("nombre_autorizacion_audiovisual_padres");
                                        } 
                                    ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Autoritzacio_acceptacio_audiovisual"); ?></b><br/>
                                    <?php 
                                        if (!$permitirModificacion) { 
                                            if (set_value("autoritzacio_acceptacio_audiovisual", $datos->autoritzacio_acceptacio_audiovisual) == 1) { 
                                                echo lang("True");
                                            } else {
                                                echo lang("False");    
                                            }
                                        } else { 
                                            ?><input type="radio" id="autoritzacio_acceptacio_audiovisual" name="autoritzacio_acceptacio_audiovisual" <?php if (set_value("autoritzacio_acceptacio_audiovisual", $datos->autoritzacio_acceptacio_audiovisual) == 1) { echo "checked"; }?> value="<?php echo lang("val_True"); ?>" /><?php echo lang("True"); ?>
                                            <input type="radio" id="autoritzacio_acceptacio_audiovisual" name="autoritzacio_acceptacio_audiovisual" <?php if (set_value("autoritzacio_acceptacio_audiovisual", $datos->autoritzacio_acceptacio_audiovisual) == 0) { echo "checked"; }?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?><?php 
                                            echo form_error("autoritzacio_acceptacio_audiovisual");
                                        } 
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div id="datos_curso_8">
                    <h2>8 .- <?php echo lang("titulo_observaciones"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("observaciones_desc"); ?></b>
                                    <?php 
                                        if (!$permitirModificacion) { echo set_value("observaciones", $datos->observaciones); } 
                                        else { 
                                            ?><textarea cols="80" rows="10" id="observaciones" name="observaciones" class="formsize_observaciones"><?php echo set_value("observaciones", $datos->observaciones); ?></textarea><?php 
                                            echo form_error("observaciones");
                                        } 
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div id="datos_curso_9">
                    <h2>9 .- <?php echo lang("titulo_como_conocido"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <?php 
                                if (!$permitirModificacion) { 
                                    ?>
                                        <div class="uk-width-1-1">
                                            <?php
                                                switch (set_value("conocido_por", $datos->conocido_por)) {
                                                    case 8:     echo lang("conocido_clientes"); break;
                                                    case 0:     echo lang("conocido_radio"); break;
                                                    case 1:     echo lang("conocido_TV"); break;
                                                    case 7:     echo lang("Metro"); break;
                                                    case 2:     echo lang("conocido_amigos"); break;
                                                    case 10:    echo lang("Autobus"); break;
                                                    case 2:     echo lang("conocido_amigos"); break;
                                                    case 3:     echo lang("conocido_internet"); break;
                                                    case 4:     echo lang("conocido_Colegio"); break;
                                                    case 6:     echo lang("Otros"); 
                                                                echo "<br/>".set_value("otros", $datos->otros);
                                                            break;
                                                }
                                            ?>
                                        </div>
                                    <?php
                                } else { 
                                    ?>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 8) { echo "checked"; }?> value="8" /><?php echo lang("conocido_clientes"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 0) { echo "checked"; }?> value="0" /><?php echo lang("conocido_radio"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 1) { echo "checked"; }?> value="1" /><?php echo lang("conocido_TV"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 7) { echo "checked"; }?> value="7" /><?php echo lang("Metro"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 2) { echo "checked"; }?> value="2" /><?php echo lang("conocido_amigos"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 10) { echo "checked"; }?> value="10" /><?php echo lang("Autobus"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 3) { echo "checked"; }?> value="3" /><?php echo lang("conocido_internet"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-1-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 4) { echo "checked"; }?> value="4" /><?php echo lang("conocido_Colegio"); ?>
                                            </span>
                                        </div>
                                        <div class="uk-width-2-5">
                                            <span>
                                                <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if (set_value("conocido_por", $datos->conocido_por) == 6) { echo "checked"; }?> value="6" /><?php echo lang("Otros"); ?>
                                                <input type="text" id="otros" name="otros" maxlength="150" class="formsize" value="<?php echo $datos->otros; ?>" />
                                            </span>
                                        </div>
                                    <?php 
                                    echo form_error("conocido_por");
                                    echo form_error("otros");
                                } 
                            ?>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div id="datos_curso_condiciones">
                    <h2>10 .- <?php echo lang("condiciones_generales"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <?php echo lang("Normativa_autoritzacio") ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <input type="checkbox" name="normativa_acceptacio" id="normativa_acceptacio" <?php if ( ( (count($_POST) == 0)?$datos->normativa_acceptacio:set_value("normativa_acceptacio") ) == 1 ) { echo "checked"; } ?> value="1" <?php if (!$permitirModificacion) { echo "disabled"; } ?> /><b><?php echo lang("Normativa_acceptacio"); ?></b>
                                    <?php echo form_error("normativa_acceptacio"); ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    * <?php echo lang("Normativa_llegir") ?> <a onclick="javascript:document.getElementById('termsAndConditions').style.display='block';" href="#"><?php echo lang("Normativa_texte_enllac") ?></a><br />
                                </span>
                            </div>
                            <div class="uk-width-1-1" id="datos_curso_proteccion" align="justify">
                                <h6><?php echo lang("proteccion_datos") ?></h6>
                                <input type="hidden" class="input" value="<?php echo set_value("email_participantes", $datos->email_participantes); ?>" id="confirmacionxmail" name="confirmacionxmail">
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <?php 
                    if ($permitirModificacion) { ?><input type="button" class="btnSubmit" value="<?php echo lang("Botonera"); ?>" onclick="javascript:validacion();"><?php } 
                ?>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>

            <div class="tm-footer">
                <div class="uk-container uk-container-center">
                    <div class="uk-grid uk-grid-preserve">
                        <div class="uk-width-medium-1-2">
                            &nbsp;
                        </div>
                        <div class="uk-width-medium-1-2 uk-text-right">
                            <p class="white">Copyright <?php echo date("Y"); ?> Viatges Rosa dels Vents S.A.</p>
                        </div>
                    </div> <!-- grid -->
                </div> <!-- container -->
            </div>

        <?php if ($permitirModificacion) { ?></form><?php } ?>

        <div id="termsAndConditions" class="my-modal">
            <div class="uk-container uk-container-center">
                <div class="my-modal-dialog uk-width-medium-8-10 uk-push-1-10">
                    <div class="toolbar">
                        <h3><?php echo $CI->inscripcion_model->getCustomerStrings("terms_and_conditions", $lang); ?></h3>
                        <div class="close" onclick="javascript:document.getElementById('termsAndConditions').style.display='none';">
                            X
                        </div>
                    </div>
                    <div class="content">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="left" valign="middle" class="terms_text">
                                        <?php echo str_replace("\n","<br>", $CI->inscripcion_model->getHotelStrings($z_hostel, "terms_and_conditions", $lang)); ?>
                                    </td>
                                </tr>  
                            </tbody>
                        </table>
                    </div><!--content-->
                </div> <!--width-->
            </div><!--uk-modal-dialog container-->
        </div><!--uk-modal-->

        <script languaje="javascript">
/*            function CheckFileName(campo) {
                var fileName = campo.value
                if (fileName == "") {
                    //alert("Browse to upload a valid File with png extension");
                    campo.value = null;
                    return false;
                }
                else if (
                        fileName.split(".")[1].toUpperCase() == "JPG" ||
                        fileName.split(".")[1].toUpperCase() == "PJPG" ||
                        fileName.split(".")[1].toUpperCase() == "GIF" ||
                        fileName.split(".")[1].toUpperCase() == "PDF" ||
                        fileName.split(".")[1].toUpperCase() == "PNG"
                        )
                    $("#ficheros").submit();
                else {
                    //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                    campo.value = null;
                    return false;
                }
                return true;
            }
*/
            $(function(){

                var progressbar = $("#progressbar"),
                    bar         = progressbar.find('.uk-progress-bar'),
                    settings    = {

                    action: '<?php echo base_url(); ?>doing/upload/<?php echo $param_url; ?>/PASS', // upload url

                    allow : '*.(jpg|jpeg|gif|png)', // allow only images

                    loadstart: function() {
                        bar.css("width", "0%").text("0%");
                        progressbar.removeClass("uk-hidden");
                    },

                    progress: function(percent) {
                        percent = Math.ceil(percent);
                        bar.css("width", percent+"%").text(percent+"%");
                    },

                    allcomplete: function(response) {

                        bar.css("width", "100%").text("100%");

                        setTimeout(function(){
                            progressbar.addClass("uk-hidden");
                        }, 250);

                        console.log(response);
                        $("#resultadothumb").removeClass("uk-hidden");                            
                        if(response.startsWith("http")) {
                            $("#imagen_pasaporte_th").attr("src", response);
                            $("#resultadothumb .ok").removeClass("uk-hidden");
                            $("#resultadothumb .ko").removeClass("uk-hidden").addClass("uk-hidden");
                        } else {
                            $("#resultadothumb .ok").removeClass("uk-hidden").addClass("uk-hidden");
                            $("#resultadothumb .ko").removeClass("uk-hidden");
                        }
                    }
                };

                var select = UIkit.uploadSelect($("#upload-select"), settings),
                    drop   = UIkit.uploadDrop($("#upload-drop"), settings);
            });


            function update_name(campo) {
                document.getElementById('nombre_autorizacion_padre').value = campo.value;
                document.getElementById('nombre_autorizacion_padre_medica').value = campo.value;                
                document.getElementById('nombre_autorizacion_audiovisual_padres').value = campo.value;                
            }

            function update_dni(campo) {
                document.getElementById('dni_autorizacion_padres').value = campo.value;
                document.getElementById('dni_autorizacion_padres_medica').value = campo.value;                
                document.getElementById('dni_autorizacion_audiovisual_padres').value = campo.value;                
            }

            function update_email(campo) {
                document.getElementById('confirmacionxmail').value = campo.value;
            }

            function edad(fnac, fent, cdest) {
                if (fnac.indexOf("/")>0) {
                    fnac = fnac.split("/")[2]+"-"+fnac.split("/")[1]+"-"+fnac.split("/")[0];
                }                
                var fechaNac = new Date(fnac);
                var fechaEnt = new Date(fent);
                var edad = fechaEnt.getTime() - fechaNac.getTime();
                var fres = new Date(edad);
                document.getElementById(cdest).value = Math.floor(fres.getFullYear()-1970);
                /* alert("fechaNac "+fechaNac+"\nfechaEnt "+fechaEnt+ "\nedad "+edad+ "\nfres "+fres); */
            }

            <?php 
                if ($permitirModificacion) {
                    ?>
                        $(function() {
                            $.datepicker.setDefaults({
                                yearRange: '<?php echo $datos->ano_minimo.":".$datos->ano_maximo;?>',
                                dateFormat: 'dd/mm/yy',
                                changeMonth: true,
                                changeYear: true,
/*                                altField: 'fecha_nacimiento',
                                altFormat: 'yy-mm-dd',
*/                                onClose: function( selectedDate) {
                                    edad(selectedDate, '<?php echo $datos->data_in; ?>', 'edad_participante');
                                }                    
                            });
                            $('#fecha_nacimiento').datepicker();
                        });
                    <?php
                }
            ?>

            function actualizarCampo(desde, hasta) {
                document.getElementById(hasta).value = document.getElementById(desde).value;
            }

            function controlAdicionales(activar, cadena) {
                var campo;
                var campos = cadena.split("|");
                for (var campo in campos) {
                    document.getElementById(campos[campo]).disabled = (activar != <?php echo lang("val_True"); ?>);
                }
            }

            function controlAdicionalesI(activar, cadena) {
                var campo;
                var campos = cadena.split("|");
                for (var campo in campos) {
                    document.getElementById(campos[campo]).disabled = (activar == <?php echo lang("val_True"); ?>);
                }
            }

            function inicializar() {
    		    controlAdicionales(document.getElementById('amics_familiars').checked, 'nombre_amigo');
                controlAdicionalesI(document.getElementById('algun_curso').checked, 'algun_curso_detalle');
                controlAdicionalesI(document.getElementById('algun_curso').checked, 'algun_curso_detalle2');
                controlAdicionales(document.getElementById('alguna_enfermedad').checked, 'alguna_enfermedad_detalle');
                controlAdicionales(document.getElementById('alguna_alergia').checked, 'alguna_alergia_detalle');
                controlAdicionales(document.getElementById('dieta_especial').checked, 'dieta_especial_detalle');
                controlAdicionales(document.getElementById('algun_medicamento').checked, 'algun_medicamento_detalle');
        	    validar_con();
            }

    		function validar_con() {	
    			if(window.document.formreservation.conocido_por[8].checked == true){
    				document.getElementById('otros').disabled = false;
    			}else{
    				document.getElementById('otros').disabled = true;
    			}
    		}
                
            function validart(campo) {
                var texto = document.getElementById(campo).value.replace(/^\s*|\s*$/g,"");
                var ok=false;
                if (texto.length>0) {
                    ok=true;
                }           
                return ok;
            }
                
             function validarr(campo) {
                //var tamanyo = document.getElementById(campo).length;
                var valor = document.getElementById(campo).checked;
                var resultado = (valor.toString() == '<?php echo lang("val_True") ?>');
                return resultado;
            }
            
            // Cambiado por Juanma
            function validarradio(campo) {
                //var tamanyo = document.getElementById(campo).length;
                //var ok=true;
                return (typeof $("#"+campo+":checked").val() != "undefined");
                //return ok;
            }

             function validarc(campo) {
                //var tamanyo = document.getElementById(campo).length;
                var valor = document.getElementById(campo).checked;
                var resultado = (valor.toString() == '<?php echo lang("val_True") ?>');
                return resultado;
            }

            function validarmt(cadena) {
                var campo, texto;
                var ok=true;
                var campos = cadena.split("|");
                
                for (campo in campos) {
                    texto = document.getElementById(campos[campo]).value.replace(/^\s*|\s*$/g,"");
                    if (texto.length>0) {
                        ok=(ok==true);
                    } else {
                        ok=false;
                    }
                }
                           
                return ok;
            }
	
            function validarf(campo){  
                var cadena = document.getElementById(campo).value;
                var fecha = new String(cadena); //  Crea un string  
                var realfecha = new Date()      //  Para sacar la fecha de hoy  
                //  Cadena Año  
                var ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
                console.log("ano "+ano);
                //  Cadena Mes  
                var mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
                console.log("mes "+mes);
                //  Cadena Día  
                var dia= new String(fecha.substring(0,fecha.indexOf("/")))
                console.log("dia "+dia);

    		    if (fecha.lastIndexOf("/")== -1) {
                    console.log("caca");
                    return false  
                }  

                // Valido el año  
                if (isNaN(ano) || ano.length<4 || parseFloat(ano)<1900){  
                    console.log('Año inválido')  
                    return false  
                }  

                // Valido el Mes  
                if (isNaN(mes) || parseFloat(mes)<1 || parseFloat(mes)>12){  
                    console.log('Mes inválido')  
                    return false  
                }  

                // Valido el Dia  
                if (isNaN(dia) || parseInt(dia, 10)<1 || parseInt(dia, 10)>31){  
                    console.log('Día inválido')  
                    return false  
                }  

                if (mes==4 || mes==6 || mes==9 || mes==11 || mes==2) {  
                    if ((mes==2 && dia>28 && (parseFloat(ano)%4!=0)) || (dia>30)) {  
                        console.log('Día inválido')  
                        return false  
                    }  
                }  

                // para que envie los datos, quitar las  2 lineas siguientes  
                // alert("Fecha correcta.")  
                return true
            }
                
            function validar_conocido() {    
                if(window.document.formreservation.conocido_por[8].checked == true){
                    return validart('otros');
                }else{
                    return true;
                }
            }

            <?php 
                if ($permitirModificacion) { 
                    ?>
                        function validacion() {
                            document.forms['formreservation'].submit();
                        }

                        function finalizar() {
                            <?php /* document.getElementById('envio').value='<?php echo $datos->formAccio ?>'; */ ?>
                            document.forms['formreservation'].submit();
                        }
                    <?php
                }
            ?>
            //-->
        </script>
    </body>
</html>
