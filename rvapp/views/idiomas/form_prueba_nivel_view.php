<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. <pre><?php echo print_r($privilegios, true); ?></pre> */
    $CI =& get_instance(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo lang("TitolWeb"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 X-Content-Type-Options=nosniff"/>
        <meta name="google" content="notranslate" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/uikit.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/upload.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/uikit.gradient.min_new.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom_idiomas.css" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />
        <!--<script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>-->
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>


        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/uikit.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/components/upload.min.js"></script>
        
        <style> 
            .btnSubmit {
                padding: 10px;
                background-color: #0069af!important; 
                border: 1px solid #0069af!important; 
                border-radius: 5px!important; 
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px; 
                text-decoration:none; 
                float: right; }
            .uk-width-1-1 {margin-top:10px!important;}
            .uk-width-1-2 {margin-top:10px!important;}
            .formsize {max-width: 60%}
            .fondorojo {
                background-color: firebrick; 
                color:white;
                border-radius: 5px; 
                padding: 5px;
            }
            .box2 {
                border: 2px solid #0069af!important;
                border-radius: 5px!important;                          
                padding: 10px;                
            }
        </style>        
        
    </head>	


    <body class="body_css">

        <div class="uk-container uk-container-center uk-main-container uk-height-1-1">
            <div class="uk-grid uk-grid-preserve">
                <div class="uk-width-medium-1-2">
                    <img class="logo" src="<?php echo base_url(); ?>images/logo-idiomas-rv-<?php echo strtolower($lang); ?>.png"> <!-- .logo -->                        
                </div>
                <div class="uk-width-medium-1-2">
                    <h1 class="uk-article-title">
                        <span><b><?php  echo lang("Titol"); ?></b> <?php /* echo $datos->dni; */ ?></span>
                    </h1>
                </div>
            </div>
            <br/>
            <br/>

            <form action="<?php echo base_url(); ?>testnivel/guardar/<?php echo $param_url; ?>/<?php echo $lang; ?>" method="post" id="formleveltest" name="formleveltest" class="col-md-12 col-sm-12 col-xs-12">
        
                <div id="cabecera">
                    <h2><?php echo lang("H_Info"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <b><?php echo lang("Participant"); ?>:</b> 
                                    <?php echo $z_nom; ?>
                                </span>
                            </div>
                            <div class="uk-width-1-1">
                                <span>
                                    <br/>
                                    <?php echo lang("Info_Descripcion"); ?>
                                    <br/>
                                    <br/>
                                   <input type="checkbox" id="NSNC" name="NSNC" value="1" onchange="javascript:activar();"/> <?php echo lang("NSNC") ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br/>
                <br/>
                <div id="grupoenquesta">
                    <h2><?php echo lang("H_Titulo"); ?></h2>
                    <div class="uk-panel-box">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <span>
                                    <h3><?php echo lang("H3_Enunciado"); ?></h3>
                                </span>
                            </div>
                            <?php
                                $ncolumnas = 3;
                                $nfilas = 5;                                
                                //$tablas[$ncolumnas];
                                /* for ($contador = 0; $contador < 5; $contador++) { */
                                for ($contador = 0; $contador <= $nfilas; $contador++) {
                                    $fila[$contador] = "";
                                }
                                $columna=0;

                                $tabla="";
                                $preguntas = explode(lang("Splitter"),lang("Textaxo"));
                                
                                for ($contador = 0; $contador < $npreguntas; $contador++) {

                                    $pregunta = $preguntas[($contador * $nfilas)];
                                    $respuesta_a = $preguntas[($contador * $nfilas) + 1];
                                    $respuesta_b = $preguntas[($contador * $nfilas) + 2];
                                    $respuesta_c = $preguntas[($contador * $nfilas) + 3];
                                    $respuesta_d = $preguntas[($contador * $nfilas) + 4];

                                    $fila[0].= 
                                            "<div class='uk-width-1-3' valign=top>".
                                                "<b>".($contador + 1)." .- ".$pregunta."</b>".
                                            "</div>";
                                    
                                    $fila[1].= 
                                            "<div class='uk-width-1-3' valign=top nowrap>".
                                                "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=A tabindex=".($contador + 1)." />  ".$respuesta_a.
                                            "</div>";
                                    
                                    $fila[2].=                                             
                                            "<div class='uk-width-1-3' valign=top nowrap>".
                                                "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=B tabindex=".($contador + 1)." />  ".$respuesta_b.
                                            "</div>";
                                    
                                    $fila[3].= 
                                            "<div class='uk-width-1-3' valign=top nowrap>".
                                                "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=C tabindex=".($contador + 1)." />  ".$respuesta_c.
                                            "</div>";
                                    $fila[4].= 
                                            "<div class='uk-width-1-3' valign=top nowrap>".
                                                "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=D tabindex=".($contador + 1)." />  ".$respuesta_d.
                                            "</div>";
                                    $fila[5].= 
                                            "<div class='uk-width-1-3' valign=top>&nbsp;</div>";

                                    if (($columna < ($ncolumnas-1)) && ($contador < ($npreguntas - 1))) 
                                        { $columna++; }
                                    else 
                                    { 

                                        $columna=0; 
                                        if ($contador != ($npreguntas-1)) {
                                            $tabla.= $fila[0].$fila[1].$fila[2].$fila[3].$fila[4].$fila[5];
                                            
                                            $fila[0]="";
                                            $fila[1]="";
                                            $fila[2]="";
                                            $fila[3]="";
                                            $fila[4]="";
                                            $fila[5]="";
                                        }
                                    }
                                }

                                $fila[0].= "<div class='uk-width-1-3' valign=top>&nbsp;</div>";       
                                $fila[1].= "<div class='uk-width-1-3' valign=top nowrap>&nbsp;</div>";                                
                                $fila[2].= "<div class='uk-width-1-3' valign=top nowrap>&nbsp;</div>";                                
                                $fila[3].= "<div class='uk-width-1-3' valign=top nowrap>&nbsp;</div>";
                                $fila[4].= "<div class='uk-width-1-3' valign=top nowrap>&nbsp;</div>";
                                $fila[5].= "<div class='uk-width-1-3' valign=top>&nbsp;</div>";
                                $tabla.= $fila[0].$fila[1].$fila[2].$fila[3].$fila[4].$fila[5];

                                echo $tabla;
                                /*ponemos 4 celdas mas para compensar que 50 tiene 1 de resto al dividirlo en 3 columnas. */
                            ?>
                            <div class="uk-width-2-3">&nbsp;</div>
                            <div class="uk-width-1-3">
                                <center>
                                    <input type="button" class="input btnSubmit" value="<?php echo lang("boton") ?>" tabindex="<?php echo ($npreguntas + 1)?>" OnClick="validacion();">
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
                <script languaje="javascript">
                    function validarr(campo) {
                        //var tamanyo = document.getElementById(campo).length;
                        eval ("var valor = ((window.document.formleveltest."+campo+"[0].checked == true) || (window.document.formleveltest."+campo+"[1].checked == true) || (window.document.formleveltest."+campo+"[2].checked == true) || (window.document.formleveltest."+campo+"[3].checked == true) )"); 
                        var resultado = (valor == <?php echo lang("val_True") ?>);
                        return resultado;
                    }

                    function validarc(campo) {
                        //var tamanyo = document.getElementById(campo).length;
                        var valor = document.getElementById(campo).checked;
                        var resultado = (valor == <?php echo lang("val_True") ?>);
                        return resultado;
                    }
                    
                    function marcar(activar, campo) {
                        eval ("window.document.formleveltest."+campo+"[0].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");                    
                        eval ("window.document.formleveltest."+campo+"[1].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");                    
                        eval ("window.document.formleveltest."+campo+"[2].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");                    
                        eval ("window.document.formleveltest."+campo+"[3].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                    }
                    
                    function activar() {
                        var valor = document.getElementById('NSNC').checked;
                        <?php
                        for ($contador = 0; $contador < $npreguntas; $contador++) {
                            ?>
                            marcar(valor,'respuesta<?php echo $contador ?>');
                            <?php
                        }
                        ?>
                    }
                    
                    function validacion() {
                        
                        if (validarc('NSNC') || (<?php
                        for ($contador = 0; $contador < $npreguntas; $contador++) {
                            echo (($contador != 0)?" && ":"")."validarr('respuesta".$contador."')";
                        }
                        ?>)) {
                            if (confirm('<?php echo lang("AlertaEnviament") ?>')) {    
                                document.forms['formleveltest'].submit();
                            }
                        } else {
                            var falloen = "";
                            <?php
                            for ($contador = 0; $contador < $npreguntas; $contador++) {
                                echo "falloen += (validarr('respuesta".$contador."')==true)?'':'".($contador+1).". ';";
                            }
                            ?>
                            alert('<?php echo lang("AlertaFaltanCampos"); ?>\r\n'+falloen);
                        }
                    }

                </script>
            </form>
        <br/>
        <br/>
        <br/>
        <br/>
        </div>
        <div class="tm-footer">
            <div class="uk-container uk-container-center">
                <div class="uk-grid uk-grid-preserve">
                <div class="uk-width-medium-1-2">
                    &nbsp;
                </div>
                <div class="uk-width-medium-1-2 uk-text-right">
                    <p class="white">Copyright <?php echo date("Y"); ?> Viatges Rosa dels Vents S.A.</p>
                </div>
            </div> <!-- grid -->
        </div> <!-- container -->
    </body>    
</html>