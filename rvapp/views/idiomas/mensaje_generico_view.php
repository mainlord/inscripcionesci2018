<?php 
    /*    
        if ($errorlog = false) {
            error_reporting(E_ALL);
            ini_set('display_errors','On');
        }
    */
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" class="uk-height-1-1">
        <head>
            <title><?php echo lang("TitolWeb"); ?></title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8  X-Content-Type-Options=nosniff"/>
            <meta name="google" content="notranslate" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/uikit.gradient.min_new.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom_idiomas.css" />
            <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
            <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
            <meta http-equiv="expires" content="0" />
            <meta http-equiv="Cache-Control" content ="no-cache" />
            <style> 
                .btnSubmit {
                    padding: 10px; 
                    background-color: #0069af!important; 
                    border: 1px solid #0069af!important; 
                    border-radius: 5px; 
                    font-weight: bold; 
                    color: #ffffff !important; 
                    font-size: 14px; 
                    text-decoration:none; 
                    float: right;	
                } 
            </style>
        </head>	

        <body class="body_css uk-height-1-1">
	                                
            <div class="uk-container uk-container-center uk-main-container uk-height-1-1">
                <div class="uk-grid uk-grid-preserve">
                    <div class="uk-width-medium-1-2">
                            <img class="logo" src="<?php echo base_url(); ?>images/logo-idiomas-rv-<?php echo $lang; ?>.png"> <!-- .logo -->
                    </div>
                </div>
                <br/>
                <center>
                    <div id="grupoinfo">
                        <h1>
                            <!-- <?php echo $control; ?> -->
                            <?php echo $mensaje; ?>
                        </h1><br/><br/></div>
                </center>
            </div>
            <div class="tm-footer">
                <div class="uk-container uk-container-center">
                    <div class="uk-grid uk-grid-preserve">
                        <div class="uk-width-medium-1-2">
                            &nbsp;
                        </div>
                        <div class="uk-width-medium-1-2 uk-text-right">
                            <p class="white">Copyright <?php echo date("Y"); ?> Viatges Rosa dels Vents S.A.</p>
                        </div>
                    </div> <!-- grid -->
                </div> <!-- container -->
            </div>
        </body>
    </html>