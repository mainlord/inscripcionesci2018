<div class='col-xs-4 padding5 align-top' style="position:relative;">
    <?php 
        $img_pdf = base_url() . "UploadedFiles/IMG/pdf150.gif";

        $extension 	= explode(".", $cvac->url);
        $extension	= $extension[(count($extension)-1)];
        $modal = 'data-toggle="modal" data-target="#myModal"';
        $url = "#";
        $foto = $cvac->url;
        if (strtoupper($extension) == "PDF") {
            $modal = 'target="_blank"';
            $url = $cvac->url;
            $foto = $img_pdf;
        }
    ?>
    <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="a_cvac<?php echo $x; ?>_th" id="a_cvac<?php echo $x; ?>_th" onclick="mostrar('imagen_cvac<?php echo $x; ?>_th');">
        <img width="150px" class='img-responsive' src="<?php echo $foto; ?>" name="imagen_cvac<?php echo $x; ?>_th" id="imagen_cvac<?php echo $x; ?>_th" />
    </a>
    <?php
    	if ($permitirModificacion) {
            ?><i class="fa fa-remove" id="remove_cvac<?php echo $x; ?>_th" name="remove_cvac<?php echo $x; ?>_th" onclick="lanzar_borrado('<?php echo $cvac->idcvac; ?>', 5);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);"></i><?php
        }
    ?>
</div>