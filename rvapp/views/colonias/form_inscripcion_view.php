<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. <pre><?php echo print_r($privilegios, true); ?></pre> */
$CI = &get_instance();
$img_pdf = base_url() . "UploadedFiles/IMG/pdf150.gif";

function get_value($nombre, $value)
{
    if ((isset($_POST[$nombre])) and ($_POST[$nombre] != "")) {
        return $_POST[$nombre];
    } else {
        return $value;
    }
}

if (!isset($colectiu)) $colectiu = false;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google" content="notranslate" />
                <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8 X-Content-Type-Options=nosniff" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="Cache-Control" content ="no-cache" />

        <title><?php echo lang("TitolWeb"); ?></title>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            
        <!-- Bootstrap -->
            <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.min.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>css/bootstrap/style-indis.css" rel="stylesheet">
            <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>

            <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/x-icon" />

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap-select.js"></script>
            <!--script type="text/javascript">
                // Igualar alturas de paneles
                $(document).ready(function (){
                    var highestCol = Math.max($('.tabla .celda').innerHeight());
                    $('.tabla .celda').height(highestCol+30);
                });
            </script-->
        <!-- !Bootstrap -->
        <?php
        if ($permitirModificacion) {
            ?><script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script><?php

                                                                                                                                                        }
                                                                                                                                                        ?>

        <style> 
            .btnSubmit {
                padding: 10px; 
                background-color: #0069af!important; 
                border: 1px solid #0069af!important; 
                border-radius: 5px; 
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px; 
                text-decoration:none; float: right; } 
            .ui-datepicker-year {
                color: rgb(14, 60, 99)!important;
            }
            .minH {
                min-height: 50px!important;
            }
            
            .col-container {
                display: table;
                width: 100%;
                border-spacing: 5px;
                border-collapse: separate;
            }
            .col {
                display: table-cell;
                padding: 16px;
                margin: 5px;
            }
            .btnSubmit {
                bottom: 10px;
            }
        </style>

        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />   
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <?php
        if ($permitirModificacion) {
            ?>
                    <script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script>
                <?php

            }
            ?>

    </head>
    <?php
    if ($permitirModificacion) {
        ?>
                <body onload="inicializar();">
                    <script languaje="javascript">
                        <!--
                            var campoFalla="";
                            function actualizarCampo(desde, hasta) {
                                document.getElementById(hasta).value = document.getElementById(desde).value;
                            }
                            
                            function controlAdicionales(activar, cadena) {
                                var campo;
                                var campos = cadena.split("|");
                                for (var campo in campos) {
                                    document.getElementById(campos[campo]).disabled = (activar != <?php echo lang("val_True"); ?>);
                                }
                            }
                            function inicializar() {
                                controlAdicionales(document.getElementById('malalt').checked, 'malalt_detall');
                                controlAdicionales(document.getElementById('medicaments').checked, 'medicaments_detall|medicaments_administracio');
                                controlAdicionales(document.getElementById('sotatractament').checked, 'sotatractamentdetall');
                                controlAdicionales(document.getElementById('regim').checked, 'regim_detall');
                                controlAdicionales(document.getElementById('desordre_alimentari').checked, 'desordre_alimentari_detall');
                                controlAdicionales(document.getElementById('operat').checked, 'operat_detall');
                                controlAdicionales(document.getElementById('alergic_altres').checked, 'alergic_detall');
                                controlAdicionales(document.getElementById('dificultatesports').checked, 'dificultatesports_quins');                    
                                <?php echo $z_hipica_js; ?>
                            }
                            
                            function validart(campo) {
                                var texto = document.getElementById(campo).value.replace(/^\s*|\s*$/g,"");
                                var ok=false;
                                if (texto.length>0) {
                                    ok=true;
                                }           
                                if (!ok) if (window.campoFalla=="") window.campoFalla=campo;
                                return ok;
                            }
                            
                            
                            function validarmt(cadena) {
                                var campo, texto;
                                var ok=true;
                                var campos = cadena.split("|");
                                
                                for (campo in campos) {
                                    texto = document.getElementById(campos[campo]).value.replace(/^\s*|\s*$/g,"");
                                    if (texto.length>0) {
                                        ok=(ok==true);
                                    } else {
                                        if (window.campoFalla=="") window.campoFalla=campos[campo];
                                        ok=false;
                                    }
                                }                               
                                
                                return ok;
                            }
                            
                        
                            function validarr(campo) {
                                //var tamanyo = document.getElementById(campo).length;
                                /* var valor = document.getElementById(campo).checked; */                    
                                var valor = document.getElementById(campo).checked; 
                                var resultado = (valor.toString() == '<?php echo lang("val_True") ?>');
                                if (!resultado && campo == "futbol_equipacion") if (window.campoFalla=="") window.campoFalla=campo;
                                return resultado;
                            }

                            function validarx(campo) {
                                var resultado = (   $("input[name="+campo+"]:checked").val() == "false" ||
                                                    $("input[name="+campo+"]:checked").val() == "true"    );
                                if (!resultado && campo != 'alergic_altres') if (window.campoFalla=="") window.campoFalla=campo;
                                return resultado;   
                            }

                            function validarc(campo) {
                                //var tamanyo = document.getElementById(campo).length;
                                var valor = document.getElementById(campo).checked;
                                var resultado = (valor.toString() == '<?php echo lang("val_True") ?>');
                                if (!resultado && campo != 'alergic_altres') if (window.campoFalla=="") window.campoFalla=campo;
                                return resultado;
                            }
                            
                            function validarf(campo){  
                                var cadena = document.getElementById(campo).value;
                                var fecha = new String(cadena); //  Crea un string  
                                var realfecha = new Date()      //  Para sacar la fecha de hoy  
                                //  Cadena Año  
                                var ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
                                //  Cadena Mes  
                                var mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
                                //  Cadena Día  
                                var dia= new String(fecha.substring(0,fecha.indexOf("/")))  

                                // evitamos que nos metan una fecha sin  /
                                if (fecha.lastIndexOf("/")== -1) {
                                    if (window.campoFalla=="") window.campoFalla=campo;
                                    return false  
                                }
                                
                                // Valido el año  
                                if (isNaN(ano) || ano.length<4 || parseFloat(ano)<1900){  
                                    //    alert('Año inválido')  
                                    if (window.campoFalla=="") window.campoFalla=campo;
                                    return false  
                                }  
                                // Valido el Mes  
                                if (isNaN(mes) || parseFloat(mes)<1 || parseFloat(mes)>12){  
                                    // alert('Mes inválido')  
                                    if (window.campoFalla=="") window.campoFalla=campo;
                                    return false  
                                }  
                                // Valido el Dia  
                                if (isNaN(dia) || parseInt(dia, 10)<1 || parseInt(dia, 10)>31){  
                                    // alert('Día inválido')  
                                    if (window.campoFalla=="") window.campoFalla=campo;
                                    return false  
                                }  
                                if (mes==4 || mes==6 || mes==9 || mes==11 || mes==2) {  
                                    if ((mes==2 && dia>28 && (parseFloat(ano)%4!=0)) || (dia>30)) {  
                                        //alert('Día inválido')  
                                        if (window.campoFalla=="") window.campoFalla=campo;
                                        return false  
                                    }  
                                }  

                                // para que envie los datos, quitar las  2 lineas siguientes  
                                // alert("Fecha correcta.")  
                                return true
                            }  
                            
                            function validacion() {
                                document.forms['formreservation'].submit();
                            }
/*
                            function CheckFileName(campo) {
                                var fileName = campo.value
                                if (fileName == "") {
                                    //alert("Browse to upload a valid File with png extension");
                                    campo.value = null;
                                    return false;
                                }
                                else if (
                                        fileName.split(".")[1].toUpperCase() == "JPG" ||
                                        fileName.split(".")[1].toUpperCase() == "PJPG" ||
                                        fileName.split(".")[1].toUpperCase() == "GIF" ||
                                        fileName.split(".")[1].toUpperCase() == "PNG" ||
                                        fileName.split(".")[1].toUpperCase() == "PDF"
                                        )
                                    return true;
                                else {
                                    //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                                    campo.value = null;                        
                                    return false;
                                }
                                return true;
                            }
*/
                        //-->
                    </script>
            <?php 
        } else {
            ?>
                <body>
            <?php 
        }
        ?>
        <header class="top-header hidden-print">
            <div class="container">
                <a href="#" target="_blank"><h1 class="logo" style="background-image:url(<?php echo base_url(); ?>images/logo_rosadelsvents-indis.png);">Rosa dels Vents</h1></a>
                <?php 
                if ($z_zurich) {
                    ?>&nbsp;<img src="<?php echo base_url(); ?>images/logo_zurich.png" style="width:150px!important;margin-left:15px" /><?php

                                                                                                                                        }
                                                                                                                                        ?>
                <div class="support">
                    <p class="question"><?php echo lang("Titol"); ?></p>
                    <p><a href="tel:934092071" class="phone">934092071</a></p>
                    <p><a href="mailto:informacio@rosadelsvents.es" class="email">informacio@rosadelsvents.es</a></p>
                </div> 
            </div>
        </header>

    <?php
    if ($permitirModificacion) {
        ?>
                <form action="<?php echo base_url(); ?>inscripcion/guardar/<?php echo $param_url; ?>/<?php echo $lang . ($forzar == true ? "/true" : ""); ?>" method="post" id="formreservation" name="formreservation"  enctype="multipart/form-data"  class="col-md-12 col-sm-12 col-xs-12">
            <?php 
        }
        ?>
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="padding5 minH">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <h1><?php echo lang("Titol"); ?></h1>
                    </div>
                </div>
                <br/>
                <div class="padding5 minH box border3 boxactive">
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("NReserva") . "/" . lang("Localitzador"); ?>:</b><br/> 
                        <input type="text" readonly value="<?php echo $datos->localizador; ?>" class="form-control"/>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Dni"); ?>:</b><br/>
                        <input type="text" readonly value="<?php echo $datos->dni; ?>" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("centro"); ?>:</b><br/>
                        <input type="text" readonly value="<?php echo $z_centro; ?>" class="form-control"/>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("programa"); ?>:</b><br/>
                        <input type="text" readonly value="<?php echo $z_programa . "  (" . $z_fdesde_txt . " - " . $z_fhasta_txt . ")  " . $z_edaddesde . " - " . $z_edadhasta ?>" class="form-control"/>                        
                        <?php /* input type="hidden" id="Autoritzacio_casa_colonies" name="Autoritzacio_casa_colonies" value="<?php echo $datos->idinscripciones;?>"/>
                        <input type="hidden" id="Autoritzacio_programa" name="Autoritzacio_programa" value="<?php echo $idtanda;?>"/*/ ?>
                    </div>
                </div>
                <br/>
                <?php 
                if (validation_errors() != "") {
                    ?>
                            <div class="alert alert-danger">
                                <label><?php echo lang("campos_obligatorios_pendientes"); ?></label>
                            </div>
                            <br /> 
                        <?php

                    }
                    ?>
                <div class="padding5">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
                        <h2>1 .- <?php echo lang("H_SeccioDPersonals"); ?></h2>
                    </div>
                </div>
                <div class="padding5 minH box border3">
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("NomComplet") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("nom_complet", $datos->nom_complet);
                        } else {
                            ?>
                                    <input type="text" id="nom_complet" name="nom_complet" class="form-control" maxlength="150" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" OnChange="javascript:actualizarCampo('nom_complet','Autoritzacio_nom_fill');actualizarCampo('nom_complet','nombreparticipante_4');actualizarCampo('nom_complet','nombreparticipante_3');actualizarCampo('nom_complet','nombreparticipante_2');actualizarCampo('nom_complet','nombreparticipante_1');"/>
                                    <?php echo form_error("nom_complet"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Telefon") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("telefon", $datos->telefon);
                        } else {
                            ?>
                                    <input type="text" id="telefon" name="telefon" maxlength="12" size="12" class="form-control input" value="<?php echo get_value("telefon", $datos->telefon); ?>"/>
                                    <?php echo form_error("telefon"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Chico_chica") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("chico_chica", $datos->chico_chica) == true) {
                                echo lang("chico");
                            } else {
                                echo lang("chica");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="chico_chica" name="chico_chica" <?php if (get_value("chico_chica", $datos->chico_chica) == lang("val_True")) {
                                                                                                echo "checked";
                                                                                            } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("chico"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="chico_chica" name="chico_chica" <?php if (get_value("chico_chica", $datos->chico_chica) != lang("val_True")) {
                                                                                                echo "checked";
                                                                                            } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("chica"); ?>
                                    <?php echo form_error("chico_chica"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("AltresTelefons") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("altres_telefons", $datos->altres_telefons);
                        } else {
                            ?>
                                    <input type="text" id="altres_telefons" name="altres_telefons" maxlength="100" class="form-control " value="<?php echo get_value("altres_telefons", $datos->altres_telefons); ?>"/>
                                    <?php echo form_error("altres_telefons"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Adreça") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("adressa", $datos->adressa);
                        } else {
                            ?>
                                    <input type="text" id="adressa" name="adressa" maxlength="150" class="form-control " value="<?php echo get_value("adressa", $datos->adressa); ?>"/>
                                    <?php echo form_error("adressa"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Poblacio") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("poblacio", $datos->poblacio);
                        } else {
                            ?>
                                    <input type="text" id="poblacio" name="poblacio" maxlength="45" class="form-control " value="<?php echo get_value("poblacio", $datos->poblacio); ?>"/>
                                    <?php echo form_error("poblacio"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("CP") ?>:</b><br/>
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("cp", $datos->cp);
                        } else {
                            ?>
                                    <input type="text" id="cp" name="cp" class="form-control input" maxlength="5" size="5" value="<?php echo get_value("cp", $datos->cp); ?>"/>
                                    <?php echo form_error("cp"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("DataNaixement") ?>:</b><br/>
                        <?php
                        if (!$permitirModificacion) {
                            echo set_value("data_naixement", $CI->soporte_general->cambia_fecha($datos->data_naixement, "-", "/"));
                        } else { 
                                // echo "<pre>FN:  ".set_value("data_naixement", $datos->data_naixement)." - ".$datos->data_naixement."</pre>";
                            $data_naixement = set_value("data_naixement", $CI->soporte_general->cambia_fecha($datos->data_naixement, "-", "/"));
                            ?><input type="text" id="data_naixement" name="data_naixement" readonly="readonly" maxlength="150" class="form-control input" value="<?php echo set_value("data_naixement", $data_naixement); ?>" /><?php 
                                                                                                                                                                                                                                    echo form_error("data_naixement");
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                ?>
                    </div>

                    <div class="col-container"><!--box boxactive-->
                        <div class="col center" style="border-radius: 15px; background-color: #eeeeee">
                            <div class=" padding5">
                                <b><?php echo lang("Foto") ?>:</b>
                            </div>
                            <div class=" padding5">
                                <?php 
                                    if ($permitirModificacion) {
                                        ?>
                                            <input type="button" id="btn_lanzar_subida" name="btn_lanzar_subida" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('IMG');" class="btnSubmit" style="float:none;"/>
                                        <?php
                                    }
                                ?>
                            </div>
                            <div class=" padding5" style="position:relative;">
                                <?php 
                                    $oculto = "";
                                    $ins_foto = $datos->foto;
                                    if (!$ins_foto) {
                                        $ins_foto = base_url() . "UploadedFiles/IMG/nofoto150.png";
                                        $oculto = "display:none;";
                                    }
                                    
                                    $extension 	= explode(".", $ins_foto);
                                    $extension	= $extension[(count($extension)-1)];
                                    $modal = 'data-toggle="modal" data-target="#myModal"';
                                    $url = "#";
                                    $foto = $ins_foto;
                                    if (strtoupper($extension) == "PDF") {
                                        $modal = 'target="_blank"';
                                        $url = $ins_foto;
                                        $foto = $img_pdf;
                                    }
                                ?>
                                <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="a_foto_th" id="a_foto_th" onclick="mostrar('imagen_foto_th');">
                                    <img width="150px" src="<?php echo $foto; ?>" name="imagen_foto_th" id="imagen_foto_th" />
                                    <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="spinner_foto_th" name="spinner_foto_th"></i>
                                </a>
                                <?php
                                    if ($permitirModificacion) {
                                        ?><i class="fa fa-remove" id="remove_foto_th" name="remove_foto_th" onclick="lanzar_borrado('imagen_foto_th', 1);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);<?php echo $oculto; ?>"></i><?php
                                    }
                                ?>

                                <div id="vomito" class="uk-width-1-1"></div>
                            </div>
                        </div>
                        
                        <div class="col center" style="border-radius: 15px; background-color: #eeeeee">
                            <div class="padding5">
                                <b><?php echo lang("FotoDniDelantera") ?>:</b>
                            </div>
                            <div class="padding5">
                                <?php 
                                    if ($permitirModificacion) {
                                        ?>
                                            <input type="button" id="btn_lanzar_subida" name="btn_lanzar_subida" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('DNIA');" class="btnSubmit" style="float:none;"/>
                                        <?php
                                    }
                                    ?>
                            </div>
                            <div class="padding5" style="position:relative;">
                                <?php 
                                    
                                    $oculto = "";
                                    $dni_delantero = $datos->dni_delantero;
                                    if (!$dni_delantero) {
                                        $dni_delantero = base_url() . "UploadedFiles/IMG/nodnia150.gif";
                                        $oculto = "display:none;";
                                    }
                                    
                                    $extension 	= explode(".", $dni_delantero);
                                    $extension	= $extension[(count($extension)-1)];
                                    $modal = 'data-toggle="modal" data-target="#myModal"';
                                    $url = "#";
                                    $foto = $dni_delantero;
                                    if (strtoupper($extension) == "PDF") {
                                        $modal = 'target="_blank"';
                                        $url = $dni_delantero;
                                        $foto = $img_pdf;
                                    }
                                ?>
                                <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="a_dnia_th" id="a_dnia_th" onclick="mostrar('imagen_dnia_th');">
                                    <img width="150px" src="<?php echo $foto; ?>" name="imagen_dnia_th" id="imagen_dnia_th" />
                                    <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="spinner_dnia_th" name="spinner_dnia_th"></i>
                                </a>
                                <?php
                                    if ($permitirModificacion) {
                                        ?><i class="fa fa-remove" id="remove_dnia_th" name="remove_dnia_th" onclick="lanzar_borrado('imagen_dnia_th', 3);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);<?php echo $oculto; ?>"></i><?php
                                    }
                                ?>

                                <div id="vomito" class="uk-width-1-1"></div>
                            </div>
                        </div>

                        <div class="col center" style="border-radius: 15px; background-color: #eeeeee">
                            <div class="padding5">
                                <b><?php echo lang("FotoDniTrasera") ?>:</b>
                            </div>
                            <div class="padding5">
                                <?php 
                                    if ($permitirModificacion) {
                                        ?>
                                            <input type="button" id="btn_lanzar_subida" name="btn_lanzar_subida" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('DNIR');" class="btnSubmit" style="float:none;"/>
                                        <?php
                                    }
                                    ?>
                            </div>
                            <div class="padding5" style="position:relative;">
                                <?php 
                                    
                                    $oculto = "";
                                    $dni_trasero = $datos->dni_trasero;
                                    if (!$dni_trasero) {
                                        $dni_trasero = base_url() . "UploadedFiles/IMG/nodnir150.gif";
                                        $oculto = "display:none;";
                                    }

                                    $extension 	= explode(".", $dni_trasero);
                                    $extension	= $extension[(count($extension)-1)];
                                    $modal = 'data-toggle="modal" data-target="#myModal"';
                                    $url = "#";
                                    $foto = $dni_trasero;
                                    if (strtoupper($extension) == "PDF") {
                                        $modal = 'target="_blank"';
                                        $url = $dni_trasero;
                                        $foto = $img_pdf;
                                    }
                                ?>
                                <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="a_dnir_th" id="a_dnir_th" onclick="mostrar('imagen_dnir_th');">
                                    <img width="150px" src="<?php echo $foto; ?>" name="imagen_dnir_th" id="imagen_dnir_th" />
                                    <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="spinner_dnir_th" name="spinner_dnir_th"></i>
                                </a>
                                <?php
                                    if ($permitirModificacion) {
                                        ?><i class="fa fa-remove" id="remove_dnir_th" name="remove_dnir_th" onclick="lanzar_borrado('imagen_dnir_th', 4);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);<?php echo $oculto; ?>"></i><?php
                                    }
                                ?>

                                <div id="vomito" class="uk-width-1-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
                        
                <br/>
                <div class="padding5 ">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                        <h2>2 .- <?php echo lang("H_InformacioSanitaria"); ?></h2>
                    </div>
                </div>
                <div class="padding5 minH box border3">
                    <div class="padding5">
                        <div class="col-container"  style="border-radius: 15px; background-color: #eeeeee">
                            <div class="col-xs-4 center">
                                <div class="padding5">
                                    <b><?php echo lang("FotosCartillaVacunas") ?>:</b>
                                </div>
                                <?php
                                    if ($permitirModificacion) {
                                        ?>
                                            <div class="col-12 padding5">
                                                <input type="button" id="btn_lanzar_subida" name="btn_lanzar_subida" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('CVAC');" class="btnSubmit" style="float:none;"/>
                                            </div>
                                        <?php

                                    }
                                ?>
                                <div class="padding5">
                                    <img width="150px" src="<?php echo base_url()."UploadedFiles/IMG/novacuna150.gif"; ?>"/>
                                    <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="spinner_cvac_th" name="spinner_cvac_th"></i>
                                    <div id="vomito" class="uk-width-1-1"></div>
                                </div>
                            </div>
                            
                            <div class="col-xs-8">
                                <?php 
                                    /* * / ?>
                                        <pre><?php echo print_r($datos_cvac, true); ?></pre>
                                        <pre><?php echo isset($datos_cvac); ?></pre>
                                        <pre><?php echo count($datos_cvac); ?></pre>
                                   <?php /* */
                                ?>
                                <div id="cvac_resultados" name="cvac_resultados" style="margin-top:2em;">
                                    <?php
                                        $entrar = false;
                                        if (!isset($datos_cvac)) {}
                                        else if (count($datos_cvac) >= 1) {
                                            $entrar = true;
                                            $x=1;
                                            foreach ($datos_cvac as $cvac) {
                                                $CI->montar_div_cvac($cvac, $x, $permitirModificacion, $ruta);
                                                $x++;
                                            }
                                        }

                                        if (!$entrar) {
                                            echo "<li><strong>".lang("no_cvac")."</strong></li>";
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("MalaltSovint"); ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("malalt", $datos->malalt) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="malalt" name="malalt" <?php if (get_value("malalt", $datos->malalt) == lang("val_True")) {
                                                                                        echo "checked";
                                                                                    } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'malalt_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="malalt" name="malalt" <?php if (get_value("malalt", $datos->malalt) != lang("val_True")) {
                                                                                        echo "checked";
                                                                                    } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'malalt_detall');" /><?php echo lang("False"); ?>
                                    <?php echo form_error("malalt"); ?>
                                <?php 
                            }
                            ?>                        
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("MalaltSovintDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("malalt_detall", $datos->malalt_detall);
                        } else {
                            ?>
                                    <input type="text" id="malalt_detall" name="malalt_detall" maxlength="150" class="form-control" value="<?php echo get_value("malalt_detall", $datos->malalt_detall); ?>"/>
                                    <?php echo form_error("malalt_detall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("PrenMedicament") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("medicaments", $datos->medicaments) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="medicaments" name="medicaments" <?php if (get_value("medicaments", $datos->medicaments) == lang("val_True")) {
                                                                                                echo "checked";
                                                                                            } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'medicaments_detall|medicaments_administracio');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="medicaments" name="medicaments" <?php if (get_value("medicaments", $datos->medicaments) != lang("val_True")) {
                                                                                                echo "checked";
                                                                                            } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'medicaments_detall|medicaments_administracio');" /><?php echo lang("False"); ?>
                                    <?php echo form_error("medicaments"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("PrenMedicamentDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("medicaments_detall", $datos->medicaments_detall);
                        } else {
                            ?>
                                    <input type="text" id="medicaments_detall" name="medicaments_detall" maxlength="150" class="form-control" value="<?php echo get_value("medicaments_detall", $datos->medicaments_detall); ?>"/>
                                    <?php echo form_error("medicaments_detall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("PrenMedicamentAdministracio") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("medicaments_administracio", $datos->medicaments_administracio);
                        } else {
                            ?>
                                    <input type="text" id="medicaments_administracio" name="medicaments_administracio" maxlength="150" class="form-control" value="<?php echo get_value("medicaments_administracio", $datos->medicaments_administracio); ?>"/>
                                    <?php echo form_error("medicaments_administracio"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("SotaTractament") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("sotatractament", $datos->sotatractament) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="sotatractament" name="sotatractament" <?php if (get_value("sotatractament", $datos->sotatractament) == lang("val_True")) {
                                                                                                        echo "checked";
                                                                                                    } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'sotatractamentdetall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="sotatractament" name="sotatractament" <?php if (get_value("sotatractament", $datos->sotatractament) != lang("val_True")) {
                                                                                                        echo "checked";
                                                                                                    } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'sotatractamentdetall');" /><?php echo lang("False"); ?>
                                    <?php echo form_error("sotatractament"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("SotaTractamentDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("sotatractamentdetall", $datos->sotatractamentdetall);
                        } else {
                            ?>
                                    <input type="text" id="sotatractamentdetall" name="sotatractamentdetall" maxlength="150" class="form-control" value="<?php echo get_value("sotatractamentdetall", $datos->sotatractamentdetall); ?>"/>
                                    <?php echo form_error("sotatractamentdetall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Regim") ?>:</b><br/>
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("regim", $datos->regim) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="regim" name="regim" <?php if (get_value("regim", $datos->regim) == lang("val_True")) {
                                                                                    echo "checked";
                                                                                } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'regim_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="regim" name="regim" <?php if (get_value("regim", $datos->regim) != lang("val_True")) {
                                                                                    echo "checked";
                                                                                } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'regim_detall');" /><?php echo lang("False"); ?>
                                    <?php echo form_error("regim"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("RegimDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("regim_detall", $datos->regim_detall);
                        } else {
                            ?>
                                    <input type="text" id="regim_detall" name="regim_detall" maxlength="150" class="form-control" value="<?php echo get_value("regim_detall", $datos->regim_detall); ?>"/>
                                    <?php echo form_error("regim_detall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("DesordreAlimentari") ?>:</b><br/>
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("desordre_alimentari", $datos->desordre_alimentari) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="desordre_alimentari" name="desordre_alimentari" <?php if (get_value("desordre_alimentari", $datos->desordre_alimentari) == lang("val_True")) {
                                                                                                                echo "checked";
                                                                                                            } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'desordre_alimentari_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="desordre_alimentari" name="desordre_alimentari" <?php if (get_value("desordre_alimentari", $datos->desordre_alimentari) != lang("val_True")) {
                                                                                                                echo "checked";
                                                                                                            } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'desordre_alimentari_detall');" /><?php echo lang("False"); ?>
                                    <?php echo form_error("desordre_alimentari"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("DesordreAlimentariDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("desordre_alimentari_detall", $datos->desordre_alimentari_detall);
                        } else {
                            ?>
                                    <input type="text" id="desordre_alimentari_detall" name="desordre_alimentari_detall" maxlength="150" class="form-control" value="<?php echo get_value("desordre_alimentari_detall", $datos->desordre_alimentari_detall); ?>"/>
                                    <?php echo form_error("desordre_alimentari_detall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Operat") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("operat", $datos->operat) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="operat" name="operat" <?php if (get_value("operat", $datos->operat) == lang("val_True")) {
                                                                                        echo "checked";
                                                                                    } ?> value="<?php echo lang("val_True"); ?>" onchange="javascript: controlAdicionales(this.value, 'operat_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="operat" name="operat" <?php if (get_value("operat", $datos->operat) != lang("val_True")) {
                                                                                        echo "checked";
                                                                                    } ?> value="<?php echo lang("val_False"); ?>" onchange="javascript: controlAdicionales(this.value, 'operat_detall');" /><?php echo lang("False"); ?>                    
                                    <?php echo form_error("operat"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("OperatDetall") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("operat_detall", $datos->operat_detall);
                        } else {
                            ?>
                                    <input type="text" id="operat_detall" name="operat_detall" maxlength="150" class="form-control" value="<?php echo get_value("operat_detall", $datos->operat_detall); ?>"/>
                                    <?php echo form_error("operat_detall"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH box boxalert">
                        <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                            <b><?php echo lang("Alergic") ?></b><br />
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 step2-desc padding5 minH">
                            <?php
                            if (!$permitirModificacion) {
                                echo lang("Alergic_celiac") . ": ";
                                if (get_value("alergic_celiac", $datos->alergic_celiac) == true) {
                                    echo lang("True");
                                } else {
                                    echo lang("False");
                                }
                            } else {
                                ?>
                                        <input type="checkbox" id="alergic_celiac" name="alergic_celiac" value="<?php echo lang("val_True"); ?>" <?php if (get_value("alergic_celiac", $datos->alergic_celiac) == lang("val_True")) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?> /> <b><?php echo lang("Alergic_celiac") ?></b>
                                        <?php echo form_error("alergic_celiac"); ?>
                                    <?php 
                                }
                                ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 step2-desc padding5 minH">
                            <?php
                            if (!$permitirModificacion) {
                                echo lang("Alergic_lactosa") . ": ";
                                if (get_value("alergic_lactosa", $datos->alergic_lactosa) == true) {
                                    echo lang("True");
                                } else {
                                    echo lang("False");
                                }
                            } else {
                                ?>
                                        <input type="checkbox" id="alergic_lactosa" name="alergic_lactosa" value="<?php echo lang("val_True"); ?>" <?php if (get_value("alergic_lactosa", $datos->alergic_lactosa) == lang("val_True")) {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?> /> <b><?php echo lang("Alergic_lactosa") ?></b>
                                        <?php echo form_error("alergic_lactosa"); ?>
                                    <?php 
                                }
                                ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 step2-desc padding5 minH">
                            <?php
                            if (!$permitirModificacion) {
                                echo lang("Alergic_ou") . ": ";
                                if (get_value("alergic_ou", $datos->alergic_ou) == true) {
                                    echo lang("True");
                                } else {
                                    echo lang("False");
                                }
                            } else {
                                ?>
                                        <input type="checkbox" id="alergic_ou" name="alergic_ou" value="<?php echo lang("val_True"); ?>" <?php if (get_value("alergic_ou", $datos->alergic_ou) == lang("val_True")) {
                                                                                                                                            echo "checked";
                                                                                                                                        } ?> /> <b><?php echo lang("Alergic_ou") ?></b>
                                        <?php echo form_error("alergic_ou"); ?>
                                    <?php 
                                }
                                ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 step2-desc padding5 minH">
                            <?php
                            if (!$permitirModificacion) {
                                echo lang("Alergic_altres") . ": ";
                                if (get_value("alergic_altres", $datos->alergic_altres) == true) {
                                    echo lang("True");
                                } else {
                                    echo lang("False");
                                }
                            } else {
                                ?>
                                        <input type="checkbox" id="alergic_altres" name="alergic_altres" value="<?php echo lang("val_True"); ?>" <?php if (get_value("alergic_altres", $datos->alergic_altres) == lang("val_True")) {
                                                                                                                                                    echo "checked";
                                                                                                                                                } ?> onchange="javascript: controlAdicionales(document.getElementById('alergic_altres').checked, 'alergic_detall');"/> <b><?php echo lang("Alergic_altres") ?></b>
                                        <?php echo form_error("alergic_altres"); ?>
                                    <?php 
                                }
                                ?>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 step2-desc padding5 minH">
                            <b><?php echo lang("Alergic_Detall") ?></b><br />
                            <?php
                            if (!$permitirModificacion) {
                                echo get_value("alergic_detall", $datos->alergic_detall);
                            } else {
                                ?>
                                        <input type="text" id="alergic_detall" name="alergic_detall" maxlength="150" class="form-control" value="<?php echo get_value("alergic_detall", $datos->alergic_detall); ?>"/>
                                        <?php echo form_error("alergic_detall"); ?>
                                    <?php 
                                }
                                ?>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("ObsGeneral") ?></b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("observaciones_generales", $datos->observaciones_generales);
                        } else {
                            ?>
                                    <textarea cols="45" rows="10" id="observaciones_generales" class="form-control" name="observaciones_generales"><?php echo get_value("observaciones_generales", $datos->observaciones_generales) ?></textarea>
                                    <?php echo form_error("observaciones_generales"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                </div>

                <br/>
                <div class="padding5 ">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
                        <h2>3 .- <?php echo lang("H_InteresGeneral"); ?></h2>
                    </div>
                </div>
                <div class="padding5 minH box border3">
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("AltresMateixTipus") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("estades_abans", $datos->estades_abans) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="estades_abans" name="estades_abans" <?php if (get_value("estades_abans", $datos->estades_abans) == lang("val_True")) {
                                                                                                    echo "checked";
                                                                                                } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="estades_abans" name="estades_abans" <?php if (get_value("estades_abans", $datos->estades_abans) != lang("val_True")) {
                                                                                                    echo "checked";
                                                                                                } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("estades_abans"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Bicicleta") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("bicicleta", $datos->bicicleta) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="bicicleta" name="bicicleta" <?php if (get_value("bicicleta", $datos->bicicleta) == lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="bicicleta" name="bicicleta" <?php if (get_value("bicicleta", $datos->bicicleta) != lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("bicicleta"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Nedar") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("nedar", $datos->nedar) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="nedar" name="nedar" <?php if (get_value("nedar", $datos->nedar) == lang("val_True")) {
                                                                                    echo "checked";
                                                                                } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="nedar" name="nedar" <?php if (get_value("nedar", $datos->nedar) != lang("val_True")) {
                                                                                    echo "checked";
                                                                                } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("nedar"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("PorAigua") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("por_aigua", $datos->por_aigua) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="por_aigua" name="por_aigua" <?php if (get_value("por_aigua", $datos->por_aigua) == lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="por_aigua" name="por_aigua" <?php if (get_value("por_aigua", $datos->por_aigua) != lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("por_aigua"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("Vertigen") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("vertigen", $datos->vertigen) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="vertigen" name="vertigen" <?php if (get_value("vertigen", $datos->vertigen) == lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="vertigen" name="vertigen" <?php if (get_value("vertigen", $datos->vertigen) != lang("val_True")) {
                                                                                            echo "checked";
                                                                                        } ?> value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("vertigen"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("DificultatSports") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            if (get_value("dificultatesports", $datos->dificultatesports) == true) {
                                echo lang("True");
                            } else {
                                echo lang("False");
                            }
                        } else {
                            ?>
                                    <input type="radio" id="dificultatesports" name="dificultatesports" <?php if (get_value("dificultatesports", $datos->dificultatesports) == lang("val_True")) {
                                                                                                            echo "checked";
                                                                                                        } ?> onchange="javascript: controlAdicionales(this.value, 'dificultatesports_quins');"  value="<?php echo lang("val_True"); ?>"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                    <input type="radio" id="dificultatesports" name="dificultatesports" <?php if (get_value("dificultatesports", $datos->dificultatesports) != lang("val_True")) {
                                                                                                            echo "checked";
                                                                                                        } ?> onchange="javascript: controlAdicionales(this.value, 'dificultatesports_quins');"  value="<?php echo lang("val_False"); ?>"/><?php echo lang("False"); ?>
                                    <?php echo form_error("dificultatesports"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                        <b><?php echo lang("DificultatSports_Quins") ?>:</b><br />
                        <?php
                        if (!$permitirModificacion) {
                            echo get_value("dificultatesports_quins", $datos->dificultatesports_quins);
                        } else {
                            ?>
                                    <input type="text" id="dificultatesports_quins" name="dificultatesports_quins" maxlength="50" class="form-control" value="<?php echo get_value("dificultatesports_quins", $datos->dificultatesports_quins); ?>"/>
                                    <?php echo form_error("dificultatesports_quins"); ?>
                                <?php 
                            }
                            ?>
                    </div>
                </div>

                <br/>
                <div class="padding5 ">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                        <h2>4 .- <?php echo lang("H_LPD"); ?></h2>
                    </div>
                </div>
                <div class="padding5 minH box border3 boxactive">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <?php echo lang("LPD") ?>
                    </div>
                </div>

                <br/>
                <div class="padding5 ">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                        <h2>5 .- <?php echo lang("H_AutoritzacioPersonal"); ?></h2>
                    </div>
                </div>
                <div class="padding5 minH box border3">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <?php echo lang("Autoritzacio_senyor") ?> <input type="text" size="30" class="form-control" maxlength="150" id="autoritzacio_nom" name="autoritzacio_nom" value="<?php echo get_value("autoritzacio_nom", $datos->autoritzacio_nom); ?>" <?php echo (!$permitirModificacion ? "readonly" : ""); ?> style="width:200px!important;display:inline!important;"/>
                        <?php echo form_error("autoritzacio_nom"); ?>
                        
                        <?php echo lang("Autoritzacio_dni") ?> <input type="text" size="10" class="form-control" maxlength="10" id="autoritzacio_dni" name="autoritzacio_dni" value="<?php echo get_value("autoritzacio_dni", $datos->autoritzacio_dni); ?>" <?php echo (!$permitirModificacion ? "readonly" : ""); ?> style="width:200px!important;display:inline!important;"/>
                        <?php echo form_error("autoritzacio_dni"); ?>
                        
                        <?php echo lang("Autoritzacio_nom_fill") ?> <input type="text" size="30" class="form-control" maxlength="150" id="Autoritzacio_nom_fill" name="Autoritzacio_nom_fill" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" readonly style="width:200px!important;display:inline!important;"/>
                        <?php echo lang("Autoritzacio_del") ?> <b><?php echo $z_fdesde_txt; ?></b>
                        <?php echo lang("Autoritzacio_al") ?> <b><?php echo $z_fhasta_txt; ?></b>
                        <?php echo lang("Autoritzacio_casa_colonies") ?> <b><?php echo $z_centro; ?></b>
                        <?php /* echo lang("Autoritzacio_poblacio_casa_colonies") ?> <b><?php echo $z_poblacio; ?></b><br /> */ ?>
                        <br />
                        <?php
                            if ($datos->tipo_reserva == 6) {
                                $sufijo_compromis = "_6";
                            } else {
                                $sufijo_compromis = ($colectiu ? "C" : "");
                            }
                            //<pre style="display:none"><?php echo print_r($datos, true); ?></pre>
                        ?>                        
                        <?php echo lang("Autoritzacio_compromis" . $sufijo_compromis) ?> <br /><br />
                        <input type="checkbox" id="autoritzacio_ok" name="autoritzacio_ok" <?php echo (get_value("autoritzacio_ok", $datos->autoritzacio_ok) ? 'checked="checked"' : ""); ?> value="<?php echo lang("val_True"); ?>" <?php echo (!$permitirModificacion ? "disabled='disabled'" : ""); ?>/><?php echo lang("Autoritzacio_acceptacio"); ?>
                        <?php echo form_error("autoritzacio_ok"); ?>
                    </div>
                </div>

                <br/>
                <div class="padding5 ">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                        <h2>6 .- <?php echo lang("H_Acceptacio_Normativa" . ($colectiu ? "C" : "")); ?></h2>                        
                    </div>
                </div>
                <div class="padding5 minH box border3">
                    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                        <?php echo lang("Normativa_autoritzacio") ?><br /><br />
                        <input type="checkbox" id="normativa_ok" name="normativa_ok" <?php echo (get_value("normativa_ok", $datos->normativa_ok) == true ? 'checked="checked"' : ""); ?> value="<?php echo lang("val_True"); ?>" <?php echo (!$permitirModificacion ? "disabled='disabled'" : ""); ?>/> <?php echo lang("Normativa_acceptacio" . ($colectiu ? "C" : "")) ?><br /><br />
                        * <?php echo lang("Normativa_llegir") ?> <a href="<?php echo $enlaceCondiciones; ?>" target="_blank"><?php echo lang("Normativa_texte_enllac") ?></a><br />
                        <?php echo form_error("normativa_ok"); ?>
                    </div>
                </div>

                <?php 
                $numPaso = 6;
                if ($z_quads == 1) {
                    $numPaso++;
                    $parametros = array(
                        "numPaso" => $numPaso,
                        "readonly" => !$permitirModificacion,
                        "val_aut_paint_quad" => $datos->aut_paint_quad
                    );
                    $CI->load->view($ruta . "/actividades_paintball_view", $parametros);
                }
                if ($z_dni_nen == 1) {
                    $numPaso++;
                    $parametros = array(
                        "numPaso" => $numPaso,
                        "readonly" => !$permitirModificacion,
                        "val_mallorca_dni" => $datos->mallorca_dni
                    );
                    $CI->load->view($ruta . "/actividades_mallorca_view", $parametros);
                }
                if ($z_futbol == 1) {
                    $numPaso++;
                    $parametros = array(
                        "numPaso" => $numPaso,
                        "readonly" => !$permitirModificacion,
                        "val_futbol_equipacion" => $datos->futbol_equipacion
                    );
                    $CI->load->view($ruta . "/actividades_futbol_view", $parametros);
                }
                if ($z_hipica == 1) {
                    $numPaso++;
                    $parametros = array(
                        "numPaso" => $numPaso,
                        "readonly" => !$permitirModificacion,
                        "val_hipica1" => $datos->hipica1,
                        "val_hipica2" => $datos->hipica2,
                        "val_hipica3" => $datos->hipica3,
                        "val_hipica4" => $datos->hipica4,
                        "val_observaciones_hipica" => $datos->observaciones_hipica
                    );
                    $CI->load->view($ruta . "/actividades_hipica_view", $parametros);
                }
                $numPaso++;

                //  A PARTIR DE AQUI METEMOS LA MODIFICACION PARA COVID19
                ?>
                        <br/>
                        <div class="padding5 ">
                            <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                                <h2><?php echo $numPaso ?> .- <?php echo lang("covid19_titol_declaracio"); ?></h2>                        
                            </div>
                        </div>
                        <div class="padding5 minH box border3">
                            <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                                <?php echo lang("covid19_texte_declaracio_introduccio1") ?><br /><br />
                                <strong><?php echo lang("covid19_texte_declaracio_subtitol") ?></strong><br />
                                <ul>
                                    <li><?php echo lang("covid19_texte_declaracio_li1_1") ?><INPUT TYPE="TEXT" id="nombreparticipante_1" name="nombreparticipante_1" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_li1_2") ?></li>
                                    <li><?php echo lang("covid19_texte_declaracio_li2") ?></li>
                                    <li><?php echo lang("covid19_texte_declaracio_li3_1") ?><INPUT TYPE="TEXT" id="nombreparticipante_2" name="nombreparticipante_2" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_li3_2") ?></li>
                                    <li><?php echo lang("covid19_texte_declaracio_li4_1") ?><INPUT TYPE="TEXT" id="nombreparticipante_3" name="nombreparticipante_3" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_li4_2") ?></li>
                                </ul><br />
                                <?php echo lang("covid19_texte_declaracio_introduccio2_1") ?><INPUT TYPE="TEXT" id="nombreparticipante_4" name="nombreparticipante_4" value="<?php echo get_value("nom_complet", $datos->nom_complet); ?>" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_introduccio2_2") ?><br />
                                <ul>
                                    <li><?php echo lang("covid19_texte_declaracio_li5") ?></li>
                                    <li><?php echo lang("covid19_texte_declaracio_li6") ?></li>
                                </ul><br />
                                <?php echo lang("covid19_texte_declaracio_opcional_1") ?><INPUT TYPE="TEXT" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_opcional_2") ?><br /><br />
                                <INPUT TYPE="checked"/>
                                <input type="checkbox" id="covid19_ok" name="covid19_ok" <?php echo (get_value("covid19_ok", $datos->covid19_ok) == true ? 'checked="checked"' : ""); ?> value="<?php echo lang("val_True"); ?>" <?php echo (!$permitirModificacion ? "disabled='disabled'" : ""); ?>/><?php echo lang("covid19_texte_declaracio_accepto_1") ?><INPUT TYPE="TEXT" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_accepto_2") ?><INPUT TYPE="TEXT" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_accepto_3") ?><INPUT TYPE="TEXT" READONLY="READONLY"/><?php echo lang("covid19_texte_declaracio_accepto_4") ?><br />
                                <?php echo form_error("covid19_ok"); ?><br /><br /><br />
                                <?php echo lang("covid19_texte_info_adicional") ?><br />
                                <textarea cols="45" rows="10" id="covid19_obs" class="form-control" name="covid19_obs"><?php echo get_value("covid19_obs", $datos->covid19_obs) ?></textarea>
                                <?php echo form_error("covid19_obs"); ?>
                            </div>
                        </div>
                <?php 
                    //  A PARTIR DE AQUI METEMOS LA MODIFICACION PARA COVID19
                    $numPaso++; 
                ?>

                <?php if ($permitirModificacion) { ?>
                    <br/>
                    <div class="padding5 ">
                        <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 ">
                            <h2><?php echo $numPaso ?> .- <?php echo lang("H_Botonera"); ?></h2>                                    
                        </div>
                    </div>
                    <div class="padding5 minH box border3">
                        <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                            <?php echo lang("Botonera") ?><br/><br/>
                            <?php echo lang("texto_caja_email"); ?> 
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                            <input type="text" class="form-control" value="" id="confirmacionxmail" name="confirmacionxmail">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                            <input type="button" class="btnSubmit" value="<?php echo lang("END") ?>" OnClick="validacion();" style="float:right;">
                        </div>
                    </div>
                <?php 
            } ?>
                <br/>
            </div>                    
        </form>
    </body>
    <script type="text/javascript">
    <!--
   
        <?php 
        if ($permitirModificacion) {
            ?>
                    $(function() {
                        $('#data_naixement').datepicker({
                            dateFormat: 'dd/mm/yy', 
                            showWeek: true,
                            firstDay: 1,
                            numberOfMonths: 1,
                            changeYear: true,
                            regional: $.datepicker.regional['<?php echo strtolower($lang); ?>'], 
                            showOtherMonths: true,
                            selectOtherMonths: true,
                            yearRange: '<?php echo $z_Anyodesde ?>:<?php echo $z_Anyohasta ?>'
                        })<?php
                            if ($data_naixement == "01/01/1800") {
                                // echo '.datepicker("setDate", new Date('.$z_Anyodesde.',1,1))';
                                echo '.datepicker("setDate", "01/01/' . $z_Anyodesde . '")';
                            } else {
                                // $fecha_cambio = explode("/", $data_naixement);
                                // $fecha_cambio = $fecha_cambio[2].",".$fecha_cambio[1].",".$fecha_cambio[0];
                                // echo '.datepicker("setDate", new Date('.$fecha_cambio.'))';
                                echo '.datepicker("setDate", "' . $data_naixement . '")';
                            }
                            ?>;
                    });

                    function CheckFileName(campo) {
                        var fileName = campo.value
                        if (fileName == "") {
                            //alert("Browse to upload a valid File with png extension");
                            campo.value = null;
                            return false;
                        }
                        else if (
                                fileName.split(".")[1].toUpperCase() == "JPG" ||
                                fileName.split(".")[1].toUpperCase() == "JPEG" ||
                                fileName.split(".")[1].toUpperCase() == "GIF" ||
                                fileName.split(".")[1].toUpperCase() == "PDF" ||
                                fileName.split(".")[1].toUpperCase() == "PNG"
                                )
                            return true;
                        else {
                            //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                            campo.value = null;                        
                            return false;
                        }
                        return true;
                    }
                <?php

            }
            ?> 
    //-->
    </script>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" class="img-responsive" id="fotomodal" name="fotomodal">
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($permitirModificacion) {
        ?>
                <script type="text/javascript">
                    function lanzar_subida(tipo) {
                        $("#tipo").val(tipo);
                        $("#imagen").click();
                    }

                    function doupload() {
                        $("#formIMAGE").submit();
                    }

                    function mostrar(imagen) {                        
                        $("#fotomodal").attr("src", $("#"+imagen).attr("src"));
                    }

                    $(document).ready(function (e) {
                        $("#formIMAGE").submit(function(e) {
                            e.preventDefault();
                            var form_data = new FormData();                  
                            var low = 0;
                            var high = (($("#tipo").val() == "CVAC") ? $('#imagen').prop('files').length : 1);

                            $(".btnSubmit").attr("disabled", "disabled");

                            var tag = "";
                            switch($("#tipo").val()) {
                                case "IMG":
                                    tag = "foto";
                                break;
                                case "DNIA":
                                    tag = "dnia";
                                break;
                                case "DNIR":
                                    tag = "dnir";
                                break;
                                case "CVAC":
                                    tag = "cvac";
                                break;
                            }
                            if ($("#tipo").val()!="CVAC") {
                                $("#imagen_"+tag+"_th").hide(100);
                            }
                            $("#spinner_"+tag+"_th").show(200);
                            
                            for (var x = low; x < high; x++) {
                                var file_data = $('#imagen').prop('files')[x];
                                form_data.append('files', file_data);
                            }
                            $.ajax({
                                url: "<?php echo base_url(); ?>doing/upload/<?php echo $param_url; ?>/"+$("#tipo").val()+"/true", // Url to which the request is send
                                type: "POST",             // Type of request to be send, called as method
                                data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                                contentType: false,       // The content type used when sending data to the server.
                                cache: false,             // To unable request pages to be cached
                                processData:false,        // To send DOMDocument or non processed data file it is set to false
                                success: function(response) { // A function to be called if request succeeds
                                    var resultado = response.split(" - ");
                                    var tag = "";
                                    var foto_error="";
                                    switch($("#tipo").val()) {
                                        case "IMG":
                                            tag = "foto";
                                            foto_error="nofoto150.png";
                                        break;
                                        case "DNIA":
                                            tag = "dnia";
                                            foto_error="nodnia150.gif";
                                        break;
                                        case "DNIR":
                                            tag = "dnir";
                                            foto_error="nodnir150.gif";
                                        break;
                                        case "CVAC":
                                            //$tag = "cvac";
                                        break;
                                    }

                                    if (resultado[0]=="Error") {
                                        $("#imagen_"+tag+"_th").attr("src", "<?php echo base_url(); ?>UploadedFiles/IMG/"+foto_error).show(200);
                                        var texto = resultado[1].replace(/<[^>]*>?/g, '');
                                        $("#imagen_"+tag+"_th").attr("title", texto);
                                        $("#spinner_"+tag+"_th").hide(100);
                                        $(".btnSubmit").removeAttr("disabled");
                                        $("#resetear").click();
                                    } else {
                                        if ($("#tipo").val()!="CVAC") {
                                            extension = response.split(".");
                                            extension = extension[extension.length-1];
                                            var url="";
                                            if (extension.toUpperCase() == "PDF") {
                                                url = response;
                                                response = '<?php echo $img_pdf; ?>';
                                            }
                                            $("#imagen_"+tag+"_th").attr("src", response).show(200);
                                            $("#imagen_"+tag+"_th").attr("title", "");
                                            $("#remove_"+tag+"_th").show(200);
                                            $("#spinner_"+tag+"_th").hide(100);
                                            $("#a_"+tag+"_th").removeAttr("data-toggle").removeAttr("data-target").removeAttr("target");
                                            if ($("#tipo").val()!="CVAC") {
                                                if (extension.toUpperCase() == "PDF") {
                                                    $("#a_"+tag+"_th").attr("target","_blank").attr("href", url);

                                                } else {
                                                    $("#a_"+tag+"_th").attr("data-toggle", "modal").attr("data-target","#myModal").attr("href", "#");
                                                }
                                            }                                        
                                        } else {
                                            $("#spinner_cvac_th").hide(100);
                                            $("#cvac_resultados").html(response);
                                        }
                                        $(".btnSubmit").removeAttr("disabled");
                                        $("#resetear").click();
                                    }
                                }, 
                                error: function(response) {
                                    $(".btnSubmit").removeAttr("disabled");
                                }
                            });
                        });
                    });
                    
                    function lanzar_borrado(param1, param2) {
                        var form_data = new FormData();                  
                        form_data.append('param1', param1);
                        form_data.append('param2', param2);
                        $.ajax({
                            url: "<?php echo base_url(); ?>doing/delete/<?php echo $param_url; ?>/true",
                            type: "POST",             // Type of request to be send, called as method
                            data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(response) { // A function to be called if request succeeds
                                switch(param2) {
                                    case 1:
                                        $("#imagen_foto_th").attr("src", "<?php echo base_url()."UploadedFiles/IMG/nofoto150.png"; ?>").show(200);
                                        $("#remove_foto_th").hide(100);
                                        $("#spinner_foto_th").hide(100);
                                    break;
                                    case 3:
                                        $("#imagen_dnia_th").attr("src", "<?php echo base_url()."UploadedFiles/IMG/nodnia150.gif"; ?>").show(200);
                                        $("#remove_dnia_th").hide(100);
                                        $("#spinner_dnia_th").hide(100);
                                    break;
                                    case 4:
                                        $("#imagen_dnir_th").attr("src", "<?php echo base_url()."UploadedFiles/IMG/nodnir150.gif"; ?>").show(200);
                                        $("#remove_dnir_th").hide(100);
                                        $("#spinner_dnir_th").hide(100);
                                    break;
                                    case 5:
                                        $("#spinner_cvac_th").hide(100);
                                        $("#cvac_resultados").html(response);
                                    break;
                                }
                                $("#resetear").click();
                            }
                        });
                    }
    
                </script>
                <form action="<?php echo base_url(); ?>doing/upload/<?php echo $param_url; ?>/IMG" method="post" id="formIMAGE" name="formIMAGE"  enctype="multipart/form-data"  class="hidden">
                    <input name="tipo" type="text" id="tipo" value="" />
                    <input name="resetear" id="resetear" type="reset" />
                    <input name="imagen" type="file" class="FormFill" id="imagen" value="" accept="image/gif,image/jpeg,image/png,image/pjpeg,application/pdf" onchange="javascript:doupload();"/><br />
                </form>
            <?php 
        }
    ?>
</html>