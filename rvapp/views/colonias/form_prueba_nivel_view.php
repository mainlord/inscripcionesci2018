<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta name="google" content="notranslate" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="Cache-Control" content ="no-cache" />

        <title><?php echo lang("TitolWeb"); ?></title>
        
        <!-- Bootstrap -->
            <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.min.css" media="all" rel="stylesheet">
            <link href="<?php echo base_url(); ?>css/bootstrap/style-indis.css" media="all" rel="stylesheet">

            <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <link href="<?php echo base_url(); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap-select.js"></script>
            <!--script type="text/javascript">
                // Igualar alturas de paneles
                $(document).ready(function (){
                    var highestCol = Math.max($('.tabla .celda').innerHeight());
                    $('.tabla .celda').height(highestCol+30);
                });
            </script-->
        <!-- !Bootstrap -->
        <style> 
            .btnSubmit {
                padding: 10px; 
                background-color: #0069af!important; 
                border: 1px solid #0069af!important; 
                border-radius: 5px; 
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px; 
                text-decoration:none; float: right; } 
        </style>
    </head>

    <body>
        <header class="top-header hidden-print">
            <div class="container">
                <a href="#" target="_blank"><h1 class="logo" style="background-image:url(<?php echo base_url(); ?>images/logo_rosadelsvents-indis.png);">Rosa dels Vents</h1></a>
                <?php 
                    if ($z_zurich) {
                        ?>&nbsp;<img src="<?php echo base_url(); ?>images/logo_zurich.png" style="width:150px!important;margin-left:15px" /><?php
                    } 
                ?>
                <div class="support">
                    <p class="question"><?php echo lang("Titol");?></p>
                    <p><a href="tel:934092071" class="phone">934092071</a></p>
                    <p><a href="mailto:informacio@rosadelsvents.es" class="email">informacio@rosadelsvents.es</a></p>
                </div> 
            </div>
        </header>


        <form action="<?php echo base_url(); ?>testnivel/guardar/<?php echo $param_url; ?>/<?php echo $lang; ?>" method="post" id="formleveltest" name="formleveltest" class="col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <!-- STEP 1 a 2 -->
                <!-- 1 PROGRAMA -->
                <!-- NOMBRE CENTRO -->
                <div class="col-md-8 col-md-offset-2 padding5">
                    <div class="col-md-12 padding5">
                        <h2><?php echo lang("Titol"); ?></h2>
                    </div>  
                    <div class="padding5 box boxactive">
                        <div class="col-xs-12">
                            <b><?php echo lang("Participant"); ?>:</b><br/>
                            <?php echo $z_nom; ?>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-md-8  col-md-offset-2 padding5 top5">
                    <div class="col-md-12 padding5">
                        <h2><?php echo lang("H_Info"); ?></h2>
                    </div>  
                    <div class="padding5 box">
                        <div class="col-xs-12 padding0">
                            <div class="col-md-12 col-sm-12 padding0">
                                <div class="col-md-12 col-sm-12 step2-grey top5">
                                    <?php echo lang("Info_Descripcion"); ?>
                                </div> 
                                <div class="col-md-12 col-sm-12 step2-grey top5">
                                    <input type="checkbox" id="NSNC" name="NSNC" value="1" onchange="javascript:activar();"/> <?php echo lang("NSNC") ?>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <br/>
                <div class="col-md-8  col-md-offset-2 padding5 top5">
                    <div class="col-md-12 padding5">
                        <h2><?php echo lang("H_Titulo"); ?></h2>
                    </div>  
                    <div class="padding5 box">
                        <div class="col-xs-12 padding0">
                            <div class="col-md-12 col-sm-12 padding0 tabla" >
                                <div class="col-md-12 col-sm-12 quants">
                                    <h4><?php echo lang("H3_Enunciado"); ?></h4>
                                </div> 
                                <?php
                                    $ncolumnas = 3;
                                    //$tablas[$ncolumnas];
                                    $columna=0;
                                    $tabla="";
                                    $fila="";
                                    $preguntas = explode(lang("Splitter"),lang("Textaxo"));
                                    for ($contador = 0; $contador < $npreguntas; $contador++) {

                                        $pregunta = $preguntas[($contador * 5)];
                                        $respuesta_a = $preguntas[($contador * 5) + 1];
                                        $respuesta_b = $preguntas[($contador * 5) + 2];
                                        $respuesta_c = $preguntas[($contador * 5) + 3];
                                        $respuesta_d = $preguntas[($contador * 5) + 4];

                                        $fila.= 
                                                "<div class=\"col-md-4 col-sm-4 col-xs-12 step2-grey celda top5\">".
                                                    "<b>".($contador + 1)." .- ".$pregunta."</b><br/>".
                                                    "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=A tabindex=".($contador + 1)." />  ".$respuesta_a."<br/>".
                                                    "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=B tabindex=".($contador + 1)." />  ".$respuesta_b."<br/>".
                                                    "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=C tabindex=".($contador + 1)." />  ".$respuesta_c."<br/>".
                                                    "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=D tabindex=".($contador + 1)." />  ".$respuesta_d."<br/>".
                                                    "&nbsp;</div>";

                                        // forzamos los cambios de pagina de impresion.
                                        if (($contador + 1) == 6) $fila.= "<div class=\"col-md-12 col-sm-12 col-xs-12 step2-grey celda page-break\">&nbsp;</div>";

                                        if (($columna < ($ncolumnas-1)) && ($contador < ($npreguntas - 1))) 
                                            { $columna++; }
                                        else 
                                            { 
                                                $columna=0; 
                                                $tabla.= 
                                                    "<div class=\"col-md-12 col-sm-12 step2-grey top15\">".
                                                        $fila.              
                                                    "</div>";
                                                
                                                $fila="";
                                            }
                                    }
                                    echo $tabla;
                                ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" class="btnSubmit pull-right hidden-print" value="<?php echo lang("boton") ?>" tabindex="<?php echo ($npreguntas + 1)?>" OnClick="validacion();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script languaje="javascript">
                var falta="";
                function validarr(campo) {
                    //var tamanyo = document.getElementById(campo).length;
                    eval ("var valor = ((window.document.formleveltest."+campo+"[0].checked == true) || (window.document.formleveltest."+campo+"[1].checked == true) || (window.document.formleveltest."+campo+"[2].checked == true) || (window.document.formleveltest."+campo+"[3].checked == true))");
                    var resultado = (valor == <?php echo lang("val_True") ?>);
                    if (!resultado) {
                        if (window.falta=="") window.falta = campo;
                    }
                    return resultado;
                }

                function validarc(campo) {
                    //var tamanyo = document.getElementById(campo).length;
                    var valor = document.getElementById(campo).checked;
                    var resultado = (valor == <?php echo lang("val_True") ?>);
                    return resultado;
                }
                
                function marcar(activar, campo) {
                    eval ("window.document.formleveltest."+campo+"[0].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                    eval ("window.document.formleveltest."+campo+"[1].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                    eval ("window.document.formleveltest."+campo+"[2].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                    eval ("window.document.formleveltest."+campo+"[3].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                }
                
                function activar() {
                    var valor = document.getElementById('NSNC').checked;
                    <?php
                    for ($contador = 0; $contador < $npreguntas; $contador++) {
                        ?>
                        marcar(valor,'respuesta<?php echo $contador ?>');
                        <?php
                    }
                    ?>
                }
                
                function validacion() {
                    window.falta = "";
                    if (validarc('NSNC') || (<?php
                    for ($contador = 0; $contador < $npreguntas; $contador++) {
                        echo (($contador != 0)?" && ":"")."validarr('respuesta".$contador."')";
                    }
                    ?>)) {
                        if (confirm('<?php echo lang("AlertaEnviament") ?>')) {    
                            document.forms['formleveltest'].submit();
                        }
                    } else {
                        alert('<?php echo lang("AlertaFaltanCampos"); ?>');
                        document.getElementById(window.falta).focus();
                        $('html, body').animate({scrollTop: $('#'+window.falta).offset().top - 180 }, 'slow');                        
                    }
                }

            </script>
        </form>
    </body>    
</html>
