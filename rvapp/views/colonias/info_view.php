<?php 
    /*    
        if ($errorlog = false) {
            error_reporting(E_ALL);
            ini_set('display_errors','On');
        }
    */
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="https://www.w3.org/1999/xhtml">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="google" content="notranslate" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
            <meta http-equiv="expires" content="0" />
            <meta http-equiv="Cache-Control" content ="no-cache" />
    
            <title><?php echo lang("TitolWeb"); ?></title>
            
            <!-- Bootstrap -->
                <link href="<?php echo base_url(); ?>/css/bootstrap/bootstrap.min.css" rel="stylesheet">
                <link href="<?php echo base_url(); ?>/css/bootstrap/style-indis.css" rel="stylesheet">
    
                <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <link href="<?php echo base_url(); ?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    
                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->
                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <script src="<?php echo base_url(); ?>/jquery/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url(); ?>/jquery/js/bootstrap-select.js"></script>
                <!--script type="text/javascript">
                    // Igualar alturas de paneles
                    $(document).ready(function (){
                        var highestCol = Math.max($('.tabla .celda').innerHeight());
                        $('.tabla .celda').height(highestCol+30);
                    });
                </script-->
            <!-- !Bootstrap -->
            <style> 
                .btnSubmit {
                    padding: 10px; 
                    background-color: #0069af!important; 
                    border: 1px solid #0069af!important; 
                    border-radius: 5px; 
                    font-weight: bold; 
                    color: #ffffff !important; 
                    font-size: 14px; 
                    text-decoration:none; float: right; } 
            </style>
        </head>
        <body>
            <header class="top-header">
                <div class="container">
                    <a href="#" target="_blank"><h1 class="logo" style="background-image:url(<?php echo base_url(); ?>/images/logo_rosadelsvents-indis.png);">Rosa dels Vents</h1></a>
                    <?php 
                        if ($z_zurich) {
                            ?>&nbsp;<img src="<?php echo base_url(); ?>images/logo_zurich.png" style="width:150px!important;margin-left:15px" /><?php
                        } 
                    ?>
                    <div class="support">
                        <p class="question"><?php echo lang("Titol");?></p>
                        <p><a href="tel:934092071" class="phone">934092071</a></p>
                        <p><a href="mailto:informacio@rosadelsvents.es" class="email">informacio@rosadelsvents.es</a></p>
                    </div> 
                </div>
            </header>
            <div class="container">
                <div class="col-md-8  col-md-offset-2 padding5 top5">
                    <form action="<?php echo base_url(); ?>inscripcion/ver/<?php echo $param_url; ?>/<?php echo $lang.($forzar==true?"/true":""); ?>" method="post" id="formreservation" name="formreservation">
                        <center>
                            <div class="padding5">
                                <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
                                    <center><h1><?php echo lang("atencion_no_reserva"); ?></h1></center>
                                </div>
                            </div>
                            <br/>
                            <div class="padding5 box border3 boxactive">
                                <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
                                    <h4><?php echo lang("atencion_no_reserva2"); ?></h4>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 push-right">
                                    <input type='submit' class='input btnSubmit' value='Continuar'>
                                </div>
                            </div>
                        </center>
                        
                    </form>
                </div> 
            </div>
        </body>
    </html>