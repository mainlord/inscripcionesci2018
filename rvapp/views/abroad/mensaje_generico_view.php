<?php 
    /*    
        if ($errorlog = false) {
            error_reporting(E_ALL);
            ini_set('display_errors','On');
        }
    */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="uk-height-1-1">
    <head>
        <title><?php echo lang("TitolWeb"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8  X-Content-Type-Options=nosniff"/>
        <meta name="google" content="notranslate" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/estilsBB.css" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="Cache-Control" content ="no-cache" />
    </head>	

    <body class="info">
        <center>
            <a href="#" target="_blank"><img class="logo" src="<?php echo base_url(); ?>images/logo_blah-blah.png" /></a>
                
            <div id="grupoinfo" style="text-align:left;">
                <h1 class="default-wrap">
                    <!-- <?php echo $control; ?> -->
                    <?php echo $mensaje; ?>
                </h1>
            </div>
        </center>
    </body>
</html>