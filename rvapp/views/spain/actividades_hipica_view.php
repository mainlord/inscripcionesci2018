<script languaje="javascript">
	function controlActivarhipica() {
		if(window.document.formreservation.hipica1[0].checked == true){
			activar = true;
		}else{
			activar = false;
		}
		window.document.formreservation.hipica2[0].disabled = activar;
		window.document.formreservation.hipica2[1].disabled = activar;
		window.document.formreservation.hipica2[2].disabled = activar;
		window.document.formreservation.hipica3[0].disabled = activar;
		window.document.formreservation.hipica3[1].disabled = activar;
		window.document.formreservation.hipica3[2].disabled = activar;
		window.document.formreservation.hipica4[0].disabled = activar;
		window.document.formreservation.hipica4[1].disabled = activar;
		window.document.formreservation.hipica4[2].disabled = activar;

	}
</script>

<br/>
<div id="datos_curso_8">
    <h2><?php echo $numPaso ?> .- <?php echo lang("titulo_hipica"); ?></h2>                                    
    <div class="uk-panel-box">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <h3><?php echo lang("cabecera_hipica"); ?></h3>
                <b><?php echo lang("q_1_hipica"); ?></b><br>
                <?php
                    if ($readonly) {
                        echo lang("hipica1_radio".($val_hipica1+1))."<br/>";
                    } else {
                        ?>
                            <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 0)?"checked":""); ?> value="0" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio1") ?><br>
                            <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 1)?"checked":""); ?> value="1" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio2") ?><br>
                            <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 2)?"checked":""); ?> value="2" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio3") ?><br>
                        <?php
                    }
                ?>
                <i><?php echo lang("comentario_hipica") ?></i><br>
                <b><?php echo lang("q_2_hipica") ?></b><br>
                <?php
                    if ($readonly) {
                        echo lang("hipica2_radio".($val_hipica2+1))."<br/>";
                    } else {
                        ?>
                            <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica2_radio1") ?><br>
                            <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica2_radio2") ?><br>
                            <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica2_radio3") ?><br>
                        <?php
                    }
                ?>
                <b><?php echo lang("q_3_hipica") ?></b><br>
                <?php
                    if ($readonly) {
                        echo lang("hipica3_radio".($val_hipica3+1))."<br/>";
                    } else {
                        ?>
                            <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica3_radio1") ?><br>
                            <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica3_radio2") ?><br>
                            <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica3_radio3") ?><br>
                        <?php
                    }
                ?>
                <b><?php echo lang("q_4_hipica") ?></b><br>
                <?php
                    if ($readonly) {
                        echo lang("hipica4_radio".($val_hipica4+1))."<br/>";
                    } else {
                        ?>
                            <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica4_radio1") ?><br>
                            <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica4_radio2") ?><br>
                            <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica4_radio3") ?><br>
                        <?php
                    }
                ?>
                <i><?php echo lang("comentario_observaciones_hipica") ?></i><br>
                <b><?php echo lang("observaciones_hipica") ?></b><br>
                <?php
                    if ($readonly) {
                        echo $val_observaciones_hipica."<br/>";
                    } else {
                        ?>
                          <textarea cols="40" <?php echo ($readonly? 'disabled="disabled"': ""); ?> rows="5" id="observaciones_hipica" class="form-control" name="observaciones_hipica"><?php echo $val_observaciones_hipica ?></textarea><br/>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>