<br/>
<div id="datos_curso_8">
    <h2><?php echo $numPaso ?> .- <?php echo lang("H_Mallorca"); ?></h2>
    <div class="uk-panel-box">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <?php echo lang("exp_mallorca"); ?><br />
                <?php 
                    if ($readonly) {
                        echo $val_mallorca_dni;
                    } else {
                        ?><input type="text" name="mallorca_dni" id="mallorca_dni" value="<?php echo $val_mallorca_dni ?>" class="input" /><?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>