<?php 
    $CI =& get_instance(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo lang("TitolWeb"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
        <meta name="google" content="notranslate" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/uikit.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/upload.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/uikit.gradient.min_new.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/estilsBB.css" />
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/uikit.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/components/upload.min.js"></script>
        
        
        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />   
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
    </head>	


    <body>
        <div class="uk-container uk-container-center uk-main-container">
            <div class="uk-grid uk-grid-preserve">
                <div class="uk-width-medium-8-10 uk-push-1-10">
                    <div class="uk-grid uk-grid-preserve">
                        <div class="uk-width-medium-1-3">
                            <img class="logo" src="<?php echo base_url(); ?>images/cabeceraBB.gif"> <!-- .logo -->                        
                        </div>
                        <div class="uk-width-medium-2-3 uk-vertical-align">
                            <div class="uk-vertical-align-bottom">
                                <h1 class="uk-article-title cstm-title-sup">FICHA DE</h1>
                                <h1 class="uk-article-title cstm-title-inf">INSCRIPCIÓN</h1>
                            </div>
                        </div>
                    </div>                        
                    <br/> 
                    <br/> 
            
                    <form action="<?php echo base_url(); ?>testnivel/guardar/<?php echo $param_url; ?>/<?php echo $lang; ?>" method="post" id="formleveltest" name="formleveltest" class="col-md-12 col-sm-12 col-xs-12">
            
                        <div id="datos_curso_1">
                            <h2><?php echo lang("Participant"); ?>: <b><?php echo $z_nom; ?></b></h2>
                        </div>
                        <br/> 
                        <div id="datos_curso_1">
                            <h2><?php echo lang("H_Info"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <span><?php echo lang("Info_Descripcion"); ?></span>
                                        <br />
                                        <br />
                                        <input type="checkbox" id="NSNC" name="NSNC" value="1" onchange="javascript:activar();"/> <?php echo lang("NSNC") ?><br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/> 
                        <div id="datos_curso_1">
                            <h2><?php echo lang("H_Titulo"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <span>
                                            <h3><?php echo lang("H3_Enunciado"); ?></h3>
                                        </span>
                                    </div>
                                    <?php
                                        $ncolumnas = 3;
                                        //$tablas[$ncolumnas];
                                        /* for ($contador = 0; $contador < 5; $contador++) { */
                                        for ($contador = 0; $contador <= 5; $contador++) {
                                            $fila[$contador] = "";
                                        }
                                        $columna=0;

                                        $tabla="";
                                        $preguntas = explode(lang("Splitter"), lang("Textaxo"));
                                        for ($contador = 0; $contador < $npreguntas; $contador++) {

                                            $pregunta = $preguntas[($contador * 5)];
                                            $respuesta_a = $preguntas[($contador * 5) + 1];
                                            $respuesta_b = $preguntas[($contador * 5) + 2];
                                            $respuesta_c = $preguntas[($contador * 5) + 3];
                                            $respuesta_d = $preguntas[($contador * 5) + 4];

                                            $fila[0].= 
                                                    "<div class='uk-width-1-3' valign=top style='margin-top:0px!important'>".
                                                        "<b>".($contador + 1)." .- ".$pregunta."</b>".
                                                    "</div>";
                                            
                                            $fila[1].= 
                                                    "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>".
                                                        "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=A tabindex=".($contador + 1)." />  ".$respuesta_a.
                                                    "</div>";
                                            
                                            $fila[2].=                                             
                                                    "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>".
                                                        "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=B tabindex=".($contador + 1)." />  ".$respuesta_b.
                                                    "</div>";
                                            
                                            $fila[3].= 
                                                    "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>".
                                                        "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=C tabindex=".($contador + 1)." />  ".$respuesta_c.
                                                    "</div>";

                                            $fila[4].= 
                                                    "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>".
                                                        "<input type=radio id=respuesta".$contador." name=respuesta".$contador." value=D tabindex=".($contador + 1)." />  ".$respuesta_d.
                                                    "</div>";

                                            $fila[5].= 
                                                    "<div class='uk-width-1-3' valign=top>&nbsp;</div>";

                                            if (($columna < ($ncolumnas-1)) && ($contador < ($npreguntas - 1))) 
                                                { $columna++; }
                                            else { 

                                                $columna=0; 
                                                if ($contador != ($npreguntas-1)) {
                                                    $tabla.= $fila[0].$fila[1].$fila[2].$fila[3].$fila[4].$fila[5];
                                                    
                                                    $fila[0]="";
                                                    $fila[1]="";
                                                    $fila[2]="";
                                                    $fila[3]="";
                                                    $fila[4]="";
                                                    $fila[5]="";
                                                }
                                            }
                                        }

                                        /*
                                        $fila[0].= "<div class='uk-width-1-3' valign=top style='margin-top:0px!important'>&nbsp;</div>";       
                                        $fila[1].= "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>&nbsp;</div>";                                
                                        $fila[2].= "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>&nbsp;</div>";                                
                                        $fila[3].= "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>&nbsp;</div>";
                                        $fila[4].= "<div class='uk-width-1-3' valign=top nowrap style='margin-top:0px!important'>&nbsp;</div>";
                                        $fila[5].= "<div class='uk-width-1-3' valign=top style='margin-top:0px!important'>&nbsp;</div>";
                                        */
                                        $tabla.= $fila[0].$fila[1].$fila[2].$fila[3].$fila[4].$fila[5];

                                        echo $tabla;

                                    ?>
                                    <div class="uk-width-2-3" style='margin-top:0px!important'>&nbsp;</div>
                                    <div class="uk-width-1-3" style='margin-top:0px!important'>
                                        <center>
                                            <input type="button" class="input btnSubmit" value="<?php echo lang("boton") ?>" tabindex="<?php echo ($npreguntas + 1)?>" OnClick="validacion();">
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
        </div>
                    
        <style>
            .botonSubmit {
                padding: 10px;
                background-color: #843F80!important; 
                border: 1px solid #843F80!important; 
                border-radius: 5px;
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px;
                text-decoration:none;
            }
        </style>

        <script languaje="javascript">

            function validarr(campo) {
                //var tamanyo = document.getElementById(campo).length;
                eval ("var valor = ((window.document.formleveltest."+campo+"[0].checked == true) || (window.document.formleveltest."+campo+"[1].checked == true) || (window.document.formleveltest."+campo+"[2].checked == true) || (window.document.formleveltest."+campo+"[3].checked == true))");
                var resultado = (valor == <?php echo lang("val_True") ?>);
                return resultado;
            }

            function validarc(campo) {
                //var tamanyo = document.getElementById(campo).length;
                var valor = document.getElementById(campo).checked;
                var resultado = (valor == <?php echo lang("val_True") ?>);
                return resultado;
            }
            
            function marcar(activar, campo) {
                eval ("window.document.formleveltest."+campo+"[0].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                eval ("window.document.formleveltest."+campo+"[1].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                eval ("window.document.formleveltest."+campo+"[2].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
                eval ("window.document.formleveltest."+campo+"[3].disabled = ("+activar+" == <?php echo lang("val_True"); ?>);");
            }
            
            function activar() {
                var valor = document.getElementById('NSNC').checked;
                <?php
                for ($contador = 0; $contador < $npreguntas; $contador++) {
                    ?>
                    marcar(valor,'respuesta<?php echo $contador ?>');
                    <?php
                }
                ?>
            }
            
            function validacion() {
            
                if (validarc('NSNC') || (<?php
                for ($contador = 0; $contador < $npreguntas; $contador++) {
                    echo (($contador != 0)?" && ":"")."validarr('respuesta".$contador."')";
                }
                ?>)) {
                    if (confirm('<?php echo lang("AlertaEnviament") ?>')) {    
                        document.forms['formleveltest'].submit();
                    }
                } else {
                    var falloen = "";
                    <?php
                    for ($contador = 0; $contador < $npreguntas; $contador++) {
                        echo "falloen += (validarr('respuesta".$contador."')==true)?'':'".($contador+1).". ';";
                    }
                    ?>
                    alert('<?php echo lang("AlertaFaltanCampos"); ?>\n'+falloen);
                }
            }

        </script>
    </body>    
</html>