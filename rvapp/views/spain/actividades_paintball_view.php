<br/>
<div id="datos_curso_8">
    <h2><?php echo $numPaso ?> .- <?php echo lang("titulo_grupo7"); ?></h2>
    <div class="uk-panel-box">
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <?php echo lang("exp_grupo7"); ?><br /><br />
                <input type="checkbox" <?php echo  ($readonly? 'disabled="disabled"': ""); ?> name="aut_paint_quad" id="aut_paint_quad" <?php echo (($val_aut_paint_quad == 1)?"checked":""); ?> value="true" class="input" value="true"/> <?php echo lang("autorizacion_grupo7"); ?>
            </div>
        </div>
    </div>
</div>