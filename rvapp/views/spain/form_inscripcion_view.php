<?php 
    $CI =& get_instance(); 

    function get_value($nombre, $value) {
        if ((isset($_POST[$nombre])) and ($_POST[$nombre] != "")) { 
            return $_POST[$nombre]; 
        } else { 
            return $value; 
        }
    }    
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><?php echo lang("TitolWeb"); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
        <meta name="google" content="notranslate" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="icon" />
        <link type="image/x-icon" href="<?php echo base_url(); ?>images/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/uikit.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>jquery/css/upload.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/uikit.gradient.min_new.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/estilsBB.css" />
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/uikit.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/components/upload.min.js"></script>
        
        
        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />   
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <?php
            if ($permitirModificacion) {
                ?>
                    <script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script>
                <?php
            }
        ?>
    </head>	

<?php
    if ($permitirModificacion) {
        ?>
            <body onload="inicializar();">
                <script languaje="javascript">
                    <!--
                        $(function(){

                        var progressbar = $("#progressbar"),
                            bar         = progressbar.find('.uk-progress-bar'),
                            settings    = {

                            action: '<?php echo base_url(); ?>doing/upload/<?php echo $param_url; ?>/IMG', // upload url

                            allow : '*.(jpg|jpeg|gif|png)', // allow only images

                            loadstart: function() {
                                bar.css("width", "0%").text("0%");
                                progressbar.removeClass("uk-hidden");
                            },

                            progress: function(percent) {
                                percent = Math.ceil(percent);
                                bar.css("width", percent+"%").text(percent+"%");
                            },

                            allcomplete: function(response) {
                                //console.log(response);
                                bar.css("width", "100%").text("100%");

                                setTimeout(function(){
                                    progressbar.addClass("uk-hidden");
                                }, 250);

                                $("#imagen_foto_th").attr("src", response);
                                // $("#vomito").html(response);
                            }
                        };

                        var select = UIkit.uploadSelect($("#upload-select"), settings),
                            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
                        });

                        function CheckFileName(campo) {
                            var fileName = campo.value
                            if (fileName == "") {
                                //alert("Browse to upload a valid File with png extension");
                                campo.value = null;
                                return false;
                            }
                            else if (
                                    fileName.split(".")[1].toUpperCase() == "PDF" ||
                                    fileName.split(".")[1].toUpperCase() == "JPG" ||
                                    fileName.split(".")[1].toUpperCase() == "PJPG" ||
                                    fileName.split(".")[1].toUpperCase() == "GIF" ||
                                    fileName.split(".")[1].toUpperCase() == "PNG"
                                    )
                                return true;
                            else {
                                //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                                campo.value = null;
                                return false;
                            }
                            return true;
                        }
                        
                        function update_name(campo) {
                            document.getElementById('nombre_autorizacion_padre').value = campo.value;
                            document.getElementById('nombre_autorizacion_padre_medica').value = campo.value;                
                            document.getElementById('nombre_autorizacion_audiovisual_padres').value = campo.value;                
                        }

                        function update_dni(campo) {
                            document.getElementById('dni_autorizacion_padres').value = campo.value;
                            document.getElementById('dni_autorizacion_padres_medica').value = campo.value;                
                            document.getElementById('dni_autorizacion_audiovisual_padres').value = campo.value;                
                        }

                        function update_email(campo) {
                            document.getElementById('confirmacionxmail').value = campo.value;
                        }

                        function edad(fnac, fent, cdest) {
                            if (fnac.indexOf("/")>0) {
                                fnac = fnac.split("/")[2]+"-"+fnac.split("/")[1]+"-"+fnac.split("/")[0];
                            }                
                            var fechaNac = new Date(fnac);
                            var fechaEnt = new Date(fent);
                            var edad = fechaEnt.getTime() - fechaNac.getTime();
                            var fres = new Date(edad);
                            document.getElementById(cdest).value = Math.floor(fres.getFullYear()-1970);
                            /* alert("fechaNac "+fechaNac+"\nfechaEnt "+fechaEnt+ "\nedad "+edad+ "\nfres "+fres); */
                        }

                        var campoFalla="";
                        
                        function actualizarCampo(desde, hasta) {
                            document.getElementById(hasta).value = document.getElementById(desde).value;
                        }
                        
                        function controlAdicionales(activar, cadena) {
                            var campo;
                            var campos = cadena.split("|");
                            for (var campo in campos) {
                                document.getElementById(campos[campo]).disabled = (activar.toString() != 'true');
                            }
                        }
                        function inicializar() {
                            controlAdicionales(document.getElementById('malalt').checked, 'malalt_detall');
                            controlAdicionales(document.getElementById('medicaments').checked, 'medicaments_detall|medicaments_administracio');
                            controlAdicionales(document.getElementById('sotatractament').checked, 'sotatractamentdetall');
                            controlAdicionales(document.getElementById('regim').checked, 'regim_detall');
                            controlAdicionales(document.getElementById('operat').checked, 'operat_detall');
                            controlAdicionales(document.getElementById('alergic_altres').checked, 'alergic_detall');
                            controlAdicionales(document.getElementById('dificultatesports').checked, 'dificultatesports_quins');                    
                            <?php echo $z_hipica_js; ?>
                        }
                        
                        function validart(campo) {
                            var texto = document.getElementById(campo).value.replace(/^\s*|\s*$/g,"");
                            var ok=false;
                            if (texto.length>0) {
                                ok=true;
                            }           
                            if (!ok) if (window.campoFalla=="") window.campoFalla=campo;
                            return ok;
                        }
                        
                        
                        function validarmt(cadena) {
                            var campo, texto;
                            var ok=true;
                            var campos = cadena.split("|");
                            
                            for (campo in campos) {
                                texto = document.getElementById(campos[campo]).value.replace(/^\s*|\s*$/g,"");
                                if (texto.length>0) {
                                    ok=(ok==true);
                                } else {
                                    if (window.campoFalla=="") window.campoFalla=campos[campo];
                                    ok=false;
                                }
                            }                               
                            
                            return ok;
                        }
                        
                    
                        function validarr(campo) {
                            //var tamanyo = document.getElementById(campo).length;
                            /* var valor = document.getElementById(campo).checked; */                    
                            var valor = document.getElementById(campo).checked; 
                            var resultado = (valor.toString() == 'true');
                            if (!resultado && campo == "futbol_equipacion") if (window.campoFalla=="") window.campoFalla=campo;
                            return resultado;
                        }

                        function validarx(campo) {
                            var resultado = (   $("input[name="+campo+"]:checked").val() == "false" ||
                                                $("input[name="+campo+"]:checked").val() == "true"    );
                            if (!resultado && campo != 'alergic_altres') if (window.campoFalla=="") window.campoFalla=campo;
                            return resultado;   
                        }

                        function validarc(campo) {
                            //var tamanyo = document.getElementById(campo).length;
                            var valor = document.getElementById(campo).checked;
                            var resultado = (valor.toString() == 'true');
                            if (!resultado && campo != 'alergic_altres') if (window.campoFalla=="") window.campoFalla=campo;
                            return resultado;
                        }
                        
                        function validarf(campo){  
                            var cadena = document.getElementById(campo).value;
                            var fecha = new String(cadena); //  Crea un string  
                            var realfecha = new Date()      //  Para sacar la fecha de hoy  
                            //  Cadena Año  
                            var ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
                            //  Cadena Mes  
                            var mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
                            //  Cadena Día  
                            var dia= new String(fecha.substring(0,fecha.indexOf("/")))  

                            // evitamos que nos metan una fecha sin  /
                            if (fecha.lastIndexOf("/")== -1) {
                                if (window.campoFalla=="") window.campoFalla=campo;
                                return false  
                            }
                            
                            // Valido el año  
                            if (isNaN(ano) || ano.length<4 || parseFloat(ano)<1900){  
                                //    alert('Año inválido')  
                                if (window.campoFalla=="") window.campoFalla=campo;
                                return false  
                            }  
                            // Valido el Mes  
                            if (isNaN(mes) || parseFloat(mes)<1 || parseFloat(mes)>12){  
                                // alert('Mes inválido')  
                                if (window.campoFalla=="") window.campoFalla=campo;
                                return false  
                            }  
                            // Valido el Dia  
                            if (isNaN(dia) || parseInt(dia, 10)<1 || parseInt(dia, 10)>31){  
                                // alert('Día inválido')  
                                if (window.campoFalla=="") window.campoFalla=campo;
                                return false  
                            }  
                            if (mes==4 || mes==6 || mes==9 || mes==11 || mes==2) {  
                                if ((mes==2 && dia>28 && (parseFloat(ano)%4!=0)) || (dia>30)) {  
                                    //alert('Día inválido')  
                                    if (window.campoFalla=="") window.campoFalla=campo;
                                    return false  
                                }  
                            }  

                            // para que envie los datos, quitar las  2 lineas siguientes  
                            // alert("Fecha correcta.")  
                            return true
                        }  

                        function validar_con() {	
                            if(window.document.formreservation.conocido_por[7].checked == true){
                                document.getElementById('otros').disabled = false;
                            }else{
                                document.getElementById('otros').disabled = true;
                            }
                        }

                        function validacion() {
                           document.forms['formreservation'].submit(); 
                        }

                        function CheckFileName(campo) {
                            var fileName = campo.value
                            if (fileName == "") {
                                //alert("Browse to upload a valid File with png extension");
                                campo.value = null;
                                return false;
                            }
                            else if (
                                    fileName.split(".")[1].toUpperCase() == "JPG" ||
                                    fileName.split(".")[1].toUpperCase() == "PJPG" ||
                                    fileName.split(".")[1].toUpperCase() == "GIF" ||
                                    fileName.split(".")[1].toUpperCase() == "PNG"
                                    )
                                return true;
                            else {
                                //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                                campo.value = null;                        
                                return false;
                            }
                            return true;
                        }

                    //-->
                </script>
        <?php 
    } else {
        ?>
            <body>
        <?php 
    }

    if ($permitirModificacion) {
        ?>
            <form action="<?php echo base_url(); ?>inscripcion/guardar/<?php echo $param_url; ?>/<?php echo $lang.($forzar==true?"/true":""); ?>" method="post" id="formreservation" name="formreservation"  enctype="multipart/form-data"  class="col-md-12 col-sm-12 col-xs-12">
        <?php 
    }
?>
		
			<div class="uk-container uk-container-center uk-main-container">
                <div class="uk-grid uk-grid-preserve">
                    <div class="uk-width-medium-8-10 uk-push-1-10">
                        <div class="uk-grid uk-grid-preserve">
                            <div class="uk-width-medium-1-3">
                                <img class="logo" src="<?php echo base_url(); ?>images/cabeceraBB.gif"> <!-- .logo -->                        
                            </div>
                            <div class="uk-width-medium-2-3 uk-vertical-align">
                                <div class="uk-vertical-align-bottom">
                                    <h1 class="uk-article-title cstm-title-sup">FICHA DE</h1>
                                    <h1 class="uk-article-title cstm-title-inf">INSCRIPCIÓN</h1>
                                </div>
                            </div>
                        </div>                        
                        <br/> 
                        <br/> 
                        <div id="datos_curso_1">
                            <h2>1 .- <?php echo lang("Titol") /*txt_titulo_datos_curso*/; ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-2">
                                        <span><b><?php echo lang("NReserva"); ?> / <?php echo lang("Localitzador"); ?>:</b> <?php echo $datos->localizador; ?></span>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <span><b><?php echo lang("programa") ?>:</b> <?php echo $z_programa."  (".$z_fdesde_txt." - ".$z_fhasta_txt.")  Edades desde ".$z_edaddesde." a ".$z_edadhasta ?></span>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <span><b><?php echo lang("centro") ?>:</b> <?php echo $z_centro ?></span>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <span><b><?php echo lang("Dni"); ?>:</b> <?php echo $datos->dni; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /> 
                        <?php 
                            if (validation_errors() != "") {
                                ?>
                                    <div class="uk-width-1-1">
                                        <div class="uk-alert uk-alert-danger">
                                            <?php echo lang("campos_obligatorios_pendientes"); ?>
                                        </div>
                                    </div>
                                    <br /> 
                                <?php
                            }
                        ?>
                        <div id="datos_curso_2">
                            <h2>2 .- <?php echo lang("H_SeccioDPersonals"); ?></h2>
                            <div class="uk-panel-box">
                                <?php /* pre><?php echo print_r($datos, true); ?></pre*/ ?>
                                <div class="uk-grid">
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("NomComplet") ?>:</b>
                                        <?php
                                            if (!$permitirModificacion) { echo get_value("nom_complet",  $datos->nom_complet); } 
                                            else {
                                                ?>
                                                    <input type="text" id="nom_complet" name="nom_complet" class="input" maxlength="150" value="<?php echo get_value("nom_complet",  $datos->nom_complet); ?>" OnChange="javascript:actualizarCampo('nom_complet','Autoritzacio_nom_fill');"/>
                                                    <?php echo form_error("nom_complet"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Telefon") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("telefon",  $datos->telefon); } 
                                            else {
                                                ?>
                                                    <input type="text" id="telefon" name="telefon" maxlength="12" size="12" class="input input" value="<?php echo get_value("telefon",  $datos->telefon); ?>"/>
                                                    <?php echo form_error("telefon"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                		<b><?php echo lang("Adreça") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("adressa",  $datos->adressa); } 
                                            else {
                                                ?>
                                                    <input type="text" id="adressa" name="adressa" maxlength="150" class="input " value="<?php echo get_value("adressa",  $datos->adressa); ?>"/>
                                                    <?php echo form_error("adressa"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Poblacio") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("poblacio",  $datos->poblacio); } 
                                            else {
                                                ?>
                                                    <input type="text" id="poblacio" name="poblacio" maxlength="45" class="input " value="<?php echo get_value("poblacio",  $datos->poblacio); ?>"/>
                                                    <?php echo form_error("poblacio"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("CP") ?>:</b>
                                    	<?php
                                            if (!$permitirModificacion) { echo get_value("cp",  $datos->cp); } 
                                            else {
                                                ?>
                                                    <input type="text" id="cp" name="cp" class="input input" maxlength="5" size="5" value="<?php echo get_value("cp",  $datos->cp); ?>"/>
                                                    <?php echo form_error("cp"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("DataNaixement") ?>:</b>
                                		<?php
                                            if (!$permitirModificacion) { echo get_value("data_naixement",  $CI->soporte_general->cambia_fecha(get_value("data_naixement",  $datos->data_naixement), "-", "/")); }
                                            else {
                                                ?>
                                                    <div class="input-compuesto">
                                                        <?php 
                                                            $fecha_nacimiento = get_value("data_naixement", $CI->soporte_general->cambia_fecha(get_value("data_naixement",  $datos->data_naixement), "-", "/"));
                                                        ?>
                                                        <input type="text" id="data_naixement" class="formsize" name="data_naixement" readonly="readonly" maxlength="10" size="10" value="<?php echo $fecha_nacimiento; ?>"/>
                                                        <span class="botoncalendario glyphicon glyphicon-calendar">
                                                        </span>
                                                    </div>
                                                    <?php echo form_error("data_naixement"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("AltresTelefons") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("altres_telefons",  $datos->altres_telefons); } 
                                            else {
                                                ?>
                                                    <input type="text" id="altres_telefons" name="altres_telefons" maxlength="100" class="input " value="<?php echo get_value("altres_telefons",  $datos->altres_telefons); ?>"/>
                                                    <?php echo form_error("altres_telefons"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-1">
                                    <div class="uk-panel box2">
                                        <div class="uk-grid">
                                            <div class="uk-width-1-1">
                                                <b><?php echo lang("Foto") ?>:</b>
                                            </div>
                                            <?php 
                                                $ins_foto = $datos->foto;
                                                if (!$ins_foto) {
                                                    $ins_foto =  base_url()."UploadedFiles/IMG/nofoto150.png";
                                                }
                                                if ($permitirModificacion) {
                                                    ?>
                                                        <div class="uk-width-2-3">
                                                            <style>
                                                                .uk-placeholder {
                                                                    margin-bottom: 15px;
                                                                    padding: 15px;
                                                                    border: 1px dashed #E5E5E5;
                                                                    background: #fafafa;
                                                                    color: #666;
                                                                    overflow: hidden;
                                                                }
                                                                .uk-form-file input[type=file] {
                                                                    position: absolute;
                                                                    top: 0;
                                                                    z-index: 1;
                                                                    width: 100%;
                                                                    opacity: 0;
                                                                    cursor: pointer;
                                                                    left: 0;
                                                                    font-size: 500px;
                                                                }
                                                            </style>

                                                            <div id="upload-drop" class="uk-placeholder uk-text-center uk-margin-top uk-position-relative uk-overflow-hiddden">
                                                                <i class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i> <?php echo lang("texto_adjuntar_1"); ?>
                                                                <a class="uk-form-file"><?php echo lang("texto_adjuntar_2"); ?><input id="upload-select" name="upload-select" type="file"/></a>.
                                                            </div>

                                                            <div id="progressbar" class="uk-progress uk-hidden">
                                                                <div class="uk-progress-bar" style="width: 0%;">...</div>
                                                            </div>
                                                        </div>
                                                    <?php 
                                                }
                                            ?>
                                            <div class="uk-width-1-3">
                                                <img width="150px" src="<?php echo $ins_foto; ?>" class="input uk-thumbnail uk-width-1-1" name="imagen_foto_th" id="imagen_foto_th" />
                                                <div id="vomito" class="uk-width-1-1"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<br /> 
                        <div id="datos_curso_2">
                            <h2>2 .- <?php echo lang("H_InformacioSanitaria"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("MalaltSovint") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("malalt",  $datos->malalt) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="malalt" name="malalt" <?php if (get_value("malalt",  $datos->malalt) == true) { echo "checked"; }?> value="true" onchange="javascript: controlAdicionales(this.value, 'malalt_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="malalt" name="malalt" <?php if (get_value("malalt",  $datos->malalt) != true) { echo "checked"; }?> value="false" onchange="javascript: controlAdicionales(this.value, 'malalt_detall');" /><?php echo lang("False"); ?>
                                                    <?php echo form_error("malalt"); ?>
                                                <?php 
                                            }
                                        ?> 
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("MalaltSovintDetall") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("malalt_detall",  $datos->malalt_detall); }
                                            else { 
                                                ?>
                                                    <input type="text" id="malalt_detall" name="malalt_detall" maxlength="150" class="input" value="<?php echo get_value("malalt_detall",  $datos->malalt_detall); ?>"/>
                                                    <?php echo form_error("malalt_detall"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("PrenMedicament") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("medicaments",  $datos->medicaments) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="medicaments" name="medicaments" <?php if (get_value("medicaments",  $datos->medicaments) == true) { echo "checked"; }?> value="true" onchange="javascript: controlAdicionales(this.value, 'medicaments_detall|medicaments_administracio');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="medicaments" name="medicaments" <?php if (get_value("medicaments",  $datos->medicaments) != true) { echo "checked"; }?> value="false" onchange="javascript: controlAdicionales(this.value, 'medicaments_detall|medicaments_administracio');" /><?php echo lang("False"); ?>
                                                    <?php echo form_error("medicaments"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("PrenMedicamentDetall") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("medicaments_detall",  $datos->medicaments_detall); } 
                                            else {
                                                ?>
                                                    <input type="text" id="medicaments_detall" name="medicaments_detall" maxlength="150" class="input" value="<?php echo get_value("medicaments_detall",  $datos->medicaments_detall); ?>"/>
                                                    <?php echo form_error("medicaments_detall"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	&nbsp;
									</div>
                                    <div class="uk-width-1-2">
										<b><?php echo lang("PrenMedicamentAdministracio") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("medicaments_administracio",  $datos->medicaments_administracio); } 
                                            else {
                                                ?>
                                                    <input type="text" id="medicaments_administracio" name="medicaments_administracio" maxlength="150" class="input" value="<?php echo get_value("medicaments_administracio",  $datos->medicaments_administracio); ?>"/>
                                                    <?php echo form_error("medicaments_administracio"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("SotaTractament") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("sotatractament",  $datos->sotatractament) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="sotatractament" name="sotatractament" <?php if (get_value("sotatractament",  $datos->sotatractament) == true) { echo "checked"; }?> value="true" onchange="javascript: controlAdicionales(this.value, 'sotatractamentdetall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="sotatractament" name="sotatractament" <?php if (get_value("sotatractament",  $datos->sotatractament) != true) { echo "checked"; }?> value="false" onchange="javascript: controlAdicionales(this.value, 'sotatractamentdetall');" /><?php echo lang("False"); ?>
                                                    <?php echo form_error("sotatractament"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
            							<b><?php echo lang("SotaTractamentDetall") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("sotatractamentdetall",  $datos->sotatractamentdetall); } 
                                            else {
                                                ?>
                                                    <input type="text" id="sotatractamentdetall" name="sotatractamentdetall" maxlength="150" class="input" value="<?php echo get_value("sotatractamentdetall",  $datos->sotatractamentdetall); ?>"/>
                                                    <?php echo form_error("sotatractamentdetall"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Regim") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("regim",  $datos->regim) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="regim" name="regim" <?php if (get_value("regim",  $datos->regim) == true) { echo "checked"; }?> value="true" onchange="javascript: controlAdicionales(this.value, 'regim_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="regim" name="regim" <?php if (get_value("regim",  $datos->regim) != true) { echo "checked"; }?> value="false" onchange="javascript: controlAdicionales(this.value, 'regim_detall');" /><?php echo lang("False"); ?>
                                                    <?php echo form_error("regim"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("RegimDetall") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("regim_detall",  $datos->regim_detall); } 
                                            else {
                                                ?>
                                                    <input type="text" id="regim_detall" name="regim_detall" maxlength="150" class="input" value="<?php echo get_value("regim_detall",  $datos->regim_detall); ?>"/>
                                                    <?php echo form_error("regim_detall"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Operat") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("operat",  $datos->operat) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="operat" name="operat" <?php if (get_value("operat",  $datos->operat) == true) { echo "checked"; }?> value="true" onchange="javascript: controlAdicionales(this.value, 'operat_detall');" /><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="operat" name="operat" <?php if (get_value("operat",  $datos->operat) != true) { echo "checked"; }?> value="false" onchange="javascript: controlAdicionales(this.value, 'operat_detall');" /><?php echo lang("False"); ?>                    
                                                    <?php echo form_error("operat"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("OperatDetall") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("operat_detall",  $datos->operat_detall); } 
                                            else {
                                                ?>
                                                    <input type="text" id="operat_detall" name="operat_detall" maxlength="150" class="input" value="<?php echo get_value("operat_detall",  $datos->operat_detall); ?>"/>
                                                    <?php echo form_error("operat_detall"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-1">
	                                    <div class="uk-panel" style="background-color:firebrick;color:white; border-radius:5px; padding: 10px;">
	                                    	<div class="uk-grid">
			                                    <div class="uk-width-1-1">
	                            					<b><?php echo lang("Alergic") ?></b>
	                        					</div>
						                        <div class="uk-width-1-3">
                                                    <?php
                                                        if (!$permitirModificacion) { 
                                                            echo lang("Alergic_celiac").": ";
                                                            if (get_value("alergic_celiac",  $datos->alergic_celiac) == true) {
                                                                echo lang("True");
                                                            } else {
                                                                echo lang("False");
                                                            } 
                                                        } else {
                                                            ?>
                                                                <input type="checkbox" id="alergic_celiac" name="alergic_celiac" value="true" <?php if (get_value("alergic_celiac",  $datos->alergic_celiac) == true) { echo "checked"; } ?> /> <b><?php echo lang("Alergic_celiac") ?></b>
                                                                <?php echo form_error("alergic_celiac"); ?>
                                                            <?php 
                                                        }
                                                    ?>
						                        </div>
						                        <div class="uk-width-1-3">
                                                    <?php
                                                        if (!$permitirModificacion) { 
                                                            echo lang("Alergic_lactosa").": ";
                                                            if (get_value("alergic_lactosa",  $datos->alergic_lactosa) == true) {
                                                                echo lang("True");
                                                            } else {
                                                                echo lang("False");
                                                            } 
                                                        } else {
                                                            ?>
                                                                <input type="checkbox" id="alergic_lactosa" name="alergic_lactosa" value="true" <?php if (get_value("alergic_lactosa",  $datos->alergic_lactosa) == true) { echo "checked"; } ?> /> <b><?php echo lang("Alergic_lactosa") ?></b>
                                                                <?php echo form_error("alergic_lactosa"); ?>
                                                            <?php 
                                                        }
                                                    ?>
						                        </div>
						                        <div class="uk-width-1-3">
                                                    <?php
                                                        if (!$permitirModificacion) { 
                                                            echo lang("Alergic_ou").": ";
                                                            if (get_value("alergic_ou",  $datos->alergic_ou) == true) {
                                                                echo lang("True");
                                                            } else {
                                                                echo lang("False");
                                                            } 
                                                        } else {
                                                            ?>
                                                                <input type="checkbox" id="alergic_ou" name="alergic_ou" value="true" <?php if (get_value("alergic_ou",  $datos->alergic_ou) == true) { echo "checked"; } ?> /> <b><?php echo lang("Alergic_ou") ?></b>
                                                                <?php echo form_error("alergic_ou"); ?>
                                                            <?php 
                                                        }
                                                    ?>
						                        </div>
						                        <div class="uk-width-1-3">
                                                    <?php
                                                        if (!$permitirModificacion) { 
                                                            echo lang("Alergic_altres").": ";
                                                            if (get_value("alergic_altres",  $datos->alergic_altres) == true) {
                                                                echo lang("True");
                                                            } else {
                                                                echo lang("False");
                                                            } 
                                                        } else {
                                                            ?>
                                                                <input type="checkbox" id="alergic_altres" name="alergic_altres" value="true" <?php if (get_value("alergic_altres",  $datos->alergic_altres) == true) { echo "checked"; } ?> onchange="javascript: controlAdicionales(document.getElementById('alergic_altres').checked, 'alergic_detall');"/> <b><?php echo lang("Alergic_altres") ?></b>
                                                                <?php echo form_error("alergic_altres"); ?>
                                                            <?php 
                                                        }
                                                    ?>
						                        </div>
						                        <div class="uk-width-2-3">
	                            				    <b><?php echo lang("Alergic_Detall") ?></b><br />
                                                    <?php
                                                        if (!$permitirModificacion) { echo get_value("alergic_detall",  $datos->alergic_detall); } 
                                                        else {
                                                            ?>
                                                                <input type="text" id="alergic_detall" name="alergic_detall" maxlength="150" class="input" value="<?php echo get_value("alergic_detall",  $datos->alergic_detall); ?>"/>
                                                                <?php echo form_error("alergic_detall"); ?>
                                                            <?php 
                                                        }
                                                    ?>
						                        </div>
					                        </div>
				                        </div>
                                    </div>
                                    <div class="uk-width-1-1">
                                    	<b><?php echo lang("ObsGeneral") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("observaciones_generales",  $datos->observaciones_generales); } 
                                            else {
                                                ?>
                                                    <textarea cols="45" rows="10" id="observaciones_generales" class="input" name="observaciones_generales"><?php echo get_value("observaciones_generales",  $datos->observaciones_generales) ?></textarea>
                                                    <?php echo form_error("observaciones_generales"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
						<br /> 
                        <div id="datos_curso_3">
                            <h2>3 .- <?php echo lang("H_InteresGeneral"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("AltresMateixTipus") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("estades_abans",  $datos->estades_abans) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="estades_abans" name="estades_abans" <?php if (get_value("estades_abans",  $datos->estades_abans) == true) { echo "checked"; }?> value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="estades_abans" name="estades_abans" <?php if (get_value("estades_abans",  $datos->estades_abans) != true) { echo "checked"; }?> value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("estades_abans"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Nedar") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("nedar",  $datos->nedar) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="nedar" name="nedar" <?php if (get_value("nedar",  $datos->nedar) == true) { echo "checked"; }?> value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="nedar" name="nedar" <?php if (get_value("nedar",  $datos->nedar) != true) { echo "checked"; }?> value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("nedar"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("PorAigua") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("por_aigua",  $datos->por_aigua) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="por_aigua" name="por_aigua" <?php if (get_value("por_aigua",  $datos->por_aigua) == true) { echo "checked"; }?> value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="por_aigua" name="por_aigua" <?php if (get_value("por_aigua",  $datos->por_aigua) != true) { echo "checked"; }?> value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("por_aigua"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("Bicicleta") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("bicicleta",  $datos->bicicleta) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="bicicleta" name="bicicleta" <?php if (get_value("bicicleta",  $datos->bicicleta) == true) { echo "checked"; }?> value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="bicicleta" name="bicicleta" <?php if (get_value("bicicleta",  $datos->bicicleta) != true) { echo "checked"; }?> value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("bicicleta"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-1">
                                    	<b><?php echo lang("Vertigen") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("vertigen",  $datos->vertigen) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="vertigen" name="vertigen" <?php if (get_value("vertigen",  $datos->vertigen) == true) { echo "checked"; }?> value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="vertigen" name="vertigen" <?php if (get_value("vertigen",  $datos->vertigen) != true) { echo "checked"; }?> value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("vertigen"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("DificultatSports") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { 
                                                if (get_value("dificultatesports",  $datos->dificultatesports) == true) {
                                                    echo lang("True");
                                                } else {
                                                    echo lang("False");
                                                } 
                                            } else {
                                                ?>
                                                    <input type="radio" id="dificultatesports" name="dificultatesports" <?php if (get_value("dificultatesports",  $datos->dificultatesports) == true) { echo "checked"; }?> onchange="javascript: controlAdicionales(this.value, 'dificultatesports_quins');"  value="true"/><?php echo lang("True"); ?>&nbsp;&nbsp;
                                                    <input type="radio" id="dificultatesports" name="dificultatesports" <?php if (get_value("dificultatesports",  $datos->dificultatesports) != true) { echo "checked"; }?> onchange="javascript: controlAdicionales(this.value, 'dificultatesports_quins');"  value="false"/><?php echo lang("False"); ?>
                                                    <?php echo form_error("dificultatesports"); ?>
                                                <?php 
                                            }
                                        ?>
									</div>
                                    <div class="uk-width-1-2">
                                    	<b><?php echo lang("DificultatSports_Quins") ?>:</b>
										<?php
                                            if (!$permitirModificacion) { echo get_value("dificultatesports_quins",  $datos->dificultatesports_quins); } 
                                            else {
                                                ?>
                                                    <input type="text" id="dificultatesports_quins" name="dificultatesports_quins" maxlength="50" class="input" value="<?php echo get_value("dificultatesports_quins",  $datos->dificultatesports_quins); ?>"/>
                                                    <?php echo form_error("dificultatesports_quins"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>                        
                    			</div>
                    		</div>
                    	</div>
                    	<br/>
                    	<div id="datos_curso_4">
							<h2>4 .- <?php echo lang("H_LPD"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
				                        <?php echo lang("LPD"); ?>
									</div>  
								</div>
							</div>
						</div>
                    	<br/>
                    	<div id="datos_curso_5">
							<h2>5 .- <?php echo lang("H_AutoritzacioPersonal"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <?php
                                            if (!$permitirModificacion) { 
                                                echo lang("Autoritzacio_senyor")." <b> ".$datos->autoritzacio_nom."</b> ";
                                                echo lang("Autoritzacio_dni")." <b> ".$datos->autoritzacio_dni."</b> ";
                                                echo lang("Autoritzacio_nom_fill")." <b> ".$datos->nom_complet."</b> ";
                                            } else {
                                                ?>
                                                    <?php echo lang("Autoritzacio_senyor") ?> <input type="text" size="30" class="input" maxlength="150" id="autoritzacio_nom" name="autoritzacio_nom" value="<?php echo get_value("autoritzacio_nom",  $datos->autoritzacio_nom); ?>" <?php echo (!$permitirModificacion ? "readonly":""); ?> style="width:200px!important;display:inline!important;"/>
                                                    <?php echo lang("Autoritzacio_dni") ?> <input type="text" size="10" class="input" maxlength="10" id="autoritzacio_dni" name="autoritzacio_dni" value="<?php echo get_value("autoritzacio_dni",  $datos->autoritzacio_dni); ?>" <?php echo (!$permitirModificacion ? "readonly":""); ?> style="width:200px!important;display:inline!important;"/>
                                                    <?php echo lang("Autoritzacio_nom_fill") ?> <input type="text" size="30" class="input" maxlength="150" id="Autoritzacio_nom_fill" name="Autoritzacio_nom_fill" value="<?php echo get_value("nom_complet",  $datos->nom_complet); ?>" readonly style="width:200px!important;display:inline!important;"/>
                                                <?php
                                            }
                                        ?>
                                        <?php echo lang("Autoritzacio_del") ?> <b><?php echo $z_fdesde_txt; ?></b>
                                        <?php echo lang("Autoritzacio_al") ?> <b><?php echo $z_fhasta_txt; ?></b>
                                        <?php echo lang("Autoritzacio_casa_colonies") ?> <b><?php echo $z_centro; ?></b>


										<br />
										<?php echo lang("Autoritzacio_compromis") ?><br /><br />
										<input type="checkbox" id="autoritzacio_ok" name="autoritzacio_ok" <?php echo (get_value("autoritzacio_ok",  $datos->autoritzacio_ok) ? 'checked="checked"':""); ?> value="true" <?php echo (!$permitirModificacion ? "disabled='disabled'":""); ?>/><?php echo lang("Autoritzacio_acceptacio"); ?>
                                        <?php echo form_error("autoritzacio_ok"); ?>
                                        <br />    
									</div>
								</div>
							</div>
						</div>
                    	<br/>
                    	<div id="datos_curso_6">
							<h2>6 .- <?php echo lang("H_Acceptacio_Normativa"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
										<?php echo lang("Normativa_autoritzacio") ?><br /><br />
										<input type="checkbox" id="normativa_ok" name="normativa_ok" <?php echo (get_value("normativa_ok",  $datos->normativa_ok) == true ? 'checked="checked"':""); ?> value="true" <?php echo (!$permitirModificacion ? "disabled='disabled'":""); ?>/> <?php echo lang("Normativa_acceptacio") ?><br /><br />
                                        * <?php echo lang("Normativa_llegir") ?> <a href="<?php echo $enlaceCondiciones;?>" target="_blank"><?php echo lang("Normativa_texte_enllac") ?></a>
                                        <?php echo form_error("normativa_ok"); ?>
                                    </div>
								</div>
							</div>
						</div>
                    	<br/>
                    	<div id="datos_curso_7">
							<h2>7 .- <?php echo lang("titulo_observaciones"); ?></h2>
                            <div class="uk-panel-box">
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
										<?php echo lang("observaciones_ampliacion") ?>
										<?php
                                            if (!$permitirModificacion) { echo get_value("observaciones",  $datos->observaciones); } 
                                            else {
                                                ?>
                                                    <textarea cols="45" rows="10" id="observaciones" class="form-control" name="observaciones"><?php echo get_value("observaciones",  $datos->observaciones) ?></textarea>
                                                    <?php echo form_error("observaciones"); ?>
                                                <?php 
                                            }
                                        ?>
									</div>
								</div>
							</div>
						</div>
                    	<br/>
						<div id="datos_curso_8">
							<h2>8 .- <?php echo lang("titulo_como_conocido"); ?></h2>
							<div class="uk-panel-box">
                                <div class="uk-grid">
                                    <?php
                                        if (!$permitirModificacion) { 
                                            echo '<div class="uk-width-1-4">';
                                            switch ($datos->conocido_por) {
                                                case 0: echo lang("conocido_radio"); break;
                                                case 1: echo lang("conocido_TV"); break;
                                                case 2: echo lang("conocido_amigos"); break;
                                                case 3: echo lang("conocido_internet"); break;
                                                case 7: echo lang("Metro"); break;
                                                case 4: echo lang("conocido_Colegio"); break;
                                                case 5: echo lang("conocido_Prensa"); break;
                                                case 6: echo lang("Otros").": ".$datos->otros; break;
                                            } 
                                            echo "</div>";
                                        } else {
                                            ?>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 0) { echo "checked"; }?> value="0" /> <?php echo lang("conocido_radio"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 1) { echo "checked"; }?> value="1" /> <?php echo lang("conocido_TV"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 2) { echo "checked"; }?> value="2" /> <?php echo lang("conocido_amigos"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 3) { echo "checked"; }?> value="3" /> <?php echo lang("conocido_internet"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 7) { echo "checked"; }?> value="7" /> <?php echo lang("Metro"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 4) { echo "checked"; }?> value="4" /> <?php echo lang("conocido_Colegio"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 5) { echo "checked"; }?> value="5" /> <?php echo lang("conocido_Prensa"); ?>
                                                </div>
                                                <div class="uk-width-1-4">
                                                    <input onclick="javascript:validar_con();" type="radio" id="conocido_por" name="conocido_por" <?php if ($datos->conocido_por == 6) { echo "checked"; }?> value="6" /> <?php echo lang("Otros"); ?>
                                                    <input type="text" id="otros" name="otros" maxlength="150" class="" style="width:200px;" value="<?php echo $datos->otros; ?>" />
                                                </div>
                                            <?php 
                                        }
                                    ?>
								</div>
							</div>
						</div>
                        <?php 
                            $numPaso = 8;
                            if ($z_quads == 1){
                                $numPaso++;
                                $parametros = array("numPaso"               =>  $numPaso,
                                                    "readonly"              =>  !$permitirModificacion,
                                                    "val_aut_paint_quad"    =>  $datos->aut_paint_quad
                                                );
                                $CI->load->view($ruta."/actividades_paintball_view", $parametros);
                            }
                            if ($z_dni_nen == 1){
                                $numPaso++;
                                $parametros = array("numPaso"           =>  $numPaso,
                                                    "readonly"          =>  !$permitirModificacion,
                                                    "val_mallorca_dni"  =>  $datos->mallorca_dni
                                                );
                                $CI->load->view($ruta."/actividades_mallorca_view", $parametros);
                            }
                            if ($z_futbol == 1){
                                $numPaso++;
                                $parametros = array("numPaso"               =>  $numPaso,
                                                    "readonly"              =>  !$permitirModificacion,
                                                    "val_futbol_equipacion" =>  $datos->futbol_equipacion
                                                );
                                $CI->load->view($ruta."/actividades_futbol_view", $parametros);
                            }
                            if ($z_hipica == 1){
                                $numPaso++;
                                $parametros = array("numPaso"                   =>  $numPaso,
                                                    "readonly"                  =>  !$permitirModificacion,
                                                    "val_hipica1"               =>  $datos->hipica1,
                                                    "val_hipica2"               =>  $datos->hipica2,
                                                    "val_hipica3"               =>  $datos->hipica3,
                                                    "val_hipica4"               =>  $datos->hipica4,
                                                    "val_observaciones_hipica"  =>  $datos->observaciones_hipica
                                                );
                                $CI->load->view($ruta."/actividades_hipica_view", $parametros);
                            }
                            $numPaso++;
                        ?>
                    	<br/>
                        <?php
                            if ($permitirModificacion) {
                                ?>						
                                    <div id="datos_curso_8">
                                        <h2><?php echo $numPaso ?> .- <?php echo lang("H_Botonera"); ?></h2>
                                        <div class="uk-panel-box">
                                            <div class="uk-grid">
                                                <div class="uk-width-1-1">
                                                    <?php echo lang("Botonera") ?> <br/><br/>
                                                    <?php echo lang("texto_caja_email"); ?> <input type="text" class="input" value="" id="confirmacionxmail" name="confirmacionxmail"><br/><br/>
                                                    <input type="button" class="input botonSubmit" value="<?php echo lang("END") ?>" OnClick="validacion();" style="float:right;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php 
                            }
                        ?>
                    </div>                
				</div>            
    <?php
        if ($permitirModificacion) {
            ?>
                </form>
            <?php 
        }
    ?>
		</div>   
	</body>
    <?php
        if ($permitirModificacion) {
            ?>
	<script type="text/javascript">
        <!--
            $('#data_naixement').datepicker({
                dateFormat: 'dd/mm/yy', 
                showWeek: true,
                firstDay: 1,
                numberOfMonths: 1,
                /* showOn: "button", */
                changeYear: true,
                regional: $.datepicker.regional['es'], 
                showOtherMonths: true,
                selectOtherMonths: true,
                yearRange: '<?php echo $z_Anyodesde ?>:<?php echo $z_Anyohasta?>'
            })<?php
                if ($fecha_nacimiento == "01/01/1800") {
                    echo '.datepicker("setDate", new Date('.$z_Anyodesde.',1,1))';
                }
            ?>;
            $('#data_naixement + .botoncalendario').click(function(){
                $('#data_naixement').focus();
            });
            validar_con();
        //-->
    </script>
            <?php
        }
    ?>
</html>


