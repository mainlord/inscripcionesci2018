<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Soporte_xss {

	protected $xss_log = "";
	protected $CI;
	protected $config = array();
	protected $method = 'aes-256-cbc';
	protected $logar = false;
	
	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->library("encryption");    
	}	

	function xss_check($data, $encoding = "UTF-8") {
		$this->CI =& get_instance();
		return htmlspecialchars($data, ENT_QUOTES, $encoding);
	}

	/* encriptacion codeigniter */
        function inicializar($modo = "cbc") {
			// echo "<pre>".config_item('encryption_key')."</pre>";
            $this->CI->encryption->initialize(
                array(  'cipher'    =>  'aes-256', 
                        'mode'      =>  $modo,
                        'key'       =>  config_item('encryption_key')
                )
            );
        }

/* Encode Hand made * /
		function encode($plaintext, $plusiv = false) {
			$password = config_item('encryption_key');
			
			// Must be exact 32 chars (256 bit)
			$password = substr(hash('sha256', $password, true), 0, 32);
			
			// IV must be exact 16 chars (128 bit)
			$iv = chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1);
			
			// av3DYGLkwBsErphcyYp+imUW4QKs19hUnFyyYcXwURU=
			$encrypted = base64_encode(openssl_encrypt($plaintext, $this->method, $password, OPENSSL_RAW_DATA, $iv));
			
			if ($plusiv) {
				if ($this->logar) echo "<pre>";
				if ($this->logar) echo "falsa iv = ".print_r($iv, true)."<br/>";
				if ($this->logar) echo "pre encrypted = ".print_r($encrypted, true)."<br/>";
				$encrypted = $iv.$encrypted;
				if ($this->logar) echo "post concatenar = ".print_r($encrypted, true)."<br/>";
				$encrypted = base64_encode($encrypted);
				if ($this->logar) echo "post encrypted = ".print_r($encrypted, true)."<br/>";
				if ($this->logar) $this->decode($encrypted, true);
			}

			return $encrypted;
		}
/* */

/* Encode propio de CodeIgniter */
        function encode($obj) {
			// return base64_encode($this->CI->encryption->encrypt($obj));
			return $this->CI->encryption->encrypt($obj);
			
	    }
/* */

/* Decode Hand made */
        function oldfashoned_decode($encrypted, $plusiv = false) {
			$password = config_item('encryption_key');

			// IV must be exact 16 chars (128 bit)
			$iv = chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1) . chr(0x1);
			
			if ($plusiv) {
				if ($this->logar) echo "<pre>";
				if ($this->logar) echo "falsa iv = ".print_r($iv, true)."<br/>";
				if ($this->logar) echo "pre encrypted = ".print_r($encrypted, true)."<br/>";
				$encrypted = base64_decode($encrypted);
				if ($this->logar) echo "decoded encrypted = ".print_r($encrypted, true)."<br/>";
				
				$iv = substr($encrypted, 0, 16);
				if ($this->logar) echo "iv = ".print_r($iv, true)."<br/>";

				$encrypted = substr($encrypted, 16);
				if ($this->logar) echo "encrypted = ".print_r($encrypted, true)."<br/>";
				if ($this->logar) echo "</pre>";				
			}

			// Must be exact 32 chars (256 bit)
			$password = substr(hash('sha256', $password, true), 0, 32);
			
			// My secret message 1234
			$decrypted = openssl_decrypt(base64_decode($encrypted), $this->method, $password, OPENSSL_RAW_DATA, $iv);
			if ($this->logar) echo "decrypted = ".print_r($decrypted, true)."<br/>";
			
			return $decrypted;
	    }
/* */

/* Decode propio de CodeIgniter */
		function decode($obj, $tupadre = false) {
			return $this->CI->encryption->decrypt($obj);
		}
/* */





	    function encode_params_proforma($IDEmpresa, $IDGrupo, $Proforma_definitiva, $Version) {
	        $array_params = array(  "IDEmpresa" => $IDEmpresa, 
	                                "IDGrupo"   => $IDGrupo, 
	                                "Proforma"  => $Proforma_definitiva, 
	                                "Version"   => $Version);
	        $texto = json_encode($array_params);
	        return base64_encode($this->encode($texto));
	    }

	    function decode_params_proforma($texto) {
	        $texto = $this->CI->encryption->decrypt(base64_decode($texto));
	        return json_decode($texto);
	    }

		function encode_params_listado_pensiones($IDEmpresa, $IDGrupo, $Proforma, $Checkin, $Checkout, $Centro) {
			$array_params = array(  "IDEmpresa" => $IDEmpresa, 
									"IDGrupo"   => $IDGrupo, 
									"Proforma"  => $Proforma, 
									"Checkin"  	=> $Checkin, 
									"Checkout"  => $Checkout, 
									"Centro"   	=> $Centro);
			$texto = json_encode($array_params);
			return base64_encode($this->CI->encryption->encrypt($texto));
		}

		function decode_params_listado_pensiones($texto) {
			$texto = $this->CI->encryption->decrypt(base64_decode($texto));
			return json_decode($texto);
		}
	/* fin - encriptacion codeigniter*/

	function slugify($text) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	function sanitizar() {
		foreach ($_POST as $key => $value) {
			$logar = false;
			$log = "";
			$cadena = $value;
			$aguja = "";
			$whitelist = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890áàÁÀéèÉÈíìÍÌóòÓÒúùÚÙ,;.:-_<>Üü{}[]+-*/ºª\\!|\"@·#$~%€&()=?¿¡çÇ ";
			$blacklist = chr(-1).chr(128).chr(129).chr(130).chr(141).chr(177).chr(178);
			$cadena_res = "";

			if ($logar) echo "<pre>cadena: ".print_r($cadena, true)."</pre>";
			if ($logar) echo "<pre>whitelist: ".print_r($whitelist, true)."</pre>";
			if ($logar) echo "<pre>blacklist: ".print_r($blacklist, true)."</pre>";
			
			for ($x = 0; $x < mb_strlen($cadena); $x++) {
				$aguja = mb_substr( $cadena, $x, 1);

				if ((mb_strpos($whitelist, $aguja) !== false) && (!mb_strpos($blacklist, $aguja))) {
					$cadena_res .= $aguja;
				}
			}
			
			if ($logar) echo "<br/>RESULTADO >>> ".$cadena_res; 
			if ($logar) echo "<br/><br/><br/> PROCESO: <br/><br/><ul>$log</ul>";

			$_POST[$key] = $cadena_res;
		}
	}

}
?>