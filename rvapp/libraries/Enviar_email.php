<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enviar_email extends CI_Controller {

	protected $CI;
    protected $IDEmpresa = 0;
    protected $Empresa = "";
    protected $hacer_test=false;

	public function __construct() {
        global $CI;
    	$CI =& get_instance();
	}
	
	/*	
		p_vp 	= true/false * 	activar o desactivar la Vista Previa
		p_log 	= false			activar o desactivar los Logs de Errores
	*/

	public function enviar($datos, $contactos, $adjuntos = array(), $adjuntos_t = array(), $p_vp = true, $p_log = false) {
        global $CI;
        $CI->load->model("enviar_email_model"); 

        // ob_start("ob_gzhandler");
        if ($p_log == true) { error_reporting(E_ALL); ini_set('display_errors','On'); }

        $log = "";
        // echo "<pre>".$CI->PHPMailer->Username."</pre>"; 
        if ($p_vp==true) { // vista previa
		
            $log .= "VISTA PREVIA:<br/>";
            $log .= "FROM:<br/>";
            $log .= "Nombre: ".$datos->from_name."<br/>";
            $log .= "Email: ".$datos->from_mail."<br/>";
            $log .= "<br/>";
            $log .= "TO:<br/>";
            foreach ($contactos->to as $contacto) {
                $log .= "<li>".$contacto->nombre." < ".$contacto->email." ></li>";
            }

            $log .= "CC:<br/>";
            foreach ($contactos->cc as $contacto) {
                $log .= "<li>".$contacto->nombre." < ".$contacto->email." ></li>";
            }
            
            $log .= "BCC:<br/>";
            foreach ($contactos->bcc as $contacto) {
                $log .= "<li>".$contacto->nombre." < ".$contacto->email." ></li>";
            }
            $log .= "<br/>";

			$log .= "Titulo: ".$datos->titulo."<br/>";
			$log .= "Cuerpo: ".$datos->cuerpo."<br/>";
            return $log;            
			
		} else {

	        $CI->load->model("ficheros_email_model"); 

            // a ver donde pongo todo esto 
            $usuario_correo_nombre = $datos->from_name;
            $usuario_correo_email = $datos->from_mail;
            $usuario_correo_usuario = $datos->from_mail;
            $usuario_correo_password = base64_decode($datos->from_password);

            // fin - a ver donde pongo todo esto 
                $log = "";
                $log .= "<pre>ENVIANDO EL EMAIL</pre>";
                if ($p_log == true) $log .= "<pre>Cargamos la clase PHPMailer</pre>";
                if ($p_log == true) $log .= "<pre>usuario_correo_email ".$usuario_correo_email."</pre>";
                if ($p_log == true) $log .= "<pre>usuario_correo_usuario ".$usuario_correo_usuario."</pre>";
                if ($p_log == true) $log .= "<pre>usuario_correo_password ".$usuario_correo_password."</pre>";

      
            $CI->load->library('email');
            $CI->email->clear(true);

                if ($p_log == true) $log .= "<pre>Inicializamos las propiedades de correo.</pre>";
            $CI->email->initialize();

                if ($p_log == true) $log .= "<pre>titulo del mensaje: ".$datos->titulo."</pre>";        
            $CI->email->subject($datos->titulo);

                if ($p_log == true) $log .= "<pre>cuerpo del mensaje: ".$datos->cuerpo."</pre>";
            $CI->email->message($datos->cuerpo);
            $CI->email->set_alt_message($datos->titulo);            
            
                if ($p_log == true) $log .= "<pre>from y reply: ".$usuario_correo_email." - ".$usuario_correo_nombre."</pre>";
            $CI->email->from($usuario_correo_email, $usuario_correo_nombre);
            $CI->email->reply_to($usuario_correo_email, $usuario_correo_nombre);
            
        	// send copy to customer
                if ($p_log == true) $log .= "<pre>To: ".print_r($contactos->to, true)."</pre>";
            $contacto_to = "";
            $array_to = array();
            foreach ($contactos->to as $contacto) { // $CI->email->AddAddress($contacto->email, $contacto->nombre); 
                $array_to[] = $contacto->email;
                $contacto_to .= $contacto->nombre." <".$contacto->email.">, ";
            }
            $CI->email->to($array_to);
            
                if ($p_log == true) $log .= "<pre>Cc: ".print_r($contactos->cc, true)."</pre>";
            $contacto_cc = "";
            $array_cc = array();
            foreach ($contactos->cc as $contacto) { // $CI->email->AddCC($contacto->email, $contacto->nombre); 
                $array_cc[] = $contacto->email;                
                $contacto_cc .= $contacto->nombre." <".$contacto->email.">, ";
            }
			$CI->email->cc($array_cc);
            
                if ($p_log == true) $log .= "<pre>Bcc: ".print_r($contactos->bcc, true)."</pre>";
            $contacto_bcc = "";
            $array_bcc = array();
            $array_bcc[] = $usuario_correo_usuario;
            $contacto_bcc .= $usuario_correo_nombre." <$usuario_correo_usuario>, ";
            foreach ($contactos->bcc as $contacto) { // $CI->email->AddBCC($contacto->email, $contacto->nombre); 
                $array_bcc[] = $contacto->email;                
                $contacto_bcc .= $contacto->nombre." <".$contacto->email.">, ";
            }
            $CI->email->bcc($array_bcc);
            
                if ($p_log == true) $log .= "<pre>Adjuntos: ".print_r($adjuntos, true)."</pre>";
            foreach ($adjuntos as $adjunto) {
                $CI->email->attach($adjunto);
            } // $this->email->attach() 

                if ($p_log == true) $log .= "<pre>Vamos a enviar el mail . . .</pre>";
    
            try {
                $resultado = $CI->email->send();
                $log .= "<pre>".print_r($resultado, true)."</pre>";
                
                if(!$resultado) {
                    $titulo_resultado = "Envio ERRONEO: ".$CI->email->print_debugger();
                    $log .= "<pre class='proceso'>".$titulo_resultado."</pre>";
                    $texto_error = $CI->email->print_debugger();
                    $estado_error = 0;
                } else {
                    $titulo_resultado = "Envio CORRECTO!";
                    $log .= "<pre>".$titulo_resultado."</pre>";
                    $texto_error = $CI->email->print_debugger();
                    // $texto_error = "";
                    $estado_error = 1;

					$objeto = $CI->session->userdata('logged_in');
					$idusuario = $objeto['id'];

                    /* borrado adjuntos temporales */
					/* foreach ($adjuntos_t as $adjunto) { */
                        /* $CI->ficheros_email_model->borrar_fichero_temp($adjunto, $idusuario); */
                    $log .= $CI->ficheros_email_model->borrar_ficheros_temp($idusuario);
					/* } // $this->email->attach() */
                }
            } catch (Exception $e) {
                $titulo_resultado = "Envio ERRONEO: ".$e->getMessage();
                $log .= "<pre class='excepcion'>".$titulo_resultado."</pre>";
                $texto_error = $e->getMessage();
                $estado_error = 0;            
            }
            
            // sanitizamos minimamente 
            $datos->contacto_to = $contacto_to;
            $datos->contacto_cc = $contacto_cc;
            $datos->contacto_bcc = $contacto_bcc;
            $datos->estado_error = $estado_error;
            $datos->texto_error = $texto_error;
            $datos->IDEmpresa = $this->IDEmpresa;
            $CI->enviar_email_model->guardar_comunicacion($datos);

            $vista = array();
            $vista += array("cuerpo_mail" => $datos->cuerpo );
            $vista += array("error" => $texto_error);
            if ($p_log == true) $vista += array("log" => $log);

            $data_header  = array();
            $data_header += array('title' => $titulo_resultado);

            $texto = "";
            /*$texto .= $CI->load->view('templates/header', $data_header, TRUE);*/

            if ($estado_error == 0) { $plantilla = "preparar_email/email_ko_view"; } // ha habido un error 
            else { $plantilla = "preparar_email/email_ok_view"; } // email enviado correctamente 

            $texto .= $CI->load->view($plantilla, $vista, TRUE);
            /*
            $texto .= $CI->load->view('templates/footer', null, TRUE);
            */
            return $texto;
        }

        // aqui le decimos que ya puede vomitar todo el html que ha ido metiendo en el buffer
        // ob_flush();
    }

    public function enviar_externo($url, $tv, $tr) {
        try {
            $test = "";
            if ($this->hacer_test == true) {
                $test="_test";
            }
            if ($tv == 1) {
                // usamos el motor de Blahblah
                $urlmotor = "https://www.blahblah.es/reservaonline".$test."/";
            } else {
                if ($tr == 1) {
                    // usamos el motor de Colonias
                    $urlmotor = "https://www.rosadelsvents.es/reservaonline".$test."/";
                } else {
                    // usamos el motor de Idiomas
                    $urlmotor = "https://www.rosadelsventsidiomas.es/reservaonline".$test."/";
                }
            }

            // Initialize session and set URL.
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $urlmotor.$url);

            // Set so curl_exec returns the result instead of outputting it.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);

            // Get the response and close the channel.
            $response = curl_exec($ch);

            curl_close($ch);
        } catch (Exception $e) {
            log_message("error", "ERROR Enviando Mail: ".$e->getMessage());
        }
    }  
}
?>