<?php 
    if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

    class Inscripcion_idiomas_model extends CI_Model {

        protected $logar        = false;

        // 20180207 - pasamos de 7 dias antes del checkin a 30 dias mas tarde del processed, y con fecha tope del 10 de julio
        // 20180611 - Rosa cambia a 4 dias mas...
        // 20180615 - Rosa pide pongamos el 15 de agosto, para obviar esta proteccion.
        protected $limite       = 30;
        protected $fechatope    = "2018-06-10 00:00:00";
        // 20180615 / fin

        protected $splitter     = "|";

        protected $order        = "";
        protected $hostel       = "";
        protected $procesarlog  = "";

        function __construct(){
            parent::__construct();
        } 

        function isset_Inscripcio($i_idinscripcio, $i_people, $i_control) {   
            $nom = "";
            
            $sql = "SELECT 
                        nombre_participante, apellidos_participante 
                    FROM
                    inscripciones_idiomas
                    WHERE 
                    id_inscripcion = '$i_idinscripcio' and
                    idpeople = '$i_people' and
                    control = '$i_control'";
            
            $query = $this->db->query($sql);

            $datos = (array)$query->result();
            
            if (count($datos) > 0) {
                $nom  =       $this->soporte_xss->decode($datos[0]->nombre_participante);
                $nom .= " ".  $this->soporte_xss->decode($datos[0]->apellidos_participante);
            }

            return $nom;
        }

        function getIsZurich($i_idinscripcio) {   
            /*    
                $sql = "SELECT  t.zurich
                        FROM inscripciones i
                            inner join hostels_tandes t
                                on t.idhotels_tandes = i.idtanda
                        WHERE i.idinscripciones = '".$i_idinscripcio."'"; //
                
                $query = mysql_query($sql);
                $res = mysql_fetch_row($query);
                return $res[0];
            */ 
            return 0;      
        }

        function comprovar_Inscripcio($i_idinscripcio, $i_localizador, $i_dni, $i_centro) {
            $nom = "";

            $sql = "SELECT 
                        nombre_participante, apellidos_participante, numero_reserva_localizador, centro
                    FROM 
                        inscripciones_idiomas 
                    WHERE 
                    id_inscripcion   = '$i_idinscripcio' and 
                    idpeople         = '$i_people' and 
                    dni              = '$dni' and 
                    control          = '$i_control'";
            
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            if (count($datos) > 0) {
                $nom  = $this->soporte_xss->decode($datos[0]->nombre_participante);
                $nom .= " ". $this->soporte_xss->decode($datos[0]->apellidos_participante);
                $_SESSION['hostel']  =  ($datos[0]->centro);
                $_SESSION['order_number']  =  ($datos[0]->numero_reserva_localizador);
            }

            return $nom;
        }
        
        /*
            function isset_LevelTest($i_idinscripcio)
            {   global $p_catalogo;
                $resultat=0;
                $sql = " SELECT "; //
                $sql.= "    count(`idinscripcion`)"; 
                $sql.= " FROM pruevanivelingles_idiomas "; 
                $sql.= " WHERE idinscripcion = '".$i_idinscripcio."'";

                $resIsSet = mysql_query($sql);
                return mysql_fetch_row($resIsSet);
            } // isset_LevelTest
        */

        function isValidInscripcio($i_idinscripcio, $i_people, $i_control) {
            $sql = "SELECT 
                    inscripciones_idiomas.estado_enviable,
                    hostels_tandes.tipo_reserva
                    FROM
                    inscripciones_idiomas
                        inner join hostels_tandes
                            on inscripciones_idiomas.idtanda = hostels_tandes.idhotels_tandes
                    WHERE 
                    inscripciones_idiomas.id_inscripcion = '$i_idinscripcio' and
                    inscripciones_idiomas.idpeople = '$i_people' and
                    inscripciones_idiomas.control = '$i_control'";

            $query = $this->db->query($sql);
            
            return $query->result();
        } 

        function get_DadesInscripcio($i_idinscripcio, $i_people, $i_control) {   
            $sql = "SELECT 
                    inscripciones_idiomas.numero_reserva_localizador as NReserva,
                    inscripciones_idiomas.centro,
                    reservations.hostel
                    FROM
                        inscripciones_idiomas
                        inner join  reservations
                            on inscripciones_idiomas.numero_reserva_localizador = reservations.order_number
                    WHERE 
                    id_inscripcion = '$i_idinscripcio' and
                    idpeople = '$i_people' and
                    control = '$i_control'";

            $query = $this->db->query($sql);

            return $query->result();
        } 
        
        function set_Inscripcio($i_ejercicio, $i_codigo_empresa, $i_codigo_departamento,  $i_numero_reserva_localizador,  $i_forfait,  $i_dni, $i_estado_gesreser,  $i_estado_enviable,  $i_nombre_amigo,  $i_amics_familiars,  $i_barna_madrid,  $i_nombre_participante,  $i_apellidos_participante, $i_chico_chica,  $i_direccion_participante, $i_codigo_postal,  $i_poblacion,  $i_telefono,  $i_movil,  $i_fecha_nacimiento,  $i_edad_participante,  $i_pasaporte,  $i_nacionalidad,  $i_email_participantes,  $i_movil_padres,  $i_nombre_padres,  $i_dni_padres,  $i_algun_curso,  $i_algun_curso_detalle,  $i_alguna_enfermedad,  $i_alguna_enfermedad_detalle, $i_dieta_especial,  $i_dieta_especial_detalle,  $i_algun_medicamento,  $i_algun_medicamento_detalle,  $i_colegio,  $i_direccion_colegio,  $i_cp_colegio,  $i_poblacion_colegio,  $i_nombre_profesor,  $i_curso,  $i_nivel_idioma,  $i_nombre_autorizacion_padre,  $i_dni_autorizacion_padres,  $i_autoritzacio_acceptacio_monitor, $i_nombre_autorizacion_padre_medica, $i_dni_autorizacion_padres_medica,  $i_autoritzacio_acceptacio_medica, $i_observaciones,  $i_conocido_por, $i_otros, $i_normativa_acceptacio, $i_foto, $i_mini_foto, $i_copia_pasaporte,  $i_mini_pasaporte, $i_data_in, $i_data_out, $i_algun_curso_detalle2) {
            
            $datos = array(
                "ejercicio" =>   $i_ejercicio,
                "codigo_empresa"    =>   $i_codigo_empresa,
                "codigo_departamento"   =>   $i_codigo_departamento,
                "numero_reserva_localizador"    =>   $i_numero_reserva_localizador,
                "forfait"   =>   $i_forfait,
                "dni"   =>   $this->soporte_xss->encode($i_dni),
                "estado_gesreser"   =>   $i_estado_gesreser,
                "estado_enviable"   =>   $i_estado_enviable,
                "nombre_amigo"  =>   $this->soporte_xss->encode($i_nombre_amigo),
                "amics_familiars"   =>   $i_amics_familiars,
                "barna_madrid"  =>   $i_barna_madrid,
                "nombre_participante"   =>   $this->soporte_xss->encode($i_nombre_participante),
                "apellidos_participante"    =>   $this->soporte_xss->encode($i_apellidos_participante),
                "chico_chica"   =>   $i_chico_chica,
                "direccion_participante"    =>   $this->soporte_xss->encode($i_direccion_participante),
                "codigo_postal" =>   $this->soporte_xss->encode($i_codigo_postal),
                "poblacion" =>   $this->soporte_xss->encode($i_poblacion),
                "telefono"  =>   $this->soporte_xss->encode($i_telefono),
                "movil" =>   $this->soporte_xss->encode($i_movil),
                "fecha_nacimiento"  =>   prepararData($i_fecha_nacimiento, "/", "-"),
                "edad_participante" =>   $this->soporte_xss->encode($i_edad_participante),
                "pasaporte" =>   $i_pasaporte,
                "nacionalidad"  =>   $this->soporte_xss->encode($i_nacionalidad),
                "email_participantes"   =>   $this->soporte_xss->encode($i_email_participantes),
                "movil_padres"  =>   $this->soporte_xss->encode($i_movil_padres),
                "nombre_padres" =>   $this->soporte_xss->encode($i_nombre_padres),
                "dni_padres"    =>   $this->soporte_xss->encode($i_dni_padres),
                "algun_curso"   =>   $i_algun_curso,
                "algun_curso_detalle"   =>   $this->soporte_xss->encode($i_algun_curso_detalle),
                "alguna_enfermedad" =>   $this->soporte_xss->encode($i_alguna_enfermedad),
                "alguna_enfermedad_detalle"     =>   $this->soporte_xss->encode($i_alguna_enfermedad_detalle),
                "dieta_especial"    =>   $i_dieta_especial,
                "dieta_especial_detalle"    =>   $this->soporte_xss->encode($i_dieta_especial_detalle),
                "algun_medicamento" =>   $i_algun_medicamento,
                "algun_medicamento_detalle" =>   $this->soporte_xss->encode($i_algun_medicamento_detalle),
                "colegio"   =>   $i_colegio,
                "direccion_colegio" =>   $this->soporte_xss->encode($i_direccion_colegio),
                "cp_colegio"    =>   $this->soporte_xss->encode($i_cp_colegio),
                "poblacion_colegio" =>   $this->soporte_xss->encode($i_poblacion_colegio),
                "nombre_profesor"   =>   $this->soporte_xss->encode($i_nombre_profesor),
                "curso" =>   $this->soporte_xss->encode($i_curso),
                "nivel_idioma"  =>   $i_nivel_idioma,
                "nombre_autorizacion_padre" =>   $this->soporte_xss->encode($i_nombre_autorizacion_padre),
                "dni_autorizacion_padres"   =>   $this->soporte_xss->encode($i_dni_autorizacion_padres),
                "autoritzacio_acceptacio_monitor"   =>   $i_autoritzacio_acceptacio_monitor,
                "nombre_autorizacion_padre_medica"  =>   $this->soporte_xss->encode($i_nombre_autorizacion_padre_medica),
                "dni_autorizacion_padres_medica"    =>   $this->soporte_xss->encode($i_dni_autorizacion_padres_medica),
                "autoritzacio_acceptacio_medica"    =>   $i_autoritzacio_acceptacio_medica,
                "observaciones" =>   $this->soporte_xss->encode($i_observaciones),
                "conocido_por"  =>   $i_conocido_por,
                "otros" =>   $this->soporte_xss->encode($i_otros),
                "normativa_acceptacio"  =>   $i_normativa_acceptacio,
                "foto"  =>   $this->soporte_xss->encode($i_foto),
                "mini_foto" =>   $this->soporte_xss->encode($i_mini_foto),
                "copia_pasaporte"   =>   $this->soporte_xss->encode($i_copia_pasaporte),
                "mini_pasaporte"    =>   $this->soporte_xss->encode($i_mini_pasaporte),
                "data_in"   =>   prepararData($i_data_in, "/", "-"),
                "data_out"  =>   prepararData($i_data_out, "/", "-"),
                "algun_curso_detalle2"  =>  $this->soporte_xss->encode($i_algun_curso_detalle2)
            );

            $this->db->set($datos);
            $this->db->insert("inscripciones_idiomas");
    
            return $this->db->insert_id(); 
        } 

        function fecha_checkin($i_idinscripcio, $i_forzar = false) {
            /* 
                20190103 - han pedido quitar el control de acceso y solo avisar de la fecha tope en el mail.
                    20180301 - cambiamos solo a fecha limite a peticion de rosa
                    //  20180207 - cambiamos checkin por processed a peticion de rosa
                    //  (datediff(now(), r.processed) > $limite) as acceso
                    $sql = "SELECT 
                                if (    if(now() > '".$this->fechatope."', false, true), 
                                        (datediff(now(), r.processed) <= ".$this->limite."), 
                                        false) as acceso
                            FROM reservations as r
                                LEFT JOIN inscripciones_idiomas as i
                                    ON r.order_number = i.numero_reserva_localizador
                            WHERE
                                r.tipo_reserva = '2' and 
                                i.id_inscripcion = '".$i_idinscripcio."'";
                $sql = "SELECT 
                            if((now() > '".$this->fechatope."') and (".$i_forzar." <> 1), false, true) as acceso
                        FROM reservations as r
                            LEFT JOIN inscripciones_idiomas as i
                                ON r.order_number = i.numero_reserva_localizador
                        WHERE
                            r.tipo_reserva = '2' and 
                            i.id_inscripcion = '".$i_idinscripcio."'";
                log_message("error", $sql);
                $query = $this->db->query($sql);
                $datos = $query->result();

                $returnAccess=0;
                if (count($datos) > 0) {
                    $returnAccess = $datos[0]->acceso;
                }
                return $returnAccess;       
            */
            return true;
        }

        function provaNivellJaRealitzada($i_idinscripcio) {
        
            $sql = " SELECT 
                    idinscripcion 
                    FROM pruevanivelingles_idiomas 
                    WHERE
                    idinscripcion = '$i_idinscripcio'";
            
            $query = $this->db->query($sql);
            $datos = $query->result();

            $returnid=0;
            if (count($datos) > 0) {
                $returnid = $datos[0]->idinscripcion;
            }
            return $returnid;
        }
        /*
        function set_provaNivell($i_idinscripcio, $i_NSNC, $i_array) {
            $datos = array(
                            "idinscripcion" =>  $i_idinscripcio,
                            "NSNC"          =>  $i_NSNC
            );

            $array = explode($splitter,$i_array);
            
            for ($i = 0; $i < count($array); $i++) {
                $numero =str_pad(($i+1), 2, "0", STR_PAD_LEFT);
                $datos["p".$numero] = $array[$i];
            }
                
            $this->db->set($datos);
            $this->db->insert("pruevanivelingles_idiomas");

            return $this->db->insert_id(); 
        }
        */
        function set_provaNivell($i_idinscripcio, $npreguntas) {
            $datos = array( "idinscripcion" =>  $i_idinscripcio );

            if (isset($_POST["NSNC"])) {
                $datos["NSNC"] =  $_POST["NSNC"];
            } else {
                for ($pos=0; $pos < $npreguntas; $pos++) { 
                    $numero =str_pad(($pos+1), 2, "0", STR_PAD_LEFT);
                    $datos["p".$numero] = $_POST["respuesta".$pos];
                }
            }
                
            $this->db->set($datos);
            $this->db->insert("pruevanivelingles_idiomas");

            return $this->db->insert_id(); 
        }

        function end_Inscripcio(  $i_idinscripcio, $i_email, $i_estado_gesreser, $i_estado_enviar, $i_idpeople, $i_clavecontrol ) {
            
            $where = "id_inscripcion = '$i_idinscripcio' and idpeople = '$i_idpeople' and control = '$i_clavecontrol'";

            $datos = array( "email_confirmacion"    => $i_email,
                            "estado_enviable"       => 1,
                            "estado_gesreser"       => 0
                        );
            
            $this->db->where($where);
            $this->db->update("inscripciones_idiomas", $datos);
        }

        function update_Inscripcio($i_id_inscripcion /* ,  $i_estado_gesreser,  $i_estado_enviable /*,  $i_nombre_amigo,  $i_amics_familiars, $i_nombre_participante,  $i_apellidos_participante, $i_chico_chica,  $i_direccion_participante, $i_codigo_postal,  $i_poblacion,  $i_telefono,  $i_movil,  $i_fecha_nacimiento,  $i_edad_participante,  $i_pasaporte,  $i_nacionalidad,  $i_email_participantes,  $i_movil_padres,  $i_nombre_padres,  $i_dni_padres,  $i_algun_curso,  $i_algun_curso_detalle,  $i_alguna_enfermedad,  $i_alguna_enfermedad_detalle, $i_dieta_especial,  $i_dieta_especial_detalle,  $i_algun_medicamento,  $i_algun_medicamento_detalle,  $i_colegio,  $i_nombre_profesor,  $i_curso,  $i_nivel_idioma, $i_nombre_autorizacion_padre,  $i_dni_autorizacion_padres,  $i_autoritzacio_acceptacio_monitor, $i_nombre_autorizacion_padre_medica, $i_dni_autorizacion_padres_medica,  $i_autoritzacio_acceptacio_medica, $i_observaciones,  $i_conocido_por, $i_otros, $i_normativa_acceptacio, $i_copia_pasaporte,  $i_mini_pasaporte, $i_idpeople, $i_clavecontrol, $i_amigos_juntos, $i_colegio_miniestancia, $i_autoritzacio_acceptacio_audiovisual, $i_algun_curso_detalle2, $i_email_padres, $i_movil_participantes, $i_alguna_alergia, $i_alguna_alergia_detalle*/) {
            $i_estado_gesreser = 0; 
            $i_estado_enviable = 1;

            // echo "<pre>post ". print_r($_POST,true)."</pre>";

            $datos = array( "estado_gesreser"                       => $i_estado_gesreser,
                            "estado_enviable"                       => $i_estado_enviable,
                            "nombre_amigo"                          => $this->soporte_xss->encode(set_value("nombre_amigo",(isset($_POST["nombre_amigo"])?$_POST["nombre_amigo"]:""))),
                            "amics_familiars"                       => set_value("amics_familiars",(isset($_POST["amics_familiars"])?$_POST["amics_familiars"]:"0")),
                            "nombre_participante"                   => $this->soporte_xss->encode(set_value("nombre_participante",(isset($_POST["nombre_participante"])?$_POST["nombre_participante"]:""))),
                            "apellidos_participante"                => $this->soporte_xss->encode(set_value("apellidos_participante",(isset($_POST["apellidos_participante"])?$_POST["apellidos_participante"]:""))),
                            "chico_chica"                           => set_value("chico_chica",(isset($_POST["chico_chica"])?$_POST["chico_chica"]:"0")),
                            "direccion_participante"                => $this->soporte_xss->encode(set_value("direccion_participante",(isset($_POST["direccion_participante"])?$_POST["direccion_participante"]:""))),
                            "codigo_postal"                         => $this->soporte_xss->encode(set_value("codigo_postal",(isset($_POST["codigo_postal"])?$_POST["codigo_postal"]:""))),
                            "poblacion"                             => $this->soporte_xss->encode(set_value("poblacion",(isset($_POST["poblacion"])?$_POST["poblacion"]:""))),
                            "telefono"                              => $this->soporte_xss->encode(set_value("telefono",(isset($_POST["telefono"])?$_POST["telefono"]:""))),
                            "movil"                                 => $this->soporte_xss->encode(set_value("movil",(isset($_POST["movil"])?$_POST["movil"]:""))),
                            "fecha_nacimiento"                      => $this->prepararData(set_value("fecha_nacimiento",(isset($_POST["fecha_nacimiento"])?$_POST["fecha_nacimiento"]:"01/01/1800")), "/", "-"),
                            "edad_participante"                     => $this->soporte_xss->encode(set_value("edad_participante",(isset($_POST["edad_participante"])?$_POST["edad_participante"]:"0"))),
                            "pasaporte"                             => set_value("pasaporte",(isset($_POST["pasaporte"])?$_POST["pasaporte"]:"0")),
                            "nacionalidad"                          => $this->soporte_xss->encode(set_value("nacionalidad",(isset($_POST["nacionalidad"])?$_POST["nacionalidad"]:""))),
                            "email_participantes"                   => $this->soporte_xss->encode(set_value("email_participantes",(isset($_POST["email_participantes"])?$_POST["email_participantes"]:""))),
                            "movil_padres"                          => $this->soporte_xss->encode(set_value("movil_padres",(isset($_POST["movil_padres"])?$_POST["movil_padres"]:""))),
                            "nombre_padres"                         => $this->soporte_xss->encode(set_value("nombre_padres",(isset($_POST["nombre_padres"])?$_POST["nombre_padres"]:""))),
                            "dni_padres"                            => $this->soporte_xss->encode(set_value("dni_padres",(isset($_POST["dni_padres"])?$_POST["dni_padres"]:""))),
                            "algun_curso"                           => set_value("algun_curso",(isset($_POST["algun_curso"])?$_POST["algun_curso"]:"0")),
                            "algun_curso_detalle"                   => $this->soporte_xss->encode(set_value("algun_curso_detalle",(isset($_POST["algun_curso_detalle"])?$_POST["algun_curso_detalle"]:""))),
                            "alguna_enfermedad"                     => set_value("alguna_enfermedad",(isset($_POST["alguna_enfermedad"])?$_POST["alguna_enfermedad"]:"0")),
                            "alguna_enfermedad_detalle"             => $this->soporte_xss->encode(set_value("alguna_enfermedad_detalle",(isset($_POST["alguna_enfermedad_detalle"])?$_POST["alguna_enfermedad_detalle"]:""))),
                            "dieta_especial"                        => set_value("dieta_especial",(isset($_POST["dieta_especial"])?$_POST["dieta_especial"]:"0")),
                            "dieta_especial_detalle"                => $this->soporte_xss->encode(set_value("dieta_especial_detalle",(isset($_POST["dieta_especial_detalle"])?$_POST["dieta_especial_detalle"]:""))),
                            "algun_medicamento"                     => set_value("algun_medicamento",(isset($_POST["algun_medicamento"])?$_POST["algun_medicamento"]:"0")),
                            "algun_medicamento_detalle"             => $this->soporte_xss->encode(set_value("algun_medicamento_detalle",(isset($_POST["algun_medicamento_detalle"])?$_POST["algun_medicamento_detalle"]:""))),
                            "colegio"                               => $this->soporte_xss->encode(set_value("colegio",(isset($_POST["colegio"])?$_POST["colegio"]:""))),
                            "nombre_profesor"                       => $this->soporte_xss->encode(set_value("nombre_profesor",(isset($_POST["nombre_profesor"])?$_POST["nombre_profesor"]:""))),
                            "curso"                                 => $this->soporte_xss->encode(set_value("curso",(isset($_POST["curso"])?$_POST["curso"]:""))),
                            "nivel_idioma"                          => set_value("nivel_idioma",(isset($_POST["nivel_idioma"])?$_POST["nivel_idioma"]:"0")),
                            "nombre_autorizacion_padre"             => $this->soporte_xss->encode(set_value("nombre_autorizacion_padre",(isset($_POST["nombre_autorizacion_padre"])?$_POST["nombre_autorizacion_padre"]:""))),
                            "dni_autorizacion_padres"               => $this->soporte_xss->encode(set_value("dni_autorizacion_padres",(isset($_POST["dni_autorizacion_padres"])?$_POST["dni_autorizacion_padres"]:""))),
                            "autoritzacio_acceptacio_monitor"       => set_value("autoritzacio_acceptacio_monitor",(isset($_POST["autoritzacio_acceptacio_monitor"])?$_POST["autoritzacio_acceptacio_monitor"]:"")),
                            "nombre_autorizacion_padre_medica"      => $this->soporte_xss->encode(set_value("nombre_autorizacion_padre_medica",(isset($_POST["nombre_autorizacion_padre_medica"])?$_POST["nombre_autorizacion_padre_medica"]:""))),
                            "dni_autorizacion_padres_medica"        => $this->soporte_xss->encode(set_value("dni_autorizacion_padres_medica",(isset($_POST["dni_autorizacion_padres_medica"])?$_POST["dni_autorizacion_padres_medica"]:""))),
                            "autoritzacio_acceptacio_medica"        => set_value("autoritzacio_acceptacio_medica",(isset($_POST["autoritzacio_acceptacio_medica"])?$_POST["autoritzacio_acceptacio_medica"]:"")),
                            "observaciones"                         => $this->soporte_xss->encode(set_value("observaciones",(isset($_POST["observaciones"])?$_POST["observaciones"]:""))),
                            "otras_consideraciones"                 => $this->soporte_xss->encode(set_value("otras_consideraciones",(isset($_POST["otras_consideraciones"])?$_POST["otras_consideraciones"]:""))),
                            "conocido_por"                          => set_value("conocido_por",(isset($_POST["conocido_por"])?$_POST["conocido_por"]:"0")),
                            "otros"                                 => $this->soporte_xss->encode(set_value("otros",(isset($_POST["otros"])?$_POST["otros"]:""))),
                            "normativa_acceptacio"                  => set_value("normativa_acceptacio",(isset($_POST["normativa_acceptacio"])?$_POST["normativa_acceptacio"]:"0")),
                            "amigos_juntos"                         => set_value("amigos_juntos",(isset($_POST["amigos_juntos"])?$_POST["amigos_juntos"]:"0")),
                            "colegio_miniestancia"                  => set_value("colegio_miniestancia",(isset($_POST["colegio_miniestancia"])?$_POST["colegio_miniestancia"]:"0")),
                            "f_modificacio"                         => date("Y-m-d H:i:s"),
                            "autoritzacio_acceptacio_audiovisual"   => set_value("autoritzacio_acceptacio_audiovisual",(isset($_POST["autoritzacio_acceptacio_audiovisual"])?$_POST["autoritzacio_acceptacio_audiovisual"]:"0")),    
                            "algun_curso_detalle2"                  => $this->soporte_xss->encode(set_value("algun_curso_detalle2",(isset($_POST["algun_curso_detalle2"])?$_POST["algun_curso_detalle2"]:""))),
                            "email_padres"                          => $this->soporte_xss->encode(set_value("email_padres",(isset($_POST["email_padres"])?$_POST["email_padres"]:""))),
                            "movil_participantes"                   => $this->soporte_xss->encode(set_value("movil_participantes",(isset($_POST["movil_participantes"])?$_POST["movil_participantes"]:""))),
                            "alguna_alergia"                        => set_value("alguna_alergia",(isset($_POST["alguna_alergia"])?$_POST["alguna_alergia"]:"0")),
                            "alguna_alergia_detalle"                => $this->soporte_xss->encode(set_value("alguna_alergia_detalle",(isset($_POST["alguna_alergia_detalle"])?$_POST["alguna_alergia_detalle"]:""))),
                            "enc"                                   => 1
            );
            $where = "id_inscripcion = '".$i_id_inscripcion."'";

            $this->db->where($where);
            $this->db->update("inscripciones_idiomas", $datos);
        } 

        function get_Inscripcio($i_idinscripcio, $i_idpeople, $i_clavecontrol, $i_forzar = false) {
            global $p_catalogo;
            $resultat=0;            
                
            $sql = " SELECT 
                    i.id_inscripcion,  
                    i.ejercicio, 
                    i.codigo_empresa,  
                    i.codigo_departamento,  
                    i.numero_reserva_localizador,  
                    i.forfait,  
                    i.dni,  
                    i.estado_gesreser,  
                    i.estado_enviable,  
                    i.nombre_amigo,  
                    i.amics_familiars,  
                    i.nombre_participante,  
                    i.apellidos_participante,  
                    i.chico_chica,  
                    i.direccion_participante,  
                    i.codigo_postal,  
                    i.poblacion,  
                    i.telefono,  
                    i.movil,  
                    i.fecha_nacimiento,  
                    i.edad_participante,  
                    i.pasaporte,  
                    i.nacionalidad,  
                    i.email_participantes,  
                    i.movil_padres,  
                    i.nombre_padres,  
                    i.dni_padres,  
                    i.algun_curso,  
                    i.algun_curso_detalle,  
                    i.alguna_enfermedad,  
                    i.alguna_enfermedad_detalle, 
                    i.dieta_especial,  
                    i.dieta_especial_detalle,  
                    i.algun_medicamento,  
                    i.algun_medicamento_detalle,  
                    i.colegio,  
                    i.direccion_colegio,  
                    i.cp_colegio,  
                    i.poblacion_colegio,  
                    i.nombre_profesor,  
                    i.curso,  
                    i.nivel_idioma,  
                    i.nombre_autorizacion_padre,  
                    i.dni_autorizacion_padres,  
                    i.autoritzacio_acceptacio_monitor, 
                    i.nombre_autorizacion_padre_medica, 
                    i.dni_autorizacion_padres_medica,  
                    i.autoritzacio_acceptacio_medica,  
                    i.observaciones,  
                    i.otras_consideraciones,
                    i.conocido_por,  
                    i.otros,  
                    i.normativa_acceptacio,  
                    i.copia_pasaporte,  
                    i.mini_pasaporte,  
                    i.data_in,  
                    i.data_out, 
                    i.centro, 
                    i.idtanda, 
                    i.amigos_juntos, 
                    i.colegio_miniestancia, 
                    if (t.edad_max = 99 , 1970 , year(now()) - t.edad_max) as ano_minimo, 
                    year(now()) - t.edad_min as ano_maximo,     
                    i.autoritzacio_acceptacio_audiovisual,         
                    i.algun_curso_detalle2,         
                    i.email_padres,         
                    i.movil_participantes,         
                    i.alguna_alergia,         
                    i.alguna_alergia_detalle  
                FROM 
                    inscripciones_idiomas i 
                        INNER JOIN hostels_tandes t 
                            ON i.idtanda = t.idhotels_tandes 
                WHERE 
                    id_inscripcion = '$i_idinscripcio' and
                    idpeople = '$i_idpeople' and 
                    control = '$i_clavecontrol'";

                $query = $this->db->query($sql);

                if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > sql: ".print_r($sql, true)."<br/>"; }

                $datos = $query->result();

                if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > datos: ".print_r($datos, true)."<br/>"; }
            
                $entrar = false;
                if ( count($datos) <= 0 ) {
                    // si no hay datos devolvemos -1
                    return $entrar;

                } else {
                    if ( $i_forzar == true ) {
                        // Tenemos datos y forzamos a mostrar, asi que continuaremos con la ejecucion
                        $entrar = true;

                    } else {

                        if ( $datos[0]->estado_enviable == 0 ) {
                            // como no ha sido realizada permitimos entrar, asi que continuamos la ejecucion.
                            $entrar = true;
                        } else {
                            // como ya ha sido realizada y no forzamos, devolvemos 0
                            return 0;
                        }
                    }
                }

                if  ( $entrar == true ) {
                    $datos[0]->dni							    = $this->soporte_xss->decode($datos[0]->dni);
                    $datos[0]->nombre_amigo					    = $this->soporte_xss->decode($datos[0]->nombre_amigo);
                    $datos[0]->nombre_participante				= $this->soporte_xss->decode($datos[0]->nombre_participante);
                    $datos[0]->apellidos_participante			= $this->soporte_xss->decode($datos[0]->apellidos_participante);
                    $datos[0]->direccion_participante			= $this->soporte_xss->decode($datos[0]->direccion_participante);
                    $datos[0]->codigo_postal					= $this->soporte_xss->decode($datos[0]->codigo_postal);
                    $datos[0]->poblacion		    			= $this->soporte_xss->decode($datos[0]->poblacion);
                    $datos[0]->telefono			     	    	= $this->soporte_xss->decode($datos[0]->telefono);
                    $datos[0]->movil			        		= $this->soporte_xss->decode($datos[0]->movil);
                    $datos[0]->edad_participante				= $this->soporte_xss->decode($datos[0]->edad_participante);
                    $datos[0]->nacionalidad				    	= $this->soporte_xss->decode($datos[0]->nacionalidad);
                    $datos[0]->email_participantes				= $this->soporte_xss->decode($datos[0]->email_participantes);
                    $datos[0]->nombre_padres					= $this->soporte_xss->decode($datos[0]->nombre_padres);
                    $datos[0]->movil_participantes				= $this->soporte_xss->decode($datos[0]->movil_participantes);
                    $datos[0]->movil_padres			    		= $this->soporte_xss->decode($datos[0]->movil_padres);
                    $datos[0]->email_padres			    		= $this->soporte_xss->decode($datos[0]->email_padres);
                    $datos[0]->dni_padres			    		= $this->soporte_xss->decode($datos[0]->dni_padres);
                    $datos[0]->algun_curso_detalle				= $this->soporte_xss->decode($datos[0]->algun_curso_detalle);
                    $datos[0]->algun_curso_detalle2				= $this->soporte_xss->decode($datos[0]->algun_curso_detalle2);
                    $datos[0]->alguna_enfermedad_detalle		= $this->soporte_xss->decode($datos[0]->alguna_enfermedad_detalle);
                    $datos[0]->dieta_especial_detalle			= $this->soporte_xss->decode($datos[0]->dieta_especial_detalle);
                    $datos[0]->algun_medicamento_detalle		= $this->soporte_xss->decode($datos[0]->algun_medicamento_detalle);
                    $datos[0]->alguna_alergia_detalle			= $this->soporte_xss->decode($datos[0]->alguna_alergia_detalle);
                    $datos[0]->colegio			        		= $this->soporte_xss->decode($datos[0]->colegio);
                    $datos[0]->direccion_colegio				= $this->soporte_xss->decode($datos[0]->direccion_colegio);
                    $datos[0]->cp_colegio			    		= $this->soporte_xss->decode($datos[0]->cp_colegio);
                    $datos[0]->poblacion_colegio				= $this->soporte_xss->decode($datos[0]->poblacion_colegio);
                    $datos[0]->nombre_profesor					= $this->soporte_xss->decode($datos[0]->nombre_profesor);
                    $datos[0]->curso			              	= $this->soporte_xss->decode($datos[0]->curso);
                    $datos[0]->nombre_autorizacion_padre		= $this->soporte_xss->decode($datos[0]->nombre_autorizacion_padre);
                    $datos[0]->dni_autorizacion_padres			= $this->soporte_xss->decode($datos[0]->dni_autorizacion_padres);
                    $datos[0]->nombre_autorizacion_padre_medica	= $this->soporte_xss->decode($datos[0]->nombre_autorizacion_padre_medica);
                    $datos[0]->dni_autorizacion_padres_medica	= $this->soporte_xss->decode($datos[0]->dni_autorizacion_padres_medica);
                    $datos[0]->observaciones					= $this->soporte_xss->decode($datos[0]->observaciones);
                    $datos[0]->otros				        	= $this->soporte_xss->decode($datos[0]->otros);
    //                $datos[0]->foto				            	= $this->soporte_xss->decode($datos[0]->foto);
    //                $datos[0]->mini_foto		    			= $this->soporte_xss->decode($datos[0]->mini_foto);
                    $datos[0]->copia_pasaporte					= $this->soporte_xss->decode($datos[0]->copia_pasaporte);
                    $datos[0]->mini_pasaporte					= $this->soporte_xss->decode($datos[0]->mini_pasaporte);
                    $datos[0]->otras_consideraciones			= $this->soporte_xss->decode($datos[0]->otras_consideraciones);
                        
                    if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > datos Modificados: ".print_r($datos, true)."<br/>"; }

                    return $datos;
                }
        } 

        function prepararData($data, $delimiterA, $delimiterB ) {
            $dataFormatejada="";

            if ($data != "")
            {
                $arraydata = explode($delimiterA, $data);
                $dataFormatejada=$arraydata[2].$delimiterB.$arraydata[1].$delimiterB.$arraydata[0];
            }

            return $dataFormatejada;
        } 
        
        function get_Inscripcions($i_numero_reserva_localizador, $i_dni) {

            $sql = " SELECT 
                        id_inscripcion, 
                        numero_reserva_localizador, 
                        CONCAT(nombre_participante, ' ', apellidos_participante), 
                        mini_foto, 
                        mini_pasaporte, 
                        estado_enviable 
                    FROM inscripciones_idiomas  
                    WHERE 
                        numero_reserva_localizador = '$i_numero_reserva_localizador' and dni = '$i_dni'
                    ORDER BY 
                        id_inscripcion";

            $query = $this->db->query($sql);
            
            return $query->result();
        }

        function get_CentroTxt($sel) {
            
            $sql = " SELECT 
                    codigo_centro,  
                    denominacion,  
                    poblacion  
                    FROM centros 
                    WHERE 
                    (codigo_centro = $sel) 
                    ORDER BY  
                    denominacion ASC";
            
            $query = $this->db->query($sql);
            
            return $query->result();
        } 

        function get_CentroCbo($sel) {
                
                $sql = "SELECT 
                            codigo_centro,  
                            denominacion,  
                            poblacion  
                        from centros 
                        where 
                            (mostrar_web=1) 
                        order by  
                            denominacion ASC ";

                $contingutWeb="";
                $primeraID=$sel;
                $query = $this->db->query($sql);
                
                foreach ($query->result() as $res) {
                    if ($primeraID == 0) {
                        $primeraID = $res->codigo_centro;
                    }
                    $contingutWeb.= "<option value=\"".$res->codigo_centro."\"".($sel==$res->codigo_centro?" selected":"").">".$res->denominacion."</option>";
                }

                return $contingutWeb.$this->splitter.$primeraID;
        } 

        function get_Inscripcions_cvac() {}

        function get_programasxcentroCbo($sel) {
            
            $sql = "SELECT   
                        idprogramasxcentro,  
                        programa,  
                        durada,  
                        edats  
                    FROM programasxcentro  
                    WHERE   
                        codigo_centro = '$sel'             
                    ORDER BY  
                        codigo_centro,  
                        programa,  
                        durada ";

            $contingut="";

            $query = $this->db->query($sql);

            foreach ($query->result() as $res) {
                $contingut.= "<option value=\"".$res->codigo_centro."\" >".$res->programa." - ".$res->durada." - ".$res->edats."</option>";
            }

            return $contingut;
        } 

        function get_programasxcentroTxt($sel, $sel2) {
        
            $sql = "SELECT   
                        t.programa,  
                        t.fecha_entrada,  
                        t.fecha_salida,  
                        t.edad_min,  
                        t.edad_max,  
                        t.quads,  
                        t.hipica,  
                        t.angles,  
                        t.dni_nen,
                        t.futbol,
                        t.zurich, 
                        t.hostel,
                        h.name as Centro ,
                        t.colectiu
                    FROM hostels_tandes as t
                        INNER JOIN hostels as h
                            ON t.hostel = h.id
                    WHERE   
                        t.codigo_centro = '$sel' AND 
                        t.idhotels_tandes = '$sel2' 
                    ORDER BY  
                        t.codigo_centro,  
                        t.fecha_entrada,  
                        t.fecha_salida "; 

            $query = $this->db->query($sql);

            return $query->result();        
        }   

        function update_ImatgeInscripcio($i_idinscripcio, $i_foto, $i_thumbnail, $tipo) {
            $i_foto         =   $this->soporte_xss->encode($i_foto);
            $i_thumbnail    =   $this->soporte_xss->encode($i_thumbnail);

            $where = "id_inscripcion = '$i_idinscripcio'";
            switch ($tipo) {
                case 'IMG':
                    $datos = array( "foto"              => $i_foto,  
                                    "thumbnail"         => $i_thumbnail );
                break;

                case 'PASS':
                    $datos = array( "copia_pasaporte"   => $i_foto,  
                                    "mini_pasaporte"    => $i_thumbnail );
                break;
            }
            $this->db->where($where);
            $this->db->update("inscripciones_idiomas", $datos);
        } 

        function get_ImatgesInscripcio($i_idinscripcio, $tipo) {
            
            $campos = "";
            switch ($tipo) {
                case 'IMG':
                    $campos = "foto as Foto, thumbnail as Thumb";
                break;

                case 'PASS':
                    $campos = "copia_pasaporte as Foto, mini_pasaporte as Thumb";
                break;
            }

            $sql = "SELECT 
                        $campos   
                    FROM inscripciones_idiomas 
                    WHERE 
                        id_inscripcion = '$i_idinscripcio'";

            $query = $this->db->query($sql);
            $res = $query->result();

            if (count($res) > 0) {
                $res[0]->Foto	= $this->soporte_xss->decode($res[0]->Foto);
                $res[0]->Thumb  = $this->soporte_xss->decode($res[0]->Thumb);
            }
            return ( (count($res) == 0) ? false : $res[0]);
        } 
            
        function get_ImatgeInscripcio($i_idinscripcio) {

            $sql = "SELECT 
                        foto   
                    FROM inscripciones_idiomas 
                    WHERE 
                        id_inscripcion = '$i_idinscripcio'";

            $query = $this->db->query($sql);

            $resFoto = "";

            foreach ($query->result() as $res) {
                $resFoto = $this->soporte_xss->decode($res->foto);
            }

            return $resFoto;
        } 

        function get_ImatgeInscripcio_pas($i_idinscripcio) {
            $sql = "SELECT 
                        copia_pasaporte  
                    FROM inscripciones_idiomas 
                    WHERE  
                        id_inscripcion = '$i_idinscripcio'";

            $query = $this->db->query($sql);

            $copia_pasaporte = "";

            foreach ($query->result() as $res) {
                $copia_pasaporte = $this->soporte_xss->decode($res->copia_pasaporte);
            }

            return $copia_pasaporte;
        } 

        function del_Inscripcio($i_idinscripcio) {

            $where = "id_inscripcion = '$i_idinscripcio'";

            $this->db->where($where);
            $this->db->update("inscripciones_idiomas", $datos);

        } 

        function get_hostel($idtanda) {

            $sql = "SELECT   
                        hostel  
                    FROM hostels_tandes  
                    WHERE   
                        idhotels_tandes = '$idtanda'";
            
            $query = $this->db->query($sql);
            
            $p_idhostel = "";

            foreach ($query->result() as $res) {
                $p_idhostel = $res->hostel;
            }

            return $p_idhostel;
        }    
        
        function dblog($logtexte) {
            
            if (isset($_SESSION['hostel'])) {
                $this->hostel = $_SESSION['hostel'];
            }

            if (isset($_SESSION['order_number'])) {
                $this->order = $_SESSION['order_number'];
            }

            $datos = array(
                "hostel" => $this->hostel, 
                "horacontrol" => date("Y-m-d H:i:s"), 
                "order_number" => $this->order, 
                "step" => 'asyncsend.php', 
                "status" => $logtexte
            );

            $this->db->set($datos);
            $this->db->insert("reservations_tpvlog");

        }
        
        function inclog($logtexte) {
            
            if (isset($_SESSION['hostel'])) {
                $this->hostel = $_SESSION['hostel'];
            }
            
            if (isset($_SESSION['order_number'])) {
                $this->order = $_SESSION['order_number'];
            }

            if ($this->procesarlog) {

                $datos = array(
                    "hostel" => $this->hostel, 
                    "horacontrol" => date("Y-m-d H:i:s"), 
                    "order_number" => $this->order, 
                    "step" => 'inscripciones.php', 
                    "status" => $logtexte
                );

                $this->db->set($datos);
                $this->db->insert("reservations_tpvlog");
            }
        }

        function getCustomerStrings($p_name, $p_lang) {

            $sqls = "   SELECT 
                            text 
                        FROM customer_strings 
                        WHERE 
                            lang='$p_lang' and name ='$p_name'";

            $query = $this->db->query($sqls);

            $resTexto = "";
            
            foreach ($query->result() as $res) {
                $resTexto = $res->text;
            }

            return $resTexto;
        } 

        function getHotelStrings($p_hostel, $p_name, $p_lang) {

            $sqlh = "   SELECT 
                            customer_string 
                        FROM hostels_strings 
                        WHERE 
                            hostel='$p_hostel' and hostel_string ='$p_name'";
    
            $query = $this->db->query($sqlh);

            $resTexto = "";

            foreach ($query->result() as $res) {
                $resTexto = $this->getCustomerStrings($res->customer_string, $p_lang);
            }

            return $resTexto;
        }
    }
?>