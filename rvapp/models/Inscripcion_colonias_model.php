<?php 
    if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

    class Inscripcion_colonias_model extends CI_Model {

        protected $logar        = false;
        protected $limite       = 7;
        protected $splitter     = "|";

        protected $order        = "";
        protected $hostel       = "";
        protected $procesarlog  = "";

        function __construct(){
            parent::__construct();
        } 

        function isset_Inscripcio($i_idinscripcio, $i_people, $i_control) {   
            $nom = "";
            
            $sql = "SELECT 
                        nom_complet
                    FROM
                        inscripciones
                    WHERE 
                        idinscripciones = '$i_idinscripcio' and
                        idpeople = '$i_people' and
                        control = '$i_control'";
            
            if ($this->logar) { echo  __file__." > ".__line__." - isset_Inscripcio > sql: ".$sql."<br/>"; }
            
            $query = $this->db->query($sql);

            $datos = (array)$query->result();
            
            if ($this->logar) { echo  __file__." > ".__line__." - isset_Inscripcio > datos: ".print_r($datos, true)."<br/>"; }
            
            if (count($datos) > 0) {
                $nom  = /* main_decript */ $this->soporte_xss->decode($datos[0]->nom_complet);
            }

            if ($this->logar) { echo  __file__." > ".__line__." - isset_Inscripcio > nom: ".print_r($nom, true)."<br/>"; }

            return $nom;
        }

        function getIsZurich($i_idinscripcio) {   
            $sql = "SELECT  t.zurich
                    FROM inscripciones i
                        inner join hostels_tandes t
                            on t.idhotels_tandes = i.idtanda
                    WHERE i.idinscripciones = '".$i_idinscripcio."'"; //
            
            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - getIsZurich > sql: ".print_r($sql, true)."<br/>"; }
                        
            $datos = (array)$query->result();
            
            if ($this->logar) { echo  __file__." > ".__line__." - getIsZurich > datos: ".print_r($datos, true)."<br/>"; }

            $zurich = false;
            if (count($datos) > 0) {
                $zurich  = /* main_decript */ ($datos[0]->zurich);
            }

            if ($this->logar) { echo  __file__." > ".__line__." - getIsZurich > zurich: ".print_r($zurich, true)."<br/>"; }

            return $zurich;
        }

        function comprovar_Inscripcio($i_idinscripcio, $i_localizador, $i_dni, $i_centro) {
            $nom = "";

            $sql = "SELECT 
                        nom_complet, localizador, centro
                    FROM 
                        inscripciones 
                    WHERE 
                        idinscripciones  = '$i_idinscripcio' and 
                        idtanda          = '$i_tanda' and 
                        centro           = '$i_centro' and 
                        dni              = '$dni' and 
                        localizador      = '$i_localizador'";
            
            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - comprovar_Inscripcio > sql: ".print_r($sql, true)."<br/>"; }
            
            $datos = (array)$query->result();

            if ($this->logar) { echo  __file__." > ".__line__." - comprovar_Inscripcio > datos: ".print_r($datos, true)."<br/>"; }
            
            if (count($datos) > 0) {
                
                $nom  = /* main_decript */ $this->soporte_xss->decode($datos[0]->nom_complet);
                $_SESSION['hostel']  = /* main_decript */ ($datos[0]->centro);
                $_SESSION['order_number']  = /* main_decript */ ($datos[0]->localizador);
            }
            
            if ($this->logar) { echo  __file__." > ".__line__." - comprovar_Inscripcio > nom: ".print_r($nom, true)."<br/>"; }

            return $nom;
        }
        
        /*
            function isset_LevelTest($i_idinscripcio)
            {   global $p_catalogo;
                $resultat=0;
                $sql = " SELECT "; //
                $sql.= "    count(`idinscripcion`)"; 
                $sql.= " FROM pruevanivelingles_idiomas "; 
                $sql.= " WHERE idinscripcion = '".$i_idinscripcio."'";

                $resIsSet = mysql_query($sql);
                return mysql_fetch_row($resIsSet);
            } // isset_LevelTest
        */

        function isValidInscripcio($i_idinscripcio, $i_people, $i_control) {
            $sql = "SELECT 
                    inscripciones.estado_enviable,
                    hostels_tandes.tipo_reserva
                    FROM
                    inscripciones
                        inner join hostels_tandes
                            on inscripciones.idtanda = hostels_tandes.idhotels_tandes
                    WHERE 
                    inscripciones.idinscripciones = '$i_idinscripcio' and
                    inscripciones.idpeople = '$i_people' and
                    inscripciones.control = '$i_control'";

            $query = $this->db->query($sql);
            
            if ($this->logar) { echo  __file__." > ".__line__." - isValidInscripcio > sql: ".print_r($sql, true)."<br/>"; }
            
            return $query->result();
        } 

        function get_DadesInscripcio($i_idinscripcio, $i_people, $i_control) {   
            $sql = "SELECT 
                        inscripciones.localizador as NReserva,
                        inscripciones.centro,
                        reservations.hostel
                    FROM
                        inscripciones
                        inner join  reservations
                            on inscripciones.localizador = reservations.order_number
                    WHERE 
                        idinscripciones = '$i_idinscripcio' and
                        idpeople = '$i_people' and
                        control = '$i_control'";

            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - get_DadesInscripcio > sql: ".print_r($sql, true)."<br/>"; }

            return $query->result();
        } 
        
        // function set_Inscripcio($i_ejercicio, $i_codigo_empresa, $i_codigo_departamento,  $i_numero_reserva_localizador,  $i_forfait,  $i_dni, $i_estado_gesreser,  $i_estado_enviable,  $i_nombre_amigo,  $i_amics_familiars,  $i_barna_madrid,  $i_nombre_participante,  $i_apellidos_participante, $i_chico_chica,  $i_direccion_participante, $i_codigo_postal,  $i_poblacion,  $i_telefono,  $i_movil,  $i_fecha_nacimiento,  $i_edad_participante,  $i_pasaporte,  $i_nacionalidad,  $i_email_participantes,  $i_movil_padres,  $i_nombre_padres,  $i_dni_padres,  $i_algun_curso,  $i_algun_curso_detalle,  $i_alguna_enfermedad,  $i_alguna_enfermedad_detalle, $i_dieta_especial,  $i_dieta_especial_detalle,  $i_algun_medicamento,  $i_algun_medicamento_detalle,  $i_colegio,  $i_direccion_colegio,  $i_cp_colegio,  $i_poblacion_colegio,  $i_nombre_profesor,  $i_curso,  $i_nivel_idioma,  $i_nombre_autorizacion_padre,  $i_dni_autorizacion_padres,  $i_autoritzacio_acceptacio_monitor, $i_nombre_autorizacion_padre_medica, $i_dni_autorizacion_padres_medica,  $i_autoritzacio_acceptacio_medica, $i_observaciones,  $i_conocido_por, $i_otros, $i_normativa_acceptacio, $i_foto, $i_mini_foto, $i_copia_pasaporte,  $i_mini_pasaporte, $i_data_in, $i_data_out, $i_algun_curso_detalle2) {
        function set_Inscripcio($i_codigo_departamento, $i_codigo_empresa, $i_centro, $i_ejercicio, $i_localizador, /*$i_dni, */
            $i_estado_gesreser, $i_estado_envio, $i_nom_complet, $i_telefon, $i_adressa, $i_poblacio, 
            $i_cp, $i_data_naixement, $i_altres_telefons, $i_foto, $i_thumbnail, $i_malalt, $i_malalt_detall, 
            $i_medicaments, $i_medicaments_detall, $i_medicaments_administracio, $i_regim, $i_regim_detall, 
            $i_operat, $i_operat_detall, $i_alergic, $i_alergic_detall, $i_estades_abans, $i_nedar, 
            $i_por_aigua, $i_bicicleta, $i_autoritzacio_nom, $i_autoritzacio_dni, /* $i_autoritzacio_inici, 
            $i_autoritzacio_final, $i_autoritzacio_idcasa, */ $i_autoritzacio_ok, $i_normativa_ok, 
            $i_aut_paint_quad, $i_hipica1, $i_hipica2, $i_hipica3, $i_hipica4, $i_observaciones_hipica, 
            $i_observaciones_generales, $i_mallorca_dni, $i_tanda ) {
            
            $datos = array( "codigo_departamento" => $i_codigo_departamento, 
                            "codigo_empresa" => $i_codigo_empresa, 
                            "centro" => $i_centro, 
                            "ejercicio" => $i_ejercicio, 
                            "localizador" => $i_localizador, 
                            "estado_gesreser" => $i_estado_gesreser, 
                            "estado_enviable" => $i_estado_envio, 
                            "nom_complet" => $this->soporte_xss->encode($i_nom_complet), 
                            "telefon" => $this->soporte_xss->encode($i_telefon), 
                            "adressa" => $this->soporte_xss->encode($i_adressa), 
                            "poblacio" => $this->soporte_xss->encode($i_poblacio), 
                            "cp" => $this->soporte_xss->encode($i_cp), 
                            "data_naixement" => $this->prepararData($i_data_naixement, "/", "-"), 
                            "altres_telefons" => $this->soporte_xss->encode($i_altres_telefons), 
                            "foto" => $this->soporte_xss->encode($i_foto), 
                            "thumbnail"   => $this->soporte_xss->encode($i_thumbnail),       
                            "malalt" => $i_malalt, 
                            "malalt_detall" => $this->soporte_xss->encode($i_malalt_detall), 
                            "medicaments" => $i_medicaments, 
                            "medicaments_detall" => $this->soporte_xss->encode($i_medicaments_detall), 
                            "medicaments_administracio" => $this->soporte_xss->encode($i_medicaments_administracio), 
                            "regim" => $i_regim, 
                            "regim_detall" => $this->soporte_xss->encode($i_regim_detall), 
                            "operat" => $i_operat, 
                            "operat_detall" => $this->soporte_xss->encode($i_operat_detall), 
                            "alergic" => $i_alergic, 
                            "alergic_detall" => $this->soporte_xss->encode($i_alergic_detall), 
                            "estades_abans" => $i_estades_abans, 
                            "nedar" => $i_nedar, 
                            "por_aigua" => $i_por_aigua, 
                            "bicicleta" => $i_bicicleta, 
                            "autoritzacio_nom" => $this->soporte_xss->encode($i_autoritzacio_nom), 
                            "autoritzacio_dni" => $this->soporte_xss->encode($i_autoritzacio_dni), 
                            "autoritzacio_ok" => $i_autoritzacio_ok, 
                            "normativa_ok" => $i_normativa_ok, 
                            "aut_paint_quad" => $i_aut_paint_quad, 
                            "hipica1" => $i_hipica1, 
                            "hipica2" => $i_hipica2, 
                            "hipica3" => $i_hipica3, 
                            "hipica4" => $i_hipica4, 
                            "observaciones_hipica" => $this->soporte_xss->encode($i_observaciones_hipica), 
                            "observaciones_generales" => $this->soporte_xss->encode($i_observaciones_generales), 
                            "mallorca_dni" => $this->soporte_xss->encode($i_mallorca_dni), 
                            "f_creacio" => date("Y-m-d h:i:s"), 
                            "f_modificacio" => date("Y-m-d h:i:s"), 
                            "idtanda" => $i_tanda
            );

            $this->db->set($datos);
            $this->db->insert("inscripciones");
    
            return $this->db->insert_id(); 
        } 

        function getProvaNivellJaRealitzada($i_idinscripcio) {
        
            $sql = " SELECT ";
            $sql.= "    NSNC "; //
            for ($i=1; $i <= 30; $i++) { 
                $sql.= " , p".str_pad($i, 2, "0", STR_PAD_LEFT)." "; //
            }
            $sql.= " FROM pruevanivelingles  ";
            $sql.= " WHERE ";
            $sql.= "    idinscripcion = '".$i_idinscripcio."'"; //
            
            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - getProvaNivellJaRealitzada > sql: ".print_r($sql, true)."<br/>"; }

            $result = $query->result();

            if ($this->logar) { echo  __file__." > ".__line__." - getProvaNivellJaRealitzada > result: ".print_r($result, true)."<br/>"; }

            // echo "<pre>result ".print_r($result, true)."</pre>";
            $tupadre = $result[0];
            $tupadre = json_decode(json_encode($tupadre), True);
            // echo "<pre>tupadre ".print_r($tupadre, true)."</pre>";

            if ($this->logar) { echo  __file__." > ".__line__." - getProvaNivellJaRealitzada > tupadre: ".print_r($tupadre, true)."<br/>"; }
            
            return $tupadre;
            
        } 
        
        function fecha_checkin($i_idinscripcio, $forzar = false) {
            /*
                $sql = "SELECT 
                            (datediff(r.checkin, now()) > $this->limite) as acceso
                        FROM reservations as r
                            LEFT JOIN inscripciones_idiomas as i
                                ON r.order_number = i.numero_reserva_localizador
                        WHERE
                            r.tipo_reserva = '2' and 
                            i.id_inscripcion = '$i_idinscripcio'";
                
                $query = $this->db->query($sql);
                $datos = $query->result();

                $returnAccess=0;
                if (count($datos) > 0) {
                    $returnAccess = $datos[0]->acceso;
                }
                return $returnAccess;       
            */ 
            return true;
        }

        function provaNivellJaRealitzada($i_idinscripcio) {
        
            $sql = " SELECT 
                        idinscripcion 
                    FROM pruevanivelingles 
                    WHERE
                        idinscripcion = '$i_idinscripcio'";
            
            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - provaNivellJaRealitzada > sql: ".print_r($sql, true)."<br/>"; }

            $datos = $query->result();

            if ($this->logar) { echo  __file__." > ".__line__." - provaNivellJaRealitzada > datos: ".print_r($datos, true)."<br/>"; }

            $returnid=0;
            if (count($datos) > 0) {
                $returnid = $datos[0]->idinscripcion;
            }

            if ($this->logar) { echo  __file__." > ".__line__." - provaNivellJaRealitzada > returnid: ".print_r($returnid, true)."<br/>"; }
            return $returnid;
        }

        function set_provaNivell($i_idinscripcio, $npreguntas) {
            $datos = array( "idinscripcion" =>  $i_idinscripcio );

            if (isset($_POST["NSNC"])) {
                $datos["NSNC"] =  $_POST["NSNC"];
            } else {
                for ($pos=0; $pos < $npreguntas; $pos++) { 
                    $numero =str_pad(($pos+1), 2, "0", STR_PAD_LEFT);
                    $datos["p".$numero] = $_POST["respuesta".$pos];
                }
            }
                
            $this->db->set($datos);
            $this->db->insert("pruevanivelingles");

            return $this->db->insert_id(); 
        }

        function end_Inscripcio(  $i_idinscripcio, $i_email, $i_estado_gesreser, $i_estado_enviar, $i_idpeople, $i_clavecontrol ) {
            
            $where = "idinscripcion = '$i_idinscripcio' and idpeople = '$i_idpeople' and control = '$i_clavecontrol'";

            $datos = array( "email_confirmacion"    => $i_email,
                            "estado_enviable"       => $i_estado_enviar,
                            "estado_gesreser"       => $i_estado_gesreser,
                            "f_modificacio"         => date("Y-m-d h:i:s")
                        );
            
            $this->db->where($where);
            $this->db->update("inscripciones", $datos);
        }

        function update_Inscripcio( $i_idinscripcio) {
            $i_estado_gesreser = 0; 
            $i_estado_enviable = 1;

            log_message("error", "post ".print_r($_POST, true));

            $datos = array( "estado_gesreser"                   => $i_estado_gesreser, 
                            "estado_enviable"                   => $i_estado_enviable,
                            "nom_complet"                       => $this->soporte_xss->encode(set_value("nom_complet", $_POST["nom_complet"])),
                            "chico_chica"                       => set_value("chico_chica", $_POST["chico_chica"]),
                            "telefon"                           => $this->soporte_xss->encode(set_value("telefon", $_POST["telefon"])),
                            "adressa"                           => $this->soporte_xss->encode(set_value("adressa", $_POST["adressa"])),
                            "poblacio"                          => $this->soporte_xss->encode(set_value("poblacio", $_POST["poblacio"])),
                            "cp"                                => $this->soporte_xss->encode(set_value("cp", $_POST["cp"])),
                            "data_naixement"                    => $this->prepararData(set_value("data_naixement", $_POST["data_naixement"]), "/", "-"),
                            "altres_telefons"                   => $this->soporte_xss->encode(set_value("altres_telefons", $_POST["altres_telefons"])),
                            // "foto" => set_value("foto", $_POST["foto"]),
                            // "thumbnail" => set_value("thumbnail", $_POST["thumbnail"]),
                            "malalt"                            => set_value("malalt", (isset($_POST["malalt"])?$_POST["malalt"]:"0")),
                            "malalt_detall"                     => $this->soporte_xss->encode(set_value("malalt_detall", (isset($_POST["malalt_detall"])?$_POST["malalt_detall"]:""))),
                            "medicaments"                       => set_value("medicaments", (isset($_POST["medicaments"])?$_POST["medicaments"]:"0")),
                            "medicaments_detall"                => $this->soporte_xss->encode(set_value("medicaments_detall", (isset($_POST["medicaments_detall"])?$_POST["medicaments_detall"]:""))),
                            "medicaments_administracio"         => $this->soporte_xss->encode(set_value("medicaments_administracio", (isset($_POST["medicaments_administracio"])?$_POST["medicaments_administracio"]:""))),
                            "regim"                             => set_value("regim", (isset($_POST["regim"])?$_POST["regim"]:"0")),
                            "regim_detall"                      => $this->soporte_xss->encode(set_value("regim_detall", (isset($_POST["regim_detall"])?$_POST["regim_detall"]:""))),
                            "desordre_alimentari"               => set_value("desordre_alimentari", (isset($_POST["desordre_alimentari"])?$_POST["desordre_alimentari"]:"0")),
                            "desordre_alimentari_detall"        => $this->soporte_xss->encode(set_value("desordre_alimentari_detall", (isset($_POST["desordre_alimentari_detall"])?$_POST["desordre_alimentari_detall"]:""))),
                            "operat"                            => set_value("operat", (isset($_POST["operat"])?$_POST["operat"]:"0")),
                            "operat_detall"                     => $this->soporte_xss->encode(set_value("operat_detall", (isset($_POST["operat_detall"])?$_POST["operat_detall"]:""))),
                            "alergic_celiac"                    => set_value("alergic_celiac", (isset($_POST["alergic_celiac"])?$_POST["alergic_celiac"]:"0")),
                            "alergic_lactosa"                   => set_value("alergic_lactosa", (isset($_POST["alergic_lactosa"])?$_POST["alergic_lactosa"]:"0")),
                            "alergic_ou"                        => set_value("alergic_ou", (isset($_POST["alergic_ou"])?$_POST["alergic_ou"]:"0")),
                            "alergic_altres"                    => set_value("alergic_altres", (isset($_POST["alergic_altres"])?$_POST["alergic_altres"]:"0")),
                            "alergic_detall"                    => $this->soporte_xss->encode(set_value("alergic_detall", (isset($_POST["alergic_detall"])?$_POST["alergic_detall"]:""))),
                            "estades_abans"                     => set_value("estades_abans", (isset($_POST["estades_abans"])?$_POST["estades_abans"]:"0")),
                            "nedar"                             => set_value("nedar", (isset($_POST["nedar"])?$_POST["nedar"]:"0")),
                            "por_aigua"                         => set_value("por_aigua", (isset($_POST["por_aigua"])?$_POST["por_aigua"]:"0")),
                            "bicicleta"                         => set_value("bicicleta", (isset($_POST["bicicleta"])?$_POST["bicicleta"]:"0")),
                            "autoritzacio_nom"                  => $this->soporte_xss->encode(set_value("autoritzacio_nom", (isset($_POST["autoritzacio_nom"])?$_POST["autoritzacio_nom"]:""))),
                            "autoritzacio_dni"                  => $this->soporte_xss->encode(set_value("autoritzacio_dni", (isset($_POST["autoritzacio_dni"])?$_POST["autoritzacio_dni"]:""))),
                            "autoritzacio_ok"                   => set_value("autoritzacio_ok", (isset($_POST["autoritzacio_ok"])?$_POST["autoritzacio_ok"]:"0")),
                            "normativa_ok"                      => set_value("normativa_ok", (isset($_POST["normativa_ok"])?$_POST["normativa_ok"]:"0")),
                            "aut_paint_quad"                    => set_value("aut_paint_quad", (isset($_POST["aut_paint_quad"])?$_POST["aut_paint_quad"]:"0")),
                            "hipica1"                           => set_value("hipica1", (isset($_POST["hipica1"])?$_POST["hipica1"]:"0")),
                            "hipica2"                           => set_value("hipica2", (isset($_POST["hipica2"])?$_POST["hipica2"]:"0")),
                            "hipica3"                           => set_value("hipica3", (isset($_POST["hipica3"])?$_POST["hipica3"]:"0")),
                            "hipica4"                           => set_value("hipica4", (isset($_POST["hipica4"])?$_POST["hipica4"]:"0")),
                            "sotatractament"                    => set_value("sotatractament", (isset($_POST["sotatractament"])?$_POST["sotatractament"]:"0")),
                            "sotatractamentdetall"              => $this->soporte_xss->encode(set_value("sotatractamentdetall", (isset($_POST["sotatractamentdetall"])?$_POST["sotatractamentdetall"]:""))),
                            "vertigen"                          => set_value("vertigen", (isset($_POST["vertigen"])?$_POST["vertigen"]:"0")),
                            "dificultatesports"                 => set_value("dificultatesports", (isset($_POST["dificultatesports"])?$_POST["dificultatesports"]:"0")),
                            "dificultatesports_quins"           => $this->soporte_xss->encode(set_value("dificultatesports_quins", (isset($_POST["dificultatesports_quins"])?$_POST["dificultatesports_quins"]:""))),
                            "observaciones_hipica"              => $this->soporte_xss->encode(set_value("observaciones_hipica", (isset($_POST["observaciones_hipica"])?$_POST["observaciones_hipica"]:""))),
                            "observaciones_generales"           => $this->soporte_xss->encode(set_value("observaciones_generales", (isset($_POST["observaciones_generales"])?$_POST["observaciones_generales"]:""))),
                            "mallorca_dni"                      => $this->soporte_xss->encode(set_value("mallorca_dni", (isset($_POST["mallorca_dni"])?$_POST["mallorca_dni"]:""))),
                            "futbol_equipacion"                 => set_value("futbol_equipacion", (isset($_POST["futbol_equipacion"])?$_POST["futbol_equipacion"]:"")),
                            "email"                             => $this->soporte_xss->encode(set_value("confirmacionxmail", (isset($_POST["confirmacionxmail"])?$_POST["confirmacionxmail"]:""))),
							"f_modificacio"                     => date("Y-m-d H:i:s"),
                            "enc"								=> 1,
                            "covid19_ok"                        => set_value("covid19_ok", (isset($_POST["covid19_ok"])?$_POST["covid19_ok"]:"0")),
                            "covid19_obs"                   => $this->soporte_xss->encode(set_value("covid19_obs", (isset($_POST["covid19_obs"])?$_POST["covid19_obs"]:"")))

                            

            );  

            foreach ($datos as $key => $value) {
                if ($value == "true") { $datos[$key] = 1; }
                if ($value == "false") { $datos[$key] = 0; }
                # code...
            }
            $where = "idinscripciones = '$i_idinscripcio'";

            log_message("error", "datos ".print_r($datos, true));
            log_message("error", "where ".print_r($where, true));
            // echo "<pre>post ". print_r($_POST,true).$where."</pre>";
            
            $this->db->where($where);
            $this->db->update("inscripciones", $datos);

        } 

        function get_Inscripcio($i_idinscripcio, $i_idpeople, $i_clavecontrol, $i_forzar = false) {
            global $p_catalogo;
            $resultat=0;
    
            $sql = " SELECT 
                        idinscripciones,
                        codigo_departamento,
                        codigo_empresa,
                        centro,
                        ejercicio,
                        localizador,
                        dni,
                        estado_gesreser,
                        estado_enviable,
                        nom_complet,
                        chico_chica,
                        telefon,
                        adressa,
                        poblacio,
                        cp,
                        data_naixement,
                        altres_telefons,
                        foto,
                        thumbnail,
                        malalt,
                        malalt_detall,
                        medicaments,
                        medicaments_detall,
                        medicaments_administracio,
                        regim,
                        regim_detall,
                        desordre_alimentari,
                        desordre_alimentari_detall,
                        operat,
                        operat_detall,
                        estades_abans,
                        nedar,
                        por_aigua,
                        bicicleta,
                        autoritzacio_nom,
                        autoritzacio_dni,
                        autoritzacio_idcasa,
                        autoritzacio_ok,
                        normativa_ok,
                        aut_paint_quad, 
                        hipica1,
                        hipica2,
                        hipica3,
                        hipica4,
                        observaciones_hipica, 
                        observaciones_generales,
                        mallorca_dni, 
                        idtanda,
                        sotatractament,
                        sotatractamentdetall,
                        vertigen,
                        dificultatesports,
                        dificultatesports_quins, 
                        futbol_equipacion, 
                        alergic_celiac,
                        alergic_lactosa,
                        alergic_ou,
                        alergic_altres,
                        alergic_detall,
                        dni_delantero,
                        tipo_reserva,
                        dni_trasero,
                        email,
                        covid19_ok,
                        covid19_obs
                    FROM 
                        inscripciones 
                    WHERE 
                        idinscripciones = '$i_idinscripcio' and 
                        idpeople = '$i_idpeople' and 
                        control = '$i_clavecontrol'";
                
            $query = $this->db->query($sql);

            if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > sql: ".print_r($sql, true)."<br/>"; }

            $datos =$query->result();

            if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > datos: ".print_r($datos, true)."<br/>"; }

            $entrar = false;
            if ( count($datos) <= 0 ) {
                // si no hay datos devolvemos -1
                return $entrar;

            } else {
                if ( $i_forzar == true ) {
                    // Tenemos datos y forzamos a mostrar, asi que continuaremos con la ejecucion
                    $entrar = true;

                } else {

                    if ( $datos[0]->estado_enviable == 0 ) {
                        // como no ha sido realizada permitimos entrar, asi que continuamos la ejecucion.
                        $entrar = true;
                    } else {
                        // como ya ha sido realizada y no forzamos, devolvemos 0
                        return 0;
                    }
                }
            }

            

            if  ( $entrar == true ) {
            
                $datos[0]->dni							= $this->soporte_xss->decode($datos[0]->dni);
                $datos[0]->nom_complet					= $this->soporte_xss->decode($datos[0]->nom_complet);
                $datos[0]->telefon						= $this->soporte_xss->decode($datos[0]->telefon);
                $datos[0]->adressa						= $this->soporte_xss->decode($datos[0]->adressa);
                $datos[0]->poblacio						= $this->soporte_xss->decode($datos[0]->poblacio);
                $datos[0]->cp							= $this->soporte_xss->decode($datos[0]->cp);
                $datos[0]->altres_telefons			    = $this->soporte_xss->decode($datos[0]->altres_telefons);
                $datos[0]->foto						    = $this->soporte_xss->decode($datos[0]->foto);
                $datos[0]->thumbnail					= $this->soporte_xss->decode($datos[0]->thumbnail);
                $datos[0]->malalt_detall				= $this->soporte_xss->decode($datos[0]->malalt_detall);
                $datos[0]->medicaments_detall			= $this->soporte_xss->decode($datos[0]->medicaments_detall);
                $datos[0]->medicaments_administracio	= $this->soporte_xss->decode($datos[0]->medicaments_administracio);
                $datos[0]->regim_detall					= $this->soporte_xss->decode($datos[0]->regim_detall);
                $datos[0]->desordre_alimentari_detall	= $this->soporte_xss->decode($datos[0]->desordre_alimentari_detall);
                $datos[0]->operat_detall				= $this->soporte_xss->decode($datos[0]->operat_detall);
                $datos[0]->alergic_detall				= $this->soporte_xss->decode($datos[0]->alergic_detall);
                $datos[0]->autoritzacio_nom				= $this->soporte_xss->decode($datos[0]->autoritzacio_nom);
                $datos[0]->autoritzacio_dni				= $this->soporte_xss->decode($datos[0]->autoritzacio_dni);
                $datos[0]->observaciones_hipica			= $this->soporte_xss->decode($datos[0]->observaciones_hipica);
                $datos[0]->mallorca_dni					= $this->soporte_xss->decode($datos[0]->mallorca_dni);
                $datos[0]->observaciones_generales		= $this->soporte_xss->decode($datos[0]->observaciones_generales);
                $datos[0]->sotatractamentdetall			= $this->soporte_xss->decode($datos[0]->sotatractamentdetall);
                $datos[0]->dificultatesports_quins		= $this->soporte_xss->decode($datos[0]->dificultatesports_quins);
                $datos[0]->dni_delantero				= $this->soporte_xss->decode($datos[0]->dni_delantero);
                $datos[0]->dni_trasero					= $this->soporte_xss->decode($datos[0]->dni_trasero);   
                $datos[0]->email					    = $this->soporte_xss->decode($datos[0]->email);   
                $datos[0]->covid19_obs					= $this->soporte_xss->decode($datos[0]->covid19_obs);   

                if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcio > datos Modificados: ".print_r($datos, true)."<br/>"; }
                
                return $datos;
            }
        } 

        function prepararData($data, $delimiterA, $delimiterB ) {
            $dataFormatejada="";

            if ($data != "")
            {
                $arraydata = explode($delimiterA, $data);
                $dataFormatejada=$arraydata[2].$delimiterB.$arraydata[1].$delimiterB.$arraydata[0];
            }

            return $dataFormatejada;
        } 
        
        function get_Inscripcions($i_numero_reserva_localizador, $i_dni, $i_centro) {

            $sql = " SELECT 
                        idinscripciones, 
                        localizador, 
                        nom_complet, 
                        thumbnail, 
                        estado_enviable,
                        data_naixement
                    FROM inscripciones  
                    WHERE 
                        localizador = '$i_numero_reserva_localizador' and 
                        dni = '$i_dni' and
                        centro = '$i_centro'
                    ORDER BY 
                        idinscripciones";

            $query = $this->db->query($sql);
            
            if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcions > sql: ".print_r($sql, true)."<br/>"; }

            $datos =$query->result();

            if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcions > datos: ".print_r($datos, true)."<br/>"; }

            $reg=0;
            foreach ($datos as $dato) {
                $datos[$reg]->nom_complet	= $this->soporte_xss->decode($dato->nom_complet);
                $datos[$reg]->thumbnail     = $this->soporte_xss->decode($dato->thumbnail);
                $reg++;
            }

            if ($this->logar) { echo  __file__." > ".__line__." - get_Inscripcions > datos Modificados: ".print_r($datos, true)."<br/>"; }

            return $datos;
        }

        function get_Inscripcions_cvac($i_idinscripcion, $idcvac = false) {

            $where = (!$idcvac ? "" : " AND idcvac = ".$idcvac);
            $sql = " SELECT 
                        idinscripcion,
                        idcvac,
                        url
                    FROM inscripciones_cvac  
                    WHERE 
                        idinscripcion = '$i_idinscripcion' and 
                        tipo = 1 $where
                    ORDER BY 
                        idcvac ASC";

            $query = $this->db->query($sql);

            return $query->result();
        }

        function set_Inscripcions_cvac($i_idinscripcion, $i_url) {

            $datos = array( "idinscripcion" => $i_idinscripcion,
                            "url"           => $i_url,
                            "tipo"          => 1);

            $this->db->set($datos);
            $this->db->insert("inscripciones_cvac");

            return $this->db->insert_id(); 
        }

        function delete_Inscripcions_cvac($i_idinscripcion, $i_cvac) {

            $tabla = "inscripciones_cvac";
            $filtro = array("idinscripcion" => $i_idinscripcion,
                            "idcvac"        => $i_cvac,
                            "tipo"          => 1);

            $this->db->delete($tabla, $filtro);
        }

        function get_CentroTxt($sel) {
            
            $sql = " SELECT 
                        codigo_centro,  
                        denominacion,  
                        poblacion  
                    FROM centros 
                    WHERE 
                        (codigo_centro = $sel) 
                    ORDER BY  
                        denominacion ASC";
            
            $query = $this->db->query($sql);
            
            return $query->result();
        } 

        function get_CentroCbo($sel) {
                
                $sql = "SELECT 
                            codigo_centro,  
                            denominacion,  
                            poblacion  
                        from centros 
                        where 
                            (mostrar_web=1) 
                        order by  
                            denominacion ASC ";

                $contingutWeb="";
                $primeraID=$sel;
                $query = $this->db->query($sql);
                
                foreach ($query->result() as $res) {
                    if ($primeraID == 0) {
                        $primeraID = $res->codigo_centro;
                    }
                    $contingutWeb.= "<option value=\"".$res->codigo_centro."\"".($sel==$res->codigo_centro?" selected":"").">".$res->denominacion."</option>";
                }

                return $contingutWeb.$this->splitter.$primeraID;
        } 

        function get_programasxcentroCbo($sel) {
            
            $sql = "SELECT   
                        idprogramasxcentro,  
                        programa,  
                        durada,  
                        edats  
                    FROM programasxcentro  
                    WHERE   
                        codigo_centro = '$sel'             
                    ORDER BY  
                        codigo_centro,  
                        programa,  
                        durada ";

            $contingut="";

            $query = $this->db->query($sql);

            foreach ($query->result() as $res) {
                $contingut.= "<option value=\"".$res->codigo_centro."\" >".$res->programa." - ".$res->durada." - ".$res->edats."</option>";
            }

            return $contingut;
        } 

        function get_programasxcentroTxt($sel, $sel2) {
        
            $sql = "SELECT   
                        t.programa,  
                        t.fecha_entrada,  
                        t.fecha_salida,  
                        t.edad_min,  
                        t.edad_max,  
                        t.quads,  
                        t.hipica,  
                        t.angles,  
                        t.dni_nen,
                        t.futbol,
                        t.zurich, 
                        t.hostel, 
                        h.name as Centro,
                        t.colectiu
                    FROM hostels_tandes as t
                        INNER JOIN hostels as h
                            ON t.hostel = h.id
                    WHERE   
                        t.codigo_centro = '$sel' AND 
                        t.idhotels_tandes = '$sel2' 
                    ORDER BY  
                        t.codigo_centro,  
                        t.fecha_entrada,  
                        t.fecha_salida "; 

            $query = $this->db->query($sql);

            return $query->result();        
        }   

        function update_ImatgeInscripcio($i_idinscripcio, $i_foto, $i_thumbnail, $tipo = "IMG") {
            $i_foto         =   $this->soporte_xss->encode($i_foto);
            $i_thumbnail    =   $this->soporte_xss->encode($i_thumbnail);
            $where = "idinscripciones = '$i_idinscripcio'";
            switch ($tipo) {
                case 'IMG':
                    $datos = array( "foto"              => $i_foto,  
                                    "thumbnail"         => $i_thumbnail );
                break;

                case 'PASS':
                    $datos = array( "copia_pasaporte"   => $i_foto,  
                                    "mini_pasaporte"    => $i_thumbnail );
                break;

                case 'DNIA':
                    $datos = array( "dni_delantero"     => $i_foto );
                break;

                case 'DNIR':
                    $datos = array( "dni_trasero"       => $i_foto);
                break;

                case 'CVAC':
                    $datos = array( "foto"              => $i_foto,  
                                    "thumbnail"         => $i_thumbnail );
                break;
            }
            $this->db->where($where);
            $this->db->update("inscripciones", $datos);
        } 

        function get_ImatgesInscripcio($i_idinscripcio, $tipo = "IMG") {
            
            $campos = "";
            switch ($tipo) {
                case 'IMG':
                    $campos = "foto as Foto, thumbnail as Thumb";
                break;

                case 'PASS':
                    $campos = "copia_pasaporte as Foto, mini_pasaporte as Thumb";
                break;

                case 'DNIA':
                    $campos = "dni_delantero as Foto, '' as Thumb";
                break;

                case 'DNIR':
                    $campos = "dni_trasero as Foto, '' as Thumb";                    
                break;

                case 'CVAC':
                    $campos = "foto as Foto, '' as Thumb";                    
                break;
            }

            $sql = "SELECT 
                        $campos   
                    FROM inscripciones 
                    WHERE 
                        idinscripciones = '$i_idinscripcio'";

            $query = $this->db->query($sql);
            $res = $query->result();
            
            if (count($res) > 0) {
                $res[0]->Foto	= $this->soporte_xss->decode($res[0]->Foto);
                $res[0]->Thumb  = $this->soporte_xss->decode($res[0]->Thumb);
            }
            return ( (count($res) == 0) ? false : $res[0]);
        } 
            
        function get_ImatgeInscripcio($i_idinscripcio) {

            $sql = "SELECT 
                        foto   
                    FROM inscripciones 
                    WHERE 
                    idinscripciones = '$i_idinscripcio'";

            $query = $this->db->query($sql);

            $resFoto = "";

            foreach ($query->result() as $res) {
                $resFoto = $this->soporte_xss->decode($res->foto);
            }

            return $resFoto;
        } 

        function get_hostel($idtanda) {

            $sql = "SELECT   
                        hostel  
                    FROM hostels_tandes  
                    WHERE   
                        idhotels_tandes = '$idtanda'";
            
            $query = $this->db->query($sql);
            
            $p_idhostel = "";

            foreach ($query->result() as $res) {
                $p_idhostel = $res->hostel;
            }

            return $p_idhostel;
        }    
        
        function dblog($logtexte) {
            
            if (isset($_SESSION['hostel'])) {
                $this->hostel = $_SESSION['hostel'];
            }

            if (isset($_SESSION['order_number'])) {
                $this->order = $_SESSION['order_number'];
            }

            $datos = array(
                "hostel" => $this->hostel, 
                "horacontrol" => date("Y-m-d H:i:s"), 
                "order_number" => $this->order, 
                "step" => 'asyncsend.php', 
                "status" => $logtexte
            );

            $this->db->set($datos);
            $this->db->insert("reservations_tpvlog");

        }
        
        function inclog($logtexte) {
            
            if (isset($_SESSION['hostel'])) {
                $this->hostel = $_SESSION['hostel'];
            }
            
            if (isset($_SESSION['order_number'])) {
                $this->order = $_SESSION['order_number'];
            }

            if ($this->procesarlog) {

                $datos = array(
                    "hostel" => $this->hostel, 
                    "horacontrol" => date("Y-m-d H:i:s"), 
                    "order_number" => $this->order, 
                    "step" => 'inscripciones.php', 
                    "status" => $logtexte
                );

                $this->db->set($datos);
                $this->db->insert("reservations_tpvlog");
            }
        }

        function getCustomerStrings($p_name) {

            $sqls = "   SELECT 
                            text 
                        FROM customer_strings 
                        WHERE 
                            lang='$_SESSION[lang]' and name ='$p_name'";

            $query = $this->db->query($sqls);

            $resTexto = "";
            
            foreach ($query->result() as $res) {
                $resTexto = $res->text;
            }

            return $resTexto;
        } 

        function getHotelStrings($p_hostel, $p_name) {

            $sqlh = "   SELECT 
                            customer_string 
                        FROM hostels_strings 
                        WHERE 
                            hostel='$p_hostel' and hostel_string ='$p_name'";
    
            $query = $this->db->query($sqls);

            $resTexto = "";

            foreach ($query->result() as $res) {
                $resTexto = getCustomerStrings($res->customer_string);
            }

            return $resTexto;
        }
    }
?>